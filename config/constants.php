<?php

return [
    'userImage' => '/images/user/',
    'userDocuments' => '/images/user/Documents/',
    'userThumb' => '/images/user/thumb/',
    'groupThumb' => '/images/user/thumb/',
    'userThumbImage' => '/images/user/thumb/userLogo.png',
    'groupThumbImage' => '/images/user/thumb/groupLogo.png',
    'postImage' => '/images/post/',
    'postThumb' => '/images/post/thumb/',
    'userDefaultImage' => '/images/user/defaultUser.png',
    'groupDefaultImage' => '/images/user/defaultGroup.png',
    'postDefaultImage' => '/images/post/default.png',
    'imageMaxWidth' => 320,
    'thumbMaxWidth' => 72,
];
