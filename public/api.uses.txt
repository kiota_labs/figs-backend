01. http://52.32.88.196/public/index.php/service/dayMenu/getList
  Input: {
    "sessionId" : "264229cf13509b6545cffad9cab14285d0716dd4",
    "itemType" : "",
    "exceptions" : ""
  }
  Output: {
    "api": "dayMenu/getList",
    "status": "OK",
    "apiMessage": "Records fetched successfully",
    "menuItems": [
      {
        "dayMenuId": "1",
        "menuDate": "2015-12-10",
        "itemId": "11",
        "itemName": "Cappuccino",
        "itemDescription": "Coffee",
        "price": "250.00",
        "itemType": "3",
        "itemImage": "",
        "itemThumb": "",
        "likeCount": "1",
        "reviewCount": "3",
        "ratingCount": "3",
        "averageRating": "3.6667",
        "iLikedIt": "1",
        "nutrients": [
          {
            "itemNutrientId": "1",
            "itemId": "11",
            "key": "Calory",
            "value": "100 K"
          }
        ],
        "ingredients": [
          {
            "itemIngredientId": "1",
            "itemId": "11",
            "key": "Potato",
            "value": "250 gms"
          },
          {
            "itemIngredientId": "2",
            "itemId": "11",
            "key": "Mustard Oil",
            "value": "1 tablespoon"
          }
        ],
        "reviews": [
          {
            "itemReviewId": "1",
            "userId": "2",
            "itemId": "11",
            "rating": "5",
            "comments": "Wow, delicious",
            "email": "vikasgupta.developer@gmail.com",
            "username": "VikasGupta_30306",
            "fullName": "Vikas Gupta",
            "image": "",
            "thumb": ""
          },
          {
            "itemReviewId": "2",
            "userId": "1",
            "itemId": "11",
            "rating": "3",
            "comments": "",
            "email": "vikasgupta.live@gmail.com",
            "username": "",
            "fullName": "Vikas Gupta",
            "image": "",
            "thumb": ""
          },
          {
            "itemReviewId": "3",
            "userId": "1",
            "itemId": "11",
            "rating": "3",
            "comments": "",
            "email": "vikasgupta.live@gmail.com",
            "username": "",
            "fullName": "Vikas Gupta",
            "image": "",
            "thumb": ""
          }
        ]
      },
      {
        "dayMenuId": "2",
        "menuDate": "2015-12-10",
        "itemId": "25",
        "itemName": "Cappy Raspberry",
        "itemDescription": "Cappy",
        "price": "150.00",
        "itemType": "3",
        "itemImage": "",
        "itemThumb": "",
        "likeCount": "0",
        "reviewCount": "0",
        "ratingCount": "0",
        "averageRating": "0.0000",
        "iLikedIt": "0",
        "nutrients": [],
        "ingredients": [],
        "reviews": []
      },
      {
        "dayMenuId": "3",
        "menuDate": "2015-12-10",
        "itemId": "32",
        "itemName": "Krombacher",
        "itemDescription": "Beer",
        "price": "220.00",
        "itemType": "3",
        "itemImage": "",
        "itemThumb": "",
        "likeCount": "0",
        "reviewCount": "0",
        "ratingCount": "0",
        "averageRating": "0.0000",
        "iLikedIt": "0",
        "nutrients": [],
        "ingredients": [],
        "reviews": []
      },
      {
        "dayMenuId": "4",
        "menuDate": "2015-12-10",
        "itemId": "39",
        "itemName": "Origin",
        "itemDescription": "Cognac",
        "price": "390.00",
        "itemType": "3",
        "itemImage": "",
        "itemThumb": "",
        "likeCount": "0",
        "reviewCount": "0",
        "ratingCount": "0",
        "averageRating": "0.0000",
        "iLikedIt": "0",
        "nutrients": [],
        "ingredients": [],
        "reviews": []
      },
      {
        "dayMenuId": "5",
        "menuDate": "2015-12-10",
        "itemId": "4",
        "itemName": "Sprite",
        "itemDescription": "Coca Cola",
        "price": "110.00",
        "itemType": "3",
        "itemImage": "",
        "itemThumb": "",
        "likeCount": "0",
        "reviewCount": "0",
        "ratingCount": "0",
        "averageRating": "0.0000",
        "iLikedIt": "0",
        "nutrients": [],
        "ingredients": [],
        "reviews": []
      },
      {
        "dayMenuId": "6",
        "menuDate": "2015-12-10",
        "itemId": "18",
        "itemName": "Tonic Water",
        "itemDescription": "Tonic Kinley",
        "price": "130.00",
        "itemType": "3",
        "itemImage": "",
        "itemThumb": "",
        "likeCount": "0",
        "reviewCount": "0",
        "ratingCount": "0",
        "averageRating": "0.0000",
        "iLikedIt": "0",
        "nutrients": [],
        "ingredients": [],
        "reviews": []
      },
      {
        "dayMenuId": "7",
        "menuDate": "2015-12-10",
        "itemId": "46",
        "itemName": "Tullamore dew",
        "itemDescription": "Irish whiskey",
        "price": "310.00",
        "itemType": "3",
        "itemImage": "",
        "itemThumb": "",
        "likeCount": "0",
        "reviewCount": "0",
        "ratingCount": "0",
        "averageRating": "0.0000",
        "iLikedIt": "0",
        "nutrients": [],
        "ingredients": [],
        "reviews": []
      }
    ]
  }

02. http://52.32.88.196/public/index.php/service/user/signup
  Input: {
    "email" : "vikasgupta.live@gmail.com",
    "fullName" : "Vikas Gupta",
    "password" : "password",
    "phone" : "8588898688",
    "referralCodeUsed" : "",
    "deviceToken" : "",
    "image" : ""
  }
  Output: {
    "api": "user/signup",
    "status": "OK",
    "apiMessage": "User registered successfully",
    "user": {
      "userId": "1",
      "role": "user",
      "email": "vikasgupta.live@gmail.com",
      "username": "",
      "facebookId": "",
      "fullName": "Vikas Gupta",
      "image": "",
      "thumb": "",
      "referralCode": "1WVP5W53",
      "createDate": "2015-12-10 13:27:01",
      "street": "",
      "locality": "",
      "landmark": "",
      "phone": "8588898688",
      "cardNo": "",
      "paypalId": "",
      "notifyMe": "1",
      "isPhoneVerified": "0",
      "referralCodeUsed": "",
      "balanceReferralAmount": "0.00"
    },
    "sessionId": "1e0a8b8c1b5a564d23bd4c77ae22c24b67be337c"
  }

03. http://52.32.88.196/public/index.php/service/user/signinFb
  Input: {
    "facebookId" : "11112222",
    "email" : "vikasgupta.developer@gmail.com",
    "fullName" : "Vikas Gupta",
    "phone" : "8588898688",
    "referralCodeUsed" : "",
    "deviceToken" : "",
    "image" : "",
    "imageUrl" : ""
  }
  Output: {
    "api": "user/signinFb",
    "status": "OK",
    "apiMessage": "User signed in successfully",
    "user": {
      "userId": "2",
      "role": "user",
      "email": "vikasgupta.developer@gmail.com",
      "username": "VikasGupta_30306",
      "facebookId": "11112222",
      "fullName": "Vikas Gupta",
      "image": "",
      "thumb": "",
      "referralCode": "2DR29QUV",
      "createDate": "2015-12-10 13:30:16",
      "street": "",
      "locality": "",
      "landmark": "",
      "phone": "8588898688",
      "cardNo": "",
      "paypalId": "",
      "notifyMe": "1",
      "isPhoneVerified": "0",
      "referralCodeUsed": "",
      "balanceReferralAmount": "0.00"
    },
    "sessionId": "e3fe9ebe9a5e939ca737f2f35f3d5fcd85931dd8",
    "isNewUser": true
  }

04. http://52.32.88.196/public/index.php/service/user/signin
  Input: {
    "email" : "vikasgupta.live@gmail.com",
    "password" : "password",
    "deviceToken" : ""
  }
  Output: {
    "api": "user/signin",
    "status": "OK",
    "apiMessage": "User signed in successfully",
    "user": {
      "userId": "1",
      "role": "user",
      "email": "vikasgupta.live@gmail.com",
      "username": "",
      "facebookId": "",
      "fullName": "Vikas Gupta",
      "image": "",
      "thumb": "",
      "referralCode": "1WVP5W53",
      "createDate": "2015-12-10 13:27:01",
      "street": "",
      "locality": "",
      "landmark": "",
      "phone": "8588898688"
    },
    "sessionId": "264229cf13509b6545cffad9cab14285d0716dd4"
  }

05. http://52.32.88.196/public/index.php/service/user/logout
  Input: {
    "sessionId": "e0e1748311bfd648c59877eb920c6aaa4c5a8717"
  }
  Output: {
    "api": "user/logout",
    "status": "OK",
    "apiMessage": "User logged out successfully"
  }

06. http://52.32.88.196/public/index.php/service/user/updateProfile
  Input: {
    "sessionId" : "234d1b541468cd05f769f95a1706a1600716509e",
    "fullName" : "Vikas Kumar Gupta",
    "phone" : "",
    "referralCodeUsed" : ""
  }
  Output: {
    "api": "user/updateProfile",
    "status": "OK",
    "apiMessage": "User record saved successfully",
    "user": {
      "userId": "3",
      "role": "user",
      "email": "vikasgupta.live@gmail.com",
      "username": "",
      "facebookId": "",
      "fullName": "Vikas Kumar Gupta",
      "image": "",
      "thumb": "",
      "referralCode": "3BHHBHJC",
      "createDate": "2015-11-20 03:06:15",
      "street": "",
      "locality": "",
      "landmark": "",
      "phone": ""
    }
  }

07. http://52.32.88.196/public/index.php/service/user/addUserAddress
  Input: {
    "sessionId" : "234d1b541468cd05f769f95a1706a1600716509e",
    "placeName" : "Home",
    "street" : "Kapoor Gali",
    "locality" : "Munirka",
    "landmark" : ""
  }
  Output: {
    "api": "user/addUserAddress",
    "status": "OK",
    "apiMessage": "User address saved successfully",
    "userAddress": {
      "userAddressId": "3",
      "userId": "3",
      "placeName": "Home",
      "street": "Kapoor Gali",
      "locality": "Munirka",
      "landmark": ""
    }
  }

08. http://52.32.88.196/public/index.php/service/user/updateUserAddress
  Input: {
    "sessionId" : "234d1b541468cd05f769f95a1706a1600716509e",
    "userAddressId" : "3",
    "placeName" : "Home",
    "street" : "Kapoor Gali",
    "locality" : "Munirka",
    "landmark" : ""
  }
  Output: {
    "api": "user/updateUserAddress",
    "status": "OK",
    "apiMessage": "User address saved successfully",
    "userAddress": {
      "userAddressId": "3",
      "userId": "3",
      "placeName": "Home",
      "street": "Kapoor Gali",
      "locality": "Munirka Village",
      "landmark": ""
    }
  }

09. http://52.32.88.196/public/index.php/service/user/updateSettings
  Input: {
    "sessionId" : "234d1b541468cd05f769f95a1706a1600716509e",
    "notifyMe" : "1"
  }
  Output: {
    "api": "user/updateSettings",
    "status": "OK",
    "apiMessage": "Settings saved successfully, , but Referral Code Not Acceptable",    //code can only be added ones only As this guy added one already.
    "user": {
      "userId": "3",
      "role": "user",
      "email": "vikasgupta.live@gmail.com",
      "username": "",
      "facebookId": "",
      "fullName": "Vikas Kumar Gupta",
      "image": "",
      "thumb": "",
      "referralCode": "3BHHBHJC",
      "createDate": "2015-11-20 03:06:15",
      "street": "",
      "locality": "",
      "landmark": "",
      "phone": "",
      "cardNo": "",
      "paypalId": "",
      "notifyMe": "1",
      "isPhoneVerified": "0"
    }
  }

10. http://52.32.88.196/public/index.php/service/user/getProfile
  Input: {
    "sessionId" : "234d1b541468cd05f769f95a1706a1600716509e"
  }
  Output: {
    "api": "user/getProfile",
    "status": "OK",
    "apiMessage": "User record fetched successfully",
    "user": {
      "userId": "3",
      "role": "user",
      "email": "vikasgupta.live@gmail.com",
      "username": "",
      "facebookId": "",
      "fullName": "Vikas Kumar Gupta",
      "image": "",
      "thumb": "",
      "referralCode": "3BHHBHJC",
      "createDate": "2015-11-20 03:06:15",
      "street": "",
      "locality": "",
      "landmark": "",
      "phone": "",
      "cardNo": "",
      "paypalId": "",
      "notifyMe": "1",
      "isPhoneVerified": "0",
      "unseenNotificationCount": 0
    }
  }

11. http://52.32.88.196/public/index.php/service/user/deleteUserAddress
  Input: {
    "sessionId" : "234d1b541468cd05f769f95a1706a1600716509e",
    "userAddressId" : "3"
  }
  Output: {
    "api": "user/deleteUserAddress",
    "status": "OK",
    "apiMessage": "User address deleted successfully"
  }

12. http://52.32.88.196/public/index.php/service/user/listUserAddress
  Input: {
    "sessionId" : "234d1b541468cd05f769f95a1706a1600716509e"
  }
  Output: {
    "api": "user/listUserAddress",
    "status": "OK",
    "apiMessage": "User address saved successfully",
    "userAddresses": [
      {
        "userAddressId": "1",
        "userId": "3",
        "placeName": "Home",
        "street": "Kapoor Gali",
        "locality": "Munirka",
        "landmark": ""
      },
      {
        "userAddressId": "2",
        "userId": "3",
        "placeName": "Office",
        "street": "Kapoor Gali",
        "locality": "Munirka",
        "landmark": ""
      }
    ]
  }

13. http://52.32.88.196/public/index.php/service/user/changePassword
  Input: {
    "sessionId" : "c038737fe5b6d96c970cdbfa27c564417900b007",
    "oldPassword" : "newpass",
    "newPassword" : "mypass"
  }
  Output: {
    "api": "user/changePassword",
    "status": "OK",
    "apiMessage": "Password changed successfully"
  }

14. http://52.32.88.196/public/index.php/service/phoneVerification/getVerificationCode
  Input: {
    "sessionId" : "",
    "phone" : "+918588898688"
  }
  Output: {
    "api": "phoneVerification/getVerificationCode",
    "status": "OK",
    "apiMessage": "Verification code sent successfully.",
    "phone": "+918588898688"
  }

15. http://52.32.88.196/public/index.php/service/purchase/add
	  Input: {
		"sessionId" : "",
		"street" : "",
		"locality" : "",
		"landmark" : "",
		"phone" : "+918588898688",
		"verificationCode" : "617775",
		"saveCutlery" : "",
		"couponCode" : "0",   //0 for not added and 1 for added. Can only be added when logged in.
		"items" : [
		  {
			"itemId" : "1",
			"quantity" : "1"
		  },
		  {
			"itemId" : "2",
			"quantity" : "1"
		  }
		]
	  }
  Output: {
    "api": "purchase/add",
    "status": "OK",
    "apiMessage": "Purchase saved successfully",
    "purchase": {
      "purchaseId": "5",
      "userId": "",
      "street": "",
      "locality": "",
      "landmark": "",
      "phone": "+918588898688",
      "saveCutlery": "0",
      "couponCode": "",
      "purchaseDate": "2015-12-01 18:47:30",
      "paymentDate": "",
      "deliveryDate": "",
      "paypalTransId": "",
      "purchaseStatus": "1",
      "isPaid": "0",
      "isCancelled": "0",
      "cancelReason": "",
      "purchaseDetail": [
        {
          "purchaseDetailId": "9",
          "purchaseId": "5",
          "itemId": "1",
          "quantity": "1.00",
          "price": "110.00",
          "itemName": "Coca Cola",
          "itemDescription": "Coca Cola",
          "itemType": "3",
          "itemImage": "",
          "itemThumb": ""
        },
        {
          "purchaseDetailId": "10",
          "purchaseId": "5",
          "itemId": "2",
          "quantity": "1.00",
          "price": "110.00",
          "itemName": "Coca Cola Light",
          "itemDescription": "Coca Cola",
          "itemType": "3",
          "itemImage": "",
          "itemThumb": ""
        }
      ]
    }
  }

16. http://52.32.88.196/public/index.php/service/itemReview/add
  Input: {
    "sessionId" : "264229cf13509b6545cffad9cab14285d0716dd4",
    "itemId" : "11",
    "rating" : "3",
    "comments" : "one more review"
  }
  Output: {
    "api": "itemReview/add",
    "status": "OK",
    "apiMessage": "Review saved successfully",
    "itemReviews": [
      {
        "itemReviewId": "1",
        "userId": "2",
        "itemId": "11",
        "rating": "5",
        "comments": "Wow, delicious",
        "email": "vikasgupta.developer@gmail.com",
        "username": "VikasGupta_30306",
        "fullName": "Vikas Gupta",
        "image": "",
        "thumb": ""
      },
      {
        "itemReviewId": "2",
        "userId": "1",
        "itemId": "11",
        "rating": "3",
        "comments": "",
        "email": "vikasgupta.live@gmail.com",
        "username": "",
        "fullName": "Vikas Gupta",
        "image": "",
        "thumb": ""
      },
      {
        "itemReviewId": "3",
        "userId": "1",
        "itemId": "11",
        "rating": "3",
        "comments": "",
        "email": "vikasgupta.live@gmail.com",
        "username": "",
        "fullName": "Vikas Gupta",
        "image": "",
        "thumb": ""
      }
    ]
  }

17. http://52.32.88.196/public/index.php/service/itemLike/add
  Input: {
    "sessionId" : "264229cf13509b6545cffad9cab14285d0716dd4",
    "itemId" : "12"
  }
  Output: {
    "api": "itemLike/add",
    "status": "OK",
    "apiMessage": "Like saved successfully",
    "itemLikes": [
      {
        "itemLikeId": "4",
        "itemId": "14",
        "userId": "1",
        "username": "",
        "userFullName": "Vikas Gupta",
        "userImage": "",
        "userThumb": "",
        "createDate": "2015-12-10 14:56:16",
        "timeElapsed": ""
      }
    ]
  }

18. http://52.32.88.196/public/index.php/service/itemLike/remove
  Input: {
    "sessionId" : "264229cf13509b6545cffad9cab14285d0716dd4",
    "itemId" : "12"
  }
  Output: {
    "api": "itemLike/remove",
    "status": "OK",
    "apiMessage": "Like removed successfully",
    "itemLikes": []
  }

19. http://localhost:8080/updated_fig/figs-backend/public/index.php/service/purchase/updatePurchase

  Input: {
 
	   "sessionId" : "cd0feba5b7f336f5b342e6bbbf3048ea26e535eb",
	   "purchaseId": 1,
	   "street": "202/",
	   "locality": "Pur",
	   "landmark": "near gov",
	   "phone": "+919868958202",
	   "saveCutlery": "0",
	   "purchaseStatus": "4",
	   "isPaid": "1",
	   "isCancelled": "1",
	   "cancelReason": "lol like i will tell you"
			
  }
   
  Output: {
    "api": "purchase/updatePurchase",
    "status": "OK",
    "apiMessage": "Update Purchase",
  }
  
20. http://localhost:8080/updated_fig/figs-backend/public/index.php/service/purchase/getTodaysPurchases

  Input:  {
    
  }
  
  Output: {
    "api": "purchase/getTodaysPurchases",
    "status": "OK",
    "apiMessage": "Last Orders Fetched.",
    "userPurchaseList": [
        {
            "purchaseId": 17,
            "userId": "1",
            "street": "djbv",
            "locality": "DLF II  ",
            "landmark": "n cmj kj ",
            "phone": "d",
            "saveCutlery": "0",
            "couponCode": "",
            "purchaseDate": "2016-01-30 18:55:21",
            "paymentDate": "",
            "deliveryDate": "",
            "paypalTransId": "",
            "purchaseStatus": "1",
            "isPaid": "0",
            "isCancelled": "0",
            "cancelReason": "",
            "purchaseDetail": [
                {
                    "purchaseDetailId": 17,
                    "purchaseId": 17,
                    "itemId": 1,
                    "quantity": "2.00",
                    "price": "90.00",
                    "itemName": "test1",
                    "itemDescription": "test1 description ",
                    "itemType": 0,
                    "itemImage": "",
                    "itemThumb": ""
                }
            ],
            "userName": "Rahul Kohli"
        }
    ]
}


21. http://localhost:8080/updated_fig/figs-backend/public/index.php/service/purchase/applyCoupon

  Input: {
    "sessionId" : "3919d86a7f7e30bd261bf4c71075bcc2ad57c2eb",
    "codeBeingUsed" : "1LT44PPS"
  }
  
  Output: {   								// If has used a code before...
    "api": "purchase/applyCoupon",
    "status": "error",
    "errorCode": 99,
    "apiMessage": "You can only use your code"
  }
  
  Output 2: {
    "api": "purchase/applyCoupon",
    "status": "OK",
    "apiMessage": "Valid Code",
    "codeAccepted": "1",        		// 1 or 0 as if balance is available or not.
    "balanceAvailable": "200.00"
}
  
  
22.  http://localhost:8080/updated_fig/figs-backend/public/index.php/service/purchase/getUserPurchases

  Input:  {
    "sessionId" : "cd0feba5b7f336f5b342e6bbbf3048ea26e535eb"
  }
  
  Output: {
    "api": "purchase/getUserPurchases",
    "status": "OK",
    "apiMessage": "Last Orders Fetched.",
    "userPurchaseList": [
        {
            "purchaseId": 17,
            "userId": "1",
            "street": "djbv",
            "locality": "DLF II  ",
            "landmark": "n cmj kj ",
            "phone": "d",
            "saveCutlery": "0",
            "couponCode": "",
            "purchaseDate": "2016-01-30 18:55:21",
            "paymentDate": "",
            "deliveryDate": "",
            "paypalTransId": "",
            "purchaseStatus": "1",
            "isPaid": "0",
            "isCancelled": "0",
            "cancelReason": "",
            "purchaseDetail": [
                {
                    "purchaseDetailId": 17,
                    "purchaseId": 17,
                    "itemId": 1,
                    "quantity": "2.00",
                    "price": "90.00",
                    "itemName": "test1",
                    "itemDescription": "test1 description ",
                    "itemType": 0,
                    "itemImage": "",
                    "itemThumb": ""
                }
            ]
        }
    ]
}
     


23. http://52.32.88.196/public/index.php/service/purchase/fetchPurchaseById
  Input: {
	"sessionId" :"" ,
	"purchaseId" :"6"   
	}

 Output: {
    "api": "purchase/fetchPurchaseById",
    "status": "OK",
    "apiMessage": "Purchase Fetched.",
    "purchaseId": 6,
    "userId": "",
    "userName": "",
    "street": "",
    "locality": "",
    "landmark": "",
    "phone": "+919868958202",
    "saveCutlery": 1,
    "couponCode": "0",
    "paypalTransId": "",
    "purchaseStatus": 1,
    "isPaid": 0,
    "isCancelled": 0,
    "cancelReason": "",
    "total": 101.9079,
    "purchaseDetail": [
        {
            "purchaseDetailId": 6,
            "purchaseId": 6,
            "itemId": 1,
            "quantity": "1.00",
            "price": "90.00",
            "itemName": "test1",
            "itemDescription": "test1 description ",
            "itemType": 0,
            "itemImage": "",
            "itemThumb": ""
        }
    ]
}


24. http://52.32.88.196/public/index.php/service/phoneVerification/getVerificationCodeOverPos

  Input: {
	"phone":"9988334455"
}

Output:  {
    "api": "phoneVerification/getVerificationCode",
    "status": "OK",
    "apiMessage": "Verification code generated successfully.",
    "code": "802964"
}