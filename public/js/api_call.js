	
var api_base_url = 'http://www.fig.delivery/public/index.php/service/';
var total=0, cartSize=0, cartItemId, cartItemCost, cartItemQty, cartItemName;
var name ;
var addresses; 
var code,sessionId, kitchesStatus="1";
/**
* This funcion is is used to convert form data to json
*/

function encode_json(formData){
	var o = {};
	 $.each(formData, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
	});
	return o;
}

function getCouponCode(){
	
	return localStorage.getItem('couponCode');
}

function getUserPurchases(){
		
	var sessionId = localStorage.getItem('sessionId');
	var orders_str="";
	document.getElementById("orderList").innerHTML="";
	//$('#processingModal').modal();
	console.log("o k ");
    JSONData = JSON.stringify({
		"sessionId" : sessionId
		
	});
        $.ajax({
		url: api_base_url+"purchase/getUserPurchases",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			//$('#processingModal').modal('hide');
                    if(response.status=='OK'){
						console.log(response);
						if(response.userPurchaseList.length==0){
							document.getElementById("orderList").innerHTML="<li>No Orders Placed Yet.</li>";
						}
						else{
							$.each(response.userPurchaseList, function( key, purchase ) {
								console.log("  12378 : " + purchase);
								var purchaseStatus = purchase.purchaseStatus;
								var cancelReason ;
								
								orders_str+='<li>';
								if(purchase.isCancelled==1){
									//cancelled
									orders_str+='<i class="fa fa-times-circle-o statusIcons cancelled"></i>'
									cancelReason= purchase.cancelReason;
								}
								else if(purchaseStatus==1){
									//verifying
									orders_str+='<i class="fa fa-question-circle statusIcons pending"></i>'
								}
								else if(purchaseStatus==2){
									//conformend
									orders_str+='<i class="fa fa-question-circle statusIcons pending"></i>'
								}else if(purchaseStatus==3){
									//En-route
									orders_str+='<i class="fa fa-question-circle statusIcons pending"></i>'
								}else if(purchaseStatus==4){
									//Delivered
									orders_str+='<i class="fa fa-check-circle-o statusIcons delivered" ></i>'
								}
								
								var bought  ="Items: ";
								var len =purchase.purchaseDetail.length;
								var total=0;
								
								$.each(purchase.purchaseDetail, function( key, item ) { //list of all items referred 1 by 1.
							
									var itemName= item.itemName;
									var itemQty= parseInt(item.quantity);
									bought+=itemName+" ("+itemQty+")";
									total+=parseInt(item.price)* itemQty;
									if(key!=(len-1)){
										bought+= ", ";
									}
									//console.log("  123 : " + itemName+ itemQty);
								});
								
								var orderId= purchase.purchaseId;
								var date = purchase.purchaseDate;
								var date_test = new Date(date.replace(/-/g,"/"));
								date_test=" "+date_test;
								date_test = date_test.replace("GMT+0530 (India Standard Time)", " ");
								
								orders_str+='<h3><b>Order ID : '+ orderId +'</b> </h3>'
								orders_str+='<p> Dated: '+date_test+'</p>';
								orders_str+='<p>'+bought+'</p>';
								total+= total*0.13125;
								total = total.toFixed(2);
								if(purchase.couponCode==1){
									total-=100;
								}
								if(typeof total==='undefined'){
									total="1000";
								}
								
								orders_str+='<p>Grand Total: '+total+'</p>';
								orders_str+='</li>';
								
							});
							document.getElementById("orderList").innerHTML=orders_str;
						}
                    }else{
                        alert(response.apiMessage);
						
						logout();
						console.log(response);
                    }	
		},
		error: function (){
                   // console.log("An error occured calling api");
    		  document.getElementById("orderList").innerHTML="Check your Net Connectivity";
			$('#processingModal').modal('hide');
		}
	});
	
	
}

var count=1;

function displayAddressAtProfile(){
	var len =addresses.length;
	$.each(addresses, function( key, address ) {
		var tstr=address.placeName.toLowerCase();
		console.log("str: "+tstr +" "+tstr.search("work"));
		
		var locality = '<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span> '+ address.locality;
		var street =address.street;
		var landmark = address.landmark;
		
		if(tstr=="home"){
			
			document.getElementById('AddLocalityButton-1').innerHTML = locality;	
			document.getElementById('removeAddress-1').style.display="inline";	
			document.getElementById("street-1").value= street;
			document.getElementById("landmark-1").value= landmark;
			document.getElementById('strAddress-1').innerHTML= "" + street + ",<br>" + address.locality + ",<br>"  + landmark;
			document.getElementById('addressId-1').innerHTML=address.userAddressId;
		}
		else if(tstr=="work"){

			document.getElementById('AddLocalityButton-2').innerHTML = locality;
			document.getElementById('removeAddress-2').style.display="inline";	
			document.getElementById("street-2").value = street;
			document.getElementById("landmark-2").value = landmark;
			document.getElementById('strAddress-2').innerHTML= "" + street + ",<br>" + address.locality + ",<br>" + landmark;
			document.getElementById('addressId-2').innerHTML=address.userAddressId;
		}
		else if(count >=1 && count <=3){
			document.getElementById('strAddressSavedAs-' + (count+2)).innerHTML = address.placeName;
			document.getElementById('removeAddress-' + (count+2)).style.display="inline";	
			document.getElementById('AddLocalityButton-'+(count+2)).innerHTML = locality;
			document.getElementById("street-"+(count+2)).value = street;
			document.getElementById("landmark-"+(count+2)).value = landmark;
			document.getElementById("addressSavedAs-"+(count+2)).value = address.placeName;
			document.getElementById('strAddress-'+(count+2)).innerHTML= "" + street + ",<br>" + address.locality + ",<br>" + landmark;
			document.getElementById('addressId-'+(count+2)).innerHTML = address.userAddressId;
			count+=1;
		}
		if(key>=5){
			return false;
		}
		
	});
}

function changeTextAddressButton(){
	
	var len =addresses.length;
	var buttons_str=" ";
	
	
	$.each(addresses, function( key, address ) {
		buttons_str+=' <a class="button" id="addressTab" onclick="addAddressAtHomePage('+key+')">'+address.placeName+'</a> ';
		
		if(key>=5){
			return false;
		}
	});
	buttons_str+='<br>';
	document.getElementById("addressTabsAdd").innerHTML=buttons_str;
	

}

function addAddressAtHomePage(num){
	
	document.getElementById("streetAddress").value= addresses[num].street;
	//document.getElementById("streetAddress").innerHTML=addresses[num].locality;
	document.getElementById('orderServiceLocatorButton').innerHTML ='<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span> <p style="display:inline;">' + addresses[num].locality +'</p>' ;
	document.getElementById("landmark").value= addresses[num].landmark;
	
}

function getAllAddress(page){
	
	var sessionId = localStorage.getItem('sessionId');
	
	//$('#processingModal').modal();
	console.log("o k ");
    JSONData = JSON.stringify({
		"sessionId" : sessionId
		
	});
        $.ajax({
		url: api_base_url+"user/listUserAddress",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			
			//$('#processingModal').modal('hide');
                    if(response.status=='OK'){
						console.log(response);
						
						addresses=response.userAddresses;
						
						$.each(response.userAddresses, function( key, address ) {
							// all address for this user
							// var id = address.userId;
							// var savedAs = address.placeName;
						});
						
						if(page==1){
							changeTextAddressButton();
						}
						else if(page==2){
							displayAddressAtProfile();
						}
						//$('#processingModal').modal('hide');
                    }else{
                        //alert("Server Errro");
						if(response.status="error"){
							if(response.errorCode=="101")
							{
								logout();
							}
							else{
								alert(response.apiMessage);
	
							}
						}
						//$('#processingModal').modal('hide');			
						console.log(response);
                    }	
		},
		error: function (){
                   // console.log("An error occured calling api");
    		//$('#processingModal').modal('hide');
		}
	});
	
}

function applyCoupon(){
	
	var sessionId = localStorage.getItem('sessionId');
	var code = document.getElementById("couponCode").value;
	var message, balance;
	$('#couponError').hide();
	$('#processingModal').modal();
	console.log("o k ");
    JSONData = JSON.stringify({
		"sessionId" : sessionId,
		"codeBeingUsed" : code
	});
        $.ajax({
		url: api_base_url+"purchase/applyCoupon",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			
			$('#processingModal').modal('hide');
			message=response.apiMessage;
			console.log(response);
                    if(response.status=='OK'){
						
						balance=response.balanceAvailable;
						
						if(response.codeAccepted=="1"){
							document.getElementById("couponModelContent").innerHTML="YOUR BALANCE IS RS. " + balance + ". <br>DO YOU WANT TO AVAIL DISCOUNT OF  RS. 100 NOW? ";	
							document.getElementById("couponModelYes").style.display="inline";
							$("#couponModelYes").click(function(){
								localStorage.setItem('codeAccepted', "1");
									displayCart();	
							});
							$("#couponModelNo").click(function(){
								$("#couponModel").modal('hide');	 
								localStorage.setItem('codeAccepted', "0");
									displayCart();	
							});
							
							$("#couponModel").modal();	 
							
							
						}
						else{
							document.getElementById("couponModelContent").innerHTML="SORRY COUPON NOT ACCEPTED. YOUR BALANCE IS ZERO. <br>SHARE YOUR CODE TO GET DISCOUNT IN FUTURE.";	
							document.getElementById("couponModelYes").style.display="none";
							localStorage.setItem('codeAccepted', "0");
							document.getElementById("couponModelNo").innerHTML="Close";
							$("#couponModelNo").click(function(){
								$("#couponModel").modal('hide');	 
							});
							
							//$("#couponModel").modal();
							$('#couponError').show()	 
							
					
						}
						//add a popUp here stating status of code.
						displayCart();	
                    }
					else{
					
						document.getElementById("couponModelContent").innerHTML= message.toUpperCase();	
						document.getElementById("couponModelYes").style.display="none";
						localStorage.setItem('codeAccepted', "0");
						document.getElementById("couponModelNo").innerHTML="Close";
						$("#couponModelNo").click(function(){
							$("#couponModel").modal('hide');	 
							displayCart();	
						});
						
						$("#couponModel").modal();	 
				
					
						//add a popUp here stating status of code.
						displayCart();	
						
                        //alert("Server Errro");
						console.log(response);
						//logout();
                    }	
		},
		error: function (){
                   // console.log("An error occured calling api");
				   alert("Please Check your Net Connection");
    		  		$('#processingModal').modal('hide');

		}
	});
	displayCart();	
}

function changeContacNo(){

	
	var sessionId = localStorage.getItem('sessionId');
	var fullName = 	localStorage.getItem('userName');
	var phone = document.getElementById("phoneAreaCodeToSave").value+""+document.getElementById("phoneNumberToSave").value;
	
	$('#processingModal').modal();
	console.log("o k ");
    JSONData = JSON.stringify({
		"sessionId" : sessionId,
		"fullName" : fullName,
		"phone" : phone,
		"referralCodeUsed" : ""	
	
	});
        $.ajax({
		url: api_base_url+"user/updateProfile",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			
			$('#processingModal').modal('hide');
                    if(response.status=='OK'){
					
					//changed phone.
					localStorage.setItem('phoneNo', phone);
					document.getElementById("userPhoneNo").innerHTML=localStorage.getItem('phoneNo');
					console.log(response+"sadsf     "+phone);
                    }else{
                        //alert("Server Errro");
						console.log(response+"sadsf     "+phone);
						logout();
                    }	
		},
		error: function (){
                   // console.log("An error occured calling api");
    		  			$('#processingModal').modal('hide');

		}
	});
}

function changePassword(){
	
	var sessionId = localStorage.getItem('sessionId');
	var oldPassword = document.getElementById("oldPsw").value;
	var newPassword = document.getElementById("newPsw").value;
	$('#processingModal').modal();
	console.log("o k ");
    JSONData = JSON.stringify({
		"sessionId" : sessionId,
		"oldPassword" : oldPassword,
		"newPassword" : newPassword
	});
        $.ajax({
		url: api_base_url+"user/changePassword",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			
			$('#processingModal').modal('hide');
                    if(response.status=='OK'){
					
					//password changed.
					console.log("password changed.");
					
	
                    }else{
                        //alert("Server Errro");
						console.log(response);
						//logout();
                    }	
		},
		error: function (){
            // console.log("An error occured calling api");
    		$('#processingModal').modal('hide');

		}
	});
	
}

function updateAddress(id){
	
	var sessionId = localStorage.getItem('sessionId');
	var len =addresses.length;
	var saveAs= "";
	var street ="", locality="", landmark ="";
	var doContinue=0;
	var userAddressId = document.getElementById('addressId-'+id).innerHTML;
	
	if(userAddressId.length !=0){//if exists...
		
		street = document.getElementById("street-"+id).value;
		locality = document.getElementById("AddLocalityButton-"+id).innerHTML;
		locality = locality.replace('<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span>' , "");
	
		landmark = document.getElementById("landmark-"+id).value;
		if(id==1){
			saveAs="HOME";
			$('#homeModal').modal('hide');
		}
		else if(id==2){
			saveAs="WORK";
			$('#workModal').modal('hide');
		}
		else{
			saveAs = document.getElementById("addressSavedAs-"+id).value;
			$('#anotherButton'+id+'Modal').modal('hide');
		}
		doContinue=1;
	}
	else{
		doContinue=0;
		addAddress(id);
	}
	
	
	if(doContinue==1){
			
		$('#processingModal').modal();
		console.log("o k ");
		JSONData = JSON.stringify({
			"sessionId" : sessionId,
			"userAddressId" : userAddressId,
			"placeName" : saveAs,
			"street" : street,
			"locality" : locality,
			"landmark" : landmark
		});
		console.log(JSONData);
			$.ajax({
			url: api_base_url+"user/updateUserAddress",
			type:"POST",
			dataType: "json",
			data: JSONData,
			contentType: "application/json; charset=utf-8",
			async: false,
			success: function(response) {
			$('#processingModal').modal('hide');
						if(response.status=='OK'){

						//address updated.
						document.getElementById('strAddressSavedAs-'+id).innerHTML= "" + saveAs;
						document.getElementById('strAddress-'+id).innerHTML= "" + street + ",<br>" + locality + ",<br>" + landmark;
						
						}else{
							//alert("Server Errro");
							if(response.status="error"){
							if(response.errorCode=="101")
							{
								logout();
							}
							else{
								alert(response.apiMessage);
	
							}
						}
			
							console.log(response);
							//logout();
						}	
			},
			error: function (){
				// console.log("An error occured calling api");
				$('#processingModal').modal('hide');		  

			}
		});
	}
	else{
		//popup say to enter every thing.
		$('#processingModal').modal('hide');
	}
}

function removeAddress(id){
	
	var sessionId = localStorage.getItem('sessionId');
	var userAddressId = document.getElementById('addressId-'+id).innerHTML;
	
	if(id==1){	
		$('#homeModal').modal('hide');
	}
	else if(id==2){
		$('#workModal').modal('hide');
	}
	else{
		$('#anotherButton'+id+'Modal').modal('hide');
	}
	
	document.getElementById('strAddress-'+id).innerHTML= "";
	if(userAddressId.length!=0){
		
		$('#processingModal').modal();		
		console.log("o k ");
		JSONData = JSON.stringify({
			"sessionId" : sessionId,
			"userAddressId" : userAddressId
		});
			$.ajax({
			url: api_base_url+"user/deleteUserAddress",
			type:"POST",
			dataType: "json",
			data: JSONData,
			contentType: "application/json; charset=utf-8",
			async: false,
			success: function(response) {
				
				$('#processingModal').modal('hide');
						if(response.status=='OK'){
	
							document.getElementById('strAddressSavedAs-'+id).innerHTML= "Another Address";
							document.getElementById('strAddress-'+id).innerHTML= "Add Address";
							document.getElementById('removeAddress-'+id).style.display="none";	
							//address removed.
							if(id==1 || id==2){
								
							}
							else{
								document.getElementById("addressSavedAs-"+id).value= "";
							}
							document.getElementById("street-"+id).value="";
							document.getElementById("AddLocalityButton-"+id).innerHTML='<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span>'+'Select your Locality';
							document.getElementById("landmark-"+id).value="";
							document.getElementById('addressId-'+id).innerHTML="";
							
						}else{
							//alert("Server Errro");
							//logout();
							if(response.status="error"){
								if(response.errorCode=="101")
								{
									logout();
								}
								else{
									alert(response.apiMessage);
		
								}
							}
			
							console.log(response);
						}	
			},
			error: function (){
					   // console.log("An error occured calling api");
							$('#processingModal').modal('hide');  

			}
		});
	}
}

function addAddress(id){
	
	var sessionId = localStorage.getItem('sessionId');
	var len =addresses.length;
	
	var saveAs= "";
	var street ="", locality="", landmark ="";
	var doContinue=0;
	var userAddressId = document.getElementById('addressId-'+id).innerHTML;
	if(userAddressId.length ==0){
		
		street = document.getElementById("street-"+id).value;
		locality = document.getElementById("AddLocalityButton-"+id).innerHTML;
		locality = locality.replace('<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span>',"");
		landmark = document.getElementById("landmark-"+id).value;
		
		if(id==1){
			saveAs="HOME";
			
			$('#homeModal').modal('hide');
		}
		else if(id==2){
			saveAs="WORK";
			$('#workModal').modal('hide');
		}
		else{
			saveAs = document.getElementById("addressSavedAs-"+id).value;
			
			$('#anotherButton'+id+'Modal').modal('hide');
		}
		doContinue=1;
		if(landmark.length==0 || street.length==0 || locality.length==0 || saveAs.length==0  || locality.search("Select your Locality")>=0){
			doContinue=0;
		}
		
	}
	if(doContinue==1){
	
		if(id==1){	
			$('#homeModal').modal('hide');
		}
		else if(id==2){
			$('#workModal').modal('hide');
		}
		else{
			$('#anotherButton'+id+'Modal').modal('hide');
		}
		
		
		$('#processingModal').modal();
		console.log("o k ");
		JSONData = JSON.stringify({
			"sessionId" : sessionId,
			"placeName" : saveAs,
			"street" : street ,
			"locality" : locality,
			"landmark" : landmark
		});
			$.ajax({
			url: api_base_url+"user/addUserAddress",
			type:"POST",
			dataType: "json",
			data: JSONData,
			contentType: "application/json; charset=utf-8",
			async: false,
			success: function(response) {
				
				
				$('#processingModal').modal('hide');
						if(response.status=='OK'){
							
							document.getElementById('removeAddress-'+id).style.display="inline";
							document.getElementById('strAddressSavedAs-'+id).innerHTML= saveAs;
							document.getElementById('strAddress-'+id).innerHTML= "" + street + ",<br>" + locality + ",<br>" + landmark;
							//address added.
							document.getElementById('addressId-'+id).innerHTML=userAddress.userAddressId;
			
						}else{
							
							document.getElementById("street-"+id).value="";
							document.getElementById("AddLocalityButton-"+id).innerHTML="";
							document.getElementById("landmark-"+id).value="";
							document.getElementById('addressId-'+id).innerHTML="";
							
							//alert("Server Errro");
							logout();
						}	
			},
			error: function (){
					   // console.log("An error occured calling api");
				$('#processingModal').modal('hide');	  

			}
		});
	}
	
}
function signUp(){
		//var sessionId = localStorage.getItem('sessionId');
	var email = document.getElementById('emailIdSignUp').value;
	var passWord = document.getElementById('passwordSignUp').value;
	var fullName = document.getElementById('userNameSignUp').value;
	 var phoneNo = document.getElementById('phoneSignUp').value;
	  var codeSignUp= document.getElementById('codeSignUp').value;
	  
		$('#processingModal').modal();
	console.log("o k ");
    JSONData = JSON.stringify({
		 "email" : email,
		"fullName" : fullName,
		"password" : passWord,
		"phone" : phoneNo,
		"referralCodeUsed" : codeSignUp,
		"deviceToken" : "",
		"image" : ""
	});
        $.ajax({
		url: api_base_url+"user/signup",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			
			$('#processingModal').modal('hide');
                    if(response.status=='OK'){
                        $("#signUpModal").modal('hide'); 
						var user = response.user;
					
						name = user.fullName;
						code = user.referralCode;
						localStorage.setItem('referralCode', code);
						localStorage.setItem('userName', name);
						//console.log("check :  "+code+ name);	
						sessionId=response.sessionId;
						localStorage.setItem('sessionId', sessionId);
						email= user.email;
						phone= user.phone;
						localStorage.setItem('emailId', email);
						localStorage.setItem('phoneNo', phone);
						document.getElementById("signInBotton").innerHTML="Hi, "+name;
						
						document.getElementById('couponCode').value=code;
						getAllAddress(1);
//						changeTextAddressButton();
                    }else{
						console.log(response);	
            
						if(response.status="error"){
							if(response.errorCode==101)
							{
								logout();
								alert(response.apiMessage);
							}
							else{
								alert(response.apiMessage);
	
							}
						}
			
                    }	
		},
		error: function (){
            // console.log("An error occured calling api");
			$('#processingModal').modal('hide');
		}
	});
	
	
}


function refreshLocalStorage(){

	localStorage.setItem('sessionId', "");
	localStorage.setItem('locality', "");	
	localStorage.setItem('referralCode', "");
	localStorage.setItem('userName', "");
	localStorage.setItem('emailId', "");
	localStorage.setItem('phoneNo', "");
	localStorage.setItem('couponCode', "")
	localStorage.setItem('codeAccepted', "0");
	localStorage.setItem('FbImageForFig', "");
	deleteCart();
	
}


function logout(){
	
	var sessionId = localStorage.getItem('sessionId');
	
	//$('#processingModal').modal();
	console.log("o k ");
    JSONData = JSON.stringify({
		"sessionId": sessionId
	});
        $.ajax({
		url: api_base_url+"user/logout",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			$('#processingModal').modal('hide');
                    if(response.status=='OK'){
					window.location="home";
	
                    }else{
                        //alert("Server Errro");
					
	
                    }	
		},
		error: function (){
                   // console.log("An error occured calling api");
    		//$('#processingModal').modal('hide');  

		}
	});

	refreshLocalStorage();
	
	
	document.getElementById("signInBotton").innerHTML="SIGN IN";
	

}

function getVerificationCode(){
	
	
	//var sessionId = localStorage.getItem('sessionId');
	var sessionId = localStorage.getItem('sessionId');
	var phone = document.getElementById('verificationPhonePrefix').value+document.getElementById('verificationPhone').value;
	$('#processingModal').modal();
			
	console.log("o k ");
    JSONData = JSON.stringify({
		"sessionId" : sessionId,
		"phone" : phone
	});
        $.ajax({
		url: api_base_url+"phoneVerification/getVerificationCode",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			
			$('#processingModal').modal('hide');
                    if(response.status=='OK'){
                    //popUp saying code sent.     
					document.getElementById("codeSentOnPhone").style.display="inline";
					
                    }else{
						console.log(response);	
            
						if(response.status="error"){
							if(response.errorCode=="101")
							{
								logout();
							}
							else{
								alert(response.apiMessage);
	
							}
						}
			
						
                    }	
		},
		error: function (){
			$('#processingModal').modal('hide');
            // console.log("An error occured calling api");
		}
	});
	
	
}

function signIn(){
	
	//var sessionId = localStorage.getItem('sessionId');
	var email = document.getElementById('emailSignIn').value;
	var passWord = document.getElementById('passwordSignIn').value;
	
	$('#processingModal').modal();
	console.log("o k ");
	
    JSONData = JSON.stringify({
				"email": email,
				"password": passWord,	
				"deviceToken" : ""
	});
        $.ajax({
		url: api_base_url+"user/signin",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			
			$('#processingModal').modal('hide');
			$("#signInModal").modal('hide');
                    if(response.status=='OK'){
                         
						var user = response.user;
					
						name = user.fullName;
						code = user.referralCode;
						localStorage.setItem('referralCode', code);
						localStorage.setItem('userName', name);
						//console.log("check :  "+code+ name);	
						sessionId=response.sessionId;
						localStorage.setItem('sessionId', sessionId);
						email= user.email;
						phone= user.phone;
						localStorage.setItem('emailId', email);
						localStorage.setItem('phoneNo', phone);
						document.getElementById("signInBotton").innerHTML="Hi, "+name;
						
						localStorage.setItem('couponCode', user.referralCode );					 
						document.getElementById('couponCode').value=code;
						getAllAddress(1);
                    }else{
						console.log(response);	
						if(response.status="error"){
							if(response.errorCode==101)
							{
								logout();
							}
							else{
								alert(response.apiMessage);
							}
						}
			
                    }	
		},
		error: function (){
                   // console.log("An error occured calling api");
				   $('#processingModal').modal('hide');
		}
	});
}

var put1=0, put2=0,put3=0;
function openModel(id){
	$("#knowMore-"+id).modal();
}

function facebookSignIn(fbId, email, name){
	//$('#processingModal').modal();
	console.log("o k ");
    JSONData = JSON.stringify({
		
		"facebookId" : fbId,
		"email" : email,
		"fullName" : name,
		"phone" : "",
		"referralCodeUsed" : "",
		"deviceToken" : "",
		"image" : "",
		"imageUrl" : ""
	});
        $.ajax({
		url: api_base_url+"user/signinFb",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			
			$('#processingModal').modal('hide');
			$("#signInModal").modal('hide');
			
                    if(response.status=='OK'){
						var user = response.user;
						name = user.fullName;
						code = user.referralCode;
						localStorage.setItem('referralCode', code);
						localStorage.setItem('userName', name);
						//console.log("check :  "+code+ name);	
						sessionId=response.sessionId;
						localStorage.setItem('sessionId', sessionId);
						email= user.email;
						phone= user.phone;
						localStorage.setItem('emailId', email);
						localStorage.setItem('phoneNo', phone);
						document.getElementById("signInBotton").innerHTML="Hi, "+name;
						localStorage.setItem('couponCode', user.referralCode );					 
						document.getElementById('couponCode').value=code;
						
						getAllAddress(1);
                    }else{
						console.log(response);	
						if(response.status="error"){
							if(response.errorCode==101)
							{
								logout();
							}
							else{
								document.getElementById("defaultModalText").innerHTML=response.apiMessage;
								$("#defaultModal").modal();
							}
						}
			
                    }	
		},
		error: function (){
                   // console.log("An error occured calling api");
				   //$('#processingModal').modal('hide');
		}
	});
	
}

function resetPassword(){
	$("#forgotPasswordModal").modal('hide');
	//var sessionId = localStorage.getItem('sessionId');
	var email = document.getElementById('forgotPasswordEmailId').value;
	console.log("o k ");
    JSONData = JSON.stringify({
				"email": email
	});
	console.log(JSONData);
        $.ajax({
		url: api_base_url+"user/resetPassword",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			
                    if(response.status=='OK'){
                         
						
						
                    }else{
						console.log(response);	
						if(response.status="error"){
							if(response.errorCode==101)
							{
								//logout();
							}
							else{
								alert(response.apiMessage);
							}
						}
			
                    }	
		},
		error: function (){
                   // console.log("An error occured calling api");
		
		}
	});
	
	
	
}

var savedMenu;


function display_products(){
    var sessionId = localStorage.getItem('sessionId');
	var check=0;
	//$('#processingModal').modal();
    JSONData = JSON.stringify({
				"itemType":'',
                "exceptions":'',
				"sessionId": sessionId
	});
        $.ajax({
		url: api_base_url+"dayMenu/getList",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			//$('#processingModal').modal('hide');
                    if(response.status=='OK'){
						
						console.log("asD : "+response);
						kitchesStatus = response.open;
						if(kitchesStatus =="0"){ 	//kitchen not open
						
							$("#kitchenClosedModel").modal();
							document.getElementById("order_btn").innerHTML = "SEE MENU";
						}
						
                        if(response.menuItems.length>0){
                            var menu_item_str ='';
							savedMenu=response.menuItems;
                            $.each(response.menuItems, function( key, menu ) {
						
						if(menu.itemType=="0" && put1==0){
							// if( parseInt(response.dateH)>=12 ){
								// return true;
							// }
							document.getElementById("breakfast").style.display="inline";
							if(check!=0){
								menu_item_str +='</div>';
							}
							else
								check=1;
							menu_item_str +='<div id="breakfast_">';
							menu_item_str +='<div  id="dotNav" class="pull-right"><ul id ="sideDotNav-1" style="display:none;"></ul></div>'
							//menu_item_str +='<div data-spy="affix" data-offset-top="100"  id="dotNav dotNavBreakfast" class="pull-right"> <ul id ="sideDot sideDotNavBreakfast">  </ul></div>'
							put1=1;
							
						}
						else if(menu.itemType==1 && put1==0){
							// if(){
								// return true;
							// }
							document.getElementById("lunch").style.display="inline";
							if(check!=0){
								menu_item_str +='</div>';
							}
							else
								check=1;
							menu_item_str +='<div id="lunch_" >';
							menu_item_str +='<div  id="dotNav" class="pull-right"><ul id ="sideDotNav-2" style="display:none;"></ul></div>';
							//menu_item_str +='<div data-spy="affix" data-offset-top="100"  id="dotNav" class="pull-right"> <ul id ="sideDotNav"><li><a class="tooltip" href="#Menu1"></a></li>	<li ><a class="tooltip" href="#Menu2"></a></li> <li ><a class="tooltip" href="#Menu3"></a></li>  </ul></div>';
							//menu_item_str +='<div data-spy="affix" data-offset-top="100"  id="dotNav dotNavLunch" class="pull-right"> <ul id ="sideDot sideDotNavLunch">  </ul></div>'
							put1=1;
							
						}
						else if(menu.itemType==2 && put2==0){
							if(check!=0){
								menu_item_str +='</div>';
							}
							else
								check=1;
							document.getElementById("alldaymenu").style.display="inline";
							menu_item_str +='<div id="alldaymenu_">';
							menu_item_str +='<div  id="dotNav" class="pull-right"><ul id ="sideDotNav-3" style="display:none;"></ul></div>';
							//menu_item_str +='<div data-spy="affix" data-offset-top="100"  id="dotNav dotNavAlldaymenu" class="pull-right"> <ul id ="sideDot sideDotNavAlldaymenu">  </ul></div>'
							put2=1;
						}
						else if(menu.itemType==3 && put3==0){
							if(check!=0){
								menu_item_str +='</div>';
							}
							else
								check=1;
							document.getElementById("beverages").style.display="inline";
							menu_item_str +='<div id="beverages_">';
							menu_item_str +='<div  id="dotNav" class="pull-right"><ul id ="sideDotNav-4" style="display:none;"></ul></div>';
							
							//menu_item_str +='<div data-spy="affix" data-offset-top="100"  id="dotNav dotNavBeverages" class="pull-right"> <ul id =" sideDot sideDotNavBeverages">  </ul></div>'
							put3=1;
						}
						
						
						// if(menu.itemType==3){
							// menu_item_str +='<script>'
							// var tstr='\'<li ><a class="tooltip" href="#Menu-'+menu.itemId+'"></a></li>\'';
							// menu_item_str +='document.getElementById("sideDotNavBeverages").appendChild='+tstr+";";
							// menu_item_str +='</script>'
						// }
						
						var strType =(parseInt(menu.itemType)+1);
						menu_item_str +='<script> var str=\'<li onclick="func'+menu.itemId+'()"><a class="tooltip" href="#st-content-'+menu.itemId+'"></a></li>\';';
						menu_item_str +='$("#sideDotNav-'+strType+'").append(str);function func'+menu.itemId+'(){window.location.href="#st-content-'+menu.itemId+'"}</script>';

						menu_item_str +='<div id="st-content-'+menu.itemId+'" class="st-container" style="width:100%;" >';
						menu_item_str +='<div class="st-content"  > <section id="Menu-'+menu.itemId+'" class="sec pull-center">';
						var assetImg="http://www.fig.delivery/public/img/"+menu.itemId+".png";//var assetImg=menu.itemImage;
						//var assetImg="img/"+menu.itemId+".png";//var assetImg=menu.itemImage;
						menu_item_str +='<img src="'+assetImg+'" width="100%"/>	<center>';
						menu_item_str +='<div id="itemDetails-'+menu.itemId+'" class="desktopDishFooter">';
						menu_item_str +='<a id="btnKnowMore2"  class="button"  onClick="openModel('+menu.itemId+')">KNOW MORE</a>';
						var itemName = menu.itemName;
						itemName = itemName.toUpperCase();
						menu_item_str +='<span class="itemNameText" id="itemName"> '+itemName+' </span>';
						menu_item_str +='<span class="pricetag"><a id="btnAdd" style="cursor:pointer" class="button-grey"onClick="addToCart('+menu.itemId+')">+</a><i class="fa fa-inr"></i><span id="price">'+menu.price+'</span></span>'
						
						menu_item_str +='</div><div id="st-trigger-effects-'+menu.itemId+'" class="mobileDishFooter" >	'
						menu_item_str +='<span class="itemNameText" id="itemName" > '+itemName+' </span>';
						menu_item_str +='<br><a id="btnKnowMore" class="button"  onClick="openModel('+menu.itemId+')">KNOW MORE</a>';
						menu_item_str +='  <span class="pricetag" ><a id="btnAdd" class="button-grey" onClick="addToCart('+menu.itemId+')">+</a><i class="fa fa-inr"></i><span id="price">'+menu.price+'</span></span>'
						menu_item_str +='</div></center></section></div></div>'

						//popUp
						menu_item_str +='<center>'
						menu_item_str +='<div class="container" style="margin-top:80px;" style="height100%;" >';

						menu_item_str +='<div class="modal fade" id="knowMore-'+menu.itemId+'" role="dialog">';
						menu_item_str +='<div class="modal-dialog widthSetter" style="height:500px; margin-top:5%;">';
						menu_item_str +='<div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >';
						menu_item_str +='<div class="modal-header" style="padding:30px ">';
						menu_item_str +='<button type="button" class="close" data-dismiss="modal">&times;</button>';
                                
                                
                                //font-family:"MyriadProRegular", arial, helvetica, sans-serif;
						menu_item_str +='<center><h4 style="font-family:\'sansProLight\', sans-serif; letter-spacing: .18em;">'+itemName+'</h4></center>';
						menu_item_str +='</div><div class="modal-body" ><div class="form-group">';
						menu_item_str +='<p class="sideBarText" id= "popUpBody-'+menu.itemId+'" style="color:white;text-align:left;font-family:\'sansProLight\' , sans-serif; letter-spacing:0.18em"> </p>';
						
						
						//menu_item_str +='<script>function func1_'+menu.itemId+'(){  document.getElementById("popUpBody-'+menu.itemId+'").innerHTML="<br> '+menu.itemDescription +'<br> "; }</script>';
						//menu_item_str +='<button class="sidebarButton" id= "popUpBt1-'+menu.itemId+'" onClick="func1_'+menu.itemId+'()">Description</button> ';
						
						
						var ing = menu.ingredients;
						var strIng='<b>INGREDIENTS</b> <br> ';
						var len=menu.ingredients.length;
						if(ing.length==0){
							strIng=""
						
						}
						else{
							for(var key in menu.ingredients){
								if(key ==0){
									strIng+= menu.ingredients[key].key;
								}
								else if (len-1==key ){
									strIng+= ' and ' + menu.ingredients[key].key;
								}
								else{
									strIng+= ', '+ menu.ingredients[key].key;	
								}
								
							}
						}
						//console.log("ok, ",strIng);
						//var func2= "$('popUpBody-"+menu.itemId+"').value='"+strIng+"'";
						//menu_item_str +='<script>function func2_'+menu.itemId+'(){  document.getElementById("popUpBody-'+menu.itemId+'").innerHTML=" <br>'+strIng +' <br>"; }</script>';
						menu_item_str +='<script> document.getElementById("popUpBody-'+menu.itemId+'").innerHTML="'+menu.itemDescription +'<br> <br>'+strIng +' <br>"</script>';
						//menu_item_str +='<button class="sidebarButton" id= "popUpBt1-'+menu.itemId+'" onClick="func2_'+menu.itemId+'()" >Ingredients</button>';
					    menu_item_str +='</div></div></div></div></div></div>' 
						menu_item_str +='</center>'	
						//popUpClose
						
						menu_item_str +='<script>$(window).scroll(function(){var t = $("#st-content-'+menu.itemId+'").position().top;';
						menu_item_str +='var h = $("#st-content-'+menu.itemId+'").innerHeight();var h9 = h * 0.85;h9 = t+h9;h=t+h;var h8 = h * 0.45;h8 = t+h8;';
						menu_item_str +='if(h9 < $(window).scrollTop() && h > $(window).scrollTop()){$("#itemDetails-'+menu.itemId+'").css("visibility","hidden"); $("#st-trigger-effects-'+menu.itemId+'").css("visibility","hidden");}';
						menu_item_str +='else{$("#itemDetails-'+menu.itemId+'").css("visibility","visible");$("#st-trigger-effects-'+menu.itemId+'").css("visibility","visible");}});</script>';
						//if(h8 < $(window).scrollTop() && h > $(window).scrollTop()){ $("#st-trigger-effects-'+menu.itemId+'").css("visibility","hidden");}else{$("#st-trigger-effects-'+menu.itemId+'").css("visibility","visible");}
	
						});
						
							menu_item_str +='</div>';
							$('#product').append(menu_item_str);
							
                        }
                    }else{
                        //alert("Please Refresh this Page");
						if(response.status="error"){
							if(response.errorCode=="101")
							{
								logout();
							}
							else{
								alert(response.apiMessage);
	
							}
						}
			
                    }
			
		},
		error: function (){
                   // console.log("An error occured calling api");
				   	$('#processingModal').modal('hide');
				   alert("Please Check your Net Connection and Refresh this Page. Thank You");
		}
		
	});
	
}


function storeCart(){

	localStorage.setItem("cartTotal", total);	
	localStorage.setItem("cartSize", cartSize);	

	localStorage.setItem("cartItemId", JSON.stringify(cartItemId));	
	localStorage.setItem("cartItemCost", JSON.stringify(cartItemCost));	
	localStorage.setItem("cartItemQty", JSON.stringify(cartItemQty));	
	localStorage.setItem("cartItemName", JSON.stringify(cartItemName));	

}

function deleteCart(){
	total=0;
	cartSize=0;
	cartItemId=[];
	cartItemCost=[];
	cartItemQty=[];
	cartItemName=[];

	localStorage.setItem("cartTotal", total);	
	localStorage.setItem("cartSize", cartSize);	
	localStorage.setItem("cartItemId", "");	
	localStorage.setItem("cartItemCost", "");	
	localStorage.setItem("cartItemQty", "");	
	localStorage.setItem("cartItemName", "");	
	localStorage.setItem('codeAccepted', "0");
	
	 var elem = document.getElementById("order_btn");
  if(typeof elem !== 'undefined' && elem !== null) {
    document.getElementById("order_btn").innerHTML="ORDER NOW";	
  }
  displayCart();
}

function getFigCart(){

	if(localStorage.getItem("cartTotal") ===null){
		total=0;
	}
	else{
		total=JSON.parse(localStorage.getItem("cartTotal"));	
	}
	if(localStorage.getItem("cartSize") === null){
		cartSize=0;
	}
	else{
		cartSize=JSON.parse(localStorage.getItem("cartSize"));		
		
	}
	if(localStorage.getItem("cartItemId")=== null || localStorage.getItem("cartItemId") ==""){
		cartItemId=[];
	}
	else{
		console.log(localStorage.getItem("cartItemId"));
		cartItemId = JSON.parse(localStorage.getItem("cartItemId"));		
	}
	if(localStorage.getItem("cartItemCost") === null || localStorage.getItem("cartItemCost") ==""){
		cartItemCost=[];
	}
	else{
		cartItemCost = JSON.parse(localStorage.getItem("cartItemCost"));		
	}
	if(localStorage.getItem("cartItemQty") === null|| localStorage.getItem("cartItemQty") ==""){
		cartItemQty=[];
	}
	else{
		cartItemQty = JSON.parse(localStorage.getItem("cartItemQty"));		
	}
	if(localStorage.getItem("cartItemName")=== null || localStorage.getItem("cartItemName") ==""){
		cartItemName=[];
	}
	else{
		cartItemName = JSON.parse(localStorage.getItem("cartItemName"));	
		displayCart();
	}
	if(localStorage.getItem('codeAccepted')===null ||localStorage.getItem('codeAccepted')=="0"){
		discount=0;
	}
	else{
		discount=100;
	}
	
	console.log("hjk "+total+ cartSize+cartItemId+ cartItemCost+ cartItemQty+ cartItemName);
	checkCartOk();
}

function displayCart(){
	document.getElementById("tContent").innerHTML="";
	var cart_str="";
	console.log("jashkdj   "+cartItemId.length);
	storeCart();
	$.each(cartItemId, function( key, id ) {
			
		cart_str+='<tr>';
		cart_str+='<td class="reviewOrderTableLeft" id="inCartItemName-'+key+'"  style="letter-spacing:0.08em; color:#1a1a1a;">'+cartItemName[key]+'</td>';
		cart_str+='<td style="text-align:right"><button class="transparentButtons" id="inCartAdd-'+key+'" onclick="addToCart('+cartItemId[key]+')"><img src="http://www.fig.delivery/public/img/plus.png" width="20px" height="20px"/></button>';
		cart_str+='<DIV class="transparentButtons" id="inCartItemQty-'+key+'">'+cartItemQty[key]+'</DIV>';
		cart_str+='<button class="transparentButtons"  id="inCartSub-'+key+'" onclick="subFromCart('+cartItemId[key]+')"><img src="http://www.fig.delivery/public/img/minus.png" width="20px" height="20px"/></button></td>';
		cart_str+='<td style="text-align:center" class="dishPriceText" id="inCartItemCost'+key+'">'+(cartItemCost[key]*cartItemQty[key])+'</td>';
		cart_str+='</tr>';
		
	});	

	if(cartSize==0){
		
		if(kitchesStatus=="1")
			document.getElementById("order_btn").innerHTML="ORDER NOW";	
		else
			document.getElementById("order_btn").innerHTML="SEE MENU";	
	}
	else{
		document.getElementById("order_btn").innerHTML="CART ("+ cartSize +")";
	}
	storeCart();
	document.getElementById("cartTotal").innerHTML=total;
	document.getElementById("tContent").innerHTML=cart_str;
	var vat= total * 0.13125;
	vat = vat.toFixed(2);
	document.getElementById("displayVat").innerHTML=vat;
	// var shippingCharges=50;
	// document.getElementById("displayShippingCharges").innerHTML=shippingCharges;
	var discount=0;
	
	if(localStorage.getItem('codeAccepted')===null || localStorage.getItem('codeAccepted')=="0"){
		discount=0;
	}
	else{
		discount=100;
	}
	document.getElementById("displayDiscount").innerHTML=discount;
	
	var gtotal=(vat  + total)-discount;
	gtotal =gtotal.toFixed(2);
	document.getElementById("displayGrandTotal").innerHTML = (+ vat  + total)-discount;
}

function addToCart(num){
var pos=0, cost=0, id=0, name,len=0;
	$.each(savedMenu, function( key, menu ) {	
		if(menu.itemId==num){
			pos = key;
			cost = parseInt(menu.price);
			id = menu.itemId;
			name = menu.itemName;	
			console.log(pos);
			return false;
		}
	});
	if(cartSize==0){
		cartItemId=[num];
		cartItemCost=[cost];
		cartItemQty=[1];
		cartItemName=[name];
		
		total+=cost;
	}
	else{
		var cartPos=-1;
		len=cartItemId.length;
		if(cartItemId.length>1){
						
			$.each(cartItemId, function( key, id ) {
				if(id==num){
					cartPos=key;
					console.log(key);
					return false;
				}
				
			});
			if(cartPos==-1){
				cartItemId[len]=num;
				cartItemCost[len]=cost;
				cartItemQty[len]=1;
				cartItemName[len]=name;
				
				total+=cost;
			}
			else{				
				cartItemQty[cartPos]=parseInt(cartItemQty[cartPos])+1;
				total+=cost;
			}
		}
		else if(cartItemId.length==1){
			if(num==cartItemId){
				cartItemQty[0]=parseInt(cartItemQty)+1;
				total+=cost;
			}
			else{
				cartItemId[len]=num;
				cartItemCost[len]=cost;
				cartItemQty[len]=1;
				cartItemName[len]=name;
				
				total+=cost;
			}
		}
	}
	cartSize=cartSize+1;
	displayCart();
	console.log(cartItemId+" "+ cartItemCost+" "+ cartItemQty+" "+cartItemName);
}


function checkCartOk(){
	//if not in meni
	$.each(cartItemId, function( key, id ) {
		var found=0;
		
		$.each(savedMenu, function( elm, menu ) {
			if(menu.itemId == cartItemId[key]){
				found=1;
				//console.log('cartItemId' +id);
				return false;
			}
		});
		if(found == 0){
			console.log('cartItemId' +id);
			while(cartItemQty[key]>0){
				subFromCart(cartItemId[key]);
			}
		}
	});
	
}

function subFromCart(num){
	if(cartItemId.length>0){
		$.each(cartItemId, function( key, id ) {
			if(num==cartItemId[key]){
				if(cartItemQty[key]==1){
					total-=cartItemCost[key];
					
					cartItemCost.splice(key, 1);
					cartItemId.splice(key, 1);
					cartItemName.splice(key, 1);
					cartItemQty.splice(key, 1);
					
				}
				else{
					cartItemQty[key]=cartItemQty[key]-1;
					total-=cartItemCost[key];
				}
				return false;
			}
		});

		
		if(cartSize>0){
		cartSize=cartSize-1;
		}
		if(cartSize==0){
			$("#myModal").modal('hide');
		}
		displayCart();
	}

	console.log(num);
}

function getTotal(){
	return total;
}

function testLoginAndOrder(){
	//console.log(localStorage.getItem('sessionId'));
	if(localStorage.getItem('sessionId')===null || localStorage.getItem('sessionId')=="" ){
		$("#addEmailToPurchase").modal();
	}
	else{
		placeOrder();
	}
	
}

function placeOrder(){
	$("#addEmailToPurchase").modal('hide');
	var sessionId="";
	if(localStorage.getItem('sessionId')===null || localStorage.getItem('sessionId')=="" ){
		sessionId="";
	}
	else{
		sessionId= localStorage.getItem('sessionId');
	}
	var email="";
	email=document.getElementById("purchaseEmailId").value;
	var JSONData="";
	var street =document.getElementById("streetAddress").value;
	var locality =document.getElementById("LocalityValue").value;
	// locality = locality.replace('<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span> <p style="display:inline;">', "");
	// locality = locality.replace('<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span>', "");
	// locality = locality.replace('<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span>', "");
	// locality = locality.replace('</p>', "");	
	locality = locality.replace(/\r?\n|\r/g, "");
	locality = locality.trim();
	// locality = locality.replace("   ", "");
	// locality = locality.replace("   ", "");
	// locality = locality.replace("   ", "");
	// locality = locality.replace("\n", "");
	// locality = locality.replace("\n", "");
	// locality = locality.replace('"', '\"');	
	locality = locality.replace('Select your Locality', '');
	
	// console.log("gghh : "+locality);
	
	if(locality.length<3){
		document.getElementById("defaultModalText").innerHTML="PLEASE SELECT A LOCALITY";
		$("#defaultModal").modal();
		return ;
	}if(street.length<3){
		document.getElementById("defaultModalText").innerHTML="PLEASE SELECT A STREET";
		$("#defaultModal").modal();
		return ;
	}
	
	var landmark =document.getElementById("landmark").value;
	if(landmark.length<3){
		document.getElementById("defaultModalText").innerHTML="PLEASE SELECT A LANDMARK";
		$("#defaultModal").modal();
		return ;
	}
	var phone =document.getElementById("verificationPhonePrefix").value+document.getElementById("verificationPhone").value;
	var couponCode ="0";
	var cutlery=0;
	
	if(document.getElementById("checkBoxCutlery").checked==true){
		cutlery=0;
	}
	else{
		cutlery=1;
	}
	if(localStorage.getItem('codeAccepted') === null){
		
	}
	else{
		couponCode=localStorage.getItem('codeAccepted');
	}
	var codeVar =document.getElementById("verificationCode").value;
	$('#processingModal').modal();
	JSONData+=" { \"sessionId\" : \""+sessionId +"\",";
	JSONData+="\"street\" : \""+ street +"\",";
	JSONData+="\"locality\" : \""+ locality +"\",";
	JSONData+="\"landmark\" : \""+ landmark +"\",";
	JSONData+="\"phone\" : \""+ phone+"\",";
	JSONData+="\"verificationCode\" : \""+codeVar +"\",";
	JSONData+="\"email\" : \""+ email +"\",";
	JSONData+="\"saveCutlery\" : \""+ cutlery +"\",";
	if(localStorage.getItem('codeAccepted')===null || localStorage.getItem('codeAccepted')=="0"){
		JSONData+="\"couponCode\" : \""+0 +"\",";
	}
	else{
		JSONData+="\"couponCode\" : \""+ 1 +"\",";
	}
	
	
	var items=" \"items\": [";	
	$.each(cartItemId, function( key, id ) {		
		
		items+="{\"itemId\": \""+id+"\",\"quantity\": \""+cartItemQty[key]+"\" }";
		if(cartItemId.length -1!=key){
			items+=", ";
		}
	});
	items+="]";
	JSONData+=items;
	JSONData+="}";
	console.log("ck chk : "+ JSONData);
	
	console.log("o k ");
    
        $.ajax({
		url: api_base_url+"purchase/add",
		type:"POST",
		dataType: "json",
		data: JSONData,
		contentType: "application/json; charset=utf-8",
		async: false,
		success: function(response) {
			$('#processingModal').modal('hide');
                    if(response.status=='OK'){
					//purchaseadded.
					localStorage.setItem('codeAccepted', "0");
					deleteCart();
					checkMyModelOpen();  //to close the myModal
					document.getElementById("defaultModalText").innerHTML='THANK YOU! YOUR ORDER HAS BEEN PLACED. WE HAVE SENT YOU AN EMAIL CONFIRMATION ON THE  REGISTERED EMAIL ADDRESS<br><br> ORDER ID: '+response.purchase.purchaseId+'<br> FOR MORE DETAILS, GO TO YOUR PROFILE';
					$("#defaultModal").modal();
					
                    }else{
                        //alert("Server Errro");
						//logout();
						console.log(response);
					document.getElementById("defaultModalText").innerHTML=response.apiMessage;
					$("#defaultModal").modal();
					$('#processingModal').modal('hide');		
                    }	
		},
		error: function (){
                   // console.log("An error occured calling api");
				   			$('#processingModal').modal('hide');
							console.log();
		}
	});	
}
