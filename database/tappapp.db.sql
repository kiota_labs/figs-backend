SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

SET NAMES 'utf8mb4';
SET CHARACTER SET utf8mb4;

SET @@global.time_zone='+05:30';
SET GLOBAL group_concat_max_len = 50000;

DROP SCHEMA IF EXISTS `fig`;
CREATE SCHEMA IF NOT EXISTS `fig` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `fig`;

-- Drop user if exists
-- GRANT USAGE ON *.* TO 'fig'@'localhost';
-- DROP USER 'fig'@'localhost';

-- Create user
CREATE USER 'fig'@'localhost' IDENTIFIED BY 'fig@000';
GRANT ALL ON fig.* TO 'fig'@'localhost';


-- -----------------------------------------------------
-- Table `fig`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`user` ;

CREATE TABLE IF NOT EXISTS `fig`.`user`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `role` VARCHAR(10) NOT NULL DEFAULT 'user',     /* comma separated values of various roles assigned to the user. */
    `facebookId` VARCHAR(128) NULL,
    `email` VARCHAR(128) NULL,
    `username` VARCHAR(128) NULL,
    `password` VARCHAR(60) BINARY NULL,
    `fullName` VARCHAR(128) NULL,
    `street` VARCHAR(250) NULL,
    `locality` VARCHAR(250) NULL,
    `landmark` VARCHAR(250) NULL,
    `zipcode` VARCHAR(10) NULL,
    `latitude` FLOAT NULL,
    `longitude` FLOAT NULL,
    `phone` VARCHAR(20) NULL,
    `image` VARCHAR(128) NULL,
    `referralCode` VARCHAR(128) NULL,               /* referralCode of user */
    `referralCodeUsed` VARCHAR(128) NULL,           /* referralCode of user who referred current user */
    `balanceReferralAmount` DECIMAL(10,2) NULL DEFAULT 0,
    `cardNo` VARCHAR(128) NULL,                     /* Debit / credit card no */
    `paypalId` VARCHAR(128) NULL,                   /* paypal id of the user */
    `notifyMe` BOOLEAN NOT NULL DEFAULT TRUE,
    `isPhoneVerified` BOOLEAN NOT NULL DEFAULT FALSE,
    `isDisabled` BOOLEAN NOT NULL DEFAULT FALSE,
    `disableReason` VARCHAR(128) NULL,
    `lastAccessDate` TIMESTAMP NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    INDEX `idxUserFacebookId` (`facebookId` ASC),
    INDEX `idxUserEmail` (`email` ASC)
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`userAddress`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`userAddress` ;

CREATE TABLE IF NOT EXISTS `fig`.`userAddress`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userId` INT(10) UNSIGNED NOT NULL,
    `placeName` VARCHAR(50) NULL,                   /* home/office/other */
    `street` VARCHAR(250) NULL,
    `locality` VARCHAR(250) NULL,
    `landmark` VARCHAR(250) NULL,
    `zipcode` VARCHAR(10) NULL,
    `latitude` FLOAT NULL,
    `longitude` FLOAT NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkUserAddressUserId` FOREIGN KEY (`userId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`userPassword`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`userPassword` ;

CREATE TABLE IF NOT EXISTS `fig`.`userPassword`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userId` INT(10) UNSIGNED NOT NULL,
    `password` VARCHAR(60) BINARY NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkUserPasswordUserId` FOREIGN KEY (`userId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`userReferral`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`userReferral` ;

CREATE TABLE IF NOT EXISTS `fig`.`userReferral`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `referringUserId` INT(10) UNSIGNED NOT NULL,
    `referredUserId` INT(10) UNSIGNED NOT NULL,
    `referralCodeUsed` VARCHAR(128) NULL,           /* referralCode of user who referred current user */
    `referralAmount` DECIMAL(10,2) NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkUserReferralSenderUserId` FOREIGN KEY (`referringUserId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `fkUserReferralReceiverUserId` FOREIGN KEY (`referredUserId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`item` ;

CREATE TABLE IF NOT EXISTS `fig`.`item`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `itemName` VARCHAR(250) NULL,
    `description` VARCHAR(1500) NULL,
    `price` DECIMAL(10,2) NOT NULL,               /* unit price */
    `image` VARCHAR(128) NULL,
    `itemType` TINYINT NOT NULL,          /* 1 breakfast (8 to 1), 2 lunch (1 to 4), 3 all day meal, 4 beverages */
    `likeCount` INT(10) UNSIGNED NOT NULL DEFAULT 0,
    `reviewCount` INT(10) UNSIGNED NOT NULL DEFAULT 0,
    `ratingCount` INT(10) UNSIGNED NOT NULL DEFAULT 0,
    `totalRating` INT(10) UNSIGNED NOT NULL DEFAULT 0,
    `isDisabled` BOOLEAN NOT NULL DEFAULT FALSE,
    `disableReason` VARCHAR(128) NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`itemNutrient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`itemNutrient` ;

CREATE TABLE IF NOT EXISTS `fig`.`itemNutrient`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `itemId` INT(10) UNSIGNED NOT NULL,
    `key` VARCHAR(250) NULL,
    `value` VARCHAR(250) NULL,
    `isDisabled` BOOLEAN NOT NULL DEFAULT FALSE,
    `disableReason` VARCHAR(128) NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkItemNutrientItemId` FOREIGN KEY (`itemId`) REFERENCES `fig`.`item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`itemIngredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`itemIngredient` ;

CREATE TABLE IF NOT EXISTS `fig`.`itemIngredient`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `itemId` INT(10) UNSIGNED NOT NULL,
    `key` VARCHAR(250) NULL,
    `value` VARCHAR(250) NULL,
    `isDisabled` BOOLEAN NOT NULL DEFAULT FALSE,
    `disableReason` VARCHAR(128) NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkItemIngredientItemId` FOREIGN KEY (`itemId`) REFERENCES `fig`.`item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`itemReview`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`itemReview` ;

CREATE TABLE IF NOT EXISTS `fig`.`itemReview`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userId` INT(10) UNSIGNED NOT NULL,
    `itemId` INT(10) UNSIGNED NOT NULL,
    `rating` TINYINT NOT NULL,
    `comments` VARCHAR(250) NULL,
    `isDisabled` BOOLEAN NOT NULL DEFAULT FALSE,
    `disableReason` VARCHAR(128) NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkItemReviewUserId` FOREIGN KEY (`userId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `fkItemReviewItemId` FOREIGN KEY (`itemId`) REFERENCES `fig`.`item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`weekMenu`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`weekMenu` ;

CREATE TABLE IF NOT EXISTS `fig`.`weekMenu`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `day` TINYINT UNSIGNED NOT NULL,        /* 1 Mon, 2 Tue, 3 Wed, 4 Thu, 5 Fri, 6 Sat, 7 Sun */
    `itemId` INT(10) UNSIGNED NOT NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkWeekMenuItemId` FOREIGN KEY (`itemId`) REFERENCES `fig`.`item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`dayMenu`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`dayMenu` ;

CREATE TABLE IF NOT EXISTS `fig`.`dayMenu`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `menuDate` DATE NOT NULL,
    `itemId` INT(10) UNSIGNED NOT NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkDayMenuItemId` FOREIGN KEY (`itemId`) REFERENCES `fig`.`item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`itemLike`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`itemLike` ;

CREATE TABLE IF NOT EXISTS `fig`.`itemLike`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `itemId` INT(10) UNSIGNED NOT NULL,
    `userId` INT(10) UNSIGNED NOT NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkItemLikeItemId` FOREIGN KEY (`itemId`) REFERENCES `fig`.`item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `fkItemLikeUserId` FOREIGN KEY (`userId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`purchase`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`purchase` ;

CREATE TABLE IF NOT EXISTS `fig`.`purchase`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userId` INT(10) UNSIGNED NULL,
    `street` VARCHAR(250) NULL,
    `locality` VARCHAR(250) NULL,
    `landmark` VARCHAR(250) NULL,
    `phone` VARCHAR(20) NULL,
    `saveCutlery` BOOLEAN NOT NULL DEFAULT FALSE,
    `couponCode` VARCHAR(10) NULL,
    `purchaseDate` DATETIME NOT NULL,
    `paymentDate` DATETIME NULL,
    `deliveryDate` DATETIME NULL,
    `paypalTransId` VARCHAR(255) NULL,
    `purchaseStatus` TINYINT NOT NULL DEFAULT 1,    /* 1-order received, 2-order accepted, 3-processing, 4-delivery rejected, 5-delivered */
    `isPaid` BOOLEAN NOT NULL DEFAULT FALSE,
    `isCancelled` BOOLEAN NOT NULL DEFAULT FALSE,
    `cancelReason` VARCHAR(128) NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkPurchaseUserId` FOREIGN KEY (`userId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`purchaseDetail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`purchaseDetail` ;

CREATE TABLE IF NOT EXISTS `fig`.`purchaseDetail`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `purchaseId` INT(10) UNSIGNED NOT NULL,
    `itemId` INT(10) UNSIGNED NOT NULL,
    `quantity` DECIMAL(10,2) UNSIGNED NOT NULL,
    `price` DECIMAL(10,2) NOT NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fkPurchaseDetailPurchaseId` FOREIGN KEY (`purchaseId`) REFERENCES `fig`.`purchase`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `fkPurchaseDetailItemId` FOREIGN KEY (`itemId`) REFERENCES `fig`.`item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`userSession`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`userSession`;

CREATE TABLE IF NOT EXISTS `fig`.`userSession`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `sessionId` VARCHAR(40) NOT NULL,
    `userId` INT(10) UNSIGNED NOT NULL,
    `userName` VARCHAR(128) NULL,
    `role` VARCHAR(10) NOT NULL,
    `deviceToken` VARCHAR(128) NULL,
    `deviceBadge` INT(10) UNSIGNED NULL DEFAULT 0,
    `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `idxUserSessionSessionId` (`sessionId` ASC),
    CONSTRAINT `fkUserSessionUserId` FOREIGN KEY (`userId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`serviceLocation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`serviceLocation` ;

CREATE TABLE IF NOT EXISTS `fig`.`serviceLocation`
(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `locationName` VARCHAR(50) NULL,
    `street` VARCHAR(250) NULL,
    `locality` VARCHAR(250) NULL,
    `landmark` VARCHAR(250) NULL,
    `zipcode` VARCHAR(10) NULL,
    `latitude` FLOAT NULL,
    `longitude` FLOAT NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`event`;

CREATE TABLE IF NOT EXISTS `fig`.`event`
(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `eventType` TINYINT UNSIGNED NOT NULL,
    `raiserId` INT UNSIGNED NOT NULL,   /* user who raised the event */
    `relatedUserId` INT UNSIGNED NULL,  /* user who has been flagged or followed or whose item has been tapped */
    `elementId` INT UNSIGNED NOT NULL,  /* itemId, followedUserId */
    `description` VARCHAR(250) NULL,    /* event description */
    `eventDate` DATETIME NOT NULL,
    `isNotified` BOOLEAN NOT NULL DEFAULT FALSE,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    `createId` INT UNSIGNED NULL,
    `updateId` INT UNSIGNED NULL,
    PRIMARY KEY (`id`),
    INDEX `idxEventRaiserId` (`raiserId` ASC),
    INDEX `idxEventRelatedUserId` (`relatedUserId` ASC),
    CONSTRAINT `fkEventRaiserId` FOREIGN KEY (`raiserId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `fkEventRelatedUserId` FOREIGN KEY (`relatedUserId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`notification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`notification`;

CREATE TABLE IF NOT EXISTS `fig`.`notification`
(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `receiverId` INT UNSIGNED NOT NULL,  /* user who will get the notification */
    `message` VARCHAR(500) NOT NULL,
    `eventId` INT UNSIGNED NOT NULL,
    `isSeen` BOOLEAN NOT NULL DEFAULT FALSE,
    `isDisabled` BOOLEAN NOT NULL DEFAULT FALSE,
    `disableReason` VARCHAR(128) NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    `createId` INT UNSIGNED NULL,
    `updateId` INT UNSIGNED NULL,
    PRIMARY KEY (`id`),
    INDEX `idxNotificationReceiverId` (`receiverId` ASC),
    CONSTRAINT `fkNotificationReceiverId` FOREIGN KEY (`receiverId`) REFERENCES `fig`.`user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `fkNotificationEventId` FOREIGN KEY (`eventId`) REFERENCES `fig`.`event`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`phoneVerification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`phoneVerification` ;

CREATE  TABLE IF NOT EXISTS `fig`.`phoneVerification`
(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `phone` VARCHAR(20) NOT NULL,
    `verificationCode` VARCHAR(6) NOT NULL,
    `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updateDate` TIMESTAMP NULL,
    `createId` INT UNSIGNED NULL,
    `updateId` INT UNSIGNED NULL,
    PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `fig`.`password_resets`
-- this table is required by laravel
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fig`.`password_resets`;

CREATE TABLE IF NOT EXISTS `fig`.`password_resets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idxUserEmail` (`email`),
  KEY `idxUserToken` (`token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=10 ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
