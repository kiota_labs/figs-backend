@extends('app')

@section('content')



<form class="form-horizontal" method="POST" enctype="multipart/form-data"
      accesskey=""   accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb" style="height:auto;">
                <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                @if(\Session::has('homePage'))

                @else
                <li><i class="fa fa-users"></i><a href="{{ url('cms/user/viewGroups') }}">All Registered Groups</a></li>

                @endif
                <li><i class="fa fa-users"></i>Group Details</li>
            </ol>
        </div>
    </div>
    @if(\Auth::user()->role == "group")
    @extends('templates.sidebar')
    @section('sidebar') 
    <li class="sub-menu">
        <a href="{{ url('cms/groupFollower/viewFollowers/'.$userDetails->id) }}" class="">
            <i class="icon_document_alt"></i>
            <span>View Group Followers</span>
        </a>
    </li> 
    @endsection
    @endif
    <!--<div class="col-sm-3">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="#">Actions</a></li>
            <li><a href="{{ url('cms/user/viewGroups') }}">View All Groups</a></li> 
            <li><a href="{{ url('cms/groupFollower/viewFollowers/'.$userDetails->id) }}">View Group Followers</a></li> 
        </ul>
    </div> -->
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>GROUP DETAILS</b>
            </div>
            <div class="panel-body">

                <div class="col-md-4 text-center">
                    <img class="resize img-responsive img-thumbnail" src="{{{isset($userDetails->image) && !empty($userDetails->image) && !empty($userDetails->image)? $userDetails->image : asset(config('constants.groupDefaultImage'))}}}"/>
                </div>

                <div class="col-md-8">
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Group Name</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->fullName) && !empty($userDetails->fullName)?$userDetails->fullName:"Not available"}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Email</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->email) && !empty($userDetails->email)?$userDetails->email:"Not available"}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Phone No</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->phone) && !empty($userDetails->phone)?$userDetails->phone:"Not available"}}</div>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Description</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->description) && !empty($userDetails->description)?$userDetails->description:"Not available"}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Signup Date</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->createDate)?date('M j Y ',strtotime($userDetails->createDate)):null}}</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Status</div>
                        <div class="col-sm-6 col-xs-8 text-left" id="ableStatus">{{$userDetails->isDisabled ? 'Inactive' : 'Active'}}</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Last Seen</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->lastAccessDate)?date('M j Y ',strtotime($userDetails->lastAccessDate)):"Not available"}}</div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Documents</div>
                            <div class="col-sm-6 col-xs-8 text-left">
                            @if (count($userDocuments) > 0)
                            @foreach($userDocuments as $userDocument)
                            <a target="_blank" class="text-left" href="{{asset('images/user/Documents/'.$userDocument->documentName)}}">{{$userDocument->documentName}}</a>
                            <br/>
                            @endforeach
                            @endif
                            </div>
                       
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Visibility</div>
                        @if($userDetails->approvalStatus == 1)
                        <a class="btn btn-success control-label text-center" id="act_{{$userDetails->id}}" data-id="{{$userDetails->id}}" href="#" data-toggle="modal" data-target="#myModalEnable" style="{{ ($userDetails->isDisabled)  ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} margin-right: 15px;">Unhide</a>
                        <a class="btn btn-danger control-label text-center" id="deact_{{$userDetails->id}}" data-id="{{$userDetails->id}}" href="#" data-toggle="modal" data-target="#myModalDisable" style="{{ ($userDetails->isDisabled) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}} margin-right: 15px;">Hide</a>
                        @else
                        <a class="btn btn-default control-label text-center" style="background-color: #B5B5B5;" disabled="disabled" href="#" data-toggle="modal" >Hide</a>
                        @endif
                    </div>

                    <div class="form-group row text-right">
                        <a class="btn btn-success" id="approve{{$userDetails->id}}" data-id="{{$userDetails->id}}" href="#" data-toggle="modal" data-target="#myModalActivation" style="{{ ($userDetails->approvalStatus == 0) ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} padding:5px;">Approve</a>

                        <a class="btn btn-danger" id="reject{{$userDetails->id}}" data-id="{{$userDetails->id}}" href="#" data-toggle="modal" data-target="#myModal" style="{{ ($userDetails->approvalStatus == 0) ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} padding:5px;">&nbsp;Reject&nbsp;&nbsp;</a>
                    </div>

                    <div class="form-group row text-right" style="padding:10px;">
                        <a id="statusLabel" class="btn btn-danger" disabled="disabled" style="{{($userDetails->approvalStatus == 0)?'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}} margin: 0px 4px;">
                            @if($userDetails->approvalStatus == 1)
                            {{"Approved"}} 
                            @elseif($userDetails->approvalStatus == 2)
                            {{"Rejected"}} 
                            @endif
                        </a>

                        <a id="verifyButton" class="btn btn-success" style="{{($userDetails->approvalStatus == 1 && $userDetails->isVerified == 0)?'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} padding:5px;" id="verify{{$userDetails->id}}" data-id="{{$userDetails->id}}" href="#" data-toggle="modal" data-target="#myModalVerification">
                            &nbsp;&nbsp;Verify&nbsp;&nbsp;&nbsp;
                        </a>

                        <a id="verifiedButton" class="btn btn-success" disabled="disabled" style="{{($userDetails->isVerified == 1)?'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} padding:5px;" id="verify{{$userDetails->id}}" data-id="{{$userDetails->id}}" href="#" data-toggle="modal" data-target="#myModalVerification">
                            &nbsp;Verified&nbsp;
                        </a>

                    </div>

                </div>
            </div>
        </div>
    </div>
</form>
@if($userDetails->approvalStatus == 1)
<div class="panel panel-default">

    <div class="panel-body">


        <form class="form-inline" style="margin-bottom:9px;" method="POST" enctype="multipart/form-data" action="{{URL::to('cms/groupFollower/searchGroupFollowers/'.$userDetails->id)}}">


            <select name="gender" id='gender' class="form-control col-xs-4" style="width: 140px; position: relative;">
                <option value="">Select Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select> 
            <select name="age" id='age' class="form-control col-xs-4" style="width: 140px; position: relative;">
                <option value="">Select Age</option>
                <option value="10-25">10-25</option>
                <option value="26-40">26-40</option>
                <option value="41-55">41-55</option>
                <option value="56-70">56-70</option>
                <option value="71-85">71-85</option>
                <option value="86-100">86-100</option>
            </select>
            <input type="hidden" id="genderHide" value="{{isset($searchCriteria['gender'])?$searchCriteria['gender']:""}}">
            <input type="hidden" id="ageHide" value="{{isset($searchCriteria['age'])?$searchCriteria['age']:""}}">

            <button class="btn btn-default" style="position: relative; height:34px;" type="submit"><i class="glyphicon glyphicon-search"></i></button>

        </form>

        @if (count($followers) > 0)
        <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
            <thead>
                <tr>
                    <th class="text-center" style="vertical-align: middle;">Name</th>
                    <th class="text-center" style="vertical-align: middle;">Gender</th>
                    <th class="text-center" style="vertical-align: middle;">D.O.B</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($followers as $follower)

                <tr>
                    <td style="vertical-align: middle;"><img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($follower->image) ? asset('images/user/thumb/'.$follower->image): asset(config('constants.userDefaultImage'))}}}"/>
                        {{ $follower->fullName }}</td>
                    <td class="text-center" style="vertical-align: middle;">{{ $follower->gender }}</td>
                    <td class="text-center" style="vertical-align: middle;">{{ $follower->dob }}</td>
                </tr>
                @endforeach
            </tbody>

        </table>
        <?php echo $followers->appends(Request::input())->render(); ?>
        @else
        <div class="alert alert-danger">
            <strong>Whoops!</strong> No records found for Group Followers.<br><br>
        </div>
        @endif



    </div>
</div>
@endif








<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Group Confirmation: Reason for Rejection</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="rejectGroupForm" id="rejectGroupForm">
                    <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                    <input type="text" class="form-control" id="rejectionReason" name="rejectionReason" placeholder="Reason for Rejection">
                    <input type="hidden" value="" name="dialogGroupId" id="dialogGroupId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Cancel</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">

            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Group Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Would you like to approve this group.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Reject</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Confirm</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="myModalVerification" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">TAPP Group Verification</h4>
            </div>
            <div class="modal-body">
                <p>Would you like to confirm that Tapp has verified this group?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-default" onClick="Verify()">Confirm</button>
                <input type="hidden" value="" name="userId" id="dialogVerifyUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="myModalDisable" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please enter reason to hide group</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                    <label style="font-size: 15px;">
                        You are about to disable this group,this will affect the visibility of the 
                        group in the App,Do you want to continue?
                    </label>
                    <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Reason to Hide">
                    <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                    <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-success" id="dailogDisableSubmit" value="Submit">

            </div>
        </div>

    </div>
</div>
<div id="myModalEnable" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Activation</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to unhide this group</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onClick="Enable()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogEnableUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogGroupId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
    $('#myModalVerification').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogVerifyUserId').val(e.relatedTarget.dataset.id);
    });
    $('#myModalDisable').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalEnable').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogEnableUserId').val(e.relatedTarget.dataset.id);
    });
    function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
    $(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#rejectionReason').val();
            var id = $('#dialogGroupId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/user/reject')}}",
                type: 'POST',
                data: $('#rejectGroupForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#approve' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#reject' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                    $('#statusLabel').html('Rejected');
                    $('#statusLabel').css({'display': 'inline', 'visibility': 'visible'});
                }
            });
        });
    });
    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/user/approve')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
                $('#approve' + userId).css({'display': 'none', 'visibility': 'hidden'});
                $('#reject' + userId).css({'display': 'none', 'visibility': 'hidden'});
                $('#statusLabel').html('Approved');
                $('#statusLabel').css({'display': 'inline', 'visibility': 'visible'});
                $('#verifyButton').css({'display': 'inline', 'visibility': 'visible'});

            }
        });
    }
    $(document).ready(function () {
        $("#dailogDisableSubmit").click(function () {
            var data = $('#disableReason').val();
            var id = $('#dialogClubId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/user/disable')}}",
                type: 'POST',
                data: $('#disableReasonForm').serialize(),
                success: function (response) {
                    $('#myModalDisable').modal('hide');
                    $('#deact_' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#act_' + id).css({'display': 'inline', 'visibility': 'visible'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                }
            });
        });
    });


    function Enable() {

        var userId = document.getElementById('dialogEnableUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/user/enable')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalEnable').modal('hide');
                $('#deact_' + userId).css({'display': 'inline', 'visibility': 'visible'});
                $('#act_' + userId).css({'display': 'none', 'visibility': 'hidden'});

            }
        });
    }
    function Verify() {

        var userId = document.getElementById('dialogVerifyUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/user/verifyUser')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalVerification').modal('hide');
                $('#verifyButton').hide();
                $('#verifiedButton').css({'display': 'inline', 'visibility': 'visible'});

            }
        });
    }
</script>

@endsection




