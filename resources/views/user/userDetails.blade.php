@extends('app')

@section('content')



<form class="form-horizontal" method="POST" enctype="multipart/form-data"
      accesskey=""   accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb" style="height:auto;">
                <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                @if(\Session::has('withdraw'))
                <li><i class="fa fa-users"></i><a href="{{url('cms/user/withdrawTappRequests')}}">Withdraw Requests</a></li>
                <li><i class="fa fa-users"></i>Requested By User</li>
                @elseif(\Session::has('redeem'))
                <li><i class="fa fa-users"></i><a href="{{url('cms/user/itemRedeemRequests')}}">All Redeem Requests</a></li>
                <li><i class="fa fa-users"></i>Requested By User</li>
                @else
                <li><i class="fa fa-users"></i><a href="{{ url('cms/user/viewUsers') }}">All Registered Users</a></li>
                <li><i class="fa fa-users"></i>User Details</li>
                @endif
            </ol>
        </div>
    </div>
    @if(\Auth::user()->role == "group")
    @extends('templates.sidebar')
    @section('sidebar') 
    @endsection
    @endif
    <!--<div class="col-sm-3">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="#">Actions</a></li>
            <li><a href="{{ url('cms/user/viewGroups') }}">View All Groups</a></li> 
            <li><a href="{{ url('cms/groupFollower/viewFollowers/'.$userDetails->id) }}">View Group Followers</a></li> 
        </ul>
    </div> -->
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>User Details</b>
            </div>
            <div class="panel-body">

                <div class="col-md-4 text-center">
                    @if($userDetails->role == 'group')
                    <img class="img-circle img-responsive img-thumbnail" src="{{{isset($userDetails->image) && !empty($userDetails->image) ? $userDetails->image : asset(config('constants.groupDefaultImage'))}}}"/>
                    @else
                    <img class="img-circle img-responsive img-thumbnail" src="{{{isset($userDetails->image) && !empty($userDetails->image) ? $userDetails->image : asset(config('constants.userDefaultImage'))}}}"/>
                    @endif                
                </div>

                <div class="col-md-8">
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Name</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->fullName) && !empty($userDetails->fullName)?$userDetails->fullName:$userDetails->username}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Email</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->email) && !empty($userDetails->email)?$userDetails->email:"Not available"}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Phone No</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->phone) && !empty($userDetails->phone)?$userDetails->phone:"Not available"}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Address</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->address) && !empty($userDetails->address)?$userDetails->address:"Not available"}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Description</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->description) && !empty($userDetails->description)?$userDetails->description:"Not available"}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Status</div>
                        <div class="col-sm-6 col-xs-8 text-left" id="ableStatus">{{$userDetails->isDisabled ? 'Inactive' : 'Active'}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Signup Date</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->createDate)?date('M j Y ',strtotime($userDetails->createDate)):"Not available"}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Last Seen</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->lastAccessDate)?date('M j Y ',strtotime($userDetails->lastAccessDate)):"Not available"}}</div>
                    </div>




                </div>
            </div>
        </div>
    </div>
</div>
</form>

<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please Enter Reason To Disable</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="rejectGroupForm" id="rejectGroupForm">
                    <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                    <input type="text" class="form-control" id="rejectionReason" name="rejectionReason" placeholder="Reason to Reject">
                    <input type="hidden" value="" name="dialogGroupId" id="dialogGroupId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">

            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Activation</h4>
            </div>
            <div class="modal-body">
                <p>Do You Want To Approve This User</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="myModalVerification" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Verification</h4>
            </div>
            <div class="modal-body">
                <p>Do You Want To Verify This Group</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onClick="Verify()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogVerifyUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogGroupId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
    $('#myModalVerification').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogVerifyUserId').val(e.relatedTarget.dataset.id);
    });
    function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
    $(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#rejectionReason').val();
            var id = $('#dialogGroupId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/user/reject')}}",
                type: 'POST',
                data: $('#rejectGroupForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#approve' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#reject' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                    $('#statusLabel').html('Rejected');
                    $('#statusLabel').css({'display': 'inline', 'visibility': 'visible'});
                }
            });
        });
    });
    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/user/approve')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
                $('#approve' + userId).css({'display': 'none', 'visibility': 'hidden'});
                $('#reject' + userId).css({'display': 'none', 'visibility': 'hidden'});
                $('#statusLabel').html('Approved');
                $('#statusLabel').css({'display': 'inline', 'visibility': 'visible'});
                $('#verifyButton').css({'display': 'inline', 'visibility': 'visible'});

            }
        });
    }
    function Verify() {

        var userId = document.getElementById('dialogVerifyUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/user/verifyUser')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalVerification').modal('hide');
                $('#verifyButton').hide();
                $('#verifiedButton').css({'display': 'inline', 'visibility': 'visible'});

            }
        });
    }
</script>

@endsection




