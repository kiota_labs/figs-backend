@extends('app')

@section('content')


<div class="container-fluid">
    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
          accesskey=""   accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                @if(\Auth::user()->role == "admin")
                                                @if(\Session::has('homePage'))
                                                <li><i class="fa fa-user"></i>Flagged User/Group Details</li>
                                                @else
                                                <li><i class="fa fa-users"></i><a href="{{ url('cms/flaggedUser/getFlaggedUsers') }}">Flagged Users</a></li>
                                                <li><i class="fa fa-user"></i>User Details</li>
                                                @endif
                                                @elseif(\Auth::user()->role == "group")
                                                <li><i class="fa fa-user"></i>My Profile</li>
                                                @endif
					</ol>
				</div>
			</div>
        @if(\Auth::user()->role == "admin")
        @extends('templates.sidebar')
        @section('sidebar') 
        @endsection
        @endif
      
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>User Details</b>
                </div>
                <div class="panel-body">
                    
                    <div class="col-md-4 text-center">
                        @if($userDetails->role == 'group')
                        <img class="img-circle img-responsive img-thumbnail" src="{{{isset($userDetails->image) && !empty($userDetails->image) ? $userDetails->image : asset(config('constants.groupDefaultImage'))}}}"/>
                        @else
                        <img class="img-circle img-responsive img-thumbnail" src="{{{isset($userDetails->image) && !empty($userDetails->image) ? $userDetails->image : asset(config('constants.userDefaultImage'))}}}"/>
                        @endif
                    </div>
                    <div class="col-md-8">
                        @if(\Auth::user()->role == "admin")
                    <div class="form-group row text-center" style="padding:5px;">
                        <a href="{{ url('cms/post/viewUserPosts/'.$userDetails->id) }}">Posts</a>&nbsp;&nbsp;
                        <a href="{{ url('cms/post/viewUserCampaigns/'.$userDetails->id) }}">Campaigns</a>&nbsp;&nbsp;
                        <a href="{{ url('cms/post/viewUserPostComments/'.$userDetails->id) }}">Commented Posts</a>&nbsp;&nbsp;
                        <a href="{{url('cms/post/viewUserCampaignComments/'.$userDetails->id)}}">Commented Campaigns</a>
                    </div>
                    @endif
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Name</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->fullName) && !empty($userDetails->fullName)?$userDetails->fullName:'Not available'}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">User Name</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->username) && !empty($userDetails->username)?$userDetails->username:'Not available'}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Email</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->email) && !empty($userDetails->email)?$userDetails->email:'Not available'}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Phone No</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->phone) && !empty($userDetails->phone)?$userDetails->phone:'Not available'}}</div>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Description</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->description) && !empty($userDetails->description)?$userDetails->description:'Not available'}}</div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Status</div>
                        <div class="col-sm-6 col-xs-8 text-left" id="ableStatus">{{$userDetails->isDisabled ? 'Inactive' : 'Active'}}</div>
                    </div>
                        
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Last Seen</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{!is_null($userDetails->lastAccessDate)?date('M j Y ',strtotime($userDetails->lastAccessDate)):"Not available"}}</div>
                    </div>
                    @if(\Auth::user()->role == "admin")
                    <div class="form-group row text-right">
                        <a class="btn btn-default" id="enable{{$userDetails->id}}" data-id="{{$userDetails->id}}" href="#" data-toggle="modal" data-target="#myModalActivation" style="{{ ($userDetails->isDisabled) ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} padding:5px;">&nbsp;Enable User</a>
                        <a class="btn btn-default" id="disable{{$userDetails->id}}" data-id="{{$userDetails->id}}" href="#" data-toggle="modal" data-target="#myModal" style="{{ ($userDetails->isDisabled) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}} padding:5px;">Disable User</a>
                    </div>
                    @elseif(\Auth::user()->role == "group")
                    <div class="form-group row text-right">
                        <a class="btn btn-default" style="padding:5px;" href="{{url('cms/user/editProfile')}}">Edit Profile</a>
                    </div>
                    @endif
                </div>   
                    </div>
            </div>
        </div>



     

    </form>
</div>
<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Disable User</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Please enter reason to disable user">
                <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">
                
            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Activation</h4>
            </div>
            <div class="modal-body">
                <p>Do You Want To enable This User</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
   $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
    $(document).ready(function () {
        $('#userTab').addClass('active');
    });
$(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#disableReason').val();
            var id = $('#dialogClubId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/user/disable')}}",
                type: 'POST',
                data: $('#disableReasonForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#disable' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#enable' + id).css({'display': 'inline', 'visibility': 'visible'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                    $('#ableStatus').html('Inactive');
                }
            });
        });
    });


    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/user/enable')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
                $('#disable' + userId).css({'display': 'inline', 'visibility': 'visible'});
                $('#enable' + userId).css({'display': 'none', 'visibility': 'hidden'});
                $('#ableStatus').html('Active');
            }
        });
    }


</script>
    
@endsection




