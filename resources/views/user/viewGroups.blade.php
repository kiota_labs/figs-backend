@extends('app')

@section('content')

<div class="container-fluid">
     <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-group"></i>All Registered Groups</li>
					</ol>
				</div>
			</div>
    
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
    <div>
        <div class="col-md-12">
           
                    
                    <form class="form-horizontal" style="margin-bottom:9px;" method="POST" enctype="multipart/form-data" action="{{URL::to('cms/user/searchGroup')}}">
                        <input type="text" class="form-control col-xs-4" style="width:200px; position: relative;" placeholder="Text" name="userName" value="{{{ Input::old('userName', isset($searchCriteria) ? $searchCriteria['userName'] : null)}}}">

                        <button class="btn btn-default" style="position: relative; height: 34px;" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </form>

                    @if (count($users) > 0)
                    <table class="table table-bordered table-responsive" id="mytable">
                        <thead>
                            <tr>
                                <th class="text-center tdHeader"><span>Group Name</span></th>
                                <th class="text-center tdHeader"><span>Email</span></th>
                                <th class="text-center tdHeader mytable"><span>Signup Date</span></th>
                                <th class="text-center tdHeader mytable firstHide">Visibility</th>
                                <th class="text-center tdHeader mytable secondHide">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr style="{{ ($user->isVerified)  ? 'background-color:#D3D3D3;' : ''}}">
                                <td style="vertical-align: middle;">
                                    <a href="{{url('/cms/user/viewGroupDetails/'.$user->id)}}"><img class="img-circle img-responsive img-thumbnail imageSize" href="{{url('/cms/user/viewGroupDetails/'.$user->id)}}"src="{{{isset($user->image)&& !empty($user->image) ? asset('images/user/thumb/'.$user->image): asset(config('constants.groupThumbImage'))}}}"/></a>
                                 <a class="text-center" href="{{url('/cms/user/viewGroupDetails/'.$user->id)}}">{{$user->fullName}}</a></td>
                                <td class="text-center" style="vertical-align: middle;">{{ $user->email }}</td>
                                <td class="text-center mytable" style="vertical-align: middle;">{{ date('M j Y ',strtotime($user->createDate)) }}</td>
                                <td class="text-center mytable firstHide" style="vertical-align: middle;">
                                    @if($user->approvalStatus == 1)
                                    <a class="btn btn-success" id="act_{{$user->id}}" data-id="{{$user->id}}" href="#" data-toggle="modal" data-target="#myModalActivation" style="{{ ($user->isDisabled)  ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}}">Unhide</a>
                                    <a class="btn btn-danger" id="deact_{{$user->id}}" data-id="{{$user->id}}" href="#" data-toggle="modal" data-target="#myModal" style="{{ ($user->isDisabled) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}}">Hide</a>
                                    @else
                                    <a class="btn btn-default" style="background-color: #B5B5B5;" disabled="disabled" href="#" data-toggle="modal" >Hide</a>
                                    @endif
                                </td>
                                <td class="text-center mytable secondHide" style="vertical-align: middle;">
                                    @if($user->approvalStatus == 1)
                                    <a class="btn btn-default" disabled="disabled" href="#" data-toggle="modal" >Approved</a>
                                    @elseif($user->approvalStatus == 2)
                                    <a class="btn btn-default" disabled="disabled" href="#" data-toggle="modal" >Rejected</a>
                                    @else
                                    <a class="btn btn-default" disabled="disabled" href="#" data-toggle="modal" >Unapproved</a>
                                    @endif
                                </td>
                         
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                    <div class="pull-right">
                    <?php echo $users->appends(Request::input())->render(); ?>
                    </div>
                    @else
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> No records found for groups.<br><br>
                    </div>
                    @endif
         
    </div>
</div>
</div>

<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please enter reason to hide group</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                    <label style="font-size: 15px;">
                        You are about to disable this group,this will affect the visibility of the 
                        group in the App,Do you want to continue?
                    </label>
                <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Reason to Hide">
                <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">
                
            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Activation</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to unhide this group</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
$(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#disableReason').val();
            var id = $('#dialogClubId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/user/disable')}}",
                type: 'POST',
                data: $('#disableReasonForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#deact_' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#act_' + id).css({'display': 'inline', 'visibility': 'visible'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                }
            });
        });
    });


    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/user/enable')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
               $('#deact_' + userId).css({'display': 'inline', 'visibility': 'visible'});
                $('#act_' + userId).css({'display': 'none', 'visibility': 'hidden'});

            }
        });
    }
    $(document).ready(function ()
    {
        $("#mytable").tablesorter();
    }
    );

</script>

@endsection
