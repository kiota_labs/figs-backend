@extends('app')

@section('content')

<div class="container-fluid">
    <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-users"></i>Flagged Groups</li>
					</ol>
				</div>
			</div>
    
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
        <div class="col-sm-12">

            <div class="heading" style="font-size: 30px; margin: auto;">
                <b>Flagged Groups</b>
                </div>
            @if (count($flaggedUsers) > 0)   
            @foreach ($flaggedUsers as $flaggedUser)
            <div class="panel-group accordion myPanel" value="{{$flaggedUser->flaggedUserId}}" id="accordion{{$flaggedUser->flaggedUserId}}" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default accordion-group">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 id="flaggedTitle{{$flaggedUser->flaggedUserId}}" class="flaggedTitle panel-title" value="{{$flaggedUser->flaggedUserId}}" data-setValue="1" data-toggle="collapse" data-parent="#accordion{{$flaggedUser->flaggedUserId}}" data-target="#collapseOne{{$flaggedUser->flaggedUserId}}" aria-expanded="true" aria-controls="collapseOne">
                                @if(isset($flaggedUser->flaggedUserId) && !empty($flaggedUser->flaggedUserId))
                                <a value="{{$flaggedUser->flaggedUserId}}" id="accordionPlus{{$flaggedUser->flaggedUserId}}" class="accordionPlus pull-right" role="button" data-toggle="collapse" data-parent="#accordion{{$flaggedUser->flaggedUserId}}" href="#collapseOne{{$flaggedUser->flaggedUserId}}" aria-expanded="true" aria-controls="collapseOne"><span class="glyphicon glyphicon-plus" style="margin-top: 13px;"></span> </a>
                                <a value="{{$flaggedUser->flaggedUserId}}" id="accordionMinus{{$flaggedUser->flaggedUserId}}" class="accordionMinus pull-right hide" role="button" data-toggle="collapse" data-parent="#accordion{{$flaggedUser->flaggedUserId}}" href="#collapseOne{{$flaggedUser->flaggedUserId}}" aria-expanded="true" aria-controls="collapseOne"><span class="glyphicon glyphicon-minus" style="margin-top: 13px;"></span> </a>
                                @endif
                                <a href="{{url('/cms/user/viewFlaggedGroupDetails/'.$flaggedUser->flaggedUserId)}}">
                                    <img id="flaggedImage{{$flaggedUser->flaggedUserId}}" value="{{$flaggedUser->flaggedUserId}}" href="{{url('/cms/user/viewFlaggedGroupDetails/'.$flaggedUser->flaggedUserId)}}" class="img-circle img-responsive img-thumbnail leftalign imageSize flaggedImage" src="{{{isset($flaggedUser->image) && !empty($flaggedUser->image) ? asset('images/user/thumb/'.$flaggedUser->image) : asset(config('constants.groupThumbImage'))}}}"/>
                                </a>
                                <a style="{{ ($flaggedUser->isDisabled) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}} margin-right:10px;" data-toggle="tooltip" data-placement="right" data-original-title="Active"><span class="glyphicon glyphicon-ok-sign" style="color: #0E325A;"></span></a>
                                <a style="{{ ($flaggedUser->isDisabled) ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} margin-right:10px;" data-toggle="tooltip" data-placement="right" data-original-title="Inactive"><span class="glyphicon glyphicon-remove-sign" style="color: #ff0000;"></span></a>
                                <a id="flaggedName{{$flaggedUser->flaggedUserId}}" class="flaggedName accordionStyle" value="{{$flaggedUser->flaggedUserId}}" href="{{url('/cms/user/viewFlaggedGroupDetails/'.$flaggedUser->flaggedUserId)}}" style="color: #007aff;">
                                    
                                    {{isset($flaggedUser->fullName)&&!empty($flaggedUser->fullName)?$flaggedUser->fullName:$flaggedUser->username}}
                                  
                                </a>
                                @if(isset($flaggedUser->flaggedUserId) && !empty($flaggedUser->flaggedUserId))
                                <a data-toggle="tooltip" data-placement="right" data-original-title="Flagged"><span class="glyphicon glyphicon-flag" style="color: #58b6f4;"></span></a>
                                @endif   
                                

                            </h4>
                        </div>
                        <div id="collapseOne{{$flaggedUser->flaggedUserId}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                @if (count($flaggedUser->userDetails) > 0)
                                @foreach ($flaggedUser->userDetails  as $flaggedUserDetail)
                                <div style="padding: 10px 20px;">
                                    @if($flaggedUserDetail->flaggedByRole == 'user')
                                    <img class="img-circle img-responsive img-thumbnail pull-left" style="width:40px; height:40px; margin-right: 10px;" src="{{{isset($flaggedUserDetail->image) && !empty($flaggedUserDetail->image) ? asset('images/user/thumb/'.$flaggedUserDetail->image) : asset(config('constants.userThumbImage'))}}}"/>
                                    @else
                                    <img class="img-circle img-responsive img-thumbnail pull-left" style="width:40px; height:40px; margin-right: 10px;" src="{{{isset($flaggedUserDetail->image) && !empty($flaggedUserDetail->image) ? asset('images/user/thumb/'.$flaggedUserDetail->image) : asset(config('constants.groupThumbImage'))}}}"/>
                                    @endif
                                    <b>{{ isset($flaggedUserDetail->flaggedBy) && !empty($flaggedUserDetail->flaggedBy)?$flaggedUserDetail->flaggedBy:$flaggedUserDetail->flaggedByUserName }}</b>
                                    <small><i>({{ date('M j, Y ',strtotime($flaggedUserDetail->date)) }})</i></small>
                                    <br/>{{ $flaggedUserDetail->flagReason }}
                                </div>
                                @endforeach
                                @else
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> This User is not flagged by anyone.<br><br>
                                </div>
                                @endif           
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="pull-right">
            <?php echo $flaggedUsers->appends(Request::input())->render(); ?>
            </div>

            @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> No records found for Flagged Users.<br><br>
            </div>
            @endif

        </div>
    </div>
</div>

    <div id="myModal" class="modal fade in" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Please Enter Reason To Disable</h4>
                </div>
                <div class="modal-body">
                    <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                        <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                        <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Reason to Disable">
                        <input type="hidden" value="" name="userId" id="dialogClubId">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">

                </div>
            </div>

        </div>
    </div>
    <div id="myModalActivation" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Activation</h4>
                </div>
                <div class="modal-body">
                    <p>Do You Want To enable This User</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                    <input type="hidden" value="" name="userId" id="dialogActivateUserId">
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script>
        $('#myModal').on('show.bs.modal', function (e) {
            //alert(e.relatedTarget.dataset.id);
            $('#dialogClubId').val(e.relatedTarget.dataset.id);
        });

        $('#myModalActivation').on('show.bs.modal', function (e) {
            //alert(e.relatedTarget.dataset.id);
            $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
        });
        function delText() {
            $('.myForm').trigger('reset');
            $('.myLabel').html('');
        }
        
         
             
            $(".accordionPlus").click(function () {
                var t = $(this).attr('value');
                $("#accordionPlus"+t).addClass('hide');
                $("#accordionMinus"+t).removeClass('hide');
            });
       
        
      
          
            $(".accordionMinus").click(function () {
                var t = $(this).attr('value');
                $("#accordionMinus"+t).addClass('hide');
                $("#accordionPlus"+t).removeClass('hide');
            });
       
        
        $(document).ready(function () {
            $("#disableSubmit").click(function () {
                var data = $('#disableReason').val();
                var id = $('#dialogClubId').val();
                var searchReg = /^[ Xa-zA-Z0-9-]+$/;
                if (data == '')
                {
                    $("#modalLabel").html('* please fill reason to disable');
                    return false;
                } else if (!searchReg.test(data)) {
                    $("#modalLabel").html("* please enter valid Reason");
                    return false;
                }
                $.ajax({
                    url: "{{asset('cms/user/disable')}}",
                    type: 'POST',
                    data: $('#disableReasonForm').serialize(),
                    success: function (response) {
                        $('#myModal').modal('hide');
                        $('#deact_' + id).css({'display': 'none', 'visibility': 'hidden'});
                        $('#act_' + id).css({'display': 'inline', 'visibility': 'visible'});
                        $('.myLabel').html('');
                        $('.myForm').trigger('reset');
                    }
                });
            });
        });
        
        $('.accordion').on('show.bs.collapse', function () {
         var id = $(this).attr('value');
         $("#accordionPlus" + id).addClass('hide');
         $("#accordionMinus" + id).removeClass('hide');
    });
    
    $('.accordion').on('hide.bs.collapse', function () {
         var id = $(this).attr('value');
         $("#accordionPlus" + id).removeClass('hide');
         $("#accordionMinus" + id).addClass('hide');
    });


        function Activate() {

            var userId = document.getElementById('dialogActivateUserId').value;

            $.ajax({
                type: "post",
                url: "{{asset('cms/user/enable')}}",
                data: {'userId': userId},
                cache: false,
                success: function (result) {
                    $('#myModalActivation').modal('hide');
                    $('#deact_' + userId).css({'display': 'inline', 'visibility': 'visible'});
                    $('#act_' + userId).css({'display': 'none', 'visibility': 'hidden'});

                }
            });
        }
        $(".flaggedImage").click(function () {

        var id = $(this).attr('value');
        $('#flaggedTitle' + id).removeAttr('href');
        $('#flaggedTitle' + id).removeAttr('data-toggle');
        document.location.href = $(this).attr('href');
    });

    $(".flaggedName").click(function () {
        var id = $(this).attr('value');
        $('#flaggedTitle' + id).removeAttr('href');
        $('#flaggedTitle' + id).removeAttr('data-toggle');
        document.location.href = $(this).attr('href');
    });
        $(document).ready(function(){
            $("#type").val($("#roleHide").val());
            $("#flag").val($("#flagHide").val());
        });
        
        $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        });
        
    </script>

    @endsection
