@extends('app')

@section('content')
<div class="col-md-10">
<div class="panel panel-default">
    <div class="panel-heading">
        <b>User Documents</b>
    </div>
    
    <div class="panel-body">
        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('cms/user/registrationComplete') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group row" >
            <input type="hidden" value="{{$userId}}" id="userId"/>
            <div class="form-inline">
                <div id="fileuploader">Select</div>

                <div id="startUpload" style="margin-top: 5px;">
                    <input type="button" class="btn btn-primary" value="Upload" style="margin-left: 10px;"/>
                </div>
            </div>

        </div> 
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Done
                </button>
            </div>
        </div>
        </form>

    </div>
</div>
</div>

<script>

    var id = $("#userId").val();
    var uploadObj = $("#fileuploader").uploadFile({
        autoSubmit: false,
        url: "{{asset('user/uploadMultipleDocuments')}}",
        fileName: "myfile",
        formData: {"id": id},
        afterUploadAll: function ()
        {
            location.reload();
        }
    });

    $("#startUpload").click(function ()
    {
        uploadObj.startUpload();
    });

</script>
@endsection