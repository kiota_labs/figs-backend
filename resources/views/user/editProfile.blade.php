@extends('app')

@section('content')
<div class="container-fluid">
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb" style="height:auto;">
                <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                <li><i class="fa fa-home"></i><a href="{{ url('cms/user/groupProfile/'.Auth::user()->id) }}">My Profile</a></li>
                <li><i class="fa fa-user"></i>Edit Profile</li>
            </ol>
        </div>
    </div>
    @extends('templates.sidebar')
    @section('sidebar')
    @endsection
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Profile</div>
            <div class="panel-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (Session::has('flash_message'))  
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close">
                        ×
                    </button>
                    <strong>Success!</strong> {{ Session::get('flash_message') }}
                </div>
                @endif 

                <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/cms/user/editProfile') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="userId" value="{{{ Input::old('userId', isset($userDetails) ? $userDetails->id : null)}}}">

                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" required="required" value="{{{ Input::old('email', isset($userDetails) ? $userDetails->email : null)}}}" style="{{($errors->has('email'))?'border:1px solid red;':null}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Group Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" required="required" name="groupName" value="{{{ Input::old('groupName', isset($userDetails) ? $userDetails->fullName : null)}}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Description</label>
                        <div class="col-md-6"><textarea id="info" required="required" name="description" rows="5" cols="52" class="form-control">{{{ Input::old('description', isset($userDetails) ? $userDetails->description : null)}}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Contact Person</label>
                        <div class="col-md-6">
                            <input type="text" required="required" class="form-control" name="contactPerson" value="{{{ Input::old('contactPerson', isset($userDetails) ? $userDetails->contactPerson : null)}}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone Number</label>
                        <div class="col-md-6">
                            <input type="text" required="required" maxlength="15" class="form-control" id="phone" name="phone" value="{{{ Input::old('phone', isset($userDetails) ? $userDetails->phone : null)}}}">
                            <span id="spnPhoneStatus"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Image</label>
                        <div class="col-md-6">
                            <input type="file" class="form-control" onchange="readURL(this, 'image');" name="image" value="{{{ Input::old('image', isset($userDetails) ? asset('images/user/thumb/'.$userDetails->image) : null)}}}">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-6">
                            <img class="img-circle img-responsive img-thumbnail leftalign" id="image" style="height:100px; width:100px;" src="{{isset($userDetails->image) && !empty($userDetails->image)? asset('images/user/thumb/'.$userDetails->image) : asset(config('constants.userDefaultImage'))}}"/>  
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
    function readURL(input, data) {
        var imageId = '#' + data;
        if (input.files && input.files[0]) {
            var reader = new FileReader();


            reader.onload = function (showImage) {
                $(imageId)
                        .attr('src', showImage.target.result)
                        .width(100)
                        .height(100);
            };

            reader.readAsDataURL(input.files[0]);
        }

    }

    $(document).ready(function ()
    {
        $(".myTabClass").hide();
    }
    );

    function validatePhone(txtPhone) {
        var a = document.getElementById(txtPhone).value;
        var filter = /^[0-9-+]+$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }
    $(document).ready(function () {
        $('#phone').blur(function (e) {
            if (validatePhone('phone')) {
                $('#spnPhoneStatus').html('Valid');
                $('#spnPhoneStatus').css('color', 'green');
            }
            else {
                $('#spnPhoneStatus').html('Invalid');
                $('#spnPhoneStatus').css('color', 'red');
            }
        });
    });


</script>
@endsection
