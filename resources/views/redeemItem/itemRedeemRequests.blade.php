@extends('app')

@section('content')

<div class="container-fluid">
     <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-group"></i>All Redeem Requests</li>
					</ol>
				</div>
			</div>
    @extends('templates.sidebar')
        @section('sidebar')
        @endsection
    <div>
        <div class="col-md-12">

            @if (count($requests) > 0)
            <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
                <thead>
                    <tr>
                        <th class="text-center tdHeader"><span>Requested By</span></th>
                        <th class="text-center tdHeader"><span>For Campaign</span></th>
                        <th class="text-center tdHeader"><span>Items</span></th>
                        <th class="text-center tdHeader">Accept</th>
                        <th class="text-center tdHeader">Reject</th>
                        <th class="text-center tdHeader mytable">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($requests as $request)
                    
                    <tr>
                        <td style="vertical-align: middle;">
                            <a href="{{url('/cms/user/userDetails/'.$request->userId)}}">
                                @if($request->role == "group")
                                <img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($request->userImage) ? asset('images/user/thumb/'.$request->userImage): asset(config('constants.groupThumbImage'))}}}"/></a>
                                @else
                                <img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($request->userImage) ? asset('images/user/thumb/'.$request->userImage): asset(config('constants.userThumbImage'))}}}"/></a>
                                @endif
                            <a href="{{url('/cms/user/userDetails/'.$request->userId)}}">{{ isset($request->fullName) && !empty($request->fullName) ? $request->fullName : $request->username }}</a></td>
                        <td style="vertical-align: middle;">
                            <a href="{{url('/cms/post/viewDetailsPost/'.$request->postId)}}">
                                <img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($request->postImage) ? asset('images/post/thumb/'.$request->postImage): asset(config('constants.postDefaultImage'))}}}"/></a>
                            <a href="{{url('/cms/post/viewDetailsPost/'.$request->postId)}}">{{ $request->title }}</a></td>
                        <td class="text-center" style="vertical-align: middle;">
                            <a href="{{url('/cms/post/redeemItemDetails/'.$request->postId)}}">View Items</a></td> 
                        @if($request->redeemStatus == 1)
                        <td class="text-center" style="vertical-align: middle;">
                         <a class="btn btn-success" id="act_{{$request->id}}" data-id="{{$request->id}}" href="#" data-toggle="modal" data-target="#myModalActivation">Accept</a>
                         
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                        <a class="btn btn-danger" id="deact_{{$request->id}}" data-id="{{$request->id}}" href="#" data-toggle="modal" data-target="#myModal">Reject</a>   
                        </td>
                        @elseif($request->redeemStatus == 2)
                        <td class="text-center" style="vertical-align: middle;">
                         <a class="btn btn-success" disabled="disabled" id="act_{{$request->id}}" data-id="{{$request->id}}" href="#" data-toggle="modal" data-target="#myModalActivation">Accept</a>
                         
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                        <a class="btn btn-danger" disabled="disabled" id="deact_{{$request->id}}" data-id="{{$request->id}}" href="#" data-toggle="modal" data-target="#myModal">Reject</a>   
                        </td>
                        @elseif($request->redeemStatus == 3)
                        <td class="text-center" style="vertical-align: middle;">
                         <a class="btn btn-success" id="act_{{$request->id}}" data-id="{{$request->id}}" href="#" data-toggle="modal" data-target="#myModalActivation">Accept</a>
                         
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                        <a class="btn btn-danger" disabled="disabled" id="deact_{{$request->id}}" data-id="{{$request->id}}" href="#" data-toggle="modal" data-target="#myModal">Reject</a>   
                        </td>
                        @endif
                        <td class="text-center mytable" style="vertical-align: middle;">
                            @if($request->redeemStatus == 1)
                             <a class="btn btn-default btn-sm" disabled="disabled" href="#" id="pendingButton{{$request->id}}">Pending</a>
                            @elseif($request->redeemStatus == 2)
                            <a class="btn btn-default btn-sm" disabled="disabled" href="#" id="acceptedButton{{$request->id}}">Accepted</a>
                            @elseif($request->redeemStatus == 3)
                            <a class="btn btn-default btn-sm" disabled="disabled" href="#" id="rejectedButton{{$request->id}}">Rejected</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
            <div class="pull-right">
            <?php echo $requests->appends(Request::input())->render(); ?>
            </div>
            @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> No records found for Redeem Requests.<br><br>
            </div>
            @endif
        </div>
    </div>
</div>

<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reject redeem request</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Please enter reason to reject redeem request">
                <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Cancel</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">
                
            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Request</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to accept this redeem request</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Yes</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
   $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
$(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#disableReason').val();
            var id = $('#dialogClubId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/item/rejectRedeemRequest')}}",
                type: 'POST',
                data: $('#disableReasonForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#deact_' + id).attr('disabled','disabled');
                    $('#pendingButton' + id).replaceWith("<a class='btn btn-default btn-sm' disabled='disabled' href='#' id=rejectedButton"+id+">Rejected</a>");
                    $('#acceptedButton' + id).replaceWith("<a class='btn btn-default btn-sm' disabled='disabled' href='#' id=rejectedButton"+id+">Rejected</a>");
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                }
            });
        });
    });


    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/item/acceptRedeemRequest')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
                $('#deact_' + userId).attr('disabled','disabled');
                $('#act_' + userId).attr('disabled','disabled');
                $('#pendingButton' + userId).replaceWith("<a class='btn btn-default btn-sm' disabled='disabled' href='#' id=acceptedButton"+userId+">Accepted</a>");
                $('#rejectedButton' + userId).replaceWith("<a class='btn btn-default btn-sm' disabled='disabled' href='#' id=acceptedButton"+userId+">Accepted</a>");

            }
        });
    }
    $(document).ready(function ()
    {
        $("#mytable").tablesorter();

    }
    );


</script>

@endsection
