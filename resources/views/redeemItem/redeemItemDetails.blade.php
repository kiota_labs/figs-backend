@extends('app')

@section('content')


<div class="container-fluid">
     <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-group"></i><a href="{{ url('cms/user/itemRedeemRequests') }}">All Redeem Requests</a></li>
                                                <li><i class="fa fa-home"></i>Item Details</li>
					</ol>
				</div>
			</div>
    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
          accesskey=""   accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
        <!--<div class="col-sm-3">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#">Actions</a></li>
                <li><a href="{{ url('cms/campaign/index') }}">View Campaigns</a></li> 
            </ul>
        </div> -->
        <div class="col-sm-12">
            @if (count($itemDetails) > 0)
                 @foreach ($itemDetails as $itemDetail)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Item Details</b>
                </div>
                 
                 
                <div class="panel-body">
                    
                    <div class="col-md-4 text-center">
                        <img class="img-circle img-responsive img-thumbnail" src="{{{isset($itemDetail->imageUrl) ? $itemDetail->imageUrl : null}}}"/>
                    </div>
                    
                    
                    <div class="col-md-8">
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Name</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$itemDetail->itemName}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Description</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$itemDetail->itemDescription}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Price</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$itemDetail->price}}</div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Quantity</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$itemDetail->quantity}}</div>
                    </div>
                   
                </div>
                   </div>
                
            </div>
                  @endforeach
                  @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> No records found for Item Details.<br><br>
            </div>
                 @endif
        </div>



     

    </form>
</div>

    
@endsection




