<!DOCTYPE html>
<html >
    <head>
		<meta charset="UTF-8">
        <title>My Profile</title>
            
          <link rel="stylesheet" type="text/css" href="{{asset('/css/font-awesome.min.css') }}">
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
			
			<link rel="stylesheet" type="text/css" href="{{asset('/css/bootstrap-theme.css')}}">
        
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
			
			<link rel="stylesheet" type="text/css" href="{{asset('/css/home.css') }}">
			<link rel="stylesheet" type="text/css" href="{{asset('/css/newCss.css') }}">
			<link rel="stylesheet" type="text/css" href="{{asset('/css/myProfileCss.css') }}">
			
			<script src="{{ asset('js/api_call.js') }}"></script>
			
          <!-- Script for Service Locator List -->
    <script>
      window.console = window.console || function(t) {};
	  
    </script>
    <script src="http://listjs.com/no-cdn/list.js"></script>
    <script src="//assets.codepen.io/assets/common/stopExecutionOnTimeout-f961f59a28ef4fd551736b43f94620b5.js"></script>
     
    <style>
        body{
        overflow-y:auto;
            overflow-x: hidden;
        }
    </style>

	<style>
		@media screen and (min-width: 0px) and (max-width: 768px) {
             .mainMenu{display:none;}
             .contentArea{width:100%; height:100%; float:left; margin-left:1%}
             #mobileMenu{display:block; width:100%; height:180px; background-color:#2bb373;}
             h4{font-family: 'sansProLight', sans-serif; letter-spacing: .15em; font-size: 0.8em;}
             h5{font-family: 'sansProLight', sans-serif; letter-spacing: .15em; font-size: 0.8em;}
             #aboutUsfooter div { font-size:10px }
             #addressList li {width: 200px;}
             #aboutUsfooter { position: relative;}
             #mobileChangeFooter{display:block}
            .profileNavMenuItems{font-size:12px}
		}


         @media screen and (min-width: 768.01px) and (max-width:20000px ) {
            .widthSetter{width:40%;margin-left:-250px}
            .contentArea{width:78%; height:100%; float:left; margin-left:300px}
            #mobileMenu{display:none;}
            #addressList li {width: 390px;}
             #aboutUsfooter {
                 position: absolute; }
             #mobileChangeFooter{display:none;height: 0px}
             .profileNavMenuItems{font-size:20px}
            }
        
    </style>
</head>


<body>          
    <nav class="mainMenu" >
		<center> 
			<ul style="padding-top:50px" >
				<li id="profileMenu" style="height:120px; width:80px;" >
					<div >
						<img id= "stickyBarImage" src="{{ asset('/img/userProfile.png')}}" style="height:80px; width:80px;border-radius: 40px;"/>
						<script>
						if(!(localStorage.getItem('FbImageForFig') === null || localStorage.getItem('FbImageForFig') == "")){
								document.getElementById("stickyBarImage").src = localStorage.getItem('FbImageForFig');
								//document.getElementById("stickyBarLogo").style.style.borderRadius = "50px";
						}
					  </script>
					</div>
				</li>

				<li class="has-subnav" >
					<p style="text-transform:uppercase;" id="userName">Name</p>
          <p  id="userEmail" style="font-size: 12px;">Email</p>
          <p  id="userPhoneNo">Mobile</p>
				</li>

				<!-- <li class="has-subnav">
					<p  id="userEmail"> Email:</p>
				</li>

			   <li class="has-subnav" id="shareMenu">
					<p  id="userPhoneNo">Mobile:</p>
				</li> -->

			</ul>
	</center>

	<center>
			<ul class="logout">
				
                <li id="changePasswordButton">
				   
						<span class="nav-text leftMenuChangeButtonText">
							CHANGE PASSWORD
						</span>
					
				</li>
                
                <li id="changeContactNumberButton" >
						<span class="nav-text leftMenuChangeButtonText" >
							CHANGE CONTACT NUMBER
						</span>
					
				</li>

                <li>
				   <a href="home">
						<span class="nav-text">
							BACK TO MENU
						</span>
					</a>
				</li>
                
                
				<li>
				   <a href="home">
						<span class="nav-text" onclick="logout()">
							LOGOUT
						</span>
					</a>
				</li>  
			</ul>
	</center>

</nav>

            <script>
                document.getElementById("userName").innerHTML=localStorage.getItem('userName');
                document.getElementById("userEmail").innerHTML=localStorage.getItem('emailId');
                document.getElementById("userPhoneNo").innerHTML=localStorage.getItem('phoneNo');
            </script>


        <div id="mobileMenu">

                <a href="home" style="float:left; color:white; padding-left:10px; padding-top:15px">MENU</a>
                <a style="float:right; color:white; padding-right:10px; padding-top:15px" onclick="logout()">LOGOUT</a>
                <br>

                <center>
                    <img id="stickyBarLogo" src="{{ asset('/img/userProfile.png')}}" height="60" width="60"  style="border-radius: 50px;">
					<script>
						if(!(localStorage.getItem('FbImageForFig') === null || localStorage.getItem('FbImageForFig') == "")){
								document.getElementById("stickyBarLogo").src = localStorage.getItem('FbImageForFig');
								//document.getElementById("stickyBarLogo").style.style.borderRadius = "50px";
						}
					  </script>
                    <div style="padding:10px;" id="mobileMenuContent">
                         <p class="mobileMenuText" style="text-transform:uppercase;" id="userNameMobile">Name:</p>
                         <p  class="mobileMenuText"   id="userEmailMobile">Email:</p>
                         <p  class="mobileMenuText"  id="userPhoneNoMobile">Mobile:</p>
                    </div>
                </center>
            
        </div>
	<script>
		document.getElementById("userNameMobile").innerHTML=localStorage.getItem('userName');
		document.getElementById("userEmailMobile").innerHTML=localStorage.getItem('emailId');
		document.getElementById("userPhoneNoMobile").innerHTML=localStorage.getItem('phoneNo');
	</script>

        
    <div  class="contentArea" >
			<div class="row">

				<div class="col-xs-12" style="width:100%; " >
				</div>
				
				<div class="col-xs-12" style="text-align:center; margin-top:30px">
					<button class="col-xs-4 profileNavMenuItems" id="addressMenu" ><center>ADDRESS BOOK</center></button>
					<button class="col-xs-3 profileNavMenuItems"></button>
					<button class="col-xs-4 profileNavMenuItems" id="orderMenu"><center>ORDER HISTORY</center></button>
				</div>
				 <div class="col-xs-12" style="margin-top:50px"> 

            <div class="col-xs-12" id="addressContent" >
              <div class="row">
                <div class="col-xs-4"><h3 id="strAddressSavedAs-1"><i class="fa fa-home"></i> HOME</h3></div>
                <div class="col-xs-8">
                  <span class="fa-stack fa-lg" id="homeButton">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-4"><h3 id="strAddressSavedAs-2"><i class="fa fa-building"></i> WORK</h3></div>
                <div class="col-xs-8">
                  <span class="fa-stack fa-lg" id="workButton">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-4"><h3 id="strAddressSavedAs-3"><i class="fa fa-building-o"></i> Another Address</h3></div>
                <div class="col-xs-8">
                  <span class="fa-stack fa-lg" id="anotherButton1">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-4"><h3 id="strAddressSavedAs-4"><i class="fa fa-building-o"></i> Another Address</h3></div>
                <div class="col-xs-8">
                  <span class="fa-stack fa-lg" id="anotherButton2">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-4"><h3 id="strAddressSavedAs-5"><i class="fa fa-building-o"></i> Another Address</h3></div>
                <div class="col-xs-8">
                  <span class="fa-stack fa-lg" id="anotherButton3">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
              </div>
              
                <!-- <ol id="addressList" style="overflow-y:scroll">

                    <li>
                      <span class="fa-stack fa-lg pull-right" id="homeButton">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse"></i>
                      </span>

                      <h3 id="strAddressSavedAs-1">HOME</h3>
                    </li>
					
                    <li>
                      <i class="fa fa-pencil-square-o" style="float:right; font-size:15px" id="workButton"></i>
                      <h3 id="strAddressSavedAs-2">WORK</h3>
                    </li>

                    <li>
                      <i class="fa fa-pencil-square-o" style="float:right; font-size:15px" id="anotherButton1"></i>
                      <h3 id="strAddressSavedAs-3">Another Address</h3>
                    </li>
                    
                     <li>
                      <i class="fa fa-pencil-square-o" style="float:right; font-size:15px" id="anotherButton2"></i>
                      <h3 id="strAddressSavedAs-4">Another Address</h3>
                    </li>
                    
                     <li>
                      <i class="fa fa-pencil-square-o" style="float:right; font-size:15px" id="anotherButton3"></i>
                      <h3 id="strAddressSavedAs-5">Another Address</h3>
                    </li>
					
                </ol> -->
            </div> 
            <div class="col-xs-10" id="orderContent">
                <ul id="orderList" style="overflow:scroll;margin-left: -40px;">
                  <li><p>No orders yet.</p></li>
                </ul>
            </div> 
        </div>
			</div> 
		</div>

    <!-- Home Modal Start -->
    <div class="modal fade" id="homeModal" role="dialog"  >
      <div class="modal-dialog widthSetter" style="height:300px; margin-top:5%; " >
        <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
          <div class="modal-header" style="padding:35px 50px; height:80px;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4>HOME</h4>
          </div>
          <div class="modal-body" >
            <form>
              <div class="form-group">
                <input type="text" class="form-control" id="street-1" placeholder="STREET ADDRESS">
              </div>

              <div class="form-group" id="AddLocalityButton-1">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                  <input type="button" class="form-control" id="LocalityValue-1"  value="Select your locality">
                  <div class="input-group-addon"><i class="fa fa-caret-square-o-down"></i></div>
                </div>
              </div>
              
              <div class="form-group">
                <input type="text" class="form-control" id="landmark-1" placeholder="LANDMARK">
              </div>

              <div class="form-group text-right">
                <button class="btn editRemoveButtons removeButton-1"  style="color:#ffffff;display:none;" id = "removeAddress-1" ><i class="fa fa-trash-o smallIcon"></i>Remove</button>
                <button class="btn editRemoveButtons editButton" onclick="updateAddress(1)" style="color:#ffffff"><i class="fa fa-floppy-o smallIcon"></i>Save</button>
              </div>

              <div id="addressId-1" style="display:none;"></div>
            </form> 
          </div>
        </div>
      </div>
    </div>
    <!-- Home Modal End   -->
          
    <!-- Work Modal Start -->
    <div class="modal fade" id="workModal" role="dialog"  >
      <div class="modal-dialog widthSetter" style="height:300px; margin-top:5%; " >
        <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
          <div class="modal-header" style="padding:35px 50px; height:80px;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4>WORK</h4>
          </div>
          <div class="modal-body" >
            <form>
              <div class="form-group">
                <input type="text" class="form-control" id="street-2" placeholder="STREET ADDRESS">
              </div>

              <div class="form-group" id="AddLocalityButton-2">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                  <input type="button" class="form-control" id="LocalityValue-2"  value="Select your locality">
                  <div class="input-group-addon"><i class="fa fa-caret-square-o-down"></i></div>
                </div>
              </div>
              
              <div class="form-group">
                <input type="text" class="form-control" id="landmark-2" placeholder="LANDMARK">
              </div>

              <div class="form-group text-right">
                <button class="btn editRemoveButtons removeButton-2"  style="color:#ffffff;display:none;" id = "removeAddress-2" ><i class="fa fa-trash-o smallIcon"></i>Remove</button>
                <button class="btn editRemoveButtons editButton" onclick="updateAddress(2)" style="color:#ffffff"><i class="fa fa-floppy-o smallIcon"></i>Save</button>
              </div>
              
              <div id="addressId-2" style="display:none;"></div>
            </form>
  				</div>		
        </div>  
      </div>
    </div>
    <!-- Work Modal End -->
          
    <!-- Another 1 Modal Start -->
    <div class="modal fade " id="anotherButton1Modal" role="dialog"  >
      <div class="modal-dialog widthSetter" style="height:300px; margin-top:5%; " >
        <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
          <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body" >
            <form>
              <div class="form-group">
                <input type="text" class="form-control" id="addressSavedAs-3" placeholder="SAVE AS">
              </div>

              <div class="form-group">
                <input type="text" class="form-control" id="street-3" placeholder="STREET ADDRESS">
              </div>

              <div class="form-group" id="AddLocalityButton-3">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                  <input type="button" class="form-control" id="LocalityValue-3"  value="Select your locality">
                  <div class="input-group-addon"><i class="fa fa-caret-square-o-down"></i></div>
                </div>
              </div>
              
              <div class="form-group">
                <input type="text" class="form-control" id="landmark-3" placeholder="LANDMARK">
              </div>

              <div class="form-group text-right">
                <button class="btn editRemoveButtons removeButton-3"  style="color:#ffffff;display:none;" id = "removeAddress-3" ><i class="fa fa-trash-o smallIcon"></i>Remove</button>
                <button class="btn editRemoveButtons editButton" onclick="updateAddress(3)" style="color:#ffffff"><i class="fa fa-floppy-o smallIcon"></i>Save</button>
              </div>

              <div id="addressId-3" style="display:none;"></div>
            </form>
          </div>  
        </div>
      </div>
    </div>
    <!-- Another 1 Modal End -->
          
    <!-- Another 2 Modal Start -->
    <div class="modal fade " id="anotherButton2Modal" role="dialog"  >
      <div class="modal-dialog widthSetter" style="height:300px; margin-top:5%; " >
        <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
          <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body" >
            <form>
              <div class="form-group">
                <input type="text" class="form-control" id="addressSavedAs-4" placeholder="SAVE AS">
              </div>

              <div class="form-group">
                <input type="text" class="form-control" id="street-4" placeholder="STREET ADDRESS">
              </div>

              <div class="form-group" id="AddLocalityButton-4">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                  <input type="button" class="form-control" id="LocalityValue-4"  value="Select your locality">
                  <div class="input-group-addon"><i class="fa fa-caret-square-o-down"></i></div>
                </div>
              </div>
              
              <div class="form-group">
                <input type="text" class="form-control" id="landmark-4" placeholder="LANDMARK">
              </div>

              <div class="form-group text-right">
                <button class="btn editRemoveButtons removeButton-4"  style="color:#ffffff;display:none;" id = "removeAddress-4" ><i class="fa fa-trash-o smallIcon"></i>Remove</button>
                <button class="btn editRemoveButtons editButton" onclick="updateAddress(4)" style="color:#ffffff"><i class="fa fa-floppy-o smallIcon"></i>Save</button>
              </div>

              <div id="addressId-4" style="display:none;"></div>
            </form>
          </div>  
        </div>
      </div>
    </div>
    <!-- Another 2 Modal End -->
          
    <!-- Another 3 Modal Start -->
    <div class="modal fade " id="anotherButton3Modal" role="dialog"  >
      <div class="modal-dialog widthSetter" style="height:300px; margin-top:5%; " >
        <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
          <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body" >
            <form>
              <div class="form-group">
                <input type="text" class="form-control" id="addressSavedAs-5" placeholder="SAVE AS">
              </div>

              <div class="form-group">
                <input type="text" class="form-control" id="street-5" placeholder="STREET ADDRESS">
              </div>

              <div class="form-group" id="AddLocalityButton-5">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                  <input type="button" class="form-control" id="LocalityValue-5"  value="Select your locality">
                  <div class="input-group-addon"><i class="fa fa-caret-square-o-down"></i></div>
                </div>
              </div>
              
              <div class="form-group">
                <input type="text" class="form-control" id="landmark-5" placeholder="LANDMARK">
              </div>

              <div class="form-group text-right">
                <button class="btn editRemoveButtons removeButton-5"  style="color:#ffffff;display:none;" id = "removeAddress-5" ><i class="fa fa-trash-o smallIcon"></i>Remove</button>
                <button class="btn editRemoveButtons editButton" onclick="updateAddress(5)" style="color:#ffffff"><i class="fa fa-floppy-o smallIcon"></i>Save</button>
              </div>

              <div id="addressId-5" style="display:none;"></div>
            </form>
          </div>  
        </div>
      </div>
    </div>
    <!-- Another 3 Modal End -->
          
    
    <!--     Change Password Modal Start -->
       <div class="container">

      <!-- Modal -->
      <div class="modal fade " id="changePasswordModal" role="dialog"  >
        <div class="modal-dialog widthSetter" style="height:300px; margin-top:5%; " >
          <!-- Modal content-->
          <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
            <div class="modal-header" style="padding:30px; ">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4>CHANGE PASSWORD</h4>
            </div>

            <div class="modal-body" >
              <form role="form" action="javascript:changePassword()">

                <div class="form-group">
                  <input type="password" class="form-control" id="oldPsw" placeholder="OLD PASSWORD" required>
                </div>

                <div class="form-group">
                  <input type="password" class="form-control" id="newPsw" placeholder="NEW PASSWORD" required>
                </div>

               <div class="form-group">
                  <input type="password" class="form-control" id="reNewPsw" placeholder="RETYPE NEW PASSWORD" required>
                </div>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal">CHANGE PASSWORD</button>

                </form>
            </div>  

            </div>

          </div>

        </div>
      </div> 
    <!--     Change Password Modal End   -->


    <!--     Change Contact Number Modal Start -->
       <div class="container">

      <!-- Modal -->
      <div class="modal fade " id="changeContactNumberModal" role="dialog"  >
        <div class="modal-dialog widthSetter" style="height:200px; margin-top:5%;" >
          <!-- Modal content-->
          <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
            <div class="modal-header" style="padding:30px;">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4>CONTACT NUMBER</h4>
            </div>

            <div class="modal-body" >
              <form role="form">

                <div class="form-group"><center>
                  <input type="text" class="form-control" id="phoneAreaCodeToSave" value="+91" style="display:inline; width:50px" maxlength="3">
                  <input type="text" class="form-control" id="phoneNumberToSave" placeholder="Contact Number" style="display:inline; width:150px" maxlength="10"></center>
                </div>
                <div class="form-group" style="text-align: center;">
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" onclick="changeContacNo()">CHANGE CONTACT NUMBER</button>
                </div>
                </form>
            </div>  

            </div>

          </div>

        </div>
      </div> 
    <!--     Change Phone Number Modal End   -->

          
    <!--     Add Address Modal Start -->
       <div class="container">

      <!-- Modal -->
      <div class="modal fade " id="addAddressModal" role="dialog"  >
        <div class="modal-dialog widthSetter" style="height:300px; margin-top:5%; " >
          <!-- Modal content-->
          <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
				<div class="modal-header" style="padding:35px 50px; height:80px;">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 id="addNewAddressHeader">ADD NEW ADDRESS</h4>
				</div>

				<div class="modal-body" >
					<div class="form-group">
					  <input type="text" class="form-control" id="streetAddress" placeholder="STREET ADDRESS">
					</div>

					<div class="form-group">
					  <input type="text" class="form-control" id="locality" placeholder="LOCALITY">
					</div>

				   <div class="form-group">
					  <input type="text" class="form-control" id="landmark" placeholder="LANDMARK">
					</div>
					  <button type="submit" class="signInsignUpButton" data-dismiss="modal" id="addAddressButton" onclick="addAddress()">ADD ADDRESS</button>
				</div>  
				
            </div>

          </div>

        </div>
      </div> 
    <!--     Add Address Modal End   -->

          
    <!--     Default Modal Start -->
       <div class="container">

      <!-- Modal -->
      <div class="modal fade " id="defaultModal" role="dialog"  >
        <div class="modal-dialog widthSetter" style="height:200px; margin-top:5%; " >
          <!-- Modal content-->
          <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
            <div class="modal-header" style="padding:35px 50px; height:80px;">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4>Message Title</h4>
            </div>

            <div class="modal-body" >
              <form role="form">
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" style="width:20%">OK</button>

                </form>
            </div>  

            </div>

          </div>

        </div>
      </div> 
    <!--     Default Modal End   -->

          
    <!--     Remove Address Modal Start -->
       <div class="container">

      <!-- Modal -->
      <div class="modal fade " id="removeAddressModal-1" role="dialog"  >
        <div class="modal-dialog widthSetter" style="height:80px; margin-top:5%; " >
          <!-- Modal content-->
          <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
            <div class="modal-body" >
                  <center><p id="errorText">Do you really want to remove the Address from your list?</p></center>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" onclick="removeAddress(1)" style="width:20%">YES</button>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" style="width:20%">NO</button>
            </div></div></div></div>

      <div class="modal fade " id="removeAddressModal-2" role="dialog"  >
        <div class="modal-dialog widthSetter" style="height:80px; margin-top:5%; " >
          <!-- Modal content-->
          <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
            <div class="modal-body" >
                  <center><p id="errorText">Do you really want to remove the Address from your list?</p></center>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" onclick="removeAddress(2)" style="width:20%">YES</button>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" style="width:20%">NO</button>
            </div></div></div></div>

      <div class="modal fade " id="removeAddressModal-3" role="dialog"  >
        <div class="modal-dialog widthSetter" style="height:80px; margin-top:5%; " >
          <!-- Modal content-->
          <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
            <div class="modal-body" >
                  <center><p id="errorText">Do you really want to remove the Address from your list?</p></center>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" onclick="removeAddress(3)"  style="width:20%">YES</button>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" style="width:20%">NO</button>
            </div></div></div></div>

      <div class="modal fade " id="removeAddressModal-4" role="dialog"  >
        <div class="modal-dialog widthSetter" style="height:80px; margin-top:5%; " >
          <!-- Modal content-->
          <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
            <div class="modal-body" >
                  <center><p id="errorText">Do you really want to remove the Address from your list?</p></center>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" onclick="removeAddress(4)" style="width:20%">YES</button>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" style="width:20%">NO</button>
            </div></div></div></div>

      <div class="modal fade " id="removeAddressModal-5" role="dialog"  >
        <div class="modal-dialog widthSetter" style="height:80px; margin-top:5%; " >
          <!-- Modal content-->
          <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
            <div class="modal-body" >
                  <center><p id="errorText">Do you really want to remove the Address from your list?</p></center>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" onclick="removeAddress(5)" style="width:20%">YES</button>
                  <button type="submit" class="signInsignUpButton" data-dismiss="modal" style="width:20%">NO</button>
            </div></div></div></div>


      </div> 
    <!--     Remove Address Modal End   -->

          
        <!--SERVICE LOCATOR Container Start-->
        <div class="container" >
				<div class="modal fade" id="serviceLocatorModal" role="dialog" >
                                    <div class="modal-dialog widthSetter" style="height:500px; margin-top:5%" >


          <!-- Modal content-->
          <div class="modal-content" style="background-color:#808080; opacity:0.9; border:none;" >

            <div class="modal-header" style="padding:35px 50px; height:80px;">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4>SERVICE LOCATOR</h4>
            </div>
     <center>
            <div class="modal-body" style="padding:20px; width:100%" id="serviceLocations"  >

			<input class="search" placeholder="SEARCH LOCALITY" />
				<ul class="list" align="left" style="cursor:pointer;text-transform: uppercase;">

					<li>
					  <h3 class="locality">Ambience Island, GURGAON (30 mins)</h3>
					</li>
				   
					<li>
					  <h3 class="locality">DLF Phase III, GURGAON (30 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">DLF Phase II, GURGAON (30 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">DLF Phase I, GURGAON (45 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Cyber City, GURGAON (30 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Cyber Greens, GURGAON(30 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">MG Road, GURGAON (30 mins)</h3>
					</li>
				   
					<li>
					  <h3 class="locality">Sushant Lok I, GURGAON (45 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Sec 29, GURGAON (45 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">South City, GURGAON (45 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Sec 15, GURGAON (45 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Sector 14, GURGAON (45 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Sector 17, GURGAON (45 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Sector 46, GURGAON (60 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Sector 44, GURGAON (60 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Sector 39, GURGAON (60 mins)</h3>
					</li>
					
					<li>
					  <h3 class="locality">Cyber Park, GURGAON (60 mins)</h3>
					</li>
					
				</ul>

            </div>
            </center>     

          </div>

        </div>
      </div>
    </div>

		<script>
			var options = {	valueNames: ['locality']};
			var userList = new List('serviceLocations', options);
		</script>    

		<script>
			if (document.location.search.match(/type=embed/gi)) {
				window.parent.postMessage("resize", "*");
			}
		</script>

            <!--SERVICE LOCATOR Container End-->

    <script>
    $(document).ready(function(){
        $("#changePasswordButton").click(function(){
            $("#changePasswordModal").modal();
        });

        $("#changeContactNumberButton").click(function(){
            $("#changeContactNumberModal").modal();
        });

        $("#changePasswordButtonMobile").click(function(){
            $("#changePasswordModal").modal();
        });

        $("#changeContactNumberButtonMobile").click(function(){
            $("#changeContactNumberModal").modal();
        });
        
         $(".addAddressButton").click(function(){
            $("#addAddressModal").modal();
        });


        $(".removeButton-1").click(function(){
            var userAddressId = document.getElementById('addressId-1').innerHTML;
             if(userAddressId.length!=0){
                $("#removeAddressModal-1").modal();	 
             }

        });
        $(".removeButton-2").click(function(){
             var userAddressId = document.getElementById('addressId-2').innerHTML;
             if(userAddressId.length!=0){
                $("#removeAddressModal-2").modal();
             }

        });
        $(".removeButton-3").click(function(){
             var userAddressId = document.getElementById('addressId-3').innerHTML;
             if(userAddressId.length!=0){
                $("#removeAddressModal-3").modal();
             }

        });
        $(".removeButton-4").click(function(){
            var userAddressId = document.getElementById('addressId-4').innerHTML;
             if(userAddressId.length!=0){
                $("#removeAddressModal-4").modal();
             }	 

        });
        $(".removeButton-5").click(function(){
            var userAddressId = document.getElementById('addressId-5').innerHTML;
             if(userAddressId.length!=0){
                $("#removeAddressModal-5").modal();	
             } 

       });


         // $(".editButton").click(function(){
            // $("#addAddressModal").modal();
            // $("#addNewAddressHeader").html("EDIT ADDRESS");
            // $('#streetAddress').val("OLD STREET ADDRESS");
            // $('#locality').val("OLD LOCALITY");
            // $('#landmark').val("OLD LANDMARK");
            // $("#addAddressButton").html("SAVE ADDRESS");        
        // });

        var loc=0;
        $("#AddLocalityButton-1").click(function(){
            $("#serviceLocatorModal").modal();
            loc=1;
        });
        $("#AddLocalityButton-2").click(function(){
            $("#serviceLocatorModal").modal();
            loc=2;
        });
        $("#AddLocalityButton-3").click(function(){
            $("#serviceLocatorModal").modal();
            loc=3;
        });
        $("#AddLocalityButton-4").click(function(){
            $("#serviceLocatorModal").modal();
            loc=4;
        });
        $("#AddLocalityButton-5").click(function(){
            $("#serviceLocatorModal").modal();
            loc=5;
        });

        $(".list").on('click','li',function (){
             var x = $(this).text();
             x = x.replace(/^\s+|\s+$/g, '');
			       x = x.substring(0, x.indexOf("("));

             $("#serviceLocatorModal").modal('hide');
                $('#LocalityValue-'+loc).val(x)  ;
        });



        $("#homeButton").click(function(){
                $("#homeModal").modal();
        });
                      
        $("#workButton").click(function(){
                $("#workModal").modal();
        });
                      
        $("#anotherButton1").click(function(){
                $("#anotherButton1Modal").modal();
        });
                      
		$("#anotherButton2").click(function(){
			$("#anotherButton2Modal").modal();
		});
                      
		$("#anotherButton3").click(function(){
			$("#anotherButton3Modal").modal();
		});
		  
    });    
    </script>

  

    
       <div class="row footer" id="aboutUsfooter"  style=" width:100%; ">
           <div id="mobileChangeFooter">
               <center>
                   <div id="changePasswordButtonMobile">CHANGE PASSWORD</div>
                   <DIV id="changeContactNumberButtonMobile">CHANGE CONTACT NUMBER</DIV>
               </center>
               <br>
           </div>
    </div>
        
    
     <div class="row footer" id="aboutUsfooter" style="padding-left:5px;padding-right:5px;">
            <div class="col-xs-4" >
                ALL RIGHTS RESERVED, FIG.DELIVERY
            </div>
			<div class="col-xs-4 " style="text-align:center;">
				
					<a class="footer-nav" style="display:inline;"  target="_blank" href="https://www.facebook.com/OrderFig/"><i class="fa fa-facebook" style="font-size:18px"></i> </a>
					<a class="footer-nav" style="display:inline;"  target="_blank" href="https://twitter.com/OrderFig"><i class="fa fa-twitter" style="font-size:18px"></i> </a>
					<a class="footer-nav" style="display:inline;" target="_blank" href="https://www.instagram.com/orderfig/"><i class="fa fa-instagram" style="font-size:18px"></i> </a>
				</div>
            <div class="col-xs-4 text-right" >
                
				<a class="footer-nav" target="_blank" href="aboutUs">About</a>
                <a class="footer-nav separator-left"  target="_blank" href="termsOfService">Terms of Service</a>
                <a class="footer-nav separator-left"  target="_blank" href="privacyPolicy">Privacy Policy</a>
                <a class="footer-nav separator-left"  target="_blank" href="mailto:info@fig.delivery">Contact</a>
			</div>            
        </div>
    
    
    
<!--
    <div class="row footer" id="aboutUsfooter"  style=" width:100%; ">
     
        <div class="col-xs-4" >
            ALL RIGHTS RESERVED, FIG.DELIVERY
        </div>

        <div class="col-xs-8 text-right">
            <a class="footer-nav" target="_blank" href="aboutUs">About</a>
            <a class="footer-nav separator-left"  target="_blank" href="termsOfService">Terms of Service</a>
            <a class="footer-nav separator-left"  target="_blank" href="privacyPolicy">Privacy Policy</a>
            <a class="footer-nav separator-left"  target="_blank" >Contact</a>
            <a class="footer-nav"  target="_blank" href="https://www.facebook.com/OrderFig/"><i class="fa fa-facebook"></i></a>
            <a class="footer-nav"  target="_blank" href="https://twitter.com/OrderFig"><i class="fa fa-twitter"></i></a>
            <a class="footer-nav"  target="_blank" ><i class="fa fa-instagram"></i></a>
        </div>
    </div>
-->
     
        <script>
        
            $(document).ready(function(){

            var x = $(window).height(); 
            var y = $("#aboutUsFooter").height();
           
           // document.getElementById("orderList").style.height = (x - y) + "px";
           // document.getElementById("addressList").style.height = (x - y) + "px";
            var minHeight = 100;
            var h = $('body').height() - 100 - $('#aboutUsFooter').height();
			h = h > minHeight ? h : minHeight;
			$('#orderList').height(h);
            $('#addressList').height(h-50);
            $("#addressContent").css("display", "block");
            $("#orderContent").css("display", "none");
            $("#addressMenu").css("border-bottom","2px solid #2bb373");
            
            if(localStorage.getItem('sessionId')=="" ||  localStorage.getItem('sessionId')===null){
                window.location="home";
            }
                
            else{
                getAllAddress(2);
                getUserPurchases();
            }


             $("#orderMenu").click(function(){
                $("#orderContent").show();
                $("#orderContent").css("display", "block");
                $("#orderMenu").css("border-bottom","2px solid #2bb373");
                $("#addressMenu").css("border-bottom","none");

                $("#addressContent").hide();
           });

            $("#addressMenu").click(function(){
                //alert("Hello");
                $("#addressContent").show();
                $("#addressContent").css("display", "block");
                $("#addressMenu").css("border-bottom","2px solid #2bb373");
                $("#orderMenu").css("border-bottom","none");

                $("#orderContent").hide();
           });
            
            
});
        </script>
			
      </body>
    </html>
