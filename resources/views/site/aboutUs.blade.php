<html>

<head>
<link rel="shortcut icon" href="img/figLogo.png" />
<title> About Us </title>

 
        <link rel="stylesheet" type="text/css" href="{{asset('/css/about_Terms.css') }}">
        <link rel="stylesheet" type="text/css" href="{{asset('/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/css/font-awesome.min.css')}}" /> 
   <link rel="stylesheet" type="text/css" href="{{asset('/css/home.css') }}">

</head>


<body  >
<div class="background-image"></div>
<div class="content">
<div class="container" style="padding:10px">
    <div class="row">
        <div class="col-xs-6">
            <img src="{{ asset('/img/figTypeface.png')}}" height="60" width="80" />
        </div>
        <div class="col-xs-6" >
            <!-- <div id="backMenu"><a href="home" >BACK TO MENU</a></div> -->
        </div>
    </div>
</div>

<div id="aboutUsHeader">
ABOUT US
</div>
<p id="aboutUsText">
Fig was created to make healthy-eating simple and sustainable. The hectic and stretched schedules force us to fill ourselves with either processed meal kits, or food that is quick but unhealthy. Fig provides wholesome meals that don't just taste well, but provide your body with the right nutrition. Our philosophy is to celebrate food in its purest form, just how nature intended it. 
</p>

</div>
 
 <div class="row footer" id="aboutUsFooter" style="padding-left:5px;padding-right:5px;">
            <div class="col-xs-4" >
                ALL RIGHTS RESERVED, FIG.DELIVERY
            </div>
			<div class="col-xs-4 " style="text-align:center;">
				
					<a class="footer-nav" style="display:inline;"  target="_blank" href="https://www.facebook.com/OrderFig/"><i class="fa fa-facebook" style="font-size:18px"></i> </a>
					<a class="footer-nav" style="display:inline;"  target="_blank" href="https://twitter.com/OrderFig"><i class="fa fa-twitter" style="font-size:18px"></i> </a>
					<a class="footer-nav" style="display:inline;" target="_blank" href="https://www.instagram.com/orderfig/"><i class="fa fa-instagram" style="font-size:18px"></i> </a>
				</div>
            <div class="col-xs-4 text-right" >
                
				<a class="footer-nav" target="_blank" href="aboutUs">About</a>
                <a class="footer-nav separator-left"  target="_blank" href="termsOfService">Terms of Service</a>
                <a class="footer-nav separator-left"  target="_blank" href="privacyPolicy">Privacy Policy</a>
                <a class="footer-nav separator-left"  target="_blank" href="mailto:info@fig.delivery">Contact</a>
			</div>            
        </div>
		
 


</body>

</html>
