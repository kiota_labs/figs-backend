<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="{{ asset('/img/figLogo.png') }}" />
  <title>FIG</title>

  <meta charset="UTF-8">
  <meta name="google" value="notranslate">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="{{asset('/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/css/bootstrap-theme.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/css/home.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset('/css/newCss.css') }}">

  <link rel="stylesheet" type="text/css" href="{{asset('/css/font-awesome.min.css')}}" /> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css') }}">		
  <link rel="stylesheet" type="text/css" href="{{asset('css/component.css') }}" />
  <script src="{{asset('js/modernizr.custom.js') }}"></script>

  <!-- Script for Service Locator List -->
  <script>
  window.console = window.console || function(t) {};
  </script>
  <script src="http://listjs.com/no-cdn/list.js"></script>
  <script src="//assets.codepen.io/assets/common/stopExecutionOnTimeout-f961f59a28ef4fd551736b43f94620b5.js"></script>
  <!-- Media Tags -->
  <style>

  @media screen and (min-width: 0px) and (max-width: 768px) {
    #dotnav{display:none;}
    body{overflow-x:hidden;}
    #serviceLocations{ width:90%; }
    .btn-lg { letter-spacing: 0.01em !important; }
    #stickyBarLogo{width:50px;height:28px;}
    a.button {padding: 4px 10px;}
    h1{font-size:1.2em}
    h2{font-size:1em}
    #viewTodaysMenu{bottom:40px;}
    #dotNav{left:-25px}
    .desktopDishFooter{display:none;}
    .mobileDishFooter{display:block;}
    .reviewOrderTableLeft{text-align:left; }
    .reviewOrderTableRight{text-align:left;}
    .paymentImages{width:50px; height:50px;}
    body{ letter-spacing:0em;}
    .overlay{height:90%}
    h3,h4,h5 {letter-spacing: 0em;font-size:0.8em}
    .itemNameText{
      font-size:18px;}
    }

    @media screen and (min-width: 768.01px) and (max-width:20000px ) {
      #dotnav{display:block;}
      .widthSetter{width:40%}
      #serviceLocations{  width:50%; }
      .btn-lg { letter-spacing: .15em; }
      #stickyBarLogo{width:100px;height:57px}
      a.button {padding: 4px 20px;}
      h1{font-size:1.5em}
      h2{font-size:1.2em}
      #viewTodaysMenu{bottom:40px !important;}
      #dotNav{left:10px}
      .desktopDishFooter{display:block; margin-top:-100px}
      .mobileDishFooter{display:none;}
      .reviewOrderTableLeft{text-align:left;}
      .reviewOrderTableRight{text-align:right;}
      .paymentImages{width:60px; height:60px;}
      body{letter-spacing:0.15em;}
      .overlay{height:50%}
      h3,h4,h5 {letter-spacing: .11em;}
      .itemNameText{
        font-size:1.5em;
      }

      @media only screen 
      and (min-device-width: 768px) 
      and (max-device-width: 1024px) 
      and (orientation: portrait) 
      and (-webkit-min-device-pixel-ratio: 1) {
       .widthSetter{width:60%}
       .itemNameText{
        font-size:16px;
      }

      </style>

      <!-- Script for Navigation Dots -->
      <script>
      /* dot nav */
      $(window).bind('scroll',function(e){
        redrawDotNav();
        });

      /*function redrawDotNav(){
       var topNavHeight = 50;
       var numDivs = $('section').length;
       $('#dotNav li a').removeClass('active').parent('li').removeClass('active');  	
       $('section').each(function(i,item){
        var ele = $(item), nextTop;

        //console.log(ele.next().html());

        if (typeof ele.next().offset() != "undefined") {
          nextTop = ele.next().offset().top;
        }
        else {
          nextTop = $(document).height();
        }
        if (ele.offset() !== null) {
          thisTop = ele.offset().top - ((nextTop - ele.offset().top) / numDivs);
        }
        else {
          thisTop = 0;
        }
        var docTop = $(document).scrollTop()+topNavHeight;

        if(docTop >= thisTop && (docTop < nextTop)){
          $('#dotNav li').eq(i).addClass('active');
          console.log(i + ' Adding active class (docTop=' + docTop + ', thisTop=' + thisTop + ', nextTop=' + nextTop + ')');
        }
        });   
     }*/

      function redrawDotNav(){
        $('section').each(function(i,item){
            if(isScrolledIntoView(item)){
              $('#dotNav li a').removeClass('active').parent('li').removeClass('active');
              $('#dotNav li').eq(i).addClass('active');
            }
          }
        );
      }

      function isScrolledIntoView(elem)
      {
          var $elem = $(elem);
          var $window = $(window);

          var docViewTop = $window.scrollTop();
          var docViewBottom = docViewTop + $window.height();

          var elemTop = $elem.offset().top;
          var elemBottom = elemTop + $elem.height();
          //return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));

          return ( ((elemTop <= docViewBottom) && (elemTop >= docViewTop)) || ((elemBottom <= docViewBottom) && (elemBottom >= docViewTop)));
      }

     /* get clicks working */
     // $('#dotNav li').click(function(){
      //   alert("I am an alert box!");
      // 	var id = $(this).find('a').attr("href"),
      //       posi,
      //       ele,
      //       padding = $('.navbar-fixed-top').height();
      //   
      // 	ele = $(id);
      // 	posi = ($(ele).offset()||0).top - padding;
      //     
      // 	$('html, body').animate({scrollTop:posi}, 'slow');
      //   
      // 	return false;
      // 
      // });


     $(document).ready(function() {
      $('#dotNav li').click(function(e) {  
        var id = $(this).find('a').attr("href"), posi, ele, padding = $('.navbar-fixed-top').height();
        ele = $(id);
        posi = ($(ele).offset()||0).top - padding;
        $('html, body').animate({scrollTop:posi}, 'slow');
        return false;
        });
      // $('.awesome-tooltip').tooltip({
       //         placement: 'right'
       // });   
      });

     /* end dot nav */
     </script>

     <!-- Script to Stick and Unstick the Menu Bar on the top -->
     <script>
     $(document).ready(function() {
      // Cache selectors for faster performance.
      var $window = $(window),
      $mainMenuBar = $('#mainMenuBar'),
      $mainMenuBarAnchor = $('#mainMenuBarAnchor');

      // Run this on scroll events.
      $window.scroll(function() {
        var window_top = $window.scrollTop();
        var div_top = $mainMenuBarAnchor.offset().top;
        if (window_top > div_top) {
          // Make the div sticky.
          $mainMenuBar.addClass('stick');
          document.getElementById('stickyBarLogo').style.display='block';
          document.getElementById("signInBotton").style.color = "#fff";
          document.getElementById("serviceLocatorButton").style.color = "#fff";
          $mainMenuBarAnchor.height($mainMenuBar.height());
        }
        else {
          // Unstick the div.
          $mainMenuBar.removeClass('stick');
          $mainMenuBarAnchor.height(0);
          document.getElementById('stickyBarLogo').style.display='none';
          document.getElementById("signInBotton").style.color = "#000";
          document.getElementById("serviceLocatorButton").style.color = "#000";
        }
        });
      });
</script>

<!-- Script to Stick and Unstick the Food Menu Header Bar on the top -->
<script>
$(document).ready(function() {
  // Cache selectors for faster performance.
  var $window = $(window),
  $dayMenuBar = $('#dayMenuHeader'),
  $dayMenuBarAnchor = $('#dayMenuHeaderAnchor');


  // Run this on scroll events.
  $window.scroll(function() {
    var window_top = $window.scrollTop();
    var div_top = $dayMenuBarAnchor.offset().top;
    if (window_top > div_top) {
      // Make the div sticky.
      $dayMenuBar.addClass('menuHeaderStick');

      document.getElementById("breakfast").style.color = "#fff";
      document.getElementById("lunch").style.color = "#fff";
      document.getElementById("alldaymenu").style.color = "#fff";
      document.getElementById("beverages").style.color = "#fff";
      document.getElementById("currentDate").style.color = "#d3d3d3";

      //		document.getElementById('dotNav').style.display='block';
      $dayMenuBarAnchor.height($dayMenuBar.height() );

    }

    else {
      // Unstick the div.
      $dayMenuBar.removeClass('menuHeaderStick');
      $dayMenuBarAnchor.height(0);

      document.getElementById("breakfast").style.color = "#666666";
      document.getElementById("lunch").style.color = "#666666";
      document.getElementById("alldaymenu").style.color = "#666666";
      document.getElementById("beverages").style.color = "#666666";
      document.getElementById("currentDate").style.color = "#666666";

      //document.getElementById('dotNav').style.display='none';

    }
    });
});
</script>

<script src="{{ asset('js/api_call.js') }}"></script>

<script>
$(document).ready(function(){
 display_products();
 getFigCart();
 });
</script>

<!-- Function to scroll to the Menu -->
<script>
$(document).ready(function() {
  $('#scrollToMenuArrow').click(function(){
    $('html, body').animate({scrollTop:$('#currentDate').position().top}, 'slow');
    return false;
    });
  });
</script>
</head>

<body style="background: url( {{ asset('/img/pageBackground.png') }});" >

<script>
// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
  console.log('statusChangeCallback');
  console.log(response);
  // The response object is returned with a status field that lets the
  // app know the current login status of the person.
  // Full docs on the response object can be found in the documentation
  // for FB.getLoginStatus().
  if (response.status === 'connected') {
    // Logged into your app and Facebook.
    testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
      'into this app.';
      } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
      }
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.


    function fb_login(){
      FB.login(function(response) {

        checkLoginState();
        
        }, {
          scope: 'public_profile,email'
          });
    }


    function checkLoginState() {
      FB.getLoginStatus(function(response) {
        console.log('here 101');
        statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
      FB.init({
        appId      : '968809446512267',
        cookie     : true,  // enable cookies to allow the server to access 
        // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.2' // use version 2.2
        });

      // Now that we've initialized the JavaScript SDK, we call 
      // FB.getLoginStatus().  This function gets the state of the
      // person visiting this page and can return one of three states to
      // the callback you provide.  They can be:
      //
      // 1. Logged into your app ('connected')
      // 2. Logged into Facebook, but not your app ('not_authorized')
      // 3. Not logged into Facebook and can't tell if they are logged into
      //    your app or not.
      //
      // These three cases are handled in the callback function.


    };

    // Load the SDK asynchronously
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "{{asset('js/facebookSDK.js')}}";
      fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
      console.log('Welcome!  Fetching your information.... ');
      FB.api('/me?fields=id,name,email', function(response) {
        facebookSignIn(response.id, response.email, response.name);
        picture();
        console.log('Successful login for: ' +  Object.keys(response));
        //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
        });
}
function picture() {
	FB.api('/me/picture?height=200', function(response) {
		console.log('Successful login for: ' +  response.data.url);
		localStorage.setItem('FbImageForFig',response.data.url);
   });
}
</script>
<!--
<div id="actualFbButton" style="display:none;">
<fb:login-button scope="public_profile,email" id = "actualFbClick" onlogin="checkLoginState();">
</fb:login-button>
</div>
-->
<div class="container-fluid" id="landingSection" style="height:100%;width:100%;"> 
<!--  onscroll="scrollUpToMenu()" -->
<!--         Stick the Following division -->
<!-- style=" background-color:#606060; opacity:0.8;  top:0;" -->

<div id="mainMenuBarAnchor"></div>
<div class="row"  id="mainMenuBar" >

<img id="stickyBarLogo" src="{{ asset('/img/figStickyBarLogo.png') }}" height="57" width="100" style="float:left; margin-left:10px;  
display:none;"/>

<div class="col-xs-12 text-right sitenav" style="padding-right:20px;">

<!--                 	<span id="cart" style="display:inline;"><a href="">CART (0)</a></span> -->

<!-- data-toggle="modal" data-target="#signInModal" wrap -->
<span><button  class="btn btn-default btn-lg" id="signInBotton" style="padding:0px;  letter-spacing:0.18em; color:#383838; margin-left: -20px;"  >SIGN IN</button></span>

<script>


if(localStorage.getItem('sessionId')!="" && !(localStorage.getItem('sessionId')===null)){
  //getAllAddress(1);
  document.getElementById("signInBotton").innerHTML="Hi, "+localStorage.getItem('userName');
  // $("#signInBotton").click(function(){
   // window.location="myProfile";
   // });
}


</script>
<span><button  class="btn btn-default btn-lg" id="serviceLocatorButton" style="padding:0px;text-transform: uppercase; letter-spacing:0.18em;color:#383838" >SERVICE LOCATOR</button></span>

<!--                     <span><a href="" id="signUpSignIn">SIGN UP/SIGN IN</a></span> -->
<!--                    <span><a href="" id="serviceLocator">SERVICE LOCATOR</a></span>-->
<!--  <span><a class="button" id="order_btn" data-toggle="modal" data-target="#myModal" wrap>ORDER NOW</a></span>  -->
<span><a class="button" id="order_btn" onclick="checkMyModelOpen()" style="letter-spacing:0.18em;">ORDER NOW</a></span>

<script>
var myModelOpenState=0;
function checkMyModelOpen(){
 if(cartSize==0){
  $('html, body').animate({scrollTop:$('#currentDate').position().top}, 'slow');
  $("#myModal").modal('hide');
  myModelOpenState=0;
}
else if(myModelOpenState==0){
  //								if(localStorage.getItem("sessionId")===null || localStorage.getItem("sessionId") ==""){
    //									document.getElementById("defaultModalText").innerHTML="PLEASE  SIGN IN TO  PLACE  AN  ORDER";
    //									$("#defaultModal").modal();
    //								}
    //								else{
     $("#myModal").modal();
     myModelOpenState=1;
     //								}

   }
   else if(myModelOpenState==1){
    $("#myModal").modal('hide');
    myModelOpenState=0;
  }
}
</script>

</div>
</div>



<div class="row" >
<div class="col-xs-3 imgdiv">
<img src="{{ asset('/img/leftImage.png') }}" />
</div>

<div class="col-xs-6 imgdiv text-center">
<img src="{{ asset('/img/figLogoHeader.png') }}" />
<h1 style="letter-spacing:0.17em;font-family: 'sansProRegular', sans-serif;  ">HEALTHY EATING MADE SIMPLE</h1>
</div>

<div class="col-xs-3 imgdiv">
<img src="{{ asset('/img/rightImage.png') }}" />
</div>

</div>

<center>
<div id="viewTodaysMenu" >  
<h2 style="letter-spacing:0.17em;">VIEW TODAY'S MENU</h2>
<p><a href="" id="scrollToMenuArrow"><span class="glyphicon glyphicon-chevron-down" style="color:#2bb373; font-size:20px;"></span></a></p>  
</div> 
</center>
</div>

<div class="row text-center">
<h2 id="currentDate" style="margin-top:60px; font-size:20px;font-family:'MyriadProRegular', arial, helvetica, sans-serif; letter-spacing:0em; font-weight:100 ">
<center>{{ date('l, jS F')}}</center>
</h2>
</div>

<div id="dayMenuHeaderAnchor">

</div>

<div id="dayMenuHeader">


<div  class="row text-center" style="padding:10px">
<a class="foodmenu" id="breakfast" onclick="smothBreakfastScroll()" style="display:none;">Breakfast</a>
<a class="foodmenu" id="lunch" onclick="smothLunchScroll()" style="display:none;">Lunch</a>
<a class="foodmenu" id="alldaymenu" onclick="smothAlldaymenuScroll()" style="display:none;">All Day Menu</a>
<a class="foodmenu" id="beverages" onclick="smothBeveragesScroll()" style="display:none;">Beverages</a>
</div>
<script>
function smothLunchScroll(){
  $('html, body').animate({scrollTop:$('#lunch_').position().top}, 'slow');
}
function smothBreakfastScroll(){
 $('html, body').animate({scrollTop:$('#breakfast_').position().top}, 'slow');
 }function smothAlldaymenuScroll(){
   $('html, body').animate({scrollTop:$('#alldaymenu_').position().top}, 'slow');
   }function smothBeveragesScroll(){
     $('html, body').animate({scrollTop:$('#beverages_').position().top}, 'slow');
   }

   </script>

   </div>


   <div class="imgdiv table-style product"  id="product" style="margin-right:0px; " >		
   </div>				

   <!--  Container for popup Starts Here-->
   <div class="container" style="margin-top:40px" >

   <div class="modal fade" id="myModal" role="dialog" style="height: 100%;background-color:#e8e8e8;">
   <div class="modal-dialog" style="height: 100%;" >

   <!-- Modal content-->
   <div style="height:50px; width:100%; background-color:#707070; text-align:center;">
    <div class="pull-right" style="margin: 15px;"><a class="button" data-dismiss="modal" >BACK TO MENU</a></div>
    <div style="color:white; font-size:30px; letter-spacing:0.18em;font-family: 'sansProLight', sans-serif;">CHECKOUT</div>
   </div>
   <div class="row" style="height: 100%;">
   <div class="col-sm-4 col-xs-12 row_border" style="padding: 0px;">
   <span class="label" style="display: block;font-size: 14pt;background-color: #2bb373;font-family:'MyriadProRegular', arial, helvetica, sans-serif;">Step 1: Review Your Order</span>
   <div  style="padding-left: 15px;margin-top: 15px;">          
   <table class="table table-hover" style="table-layout: fixed;">
   <thead>
   <tr >
   <th class="reviewOrderTableLeft orderTableBoldItalicsStyling" style="width:45%;letter-spacing:0.08em;">Dish</th>
   <th class=" orderTableBoldItalicsStyling" style="width:35%; text-align:left; padding-left:10px;letter-spacing:0.08em;">Quantity</th>
   <th class=" orderTableBoldItalicsStyling" style="width:20%; text-align:center;letter-spacing:0.08em;">Amount</th>
   </tr>
   </thead>

   <tbody id="tContent">

   <!--     <tr>
   <td class="reviewOrderTableLeft">Pasta de Brocoli</td>
   <td>
   <button class="transparentButtons"><img src="{{asset('/img/plus.png')}}" width="20px" height="20px"/></button>
   <!--</td>
   <td class="reviewOrderTableLeft" style="text-align:center">
   1
   </td>
   <td >
   1
   <button class="transparentButtons"><img src="{{asset('/img/minus.png')}}" width="20px" height="20px"/></button>
   </td>
   <td class="reviewOrderTableRight">205</td>
   </tr>
   <tr>
   <td class="reviewOrderTableLeft">Pasta de Brocoli</td>
   <td><button class="transparentButtons"><img src="{{asset('/img/plus.png')}}" width="20px" height="20px"/></button></td>
   <td class="reviewOrderTableLeft" style="text-align:center">1</td>
   <td><button class="transparentButtons" ><img src="{{asset('/img/minus.png')}}" width="20px" height="20px"/></button></td>
   <td class="reviewOrderTableRight">205</td>
   </tr>
   -->
   </tbody>

   <tbody>    
   <tr>
   <td style="text-align: left; background:none !important;" colspan="2">
   <div class="input-append">
   <input name="couponCode" id="couponCode" style="background: #fff;  padding:5px"  placeholder="APPLY COUPON"/>
   <script>
   if(localStorage.getItem('sessionId')!=""){
    document.getElementById("couponCode").value=localStorage.getItem('couponCode');
  }
  else{
    document.getElementById("couponCode").value="";
  }
  </script>

  <a  class="button" onclick="applyCoupon()">APPLY</a>
  <br/>
  <span id="couponError" style="color: red; display:none">Invalid coupon code.</span>
  </td>
  </div>


  <!--<td style="text-align: left;background:none !important; font-weight:bold " >TOTAL</td>-->
  <td style="text-align: right;background:none !important; padding-right:20px;font-family: 'sansProBold', sans-serif; letter-spacing: 0em;" class=" pricingText"  id="cartTotal" >1000</td>                                            
  </tr>                                        
  <tr>
  <td style="background:none !important;"  rowspan="5">
  <div class="checkbox">
  <input type="checkbox" id="checkBoxCutlery" checked><p class="orderTableBoldItalicsStyling" style="letter-spacing:0.08; font-family: 'sansProBoldIt', sans-serif;">I don't need any cutlery <br/> Less plastic means a happier environment</p>
  </div>
  </td>

  <td style="text-align: right;background:none !important;font-family: 'sansProBold', sans-serif; letter-spacing: .18em; " >DISCOUNT </td>
  <td style="text-align: right;background:none !important; padding-right:20px; font-family: 'sansProBold', sans-serif; letter-spacing: 0em; " class=" pricingText" id="displayDiscount">100</td>  

  </tr>
  <!--
  <tr>
  <td style="text-align: right;background:none !important; font-weight:bold "  >SHIPPING CHARGES </td>
  <td style="text-align: right;background:none !important; padding-right:20px " class=" pricingText" id="displayShippingCharges">0</td> 
  </tr> -->
  <tr>                                            
  <td style="text-align: right;background:none !important; font-family: 'sansProBold', sans-serif; letter-spacing: 0.18em; " >VAT (Including Surcharge)</td>
  <td style="text-align: right;background:none !important; padding-right:20px;font-family: 'sansProBold', sans-serif; letter-spacing: 0em; " class=" pricingText" id="displayVat" >100</td>  
  </tr>

  <!-- <tr>

  <td style="text-align: left;background:none !important; font-weight:bold " colspan="2" >SERVICE TAX </td>
  <td style="text-align: right;background:none !important; " class="currencyText pricingText" >100</td>  

  </tr>
  -->
  <tr>

  <td style="text-align: right;background:none !important; font-family: 'sansProBold', sans-serif; letter-spacing: 0.18em; color:#2bb373 " >GRAND TOTAL </td>
  <td style="text-align: right;background:none !important; color:#2bb373; padding-right:20px;font-family: 'sansProBold', sans-serif; letter-spacing: 0em;" class=" pricingText" id="displayGrandTotal">1000</td>

  </tr>
  </tbody>
  </table>
  </div>
  <div class="div_center">
  <!-- <a class="button" data-dismiss="modal" >BACK</a>&nbsp;&nbsp; -->
  <a  class="button" id="reviewOrderProceedButton">SELECT ADDRESS</a>
  </div>
  <br/><br/>
  </div>

  <div class="col-sm-8 col-xs-12" style="border-left: 8px solid black;height: 100%;">

  <div class="row">
  <div class="overlay" id="addressTint"></div>
  <div class="col-sm-12" style="padding: 0px;">
  <span class="label" style="display: block;font-size: 14pt;background-color: #2bb373; letter-spacing:0.18em">Step 2: Choose A Delivery Location</span>
  <div role="form" style="width: 70%;margin: 0 auto;">

  <center><div style="padding:5px" id="addressTabsAdd">
  <!--<button class="addressTabs">HOME</button>
  <button class="addressTabs">WORK</button>

  <button class="addressTabs">HOME</button>
  <button class="addressTabs">HOME</button>	-->
  </div></center>

  <script>
  if(localStorage.getItem('sessionId')!="" && !(localStorage.getItem('sessionId')===null)){
    getAllAddress(1);

  }
  </script>										

  <div class="form-group">
  <label for="usr" style="padding-left: 2%; letter-spacing:0.08em">STREET ADDRESS</label>
  <input type="text" class="form-control" id="streetAddress">
  </div>


  <div class="form-group" id="orderServiceLocatorButton">
    <div class="input-group">
      <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
      <input type="button" class="form-control" id="LocalityValue"  value="SELECT YOUR LOCALITY">
      <div class="input-group-addon"><i class="fa fa-caret-square-o-down"></i></div>
    </div>
  </div>

  <div class="form-group">
  <label for="pwd" style="padding-left: 2%;  letter-spacing:0.08em">LANDMARK/DELIVERY INSTRUCTIONS</label>
  <input type="text" class="form-control" id="landmark">
  </div>
  <div class="div_center">
  <!-- <a href="" class="button"  data-dismiss="modal">BACK</a>&nbsp;&nbsp; -->
  <a class="button" id="addressProceedButton">MAKE PAYMENT</a>
  </div>
  <br/><br/>

  </div>
  </div>

  </div>

  <div class="row" style="border-top: 8px solid black;">
  <div class="overlay" id="paymentTint"></div>
  <div class="col-sm-12" style="padding: 0px;">
  <span class="label" style="display: block;font-size: 14pt;background-color: #2bb373;">Step 3: Choose A Payment Method</span>

  <div style="text-align:center;">
  <button class=" paymentButton ">
  <img src="{{asset('/img/cash.png')}}" class="paymentImages"/>
  <p> Cash</p>
  </button>

  <button class=" paymentButton ">
  <img src="{{asset('/img/card.png')}}"  class="paymentImages"/>
  <p> Credit Card</p>
  <p> (Coming Soon)</p>
  </button>

  <button class=" paymentButton ">
  <img src="{{asset('/img/paytm.png')}}" class="paymentImages"/>
  <p> PayTM </p>
  <p> (Coming Soon)</p>
  </button>


  </div>

  <div class="row" >
  <div class="col-sm-12" style="padding: 0px;" style="display:table-cell">

  <div class="col-sm-4" style="text-align:center; padding:0px 20px;">Enter your Phone number for Verification</div>
  <div class="col-sm-4" style="text-align:center;"> <input type="text" class="form-control" id="verificationPhonePrefix" maxlength="3" value="+91" style="width:50px !important; display:inline"> 
  <input type="text" class="form-control" id="verificationPhone" placeholder="Phone Number" style="width:150px !important; display:inline" maxlength="10"></div>
  <div class="col-sm-2" style="padding:8px; text-align:center;"> <a class=" button" onclick="getVerificationCode()">GET SMS</a></div>
  </div>
  </div>
  <div class="row" id="codeSentOnPhone" style="display:none;" >
  <div class="col-sm-12" style="padding: 0px;" style="display:table-cell">
  <div class="col-sm-4" style="text-align:right">Enter the Verification Code</div>
  <div class="col-sm-4"> <input type="text" class="form-control" placeholder="Code" id="verificationCode"></div>	
  </div>
  </div>
  <div class="div_center" style="padding:30px"><a onclick="testLoginAndOrder()" class="button">CONFIRM ORDER</a></div>
  </div>
  </div>
  </div>      
  </div>
  </div>
  </div>
  </div>
  <!--  Container for popup Ends Here-->  

  <!--  SIGN IN Container Start -->
  <div class="container" style="width:100%;" align="center" >

  <!-- Modal -->
  <div class="modal fade " id="signInModal" role="dialog"  >
  <div class="modal-dialog widthSetter" style="height:500px; margin-top:5%; " >
  <!-- Modal content-->
  <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
  <div class="modal-header" style="padding:35px 50px; height:80px;">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4>SIGN IN</h4>
  </div>               
  <div class="modal-body " >
  <!-- class = "fb-login-button" id="fbButton"-->
  <div class="form-group">

  <button type="submit" style="border:none; background:none" onclick="fb_login();">
  <img src="{{ asset('/img/facebookButton.png') }}" style="width:50%"/>
  </button> 

  <!-- 
  <div class="fb-login-button" data-max-rows="1" id="fbButton" data-size="large" data-show-faces="false" data-auto-logout-link="false"></div>
  -->
  <label class="loginStaticText">- or -</label>
  </div>




  <form id="signInForm" action ="javascript:signIn()" class="form-group" data-fv-framework="bootstrap" data-fv-icon-valid="glyphicon glyphicon-ok"	data-fv-icon-invalid="glyphicon glyphicon-remove"	data-fv-icon-validating="glyphicon glyphicon-refresh">
  <div class="form-group">	
  <input type="text" class="form-control" id="emailSignIn" required data-fv-notempty-message="Email is required" placeholder="EMAIL">
  </div>
  <div class="form-group">             
  <input type="password" class="form-control" id="passwordSignIn" required data-fv-notempty-message="Password is required" placeholder="PASSWORD">
  </div>
  <div class="form-group">
  <button type="submit" class="signInsignUpButton" >SIGN IN</button>
  </div>
  </form>

  <center><button  class="btn" id="forgotPassword" style="font-size:14px;">FORGOT YOUR PASSWORD?</button></center>

  <label class="loginStaticText" style="letter-spacing:0.11em">Don't have an account?</label>

  <div class="modal-footer "   >
  <button  class="btn signInsignUpButton" style="width:60%; float:left;" id="signUpForFreeButton" >CREATE AN ACCOUNT</button>
  <button type="submit" id="skipLogin"  data-dismiss="modal">SKIP LOGIN</button>

  </div>
  </div>
  </div>
  </div> 
  </div>
  </div>
  <!--  SIGN IN Container End -->


  <!-- SIGN UP Container Start -->
  <div class="container" style="width:100%;" align="center" >
  <div class="modal fade" id="signUpModal" role="dialog">
  <div class="modal-dialog widthSetter" style="height:500px; margin-top:5%" >

  <!-- Modal content-->
  <div class="modal-content" style="background-color:#808080; opacity:0.9; border:none;" >
  <div class="modal-header" style="padding:35px 50px; height:80px;">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4>SIGN UP</h4>
  </div>

  <center>
  <div class="modal-body" >


  <div class="form-group">

  <!--<button type="submit" id="fbButton">FACEBOOK SIGN UP</button>-->
  <button type="submit" style="border:none; background:none" onclick="fb_login();" >
  <img src="{{ asset('/img/facebookButton.png') }}" style="width:50%"/>
  </button> 

  <label class="loginStaticText">- or -</label>

  <form role="form" action ="javascript:signUp()">
  <div class="form-group">
  <input type="text" class="form-control" id="userNameSignUp" placeholder="NAME" required>
  </div>

  <div class="form-group">
  <input type="text" class="form-control" id="emailIdSignUp" placeholder="EMAIL" required>
  </div>

  <div class="form-group">

  <input type="password" class="form-control" id="passwordSignUp" placeholder="PASSWORD" required>
  </div>

  <div class="form-group">
  <div class="row"><div class="col-xs-2"><input type="text" class="form-control" id="phonePrefix" value="+91" style=" display:inline; float:left" required></div>
  <div class="col-xs-10"> <input type="text" class="form-control" id="phoneSignUp" style=" display:inline; float:left; " placeholder="CONTACT NUMBER" required></div>
  </div>
  </div>

  <div class="form-group">

  <input type="text" class="form-control" id="codeSignUp" placeholder="HAVE A CODE?">
  </div>
  <div class="form-group">

  <button style="display:none" id="forceClickSignUp" >asfas</button>
  </div>					
  </form>
  <!--			   <button type="submit" id="signInsignUpButton">SIGN IN</button> -->
  </div> 

  </div> 
  <!-- 
  <center><p><a href="#" id="forgotPassword">FORGOT YOUR PASSWORD?</a></p></center>
  -->
  <!--		 <label class="loginStaticText">Don't have an account?</label> -->	
  <div class="modal-footer">
  <button type="submit" class="signInsignUpButton" style="width:60%; float:left;" onclick="$('#forceClickSignUp').click();">SIGN UP</button>
  <button type="submit" id="skipLogin"	data-dismiss="modal">SKIP LOGIN</button>
  <!--		   class="btn btn-danger btn-default pull-left" -->
  </div>

  </center>  
  </div>
  </div> 
  
  
  </div>
  </div>
  <!--SIGN UP Container End -->


  <!--SERVICE LOCATOR Container Start-->
  <div class="container" style="width:100%;" align="center" >
  <div class="modal fade" id="serviceLocatorModal" role="dialog" >
  <div class="modal-dialog widthSetter" style="height:500px; margin-top:5%" >


  <!-- Modal content-->
  <div class="modal-content" style="background-color:#808080; opacity:0.9; border:none;" >

  <div class="modal-header" style="padding:30px;">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4>SERVICE LOCATOR</h4>
  </div>
  <center>
  <div class="modal-body" style="padding:20px; width:100%" id="serviceLocations"  >


  <input class="search" placeholder="SEARCH LOCALITY" />



  <ul class="list" align="left" style="cursor:pointer;text-transform: uppercase;">

  <li>
  <h3 class="locality">Ambience Island, GURGAON (30 mins)</h3>
  </li>

  <li>
  <h3 class="locality">DLF Phase III, GURGAON (30 mins)</h3>
  </li>

  <li>
  <h3 class="locality">DLF Phase II, GURGAON (30 mins)</h3>
  </li>

  <li>
  <h3 class="locality">DLF Phase I, GURGAON (45 mins)</h3>
  </li>

  <li>
  <h3 class="locality">Cyber City, GURGAON (30 mins)</h3>
  </li>

  <li>
  <h3 class="locality">Cyber Greens, GURGAON(30 mins)</h3>
  </li>

  <li>
  <h3 class="locality">MG Road, GURGAON (30 mins)</h3>
  </li>

  <li>
  <h3 class="locality">Sushant Lok I, GURGAON (45 mins)</h3>
  </li>

  <li>
  <h3 class="locality">Sec 29, GURGAON (45 mins)</h3>
  </li>

  <li>
  <h3 class="locality">South City, GURGAON (45 mins)</h3>
  </li>
  <li>
  <h3 class="locality">Sec 15, GURGAON (45 mins)</h3>
  </li>
  <li>
  <h3 class="locality">Sector 14, GURGAON (45 mins)</h3>
  </li>
  <li>
  <h3 class="locality">Sector 17, GURGAON (45 mins)</h3>
  </li>
  <li>
  <h3 class="locality">Sector 46, GURGAON (60 mins)</h3>
  </li>
  <li>
  <h3 class="locality">Sector 44, GURGAON (60 mins)</h3>
  </li>
  <li>
  <h3 class="locality">Sector 39, GURGAON (60 mins)</h3>
  </li>
  <li>
  <h3 class="locality">Cyber Park, GURGAON (60 mins)</h3>
  </li>

  </ul>

  </div>
  </center>     

  </div>

  </div>
  </div>
  </div>

  <script>
  var options = {
    valueNames: [
    'locality'
    ]
  };
  var userList = new List('serviceLocations', options);
  //@ sourceURL=pen.js
  </script>    

  <script>
  if (document.location.search.match(/type=embed/gi)) {
    window.parent.postMessage("resize", "*");
  }
  
  
  $(".list").on('click','li',function (){

    var x = $(this).text();
    //alert(x);
    $("#serviceLocatorModal").modal('hide');     
    //document.cookie="serviceLocation=" + x ;
    x = x.substring(0, x.indexOf("("));
    $("#LocalityValue").val(x);
      //document.getElementById('serviceLocatorButton').innerHTML ='<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span> <p style="display:inline;">' + x +'</p>' ;
      //localStorage.setItem('locality', x);
      //document.getElementById('orderServiceLocatorButton').innerHTML ='<span class="glyphicon glyphicon-map-marker" style="font-size:15px; padding:0px;"></span> <p style="display:inline;">' + x +'</p>' ;  
      });
    </script>
    <!--SERVICE LOCATOR Container End-->
    <!--     Default Modal Start -->
    <div class="container" style="width:50%;"  >

    <!-- Modal -->
    <div class="modal fade " id="defaultModal" role="dialog"  >
    <div class="modal-dialog widthSetter" style="height:200px; margin-top:5%; " >
    <!-- Modal content-->
    <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
    <div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <!--<h4>Message Title</h4>-->
    </div>          
    <div class="modal-body" >
    <form role="form">
    <center><p id="defaultModalText" style="color:white" ></p></center>
    <a type="submit" class="signInsignUpButton button" data-dismiss="modal" style="width:20%">OK</a>
    </form>
    </div>  
    
    </div>

    </div>

    </div>
    </div> 
    <!--     Default Modal End   -->

    <!-- Coupon Model-->
    <div class="modal fade" id="couponModel" role="dialog">
    <div class="modal-dialog widthSetter" style="height:80px; margin-top:5%; " >
    <!-- Modal content-->
    <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
    <div class="modal-body" >
    <center><p id="couponModelContent" style="color:white" >Do you really want to remove the Address from your list?</p></center>
    <a type="submit" class="signInsignUpButton button" data-dismiss="modal"  style="width:20%" id="couponModelYes" >Yes</a>
    <a type="submit" class="signInsignUpButton button" data-dismiss="modal" style="width:20%" id="couponModelNo">NO</a>
    </div></div></div></div>
    <!-- Coupon Model end-->

    <!--     Add email to purchase start -->
    <div class="container" style="width:50%;" align="center" >
    <!-- Modal -->
    <div class="modal fade " id="addEmailToPurchase" role="dialog"  >
    <div class="modal-dialog widthSetter" style="height:200px; margin-top:5%; " >
    <!-- Modal content-->
    <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
    <div class="modal-header" style="padding:35px 50px; height:80px;">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4>Add Email to receive EBill. </h4>
    </div>
    <div class="modal-body" >
    <form role="form" action="javascript:placeOrder();">
    <div class="form-group">
    <p id="errorText" style="text-align:left">  </p>
    </div>
    <div class="form-group">
    <input type="text" class="form-control" id="purchaseEmailId" placeholder="EMAIL" required>
    </div>
    <div class="form-group">
    <button type="submit" class="signInsignUpButton" style="width:20%" id="purchaseSendMailButton">SEND EMAIL</button>
    </div>
    </form>
    </div>
    </div>	  
    </div>
    </div>
    </div> 
    <!--     Add email to purchase end -->

    <!--     Forgot Password Start -->
    <div class="container" style="width:50%;" align="center" >

    <!-- Modal -->
    <div class="modal fade " id="forgotPasswordModal" role="dialog"  >
    <div class="modal-dialog widthSetter" style="height:200px; margin-top:5%; " >
    <!-- Modal content-->
    <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
    <div class="modal-header" style="padding:35px 50px; height:80px;">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4>FORGOT PASSWORD</h4>
    </div>

    <div class="modal-body" >
    <form role="form" action="javascript:resetPassword();">

    <div class="form-group">
    <p id="errorText" style="text-align:left">ENTER YOUR REGISTERED EMAIL</p>
    </div>

    <div class="form-group">
    <input type="text" class="form-control" id="forgotPasswordEmailId" placeholder="EMAIL" required>
    </div>

    <div class="form-group">
    <button type="submit" class="signInsignUpButton" style="width:20%" id="forgotPasswordSendMailButton">SEND CODE</button>
    </div>
    </form>
    </div>  
    
    </div>

    </div>

    </div>
    </div> 
    <!--     Forgot Password End   -->


    <!--     Processing Modal Start -->
    <div class="container" style="width:50%;" align="center" >

    <!-- Modal -->
    <div class="modal fade " id="processingModal" role="dialog"  >
    <div class="modal-dialog widthSetter" style="height:50px; margin-top:5%;width:300px " >
    <!-- Modal content-->
    <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >


    <div class="modal-body" >
    <form role="form">

    <center><div class="form-group">
    <img src="{{ asset('/img/gif.gif') }}" />
    </div>
    <p style="color:white"> Processing.. Please Wait...</p>
    </center>

    </form>
    </div>  

    </div>

    </div>

    </div>
    </div> 
    <!--</div>-->

    <!--     Processing Modal End   -->

    <!-- Kitchen Closed Modal-->
    <div class="container" style="width:50%;" align="center" >
    <div class="modal fade" id="kitchenClosedModel" role="dialog">
    <div class="modal-dialog widthSetter" style="height:250px; margin-top:20%; " >
    <!-- Modal content-->
    <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
    <div class="modal-body" >
    <center><p style="color:white; text-transform:uppercase; font-size:40px;letter-spacing: .18em;" >KITCHEN'S CLOSED</p></center>
    <p style="color:white; font-size:25px;letter-spacing: .18em;"> We are open:<br> 8:00AM to 5:00PM</p>
    <button type="submit" class="signInsignUpButton" data-dismiss="modal" style="width:60%; font-size:25px;letter-spacing: .18em;" id="couponModelNo">SEE MENU</button>
    </div></div></div></div>
    </div>

    <!-- Kitchen Closed end-->

    <!-- 
    <div class="container" style="width:50%;" align="center" >
    <div class="modal fade" id="forgotPasswordModal" role="dialog">
    <div class="modal-dialog widthSetter" style="height:250px; margin-top:5%; " >
    <!-- Modal content--><!--
    <div class="modal-content " style="background-color:#808080; opacity:0.9; border:none; " >
    <div class="modal-body" >

    <p style="color:white; font-size:25px"> We are open:<br> 8:00AM to 5:00PM</p>
    <button type="submit" class="signInsignUpButton" data-dismiss="modal" style="width:40%; font-size:25px" id="couponModelNo">SEE MENU</button>
    </div></div></div>

    </div>
    </div> -->




    <div class="row footer" style="padding-left:5px;padding-right:5px;">
    <div class="col-xs-4" >
    ALL RIGHTS RESERVED, FIG.DELIVERY
    </div>
    <div class="col-xs-4 " style="text-align:center;">

    <a class="footer-nav" style="display:inline;"  target="_blank" href="https://www.facebook.com/OrderFig/"><i class="fa fa-facebook" style="font-size:18px"></i> </a>
    <a class="footer-nav" style="display:inline;"  target="_blank" href="https://twitter.com/OrderFig"><i class="fa fa-twitter" style="font-size:18px"></i> </a>
    <a class="footer-nav" style="display:inline;" target="_blank" href="https://www.instagram.com/orderfig/"><i class="fa fa-instagram" style="font-size:18px"></i> </a>
    </div>
    <div class="col-xs-4 text-right" >

    <a class="footer-nav" target="_blank" href="aboutUs">About</a>
    <a class="footer-nav separator-left"  target="_blank" href="termsOfService">Terms of Service</a>
    <a class="footer-nav separator-left"  target="_blank" href="privacyPolicy">Privacy Policy</a>
    <a class="footer-nav separator-left"  target="_blank" href="mailto:info@fig.delivery">Contact</a>
    </div>            
    </div>

    <script src="{{ asset('js/classie.js') }}"></script>
    <script src="{{ asset('js/sidebarEffects.js') }}"></script> 

    <!-- 		Handling Modal -->

    <script>
    $(document).ready(function(){


      // document.onscroll = function (event) {
        // var elements = document.getElementsByClassName("product");
        // console.log(elements);
        // for (var i = 0, len = elements.length; i < len; i++) {
          // var scrollTop = (document.documentElement && document.documentElement.scrollTop) || 
          // document.body.scrollTop;
          // if (elements[i].offsetTop >= scrollTop) {
            // // found top-most tab in view
            // // remove previous "selected" class
            // var selectedElements = document.getElementsByClassName("selected");
            // if (selectedElements.length === 1) {
              // selectedElements[0].className = "";
              // }
              // // assign new "selected" class
              // var tab = document.getElementById("tab" + (i + 1).toString() + "Selector");
              // if (tab) {
                // tab.parentElement.className = "selected";
                // }
                // break;
                // }
                // }
                // }	;




                $("#signInBotton").click(function(){
                  if(localStorage.getItem('sessionId')!="" && !(localStorage.getItem('sessionId') === null)){
                   window.location='myProfile';
                 }
                 else{
                   $("#signInModal").modal();
                   $('#signUpModal').modal('hide');
                   $("#serviceLocatorModal").modal('hide');
                 }
                 });

                $("#serviceLocatorButton").click(function(){
                  $("#serviceLocatorModal").modal();
                  $("#signUpModal").modal('hide');
                  $('#signInModal').modal('hide');

                  });

                $("#signUpForFreeButton").click(function(){
                  $("#signInModal").modal('hide');
                  $("#serviceLocatorModal").modal('hide');
                  $('#signUpModal').modal();
                  });

                $("#forgotPassword").click(function(){
                  $("#signInModal").modal('hide');
                  $("#serviceLocatorModal").modal('hide');
                  $('#signUpModal').modal('hide');
                  $("#forgotPasswordModal").modal();

                  //forgotPasswordModal
                  });

                // $("#forgotPasswordSendMailButton").click(function(){
                  // $("#signInModal").modal('hide');
                  // $("#forgotPasswordModal").modal('hide');
                  // $('#signUpModal').modal('hide');
                  // $("#serviceLocatorModal").modal('hide');
                  // $("#defaultModal").modal();
                  // });

                $("#orderServiceLocatorButton").click(function(){
                 // $("#myModal").modal('hide');
                 $("#serviceLocatorModal").modal();

                 });


                });
</script>

<!-- 		Handling Tints in Checkout  -->
<script>

$(document).ready(function(){

  $("#reviewOrderProceedButton").click(function(){
    $( "#addressTint" ).removeClass( "overlay" );
    });

  $("#addressProceedButton").click(function(){
    $( "#paymentTint" ).removeClass( "overlay" );
    });

  });

$(window).scroll(function(){
	
	if($("#breakfast_").length!=0){
		if( ($("#breakfast_").position().top + $("#breakfast_").innerHeight()-3 ) < $(window).scrollTop()){
			$("#breakfast").css("border-bottom"," 0px");
			$("#sideDotNav-1").css("display","none");
		}
		else if($("#breakfast_").position().top-3 <= $(window).scrollTop()){
			$("#breakfast").css("border-bottom","2px solid #2bb373");
			$("#sideDotNav-1").css("display","block");
		}
		else{
			$("#breakfast").css("border-bottom"," 0px");	
			$("#sideDotNav-1").css("display","none");
		}
	}
	if($("#lunch_").length!=0){
		if ( ($("#lunch_").position().top + $("#lunch_").innerHeight()-3) < $(window).scrollTop()){
			$("#lunch").css("border-bottom","0px");
			$("#sideDotNav-2").css("display","none");
		}
		else if($("#lunch_").position().top-3 <= $(window).scrollTop()){
			$("#lunch").css("border-bottom","2px solid #2bb373");
			$("#sideDotNav-2").css("display","block");
		}
		else{
			$("#lunch").css("border-bottom","0px ");
			$("#sideDotNav-2").css("display","none");
		}
	}
	if($("#alldaymenu_").length!=0){

		if( ($("#alldaymenu_").position().top + $("#alldaymenu_").innerHeight()-3 ) < $(window).scrollTop()){
			$("#alldaymenu").css("border-bottom"," 0px");
			$("#sideDotNav-3").css("display","none");
		}
		else if($("#alldaymenu_").position().top-3 <= $(window).scrollTop()){
			$("#alldaymenu").css("border-bottom","2px solid #2bb373");
			$("#sideDotNav-3").css("display","block");
		}
		else{
			$("#alldaymenu").css("border-bottom"," 0px");	
			$("#sideDotNav-3").css("display","none");
		}
	}
	if($("#beverages_").length!=0){
		if( ($("#beverages_").position().top + $("#beverages_").innerHeight() -3) < $(window).scrollTop()){
			$("#beverages").css("border-bottom"," 0px");
			$("#sideDotNav-4").css("display","none");
		}
		
		else if($("#beverages_").position().top-3 <= $(window).scrollTop()){
			$("#beverages").css("border-bottom","2px solid #2bb373");
			$("#sideDotNav-4").css("display","block");
		}
		
		else{
			$("#beverages").css("border-bottom"," 0px");	
			$("#sideDotNav-4").css("display","none");
		}
	}

  });
</script>
</body>

</html>

