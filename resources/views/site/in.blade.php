<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/reset.css') }}">

    <script type="text/javascript" src="//www.parsecdn.com/js/parse-1.6.7.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://www.parsecdn.com/js/parse-latest.js"></script>
  </head>

  <script type="text/javascript">
    Parse.initialize("PfcZZQF2KgsnfLZkTrySydDfgbUTcDqIl4i6pllT", "5GXw9YJ71repM4bggfc37XOgfeypblhIOiIA1V0C");        

    function myFunction(){         
      var x = document.getElementById("emailField");
      var a = validateEmail(x);
      
      if (a == true){
        var emailText = x.value;
        if (emailText == null || emailText == "") {
          document.getElementById("resultText").innerHTML = "Invalid Email Address";
          document.getElementById("resultText").style.color ="red";
          return false;
        }
         
        var emailAddressObject = Parse.Object.extend("WebsiteAddress");
        var emailAddress = new emailAddressObject();

        emailAddress.set("emailID", emailText);
        emailAddress.save(null, {
          success: function(emailAddress) {
            // alert('Thanks for your concern');
            document.getElementById("resultText").innerHTML = "Thanks! You are now connected with our community. We shall update you with our launch offers and other community events. ";
            document.getElementById("resultText").style.color ="green";
            x.value = "";
          },
          error: function(emailAddress, error) {
            // alert('Failed to create new object, with error code: ' + error.message);
            document.getElementById("resultText").innerHTML = "Please Check your Connection or Try again later.";
          }
        });
      }
      else{
        //  alert('Invalid Email Address');
        document.getElementById("resultText").innerHTML = "Invalid Email Address";
        document.getElementById("resultText").style.color ="red";
      }
    }
        
    function validateEmail(emailField){
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

      if (reg.test(emailField.value) == false) 
      {
        //alert('Invalid Email Address');
        return false;
      }
      return true;
    }
  </script>
    
  <script type="text/javascript" language="javascript">
    $(function() {
      $(this).bind("contextmenu", function(e) {
        e.preventDefault();
      });
    }); 
  </script>
    
    <style>
    @media screen and (min-width: 0px) and (max-width: 1200px) {
        #logoHeader{text-align: center}
        #header{text-align: center}
    #imagesSection{display: none}
    #mobileImagesSection{display: block}

}
    
    
  <style>
    @media screen and (min-width: 0px) and (max-width: 1200px) {
      #logoHeader{text-align: center}
      #header{text-align: center}
      #imagesSection{display: none}
      #mobileImagesSection{display: block}
    }

    @media screen and (min-width: 1200.01px) and (max-width:20000px ) {
      #logoHeader{text-align: right}
      #header{text-align: right}
      #imagesSection{display: block}
      #mobileImagesSection{display: none}
    }
  </style>
    
  <body>
    <div id="logoHeader"><img src="{{ asset('/img/appsImage.png') }}" height="70" style="padding:20px"/></div>
      
    <div style="height:500px;" id="imagesSection">
      <div id="leftImage"><img src="{{ asset('/img/leftImage.png') }}" height="500" /></div>
      <div id="logoImage"><img src="{{ asset('/img/figLogo.png') }}" height="500"/><p>Eat Right, Now</p></div>
      <div id="rightImage"><img src="{{ asset('/img/rightImage.png') }}" height="500"/></div>
    </div>
    
    <div style="height:500px;" id="mobileImagesSection">
      <div id="mobileRightImage" height="200px"><img src="{{ asset('/img/rightImage.png') }}" height="200" /></div>
      <div id="mobileLogoImage"><img src="{{ asset('/img/figLogo.png') }}" height="200"/><p id="mobileText">Eat Right, Now</p></div>
      <div id="mobileLeftImage"><img src="{{ asset('/img/leftImage.png') }}" height="250"/></div>
    </div>
    
    <div id="footer">
      <p>While we are busy preparing the best food, knock on our kitchen door and be surprised. </p>

      <div> 
        <input id="emailField" type="text" placeholder="Leave us your Email Address" onblur="validateEmail(this);">
        <button id="submitButton">SUBMIT</button>
      </div>
      
      <p id="resultText"></p>
    </div>
  </body>

</html>

