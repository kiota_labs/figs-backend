<html>

<head>
<link rel="shortcut icon" href="img/figLogo.png" />
<title> Privacy Policy </title>
   
        <link rel="stylesheet" type="text/css" href="{{asset('/css/about_Terms.css') }}">
        <link rel="stylesheet" type="text/css" href="{{asset('/css/bootstrap.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('/css/home.css') }}">

	<link rel="stylesheet" type="text/css" href="{{asset('/css/font-awesome.min.css')}}" /> 
</head>


<body  >
<div class="background-image"></div>
<div class="content">
<div class="container" style="padding:10px">
    <div class="row">
        <div class="col-xs-6">
<img src="{{ asset('/img/figTypeface.png')}}" height="60" width="80"/>
        </div>
        <div class="col-xs-6" >
         <!--   <div id="backMenu"><a href="home" >BACK TO MENU</a></div>  -->
        </div>
    </div>
</div>

<div id="aboutUsHeader">
PRIVACY POLICY
</div>
<p id="aboutUsText">
Fig Tree Gourmet Private Limited believes in protecting and respecting your privacy. This policy (together with our Terms and any other documents referred to herein) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. Information we may collect from you.

<strong><br><br>Personal Information</strong><br>

When you interact with us, we automatically receive and store certain types of information, such as the content you view, the date and time that you view this content, the products you purchase, or your location information associated with your IP address. We use the information we collect to serve you more relevant advertisements (referred to as "Retargeting"). This is statistical information used to monitor the usage of our website and for advertising purposes. This information does not include personal information.

<strong><br><br>Cookies</strong><br>

At times when you are browsing through Fig website, advertising cookies will be placed on your computer so that we can understand what you are interested in. This is statistical data about our users' browsing actions and patterns, and does not identify any individual. For the same reason, we may obtain information about your general Internet usage by using a cookie file, which is stored on the hard drive of your computer. Cookies contain information that is transferred to your computer's hard drive. They help us to improve our Site and to deliver a better and more personalized service. They enable us: (1) To estimate our audience size and usage pattern, (2) To store information about your preferences, and so allow us to customize our Site according to your individual interests, (3) To speed up your searches and (4) To recognize you when you return to our Site.

<strong><br><br>Security</strong><br>

We implement a secure processing server on our site when collecting information to ensure a high level of security for your personal information entered such as bank details and credit card information.

<strong><br><br>Changes to our privacy policy</strong><br>

Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail.

<strong><br><br>Contact</strong><br>

Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to 
<a href="mailto:info@fig.delivery">info@fig.delivery</a>.

</p>

</div>

<div class="row footer" style="padding-left:5px;padding-right:5px;">
            <div class="col-xs-4" >
                ALL RIGHTS RESERVED, FIG.DELIVERY
            </div>
			<div class="col-xs-4 " style="text-align:center;">
				
					<a class="footer-nav" style="display:inline;"  target="_blank" href="https://www.facebook.com/OrderFig/"><i class="fa fa-facebook" style="font-size:18px"></i> </a>
					<a class="footer-nav" style="display:inline;"  target="_blank" href="https://twitter.com/OrderFig"><i class="fa fa-twitter" style="font-size:18px"></i> </a>
					<a class="footer-nav" style="display:inline;" target="_blank" href="https://www.instagram.com/orderfig/"><i class="fa fa-instagram" style="font-size:18px"></i> </a>
				</div>
            <div class="col-xs-4 text-right" >
                
				<a class="footer-nav" target="_blank" href="aboutUs">About</a>
                <a class="footer-nav separator-left"  target="_blank" href="termsOfService">Terms of Service</a>
                <a class="footer-nav separator-left"  target="_blank" href="privacyPolicy">Privacy Policy</a>
                <a class="footer-nav separator-left"  target="_blank" href="mailto:info@fig.delivery">Contact</a>
			</div>            
        </div>
		
</body>

</html>
