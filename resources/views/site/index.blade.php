<!DOCTYPE html>
<html>
  <head>
  <link rel="shortcut icon" href="img/figLogo.png" />
  <title>FIG </title>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:600,400">
	
	<link rel="stylesheet" type="text/css" href="{{asset('/css/font-awesome.min.css')}}" /> 

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/reset.css') }}">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script type="text/javascript" src="//www.parsecdn.com/js/parse-1.6.7.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://www.parsecdn.com/js/parse-latest.js"></script>
	    <link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css') }}">
  </head>

  <script type="text/javascript">
    Parse.initialize("PfcZZQF2KgsnfLZkTrySydDfgbUTcDqIl4i6pllT", "5GXw9YJ71repM4bggfc37XOgfeypblhIOiIA1V0C");        

    function myFunction(){         
      var x = document.getElementById("emailField");
      var a = validateEmail(x);
      
      if (a == true){
        var emailText = x.value;
        if (emailText == null || emailText == "") {
          document.getElementById("resultText").innerHTML = "Invalid Email Address";
          document.getElementById("resultText").style.color ="red";
          return false;
        }
         
        var emailAddressObject = Parse.Object.extend("WebsiteAddress");
        var emailAddress = new emailAddressObject();

        emailAddress.set("emailID", emailText);
        emailAddress.save(null, {
          success: function(emailAddress) {
            // alert('Thanks for your concern');
            document.getElementById("resultText").innerHTML = "Thanks! You are now connected with our community. We shall update you with our launch offers and other community events. ";
            document.getElementById("resultText").style.color ="green";
            x.value = "";
          },
          error: function(emailAddress, error) {
            // alert('Failed to create new object, with error code: ' + error.message);
            document.getElementById("resultText").innerHTML = "Please Check your Connection or Try again later.";
          }
        });
      }
      else{
        //  alert('Invalid Email Address');
       document.getElementById("resultText").innerHTML = "Invalid Email Address";
        document.getElementById("resultText").style.color ="red";
      }
    }
        
    function validateEmail(emailField){
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

      if (reg.test(emailField.value) == false) 
      {
        //alert('Invalid Email Address');
        return false;
      }
      return true;
    }
  </script>

  <script type="text/javascript" language="javascript">
    $(function() {
      $(this).bind("contextmenu", function(e) {
        e.preventDefault();
      });
    }); 
  </script>

    <style>
    @media screen and (min-width: 0px) and (max-width: 1000px) {
        #logoHeader{text-align: center}
    
    #im2{margin-top:60%}
    
    #im1{margin-top:100%; }
    
    

}

@media screen and (min-width: 1000.01px) and (max-width:20000px ) {

        #logoHeader{text-align: right}
    
            #header{text-align: right}
      
    #imagesSection{display: block}
    #mobileImagesSection{display: none}

}

    </style>
  <body>
    <div id="logoHeader"><img src="{{ asset('/img/appsImage.png') }}" style="height:100% ;margin:20px;"/></div>


      <div class="row">
        <div class =" col-lg-4 col-xs-4 im1"><img id="im1" src="{{ asset('/img/leftImage.png') }}" style="width:100%"/></div>
        
        <div  class =" col-lg-4 col-xs-4 im2">
          <img id="im2"  align="center" src="{{ asset('/img/figLogo.png') }}" style="width:90%"/>
          <h2 align="center">Eat Right, Now</h2>
  
<div id="footer" style='margin-top:20px'>
 
   <p>Subscribe to be part of our beta testing program. </p>
       <div> <input id="emailField" type="text" placeholder="Leave us your Email Address" onblur="validateEmail(this);">
        <button onclick="myFunction()" id="submitButton">SUBMIT</button>
</div>
            <p id="resultText" style='margin-top:10px'></p>


    </div>
      </div>

        <div  class =" col-lg-4 col-xs-4 im3" style="padding-right:0px" ><img src="{{ asset('/img/rightImage.png') }}" style="width:100%"/></div>
      </div>

<div class="row footer" style="padding-left:5px;padding-right:5px;">
            <div class="col-xs-4" >
                ALL RIGHTS RESERVED, FIG.DELIVERY
            </div>
			<div class="col-xs-1" ></div>
			<div class="col-xs-2 " style="text-align:center;">
				
					<a class="footer-nav" style="display:inline;"  target="_blank" href="https://www.facebook.com/OrderFig/"><i class="fa fa-facebook-square"></i> </a>
					<a class="footer-nav" style="display:inline;"  target="_blank" href="https://twitter.com/OrderFig"><i class="fa fa-twitter-square"></i> </a>
					<a class="footer-nav" style="display:inline;" target="_blank" href="https://www.instagram.com/orderfig/"><i class="fa fa-instagram"></i> </a>
				</div>
            <div class="col-xs-5 text-right" >
                
				<a class="footer-nav separator-left" target="_blank" href="index.php/aboutUs">About</a>
                <a class="footer-nav separator-left"  target="_blank" href="index.php/termsOfService">Terms of Service</a>
                <a class="footer-nav separator-left"  target="_blank" href="index.php/privacyPolicy">Privacy Policy</a>
                <a class="footer-nav separator-left"  target="_blank" href="mailto:info@fig.delivery">Contact</a>
			</div>            
</div>

  </body>

</html>
