<!DOCTYPE html>
<html lang="en">
    <body>
        <table width="100%" border="0" style="border-collapse: collapse;">
        	<tr bgcolor="">
                <td colspan="3" align="center" style="border-bottom: 1px solid #cccccc;">
                    <img src="{{{ asset('images/logo_figx128.png') }}}" width="128px"/>
                </td>
            </tr>

            <tr>
        		<td colspan="3" align="left" style="padding: 20px 0px;">
                    <p>
                        @if($purchase->fullName)
                            <span>Hi {{ $purchase->fullName }},</span>
                        @else
                            <span>Dear customer,</span>
                        @endif

            			<br/><span>Phone: {{ $purchase->phone }}</span>
                    </p>
        			
        			<p>Thanks for using FIG! Your order is being processed and will reach you shortly.</p>
                    <p>Looking forward to serve you.</p>
        		</td>
        	</tr>

        	<tr>
                <td colspan="3" align="left" style="font-weight: bold;">
                    <small>INVOICE NO. {{ $purchase->purchaseId }}</small><br/>
                    <small>{{ date("d/m/Y") }}</small>
                </td>
            </tr>

            <tr bgcolor="#cccccc">
                <td align="left" style="padding: 4px 0px; font-weight: bold">Item</td>
                <td align="right" style="padding: 4px 0px; font-weight: bold">Quantity</td>
                <td align="right" style="padding: 4px 0px; font-weight: bold">Price</td>
            </tr>
            <?php $grandTotal = 0 ?>
            @foreach($purchase->purchaseDetail as $pd)
        	<tr>
        		<td align="left" style="padding: 4px 0px;">{{ $pd->itemName }}</td>
        		<td align="right" style="padding: 4px 0px;">{{ number_format($pd->quantity, 0) }}</td>
        		<td align="right" style="padding: 4px 0px;">{{ number_format($pd->quantity * $pd->price, 2) }}</td>
        	</tr>
            <?php $grandTotal += $pd->quantity * $pd->price ?>
        	@endforeach
			
			<?php 
			if(!is_null($purchase->couponCode) && $purchase->couponCode==0){
				$discount = 0;
			}
			else{
				$discount = 100;
			}
			?>
			
			<?php $grandTotal +=  $grandTotal*0.13125 + $discount?>
            <tr style="border-top: 1px solid #cccccc;">
                <td colspan="2" align="right" style="padding: 4px 0px; font-weight: bold">
                    Grand Total
                </td>

                <td colspan="1" align="right" style="padding: 4px 0px; font-weight: bold">
                    <span>{{ number_format($grandTotal, 2) }}</span>
                </td>
            </tr>

            <tr>
                <td colspan="3" align="left" style="padding-top: 50px;">
                    <b>Delivery address</b>
                    <p>{{ $purchase->street }} {{ $purchase->locality }}</p>
                </td>
            </tr>

            <tr>
                <td colspan="3" align="left">
                    <b>Landmark</b>
                    <p>{{ $purchase->landmark }}</p>
                </td>
            </tr>

            <tr style="border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;">
                <td colspan="1" align="left" valign="center">
                    <b>Get App</b> 
                    <a href="#"><img src="{{{ asset('images/android16.png') }}}" /></a>
                    <a href="#"><img src="{{{ asset('images/apple16.png') }}}" /></a>
                </td>

                <td colspan="2" align="right">
                    <b>Follow us: </b> 
                    <a href="#"><img src="{{{ asset('images/facebook16.png') }}}" /></a>
                    <a href="#"><img src="{{{ asset('images/twitter16.png') }}}" /></a>
                    <a href="#"><img src="{{{ asset('images/pinterest16.png') }}}" /></a>
                    <a href="#"><img src="{{{ asset('images/instagram16.png') }}}" /></a>
                </td>
            </tr>

            <tr>
                <td colspan="3" align="center" >
                    &copy; {{ date('Y') }} - fig. All rights reserved.
                    <p>address here</p>
                </td>
            </tr>

            
        </table>
    </body>
</html>