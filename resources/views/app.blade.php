<!DOCTYPE html>
<html lang="en">
    @include('templates.head')
    <body>
        
        <section id="container">
            @include('templates.header')

            <section id="main-content" style="margin-left: 200px;">
                <section class="wrapper">
                   
                    @yield('content')
                      
                </section>
            </section>
        </section>
  
    </body>
</html>