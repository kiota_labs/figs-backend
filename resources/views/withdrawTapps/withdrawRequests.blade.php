@extends('app')

@section('content')

<div class="container-fluid">
    <div>
        <div>
            <div class="col-sm-12">
                <ol class="breadcrumb" style="height:auto;">
                    <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                    <li><i class="fa fa-users"></i>Withdraw Requests</li>                                                
                </ol>
            </div>
        </div>
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection

        <div class="col-sm-12">

            @if (Session::has('flash_message'))  
            <div class="alert alert-success">
                <button data-dismiss="alert" class="close">
                    ×
                </button>
                <strong>Success!</strong> {{ Session::get('flash_message') }}
            </div>
            @endif 

            @if (count($withdrawRequests) > 0)
            <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
                <thead>
                    <tr>
                        <th class="text-center tdHeader"><span>Requested By</span></th>
                        <th class="text-center tdHeader"><span>Tapps Requested</span></th>
                        <th class="text-center tdHeader secondHide">Status</th>
                        <th class="text-center tdHeader">Actions</th>
                        <th class="text-center tdHeader">Reject</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($withdrawRequests as $withdrawRequest)

                    <tr>
                        <td style="vertical-align: middle;">
                            <a href="{{url('/cms/user/userDetails/'.$withdrawRequest->userId)}}">
                                @if($withdrawRequest->role == "group")
                                <img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($withdrawRequest->image) ? asset('images/user/thumb/'.$withdrawRequest->image): asset(config('constants.groupThumbImage'))}}}"/>
                                @else
                                <img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($withdrawRequest->image) ? asset('images/user/thumb/'.$withdrawRequest->image): asset(config('constants.userThumbImage'))}}}"/>
                                @endif
                            </a>
                            <a href="{{url('/cms/user/userDetails/'.$withdrawRequest->userId)}}">
                                {{ isset($withdrawRequest->fullName)&& !empty($withdrawRequest->fullName)?$withdrawRequest->fullName:$withdrawRequest->username}}
                            </a>
                        </td>
                        <td class="text-center" style="vertical-align: middle;">{{ $withdrawRequest->tappsRequested }}</td>
                        <td class="text-center secondHide" style="vertical-align: middle;">
                            @if($withdrawRequest->requestStatus == 1)
                            <a class="btn btn-success" id="completeButton{{$withdrawRequest->id}}">Completed</a>
                            @elseif($withdrawRequest->requestStatus == 0)
                            <a class="btn btn-primary" id="pendingButton{{$withdrawRequest->id}}">Pending</a>
                            @elseif($withdrawRequest->requestStatus == 2)
                            <a class="btn btn-danger" id="rejectedButton{{$withdrawRequest->id}}">Rejected</a>
                            @endif
                        </td>
                        @if($withdrawRequest->requestStatus == 0)
                        
                        <td class="text-center" style="vertical-align: middle;"> 
                        @if($withdrawRequest->balanceTapps < $withdrawRequest->tappsRequested)
                        <a class="btn btn-danger btn-sm" disabled="disabled" href="#" id="completeButton{{$withdrawRequest->id}}">Unsufficient Tapps</a>
                        @else
                            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" name="_xclick">
                                    <input type="hidden" name="cmd" value="_xclick" /> 
                                    <input type="hidden" name="business" value="{{$withdrawRequest->receiverPaypalId}}" />
                                    <input type="hidden" name="currency_code" value="USD" />
                                    <input type="hidden" name="item_name" value="Tapps" />
                                    <input type="hidden" name="notify_url" value="{{url('cms/user/saveWithdrawTappDetails')}}" />
                                    <input type="hidden" name="cbt" value="Return to TAPP CMS" />
                                    <input type="hidden" name="amount" value="{{$withdrawRequest->tappsRequested * $tappValue}}" /> 
                                    <input type="hidden" name="custom" value="{{$withdrawRequest->id}}" />
                                    <input type="hidden" name="return" value="{{url('cms/user/saveWithdrawTappDetails')}}" /> 
                                    <input type="hidden" name="cancel_return" value="{{url('cms/user/withdrawTappRequests')}}" /> 
                                    <input type="image" id="pendingButton2{{$withdrawRequest->id}}" style="vertical-align: middle;" alt="paypal payment" name="submit" src="https://www.paypalobjects.com/webstatic/en_US/btn/btn_paynow_86x21.png" />
                                </form>
                            </div>
                        @endif
                        </td>
                        @elseif($withdrawRequest->requestStatus == 1)
                        <td class="text-center" style="vertical-align: middle;"> 
                            <a class="btn btn-default btn-sm" disabled="disabled" href="#" id="completeButton{{$withdrawRequest->id}}">Payment Done</a>
                        </td>
                        @elseif($withdrawRequest->requestStatus == 2)
                        <td class="text-center" style="vertical-align: middle;"> 
                            <a class="btn btn-default btn-sm" disabled="disabled" href="#" id="rejectedButton{{$withdrawRequest->id}}">Payment Rejected</a>
                        </td>
                        @endif
                        @if($withdrawRequest->requestStatus == 0)
                        <td class="text-center" style="vertical-align: middle;"> 
                            <a class="btn btn-danger" id="reject{{$withdrawRequest->id}}" data-id="{{$withdrawRequest->id}}" href="#" data-toggle="modal" data-target="#myModalRejection">
                                Reject
                            </a>  
                        </td>
                        @elseif($withdrawRequest->requestStatus == 1)
                        <td class="text-center" style="vertical-align: middle;"> 
                            <a class="btn btn-default" disabled="disabled" href="#" >Reject</a>
                        </td>
                        @elseif($withdrawRequest->requestStatus == 2)
                        <td class="text-center" style="vertical-align: middle;"> 
                            <a class="btn btn-default" disabled="disabled" href="#" >Reject</a>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>

            </table>
            <?php echo $withdrawRequests->appends(Request::input())->render(); ?>
            @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> No records found for Withdrawal Requests.<br><br>
            </div>
            @endif
        </div>
    </div>
</div>
<div id="myModalRejection" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please enter reason to reject withdrawat Request</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                    <label style="font-size: 15px;">
                        Do you want to reject this withdrawal request?
                    </label>
                    <input type="text" class="form-control" id="rejectReason" name="rejectReason" placeholder="Reason to reject">
                    <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                    <input type="hidden" value="" name="userId" id="dialogRejectId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onClick="Reject()">Submit</button>

            </div>
        </div>

    </div>
</div><!-- /.modal -->
<script>
    $('#myModalRejection').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogRejectId').val(e.relatedTarget.dataset.id);
    });
    function Reject() {

        var userId = document.getElementById('dialogRejectId').value;
        var rejectReason = document.getElementById('rejectReason').value;
        $.ajax({
            type: "post",
            url: "{{asset('cms/user/rejectWithdrawal')}}",
            data: {'userId': userId, 'rejectReason': rejectReason},
            cache: false,
            success: function (result) {
                $('#myModalRejection').modal('hide');
                $('#reject' + userId).addClass('btn-default');
                $('#reject' + userId).removeClass('btn-danger');
                $('#reject' + userId).attr('disabled', 'disabled');
                $('#pendingButton' + userId).replaceWith("<a class='btn btn-danger' href='#'>Rejected</a>");
                $('#pendingButton2' + userId).replaceWith("<a class='btn btn-default btn-sm' disabled='disabled' href='#'>Payment Rejected</a>");
                
            }
        });
    }
        $(document).ready(function ()
    {
        $("#mytable").tablesorter();
    }
    );

</script>



@endsection
