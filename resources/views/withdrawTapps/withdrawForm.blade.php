@extends('app')

@section('content')
<div class="container-fluid">
     <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-group"></i>Withdraw Tapps</li>
                                                <li><i class="fa fa-group"></i>New Request</li>
					</ol>
				</div>
			</div>
    
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Withdraw Tapps</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                     @if (Session::has('flash_message'))  
        <div class="alert alert-success">
            <button data-dismiss="alert" class="close">
                ×
            </button>
            <strong>Awesome!</strong> {{ Session::get('flash_message') }}
        </div>
        @endif

                    <form id="withdrawForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('cms/user/withdrawTapps') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                        <div class="form-group">
                            <label class="col-md-4 control-label">Total Balance Tapps</label>
                            <div class="col-md-6">
                                <label class="control-label" id='totalTapps' name="totalTapps"> {{$totalTapps}}  </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label required">Withdraw Tapps</label>
                            <div class="col-md-6">
                            <input type="number" min="0" class="form-control" id="withdrawTapps" name="withdrawTapps" required="required" value="{{ old('withdrawTapps') }}">
                            <span id="spnStatus"></span>
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" id="withdrawSubmit" class="btn btn-primary">
                                    Withdraw
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

$("#withdrawForm").submit(function(e) {
    var withdrawTapps = $('#withdrawTapps').val();
    var totalTapps = $('#totalTapps').text();
   
  if(parseInt(withdrawTapps) < parseInt(totalTapps) && parseInt(withdrawTapps) >= 0){
      return true;
  }else{
    $('#spnStatus').html('Value should be less than Total Tapps and non negative');
    $('#spnStatus').css('color', 'red');
    return false; 
  }
  
});

       


</script>
@endsection


