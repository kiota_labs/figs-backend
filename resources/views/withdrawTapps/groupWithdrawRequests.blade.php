@extends('app')

@section('content')

<div class="container-fluid">
    <div>
         <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
						<li><i class="fa fa-users"></i>Withdraw Tapps</li>  
                                                <li><i class="fa fa-users"></i>All Requests</li>
					</ol>
				</div>
			</div>
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
         
        <div class="col-sm-12">

            @if (count($withdrawRequests) > 0)
            <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
                <thead>
                    <tr>
                        <th class="text-center tdHeader"><span>Tapps Requested</span></th>
                        <th class="text-center tdHeader">Status</th>
                        <th class="text-center tdHeader"><span>Request Date</span></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($withdrawRequests as $withdrawRequest)
                    
                    <tr>
                        <td class="text-center">{{ $withdrawRequest->tappsRequested }}</td>
                        <td class="text-center">
                                    
                                    <a class="btn btn-success" style="{{ ($withdrawRequest->requestStatus)  ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}}">Approved</a>
                                    <a class="btn btn-danger" style="{{ ($withdrawRequest->requestStatus) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}}">Pending</a>
                                  
                                </td>
                        <td class="text-center">{{date('M j Y ',strtotime($withdrawRequest->createDate)) }}</td>

                    </tr>
                    @endforeach
                </tbody>

            </table>
            <?php echo $withdrawRequests->appends(Request::input())->render(); ?>
            @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> No records found for Group Followers.<br><br>
            </div>
            @endif
        </div>
    </div>
</div>
</div>
<script>
         $(document).ready(function ()
    {
        $("#mytable").tablesorter();
    }
    );

</script>


@endsection