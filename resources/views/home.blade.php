@extends('app')

@section('content')

@if (Session::has('flash_message'))  
<div class="alert alert-success">
    <button data-dismiss="alert" class="close">
        ×
    </button>
    <strong>Success!</strong> {{ Session::get('flash_message') }}
</div>
@endif 

@if(\Auth::user()->role == 'group')
<div class="container-fluid">
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i>Home</li>

        </div>
    </div>

    @extends('templates.sidebar')
    @section('sidebar')
    @endsection
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">Recent Campaigns</div>
            <div class="panel-body">
                @if (count($campaigns) > 0)
                <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
                    <thead>
                        <tr>
                            <th class="text-center tdHeader"><span>Title</span></th>
                            <th class="text-center tdHeader mytable"><span>Goal Amount</span></th>
                            <th class="text-center tdHeader mytable"><span>Tapps Received</span></th>
                            <th class="text-center tdHeader">Visibility</th>
                            <th class="text-center tdHeader">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($campaigns as $post)

                        <tr>
                            <td style="vertical-align: middle;">
                                <a href="{{url('/cms/post/viewDetailsPost/'.$post->id)}}">
                                    <img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($post->image) ? asset('images/post/thumb/'.$post->image): asset(config('constants.postDefaultImage'))}}}"/></a>
                                <a href="{{url('/cms/post/viewDetailsPost/'.$post->id)}}">{{ $post->title }}</a></td>
                            <td class="text-center mytable" style="vertical-align: middle;">{{ $post->tappsAssigned }}</td>
                            <td class="text-center mytable" style="vertical-align: middle;">{{ $post->tappsReceived }}</td>
                            <td class="text-center" style="vertical-align: middle;">
                                <a class="btn btn-success" id="act_{{$post->id}}" data-id="{{$post->id}}" href="#" data-toggle="modal" data-target="#myModalActivation" style="{{ ($post->isDisabled) ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}}">Unhide</a>
                                <a class="btn btn-danger" id="deact_{{$post->id}}" data-id="{{$post->id}}" href="#" data-toggle="modal" style="{{ ($post->isDisabled) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}}" data-target="#myModal">Hide</a>   
                            </td>
                            <td class="text-center" style="vertical-align: middle;"> 
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                    <a class="btn btn-default" href="{{url('/cms/post/updatePost/'.$post->id) }}">Edit</a>
                                </div>
                            </td> 
                        </tr>
                        @endforeach
                    </tbody>

                </table>

                @else
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> No records found for Campaigns.<br><br>
                </div>
                @endif
                <div class="form-group row text-right">
                        <a href="{{ url('cms/campaign/index') }}" style="margin-right: 20px; font-size: 15px;">View more....</a>&nbsp;&nbsp;&nbsp;
                    </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Recent Posts</div>
            <div class="panel-body">
                @if (count($posts) > 0)
                <table class="table table-striped table-bordered table-hover table-responsive" id="mytable2">
                    <thead>
                        <tr>
                            <th class="text-center tdHeader"><span>Title</span></th>
                            <th class="text-center tdHeader"><span>Assigned To</span></th>
                            <th class="text-center tdHeader"><span>Tapps Received</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($posts as $post)

                        <tr>
                            <td style="vertical-align: middle;">
                                <a href="{{url('/cms/post/postDetails/'.$post->id)}}"><img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($post->image) ? asset('images/post/thumb/'.$post->image): asset(config('constants.postDefaultImage'))}}}"/></a>
                                <a href="{{url('/cms/post/postDetails/'.$post->id)}}">{{ $post->title }}</a></td>
                            <td class="text-center" style="vertical-align: middle;">{{ $post->sendToCampaignTitle?$post->sendToCampaignTitle:'Not Assigned' }}</td>
                            <td class="text-center" style="vertical-align: middle;">{{ $post->tappsReceived }}</td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>

                @else
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> No records found for Posts.<br><br>
                </div>
                @endif
                <div class="form-group row text-right">
                        <a href="{{ url('cms/post/posts') }}" style="margin-right: 20px; font-size: 15px;">View more....</a>&nbsp;&nbsp;&nbsp;
                    </div>
            </div>
        </div>

    </div>
</div>



<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please enter reason to hide campaign</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                    <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                    <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Reason to hide">
                    <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">

            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Activation</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to unhide this campaign</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
    $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
    function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
    $(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#disableReason').val();
            var id = $('#dialogClubId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/post/disable')}}",
                type: 'POST',
                data: $('#disableReasonForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#deact_' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#act_' + id).css({'display': 'inline', 'visibility': 'visible'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                }
            });
        });
    });


    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/post/enable')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
                $('#deact_' + userId).css({'display': 'inline', 'visibility': 'visible'});
                $('#act_' + userId).css({'display': 'none', 'visibility': 'hidden'});

            }
        });
    }

</script>

@elseif(\Auth::user()->role == 'admin')


    
    <div class="container-fluid">
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i>Home</li>

        </div>
    </div>
    <div class="col-sm-12">

        <div class="panel panel-default">
            <div class="panel-heading">Recently Flagged Users/Groups</div>
            <div class="panel-body">
                @if (count($flaggedUsers) > 0)   
                @foreach ($flaggedUsers as $flaggedUser)
                <div class="panel-group accordion myPanel" value="{{$flaggedUser->flaggedUserId}}" id="accordion{{$flaggedUser->flaggedUserId}}" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default accordion-group">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 id="flaggedTitle{{$flaggedUser->flaggedUserId}}" class="flaggedTitle panel-title" value="{{$flaggedUser->flaggedUserId}}" data-setValue="1" data-toggle="collapse" data-parent="#accordion{{$flaggedUser->flaggedUserId}}" data-target="#collapseOne{{$flaggedUser->flaggedUserId}}" aria-expanded="true" aria-controls="collapseOne">
                                @if(isset($flaggedUser->flaggedUserId) && !empty($flaggedUser->flaggedUserId))
                                <a value="{{$flaggedUser->flaggedUserId}}" id="accordionPlus{{$flaggedUser->flaggedUserId}}" class="accordionPlus pull-right" role="button" data-toggle="collapse" data-parent="#accordion{{$flaggedUser->flaggedUserId}}" href="#collapseOne{{$flaggedUser->flaggedUserId}}" aria-expanded="true" aria-controls="collapseOne"><span class="glyphicon glyphicon-plus" style="margin-top: 13px;"></span> </a>
                                <a value="{{$flaggedUser->flaggedUserId}}" id="accordionMinus{{$flaggedUser->flaggedUserId}}" class="accordionMinus pull-right hide" role="button" data-toggle="collapse" data-parent="#accordion{{$flaggedUser->flaggedUserId}}" href="#collapseOne{{$flaggedUser->flaggedUserId}}" aria-expanded="true" aria-controls="collapseOne"><span class="glyphicon glyphicon-minus" style="margin-top: 13px;"></span> </a>
                                @endif
                                <a href="{{url('/cms/user/viewUserDetails/'.$flaggedUser->flaggedUserId)}}">
                                    @if($flaggedUser->role == "group")
                                    <img id="flaggedImage{{$flaggedUser->flaggedUserId}}" value="{{$flaggedUser->flaggedUserId}}" href="{{url('/cms/user/viewUserDetails/'.$flaggedUser->flaggedUserId)}}" class="img-circle img-responsive img-thumbnail leftalign imageSize flaggedImage" src="{{{isset($flaggedUser->image) && !empty($flaggedUser->image) ? asset('images/user/thumb/'.$flaggedUser->image) : asset(config('constants.groupThumbImage'))}}}"/>
                                    @else
                                    <img id="flaggedImage{{$flaggedUser->flaggedUserId}}" value="{{$flaggedUser->flaggedUserId}}" href="{{url('/cms/user/viewUserDetails/'.$flaggedUser->flaggedUserId)}}" class="img-circle img-responsive img-thumbnail leftalign imageSize flaggedImage" src="{{{isset($flaggedUser->image) && !empty($flaggedUser->image) ? asset('images/user/thumb/'.$flaggedUser->image) : asset(config('constants.userThumbImage'))}}}"/>
                                    @endif
                                </a>
                                <a style="{{ ($flaggedUser->isDisabled) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}} margin-right:10px;" data-toggle="tooltip" data-placement="right" data-original-title="Active"><span class="glyphicon glyphicon-ok-sign" style="color: #0E325A;"></span></a>
                                <a style="{{ ($flaggedUser->isDisabled) ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} margin-right:10px;" data-toggle="tooltip" data-placement="right" data-original-title="Inactive"><span class="glyphicon glyphicon-remove-sign" style="color: #ff0000;"></span></a>
                                <a id="flaggedName{{$flaggedUser->flaggedUserId}}" class="flaggedName accordionStyle" value="{{$flaggedUser->flaggedUserId}}" href="{{url('/cms/user/viewUserDetails/'.$flaggedUser->flaggedUserId)}}" style="color: #007aff;">
                                   
                                    {{isset($flaggedUser->fullName)&&!empty($flaggedUser->fullName)?$flaggedUser->fullName:$flaggedUser->username}}
                                    
                                </a>
                                @if(isset($flaggedUser->flaggedUserId) && !empty($flaggedUser->flaggedUserId))
                                <a data-toggle="tooltip" data-placement="right" data-original-title="Flagged"><span class="glyphicon glyphicon-flag" style="color: #58b6f4;"></span></a>
                                @endif   
                                

                            </h4>
                        </div>
                        <div id="collapseOne{{$flaggedUser->flaggedUserId}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                @if (count($flaggedUser->userDetails) > 0)
                                @foreach ($flaggedUser->userDetails  as $flaggedUserDetail)
                                <div style="padding: 10px 20px;">
                                    @if($flaggedUserDetail->flaggedByRole == 'user')
                                    <img class="img-circle img-responsive img-thumbnail pull-left" style="width:40px; height:40px; margin-right: 10px;" src="{{{isset($flaggedUserDetail->image) && !empty($flaggedUserDetail->image) ? asset('images/user/thumb/'.$flaggedUserDetail->image) : asset(config('constants.userThumbImage'))}}}"/>
                                    @else
                                    <img class="img-circle img-responsive img-thumbnail pull-left" style="width:40px; height:40px; margin-right: 10px;" src="{{{isset($flaggedUserDetail->image) && !empty($flaggedUserDetail->image) ? asset('images/user/thumb/'.$flaggedUserDetail->image) : asset(config('constants.groupThumbImage'))}}}"/>
                                    @endif                                    <b>{{ isset($flaggedUserDetail->flaggedBy) && !empty($flaggedUserDetail->flaggedBy)?$flaggedUserDetail->flaggedBy:$flaggedUserDetail->flaggedByUserName }}</b>
                                    <small><i>({{ date('M j, Y ',strtotime($flaggedUserDetail->date)) }})</i></small>
                                    <br/>{{ $flaggedUserDetail->flagReason }}
                                </div>
                                @endforeach
                                @else
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> This User is not flagged by anyone.<br><br>
                                </div>
                                @endif           
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                @else
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> No records found for Flagged Users.<br><br>
                </div>
                @endif
                
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Recently Registered Groups</div>
            
                @if (count($users) > 0)
                
                <table class="table table-bordered table-hover" id="mytable">
                    <thead>
                        <tr>
                            
                            <th class="text-center tdHeader"><span class="mySpan">Group Name</span></th>
                            <th class="text-center tdHeader"><span class="mySpan">Email</span></th>
                            <th class="text-center tdHeader mytable"><span class="mySpan">Signup Date</span></th>
                            <th class="text-center tdHeader mytable firstHide">Visibility</th>
                            <th class="text-center tdHeader mytable secondHide">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr style="{{ ($user->isVerified)  ? 'background-color:#D3D3D3;' : ''}}">
                            
                            <td style="vertical-align: middle;">
                                <a href="{{url('/cms/user/viewGroupDetails/'.$user->id)}}"><img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($user->image)&& !empty($user->image) ? asset('images/user/thumb/'.$user->image): asset(config('constants.groupThumbImage'))}}}"/></a>
                                <a class="text-center" href="{{url('/cms/user/viewGroupDetails/'.$user->id)}}">{{$user->fullName}}</a></td>
                            <td class="text-center" style="vertical-align: middle;">{{ $user->email }}</td>
                            <td class="text-center mytable" style="vertical-align: middle;">{{ date('M j Y ',strtotime($user->createDate)) }}</td>
                            <td class="text-center mytable firstHide" style="vertical-align: middle;">
                                @if($user->approvalStatus == 1)
                                <a class="btn btn-success" id="act_{{$user->id}}" data-id="{{$user->id}}" href="#" data-toggle="modal" data-target="#myModalActivation" style="{{ ($user->isDisabled)  ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}}">Unhide</a>
                                <a class="btn btn-danger" id="deact_{{$user->id}}" data-id="{{$user->id}}" href="#" data-toggle="modal" data-target="#myModal" style="{{ ($user->isDisabled) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}}">Hide</a>
                                @else
                                <a class="btn btn-default" style="background-color: #B5B5B5;" disabled="disabled" href="#" data-toggle="modal" >Hide</a>
                                @endif
                            </td>
                            <td class="text-center secondHide mytable" style="vertical-align: middle;">
                                @if($user->approvalStatus == 1)
                                <a class="btn btn-default" disabled="disabled" href="#" data-toggle="modal" >Approved</a>
                                @elseif($user->approvalStatus == 2)
                                <a class="btn btn-default" disabled="disabled" href="#" data-toggle="modal" >Rejected</a>
                                @else
                                <a class="btn btn-default" disabled="disabled" href="#" data-toggle="modal" >Unapproved</a>
                                @endif
                            </td>

                        </tr>
                        @endforeach
                    </tbody>

                </table>
               
                @else
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> No records found for unapproved groups.<br><br>
                </div>
                @endif
            <div class="form-group row text-right">
                        <a href="{{ url('cms/user/viewGroups') }}" style="margin-right: 20px; font-size: 15px;">View more....</a>&nbsp;&nbsp;&nbsp;
                    </div>
        </div>

    </div>
</div>



<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please enter reason to hide group</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                    <label style="font-size: 15px;">
                        You are about to disable this group,this will affect the visibility of the 
                        group in the App,Do you want to continue?
                    </label>
                    <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Reason to Hide">
                    <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                    <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">

            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Activation</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to unhide this group</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
    $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
    
    function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
    
    $('.accordion').on('show.bs.collapse', function () {
         var id = $(this).attr('value');
         $("#accordionPlus" + id).addClass('hide');
         $("#accordionMinus" + id).removeClass('hide');
    });
    
    $('.accordion').on('hide.bs.collapse', function () {
         var id = $(this).attr('value');
         $("#accordionPlus" + id).removeClass('hide');
         $("#accordionMinus" + id).addClass('hide');
    });

    $(".flaggedImage").click(function () {

        var id = $(this).attr('value');
        $('#flaggedTitle' + id).removeAttr('href');
        $('#flaggedTitle' + id).removeAttr('data-toggle');
        document.location.href = $(this).attr('href');
    });

    $(".flaggedName").click(function () {
        var id = $(this).attr('value');
        $('#flaggedTitle' + id).removeAttr('href');
        $('#flaggedTitle' + id).removeAttr('data-toggle');
        document.location.href = $(this).attr('href');
    });
 
    $(".accordionPlus").click(function () {
        var t = $(this).attr('value');
        $("#accordionPlus" + t).addClass('hide');
        $("#accordionMinus" + t).removeClass('hide');
    });
    
    


    $(".accordionMinus").click(function () {
        var t = $(this).attr('value');
        $("#accordionMinus" + t).addClass('hide');
        $("#accordionPlus" + t).removeClass('hide');
    });

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

</script>
@endif
@endsection
