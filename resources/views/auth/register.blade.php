@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/auth/register') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label required">E-Mail Address</label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" required="required" value="{{ old('email') }}" style="{{($errors->has('email'))?'border:1px solid red;':null}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label required">Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" required="required" name="password" style="{{($errors->has('password'))?'border:1px solid red;':null}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label required">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" required="required" name="password_confirmation" style="{{($errors->has('password'))?'border:1px solid red;':null}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label required">Group Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" required="required" name="groupName" value="{{ old('groupName') }}">
                            </div>
                        </div>
                        
                         <div class="form-group">
                            <label class="col-md-4 control-label required">Address</label>
                            <div class="col-md-6"><textarea id="info" required="required" name="address" rows="5" cols="52" class="form-control">{{old('address')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label required">Description</label>
                            <div class="col-md-6"><textarea id="info" required="required" name="description" rows="5" cols="52" class="form-control">{{old('description')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label required">Contact Person</label>
                            <div class="col-md-6">
                                <input type="text" required="required" class="form-control" name="contactPerson" value="{{ old('contactPerson') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label required">Phone Number</label>
                            <div class="col-md-6">
                                <input type="text" required="required" maxlength="15" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                            </div>
                            <div class="col-md-2 pull-left">
                             <span id="valid" class="glyphicon glyphicon-ok hide" style="color: #00ff00; margin-top: 5px;" data-toggle="tooltip" data-placement="right" data-original-title="Valid phone number"></span>
                             <span id="invalid" class="glyphicon glyphicon-remove hide" style="color: #ff0000; margin-top: 5px;" data-toggle="tooltip" data-placement="right" data-original-title="Invalid phone number"></span>
                            </div>
                             

                            

                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label required">Image</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" onchange="readURL(this, 'image');" name="image">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                <img id="image" style="height:100px; width:100px;"/>  
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function readURL(input, data) {
        var imageId = '#' + data;
        if (input.files && input.files[0]) {
            var reader = new FileReader();


            reader.onload = function (showImage) {
                $(imageId)
                        .attr('src', showImage.target.result)
                        .width(100)
                        .height(100);
            };

            reader.readAsDataURL(input.files[0]);
        }

    }

    $(document).ready(function ()
    {
        $(".myTabClass").hide();
    }
    );

    function validatePhone(txtPhone) {
        var a = document.getElementById(txtPhone).value;
        var filter = /^[0-9-+]+$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }
    $(document).ready(function () {
        $('#phone').blur(function (e) {
            if (validatePhone('phone')) {
                $('#valid').removeClass('hide');
                $('#invalid').addClass('hide');
            }
            else {
                $('#invalid').removeClass('hide');
                $('#valid').addClass('hide');
            }
        });
    });
    
     $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        });


</script>
@endsection
