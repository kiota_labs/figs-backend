<!DOCTYPE html>
<html lang="en">
    @include('templates.head')

    <body class="background-theme-transparent">
            @include('templates.header')
        <div class="container">
            
            
            
            <form class="login-form" method="POST" action="{{ url('/auth/login') }}"> 
                @if (count($errors) > 0)
            <div class="alert alert-danger">
                
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (Session::has('flash_message'))  
            <div class="alert alert-success">
                <button data-dismiss="alert" class="close">
                    ×
                </button>
                <strong>Success!</strong> {{ Session::get('flash_message') }}
            </div>
            @endif 
            @if (Session::has('flash_error'))  
            <div class="alert alert-danger">
                <button data-dismiss="alert" class="close">
                    ×
                </button>
                <strong>Error!</strong> {{ Session::get('flash_error') }}
            </div>
            @endif 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="login-wrap" style="margin-top: 10px;">
                    <p class="login-img"><i class="icon_lock_alt"></i></p>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon_profile"></i></span>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus="">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon_key"></i></span>
                        <input type="password" class="form-control" name="password">
                    </div>
                    <label class="checkbox">
                        <input type="checkbox" value="remember"> Remember me
                        <span class="pull-right"> <a href="{{ url('/password/email') }}"> Forgot Password?</a></span>
                    </label>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>

                </div>
            </form>

        </div>


    </body>
</html>
