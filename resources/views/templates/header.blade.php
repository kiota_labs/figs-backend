    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom" id="showSettings"><i class="icon_menu"></i></div>
        </div>

        <!--logo start-->
        <a href="{{ url('/') }}" class="logo"><span><img src="{{asset('images/app_logo.png')}}"</span></a>
        
        <!--logo end-->

        <div class="top-nav notification-row">                
            <!-- notificatoin dropdown start-->
            <ul class="nav pull-right top-menu">

                <!-- task notificatoin start -->
                 @if (Auth::guest())
                <li id="task_notificatoin_bar" class="myTabClass">
                    <a href="{{ url('/auth/register') }}">
                        <span>Register</Span>
                    </a>
                </li>
                <li id="task_notificatoin_bar">
                    <a href="{{ url('/auth/login') }}">
                        <span> Login</span>
                    </a>
                </li>
                @elseif (Auth::user()->role == 'admin')
               
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="profile-ava">
                            <img class="img-circle img-responsive img-thumbnail" style="width:40px; height:40px;" src="{{{isset(\Auth::user()->image) && !empty(\Auth::user()->image) ? asset('images/user/thumb/'.\Auth::user()->image) : asset(config('constants.userDefaultImage'))}}}">
                        </span>
                        <span class="username">{{ Auth::user()->username }}</span>
                        <b class="caret"></b>
                    </a>
                    
                    <ul class="dropdown-menu extended">
                        <div class="log-arrow-up"></div>
                        <li>
                            <a href="{{ url('cms/user/tappConversionValue') }}">
                                <i class="fa fa-gears">&nbsp;</i>Settings</a>
                        </li>
                        <li>
                            <a href="{{ url('/cms/user/changePassword') }}">
                                <i class="icon_key_alt"></i>Change Password</a>
                        </li>
                        <li>
                            
                            <a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out"></i>Logout</a>
                        </li>
                    </ul>
                </li>
                @elseif(\Auth::user()->role == 'group')
                 
               
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown" href="#">
                        <span class="profile-ava">
                            <img class="img-circle img-responsive img-thumbnail" style="width:40px; height:40px;" src="{{{isset(\Auth::user()->image) && !empty(\Auth::user()->image) ? asset('images/user/thumb/'.\Auth::user()->image) : asset(config('constants.userDefaultImage'))}}}">
                        </span>
                        <span class="username">{{ Auth::user()->username }}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended">
                        <div class="log-arrow-up"></div>
                         <li class="eborder-top">
                            <a href="{{ url('cms/user/groupProfile/'.Auth::user()->id) }}"><i class="icon_profile"></i> My Profile</a>
                        </li>
                        <li>
                            <a href="{{ url('/cms/user/changePassword') }}"><i class="icon_key_alt"></i> Change Password</a>
                        </li>
                        <li>
                            <a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </li>
                @endif
                <!-- user login dropdown end -->
            </ul>
            <!-- notificatoin dropdown end-->
        </div>
    </header>      
    <!--header end-->
    
        