<!DOCTYPE html>
<html lang="en">
    @extends ('templates/head')
    <body>
        <section id="container">
        @extends ('templates/header')
         
        @extends ('templates/sidebar')
       

        
            @yield('content')
        </section>
    </body>
</html>