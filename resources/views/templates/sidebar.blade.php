<aside>
    <div class='container-fluid'>
        <div id="sidebar"  class="nav-collapse">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu">                
                <li id="dashboardTab" class="{{ (strpos($_SERVER['REQUEST_URI'], '/home') !== false ) ? 'active' : '' }}">
                    <a class="" href="{{url('/')}}">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                @if (Auth::user()->role == 'admin')
               <!-- <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon_documents_alt"></i>
                        <span>Groups</span>
                        <span class="menu-arrow arrow_carrot-right"></span>
                    </a>
                    <ul class="sub">                          
                        <li><a class="" href="{{ url('cms/user/viewGroups') }}">Recently Registered Groups</a></li>
                        <li><a class="" href="{{ url('cms/flaggedUser/getFlaggedGroups') }}">Flagged Groups</a></li>
                    </ul>
                </li> -->
                <li class="{{ (strpos($_SERVER['REQUEST_URI'], 'user/viewGroups') !== false ) ? 'active' : '' }}"> 
                   <a href="{{ url('cms/user/viewGroups') }}" class="">
                        <i class="fa fa-group"></i>
                        <span>All Registered Groups</span>
                    </a>
              </li>
              <li class="{{ (strpos($_SERVER['REQUEST_URI'], 'flaggedUser/getFlaggedGroups') !== false ) ? 'active' : '' }}">
                    <a href="{{ url('cms/flaggedUser/getFlaggedGroups') }}" class="">
                        <i class="fa fa-flag"></i>
                        <span>Flagged Groups</span>
                    </a>
              </li>
              <li class="{{ (strpos($_SERVER['REQUEST_URI'], 'user/viewUsers') !== false ) ? 'active' : '' }}">
                    <a href="{{ url('cms/user/viewUsers') }}" class="">
                        <i class="fa fa-user"></i>
                        <span>All Registered Users</span>
                    </a>
              </li>
              <li class="{{ (strpos($_SERVER['REQUEST_URI'], 'flaggedUser/getFlaggedUsers') !== false ) ? 'active' : '' }}">
                    <a href="{{ url('cms/flaggedUser/getFlaggedUsers') }}" class="">
                        <i class="fa fa-flag"></i>
                        <span>Flagged Users</span>
                    </a>
              </li>
            
              <li class="{{ (strpos($_SERVER['REQUEST_URI'], 'user/withdrawTappRequests') !== false ) ? 'active' : '' }}">
                    <a href="{{ url('cms/user/withdrawTappRequests') }}" class="">
                        <i class="fa fa-credit-card"></i>
                        <span>Withdrawal Requests</span>
                    </a>
              </li>
              <li class="{{ (strpos($_SERVER['REQUEST_URI'], 'user/itemRedeemRequests') !== false ) ? 'active' : '' }}">
                    <a href="{{ url('cms/user/itemRedeemRequests') }}" class="">
                        <i class="fa fa-money"></i>
                        <span>Redeem Requests</span>
                    </a>
              </li>
                <!--   <li class="menu">
                    <a class="" href="{{url('cms/user/withdrawRequests')}}">
                        <i class="icon_creditcard"></i>
                        <span>Withdraw Requests</span>
                    </a>
                </li> -->
                @elseif(\Auth::user()->role == 'group')
               
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="fa fa-plus"></i>
                        <span>Create</span>
                        <span class="menu-arrow arrow_carrot-right"></span>
                    </a>
                    <ul class="sub">                          
                        <li><a href="{{ url('cms/campaign/create') }}"><i class="fa fa-file-image-o"></i><span>Campaign</span></a></li>
                        <li><a href="{{ url('cms/post/create') }}"><i class="fa fa-file-powerpoint-o"></i><span>Post</span></a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ url('cms/post/posts') }}" class="">
                        <i class="icon_documents_alt"></i>
                        <span>My Posts</span>
                    </a>
                </li>
              <li>
                    <a href="{{ url('cms/campaign/index') }}" class="">
                        <i class="icon_documents_alt"></i>
                        <span>My Campaigns</span>
                    </a>
              </li>
              <li>
                    <a href="{{ url('cms/groupFollow/getFollowers') }}" class="">
                        <i class="fa fa-group"></i>
                        <span>My Followers</span>
                    </a>
              </li>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="fa fa-credit-card"></i>
                        <span>Withdraw Tapps</span>
                        <span class="menu-arrow arrow_carrot-right"></span>
                    </a>
                    <ul class="sub">                          
                        <li><a href="{{url('cms/user/withdrawTapps')}}"><i class="fa fa-plus"></i><span>New Request</span></a></li>
                        <li><a href="{{ url('cms/user/groupWithdrawRequests') }}"><i class="fa fa-files-o"></i><span>All Requests</span></a></li>
                    </ul>
                </li>
            
                @endif
                
                @yield('sidebar')

            </ul>
            <!-- sidebar menu end-->
        </div>
    </div>
</aside>