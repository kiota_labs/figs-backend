@extends('app')

@section('content')
<div class="container-fluid">
     <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-gears"></i>Settings</li>
                                                <li><i class="fa fa-money"></i>Tapp Conversion Value</li>
					</ol>
				</div>
			</div>
    
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tapp Conversion Value</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                     @if (Session::has('flash_message'))  
        <div class="alert alert-success">
            <button data-dismiss="alert" class="close">
                ×
            </button>
            <strong>Awesome!</strong> {{ Session::get('flash_message') }}
        </div>
        @endif

                    <form id="withdrawForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('cms/user/addConversionValue') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label required">Current Tapp Value</label>
                            <div class="col-md-6">
                                <label class="control-label">{{$tappValue}} $</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label required">New Tapp Value</label>
                            <div class="col-md-6">
                            <input type="number" step="any" min="0" class="form-control" id="tappValue" name="tappValue" required="required" value="{{ old('tappValue') }}" placeholder="value in dollar">
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" id="tappValueSubmit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel panel-default">
<div class="panel-heading">Tapp Value History</div>
    <div class="panel-body">


        @if (count($tappValueHistory) > 0)
        <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
            <thead>
                <tr>
                    <th class="text-center" style="vertical-align: middle;">tapp Value(in $)</th>
                    <th class="text-center" style="vertical-align: middle;">Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tappValueHistory as $tappHistory)

                <tr>
                    <td class="text-center" style="vertical-align: middle;">{{ $tappHistory->tappValue }}</td>
                    <td class="text-center" style="vertical-align: middle;">{{ !is_null($tappHistory->createDate)?date('M j Y ',strtotime($tappHistory->createDate)):'Not Available' }}</td>
                </tr>
                @endforeach
            </tbody>

        </table>
       
        @else
        <div class="alert alert-danger">
            <strong>Whoops!</strong> No records found for Tapp Value History.<br><br>
        </div>
        @endif



    </div>
</div>
        </div>
    </div>
</div>

@endsection


