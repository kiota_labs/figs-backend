@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Unauthorized Access</div>
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> You are not authorized to access this page.<br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
