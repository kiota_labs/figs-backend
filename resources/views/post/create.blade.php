@extends('app')

@section('content')


<div class="container-fluid">
    @if(isset($postDetails))
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb" style="height:auto;">
                <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                <li><i class="fa fa-group"></i><a href="{{ url('cms/campaign/index') }}">All Campaigns</a></li>
                <li><i class="fa fa-file"></i>Edit Campaign</li>
            </ol>
        </div>
    </div>
    @else
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb" style="height:auto;">
                <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                <li><i class="fa fa-file"></i>Create Campaign</li>
            </ol>
        </div>
    </div>
    @endif
    <form method="POST" enctype="multipart/form-data" action="{{URL::to('cms/campaign/create')}}"
          accesskey=""   accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{{ Input::old('id', isset($postDetails) ? $postDetails->id : null)}}}">
        <?php if ($errors->count() > 0) { ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors->all() as $messages) { ?>
                        <li> <?php echo $messages ?> </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
        @if (Session::has('flash_message'))  
        <div class="alert alert-success">
            <button data-dismiss="alert" class="close">
                ×
            </button>
            <strong>Success!</strong> {{ Session::get('flash_message') }}
        </div>
        @endif 
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="title" class="control-label required">Title</label>
                <div>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" required="required" style="{{($errors->has('title'))?'border:1px solid red;':null}}"
                           value="{{{ Input::old('title', isset($postDetails) ? $postDetails->title : null)}}}">
                </div>

            </div>

            <div class="form-group row">
                <label for="description" class="control-label required">Description</label>
                <div><textarea id="info" name="description" rows="5" cols="52" class="form-control" style="{{($errors->has('description'))?'border:1px solid red;':null}}"
                               required="required" placeholder="Description">{{{Input::old('description',isset($postDetails) ? $postDetails->description : null)}}}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="control-label">Status</label>
                <div>
                    <input type="radio" name="isPublic" value="1" checked <?php
                    if (Input::old('isPublic') == "1") {
                        echo 'checked="checked"';
                    }
                    ?>>&nbsp;Public
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="isPublic" value="0" <?php
                    if (Input::old('isPublic') == "0") {
                        echo 'checked="checked"';
                    }
                    ?><?php if (isset($postDetails)) echo ($postDetails->isPublic) ? '' : "checked" ?>>&nbsp;Private
                </div>
            </div>

            <div class="form-group row">
                <label for="goal" class="control-label required">Goal</label>
                <div>
                    <input type="number" class="form-control" id="goal" name="goal" placeholder="Goal" required="required" style="{{($errors->has('goal'))?'border:1px solid red;':null}}"
                           value="{{{ Input::old('goal',isset($postDetails) ? $postDetails->tappsAssigned : null)}}}">
                </div>
            </div>



            <div class="form-group row">
                <label class="control-label">Image</label>
                <div>
                    <input type="file" class="form-control" onchange="readURL(this, 'image');" name="image"
                           value="{{{Input::old('image',isset($postDetails)?$postDetails->postImage : null)}}}">
                </div>
                <img id="image" style="height:100px; width:100px;" src="{{{isset($postDetails->postImage)?$postDetails->postImage : null}}}"/>
            </div>

            <div class="form-group row">
                <label class="control-label required">End Date</label>
                <div>
                    <input id="endDate" name="endDate" class="form-control" type="text" placeholder="End Date" required="required" style="{{($errors->has('endDate'))?'border:1px solid red;':null}}"
                           value="{{{Input::old('endDate',isset($postDetails)?$postDetails->endDate : null)}}}">
                    <span id="spnDateStatus"></span>
                </div>

            </div>


            <div class="form-group">
                <div class="col-sm-12 row">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>



        </div>
        <div class="col-sm-3"></div>
    </form>


</div>
<script>
    function readURL(input, data) {
        var imageId = '#' + data;
        if (input.files && input.files[0]) {
            var reader = new FileReader();


            reader.onload = function (showImage) {
                $(imageId)
                        .attr('src', showImage.target.result)
                        .width(100)
                        .height(100);
            };

            reader.readAsDataURL(input.files[0]);
        }

    }

    $(function () {
        $("#endDate").datepicker({dateFormat: 'M dd, yy', minDate: 0});
    });



</script>
@endsection
