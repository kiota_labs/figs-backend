@extends('app')

@section('content')


<div class="container-fluid">
     <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-group"></i><a href="{{ url('cms/post/posts') }}">All Posts</a></li>
                                                <li><i class="fa fa-home"></i>Post Details</li>
					</ol>
				</div>
			</div>
    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
          accesskey=""   accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="postId" id="postId" value="{{ $postDetails->id }}">
        
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
        <!--<div class="col-sm-3">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#">Actions</a></li>
                <li><a href="{{ url('cms/campaign/index') }}">View Campaigns</a></li> 
            </ul>
        </div> -->
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>post Details</b>
                </div>
                <div class="panel-body">
                    
                    <div class="col-md-4 text-center">
                        <img class="img-circle img-responsive img-thumbnail" src="{{{isset($postDetails->image) ? asset('images/post/'.$postDetails->image): asset(config('constants.postDefaultImage'))}}}"/>
                    </div>
                    
                    
                    <div class="col-md-8">
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Title</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->title}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Description</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->description}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Assigned To Campaign</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->sendToCampaignTitle}}</div>
                    </div>
                        
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Status</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->isPublic?'Private':'Public'}}</div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Tapps Received</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->tappsReceived}}</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Create Date</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{date('M j Y ',strtotime($postDetails->createDate))}}</div>
                    </div>
                   
                
                </div>
                </div>
            </div>
           
                    <div class="panel panel-default">
                
                <div class="panel-body">
                    
                    <div class="form-group row" style="padding: 15px;">
                        <input type="submit" style="margin-top: 20px;" class="btn btn-default col-sm-6 rightalign" id="commentShow" value="Comments">
                        <input type="submit" style="margin-top: 20px;" class="btn btn-default col-sm-6 leftalign" id="tappShow" value="Tapps Received">
                    </div>
                    <div id="commentsDiv" class="text-center">
                    @if(count($postComments) > 0)
                            <table class="table table-striped table-bordered table-hover table-responsive" style="width:80%; margin: auto;">
                                <thead>
                                    <tr>
                                        <th class="text-center">User</th>
                                        <th class="text-center">Comment</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($postComments as $comment)

                                    
                                    <tr>
                                        <td class="text-center"><img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($comment->image) && !empty($comment->image) ? asset('images/user/thumb/'.$comment->image): asset(config('constants.userDefaultImage'))}}}"/></td>
                                        @if($comment->userId == Auth::user()->id)
                                        <td class="text-center" width="70%" style='font-size:16px;'><b>{{ $comment->comment }}</b></td>
                                        @else
                                        <td class="text-center" width="70%">{{ $comment->comment }}</td>
                                        @endif
                                        <td class="text-center"> 
                                            @if($comment->userId == Auth::user()->id)
                                            <a id="act_{{$comment->id}}" data-id="{{$comment->id}}" href="#" data-toggle="modal" data-target="#myModalActivation" style="{{ ($comment->isDisabled) ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}}"><span class="glyphicon glyphicon-ok-sign" style="color: #00ff00;"></span></a>
                                            <a id="deact_{{$comment->id}}" data-id="{{$comment->id}}" href="#" data-toggle="modal" data-target="#myModal" style="{{ ($comment->isDisabled) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}}"><span class="glyphicon glyphicon-remove-sign" style="color: #ff0000;"></span></a>
                                            @endif
                                        </td> 
                                    </tr>
                                    

                                    @endforeach
                                </tbody>

                            </table>
                            @endif
                            <div style="margin:auto; margin-top: 20px;" class="text-center">
                            <textarea style="min-width:250px; max-width:300px; height:80px; border-radius: 5px; border-color: grey;" placeholder="Comment" name="comment" id="comment"></textarea><br/>
                            <input type="submit" value="Add Comment" class="btn btn-default text-center" style="height: 34px; margin-top: 5px;" id="commentSubmit"><br>
                            <label id="myLabels" class="myLabel" style="color: red;"></label><br>

                            </div>
                    </div>
                    <div id="tappsDiv" class="hide">
                      @if(count($postDetails->postTapps) > 0)
                            <table class="table table-striped table-bordered table-hover table-responsive" style="width:80%; margin: auto;">
                                <thead>
                                    <tr>
                                        <th class="text-center">User</th>
                                        <th class="text-center">Tapps Donated</th>
                                        <th class="text-center">Gender</th>
                                        <th class="text-center">Age</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($postDetails->postTapps as $comment)

                                    
                                    <tr>
                                        <td class="text-center">
                                        <img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($comment->image) && !empty($comment->image) ? asset('images/user/thumb/'.$comment->image): asset(config('constants.userDefaultImage'))}}}"/>
                                        <b>{{ $comment->fullName }}</b>
                                        </td>
                                       
                                        <td class="text-center"><b>{{ $comment->tappCount }}</b></td>
                                        
                                        <td class="text-center">{{ $comment->gender }}</td>
                                        
                                        <td class="text-center">{{$comment->age}}
                                        </td> 
                                    </tr>
                                    

                                    @endforeach
                                </tbody>

                            </table>
                            @endif  
                    
            </div>
                </div></div>
           
        </div>



     

    </form>
</div>

 <div id="myModal" class="modal fade in" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Hide Comment</h4>
                </div>
                <div class="modal-body">
                    <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                        <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                        <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Please enter reason for hiding this comment">
                        <input type="hidden" value="" name="userId" id="dialogClubId">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">

                </div>
            </div>

        </div>
    </div>
    <div id="myModalActivation" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Unhide</h4>
                </div>
                <div class="modal-body">
                    <p>Do You Want To Unhide This Comment</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                    <input type="hidden" value="" name="userId" id="dialogActivateUserId">
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<script>
   $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
$(document).ready(function () {
            $("#disableSubmit").click(function () {
                var data = $('#disableReason').val();
                var id = $('#dialogClubId').val();
                var searchReg = /^[ Xa-zA-Z0-9-]+$/;
                if (data == '')
                {
                    $("#modalLabel").html('* please fill reason to disable');
                    return false;
                } else if (!searchReg.test(data)) {
                    $("#modalLabel").html("* please enter valid Reason");
                    return false;
                }
                $.ajax({
                    url: "{{asset('cms/post/disableComment')}}",
                    type: 'POST',
                    data: $('#disableReasonForm').serialize(),
                    success: function (response) {
                        $('#myModal').modal('hide');
                        $('#deact_' + id).css({'display': 'none', 'visibility': 'hidden'});
                        $('#act_' + id).css({'display': 'inline', 'visibility': 'visible'});
                        $('.myLabel').html('');
                        $('.myForm').trigger('reset');
                    }
                });
            });
        });
        $(document).ready(function () {
        $("#tappShow").click(function (){
            $("#tappsDiv").removeClass('hide');
            $("#commentsDiv").addClass('hide');
            return false;
            
        });
    });
   

        function Activate() {

            var userId = document.getElementById('dialogActivateUserId').value;

            $.ajax({
                type: "post",
                url: "{{asset('cms/post/enableComment')}}",
                data: {'userId': userId},
                cache: false,
                success: function (result) {
                    $('#myModalActivation').modal('hide');
                    $('#deact_' + userId).css({'display': 'inline', 'visibility': 'visible'});
                    $('#act_' + userId).css({'display': 'none', 'visibility': 'hidden'});

                }
            });
        }
   
        $("#commentSubmit").click(function () {
            //e.preventDefault();
            var data = $("textarea#comment").val();
            
            var id = $('#postId').val();
         
            if (data == '')
            {
                $("#myLabels").html('* please enter a comment');
                return false;
            } 
            $.ajax({
                type : "post",
                url : "{{asset('cms/post/addComment')}}",
                data : {'postId': id,'comment':data},
                cache : false,
                success: function (response) {
                    $('.myLabel').html('');
                    if(response == "comment added successfully"){
                    location.reload(true);
                }
                }
                 
            });
            
        });
    
   




</script>

    
@endsection




