@extends('app')

@section('content')


<div class="container-fluid">
    @if(isset($postDetails))
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb" style="height:auto;">
                <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                <li><i class="fa fa-group"></i><a href="{{ url('cms/post/viewPosts') }}">All Posts</a></li>
                <li><i class="fa fa-file"></i>Edit Post</li>
            </ol>
        </div>
    </div>
    @else
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb" style="height:auto;">
                <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                <li><i class="fa fa-file"></i>Create Post</li>
            </ol>
        </div>
    </div>
    @endif
    <form method="POST" id="createPost" enctype="multipart/form-data" action="{{URL::to('cms/post/create')}}"
          accesskey=""   accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{{ Input::old('id', isset($postDetails) ? $postDetails->id : null)}}}">
        <?php if ($errors->count() > 0) { ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors->all() as $messages) { ?>
                        <li> <?php echo $messages ?> </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
        @if (Session::has('flash_message'))  
        <div class="alert alert-success">
            <button data-dismiss="alert" class="close">
                ×
            </button>
            <strong>Success!</strong> {{ Session::get('flash_message') }}
        </div>
        @endif 
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="title" class="control-label required">Title</label>
                <div>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" required="required" style="{{($errors->has('title'))?'border:1px solid red;':null}}"
                           value="{{{ Input::old('title', isset($postDetails) ? $postDetails->title : null)}}}">
                </div>

            </div>

            <div class="form-group row">
                <label for="description" class="control-label required">Description</label>
                <div><textarea id="info" name="description" rows="5" cols="52" class="form-control" style="{{($errors->has('description'))?'border:1px solid red;':null}}"
                               required="required" placeholder="Description">{{{Input::old('description',isset($postDetails) ? $postDetails->description : null)}}}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="control-label">Status</label>
                <div>
                    <input type="radio" name="isPublic" value="1" checked <?php
                    if (Input::old('isPublic') == "1") {
                        echo 'checked="checked"';
                    }
                    ?>>&nbsp;Public
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="isPublic" value="0" <?php
                    if (Input::old('isPublic') == "0") {
                        echo 'checked="checked"';
                    }
                    ?><?php if (isset($postDetails)) echo ($postDetails->isPublic) ? '' : "checked" ?>>&nbsp;Private
                </div>
            </div>


            <div class="form-group row">
                <label class="control-label">Image</label>
                <div>
                    <input type="file" class="form-control" onchange="readURL(this, 'image');" name="image"
                           value="{{{Input::old('image',isset($postDetails)?$postDetails->postImage : null)}}}">
                </div>
                <img id="image" style="height:100px; width:100px;" src="{{{isset($postDetails->postImage)?$postDetails->postImage : null}}}"/>
            </div>

            <div class="form-group row">
                <label for="getAllCampaigns" class="control-label">Campaign</label>
                <div>
                    <input type="text" class="form-control" id="getAllCampaigns" name="getAllCampaigns" placeholder="Campaign" style="{{($errors->has('getAllCampaigns'))?'border:1px solid red;':null}}"
                           value="{{{ Input::old('getAllCampaigns', isset($postDetails) ? $postDetails->campaign : null)}}}">
                    <input type="hidden" value="" name="selectedCampaign" id="selectedCampaign">
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-12 row">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>



        </div>
        <div class="col-sm-3"></div>
    </form>


</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onClick="delText()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select Campaign</h4>
            </div>
            <div class="modal-body" id="myDiv">
                <input type="text" class="form-control col-xs-4" style="width:200px; position: relative;" placeholder="Title" name="titleSearch" id="titleSearch" value="{{{ Input::old('title', isset($searchCriteria) ? $searchCriteria['title'] : null)}}}">

                <button class="btn btn-default" id="search" style="position: relative; height: 34px;" type="submit"><i class="glyphicon glyphicon-search"></i></button>


                <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
                    <thead>
                        <tr>
                            <th class="titleClass">Title</th>
                        </tr>
                    </thead>

                    <tbody id="tbody">




                    </tbody>

                </table>
               


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>
    function readURL(input, data) {
        var imageId = '#' + data;
        if (input.files && input.files[0]) {
            var reader = new FileReader();


            reader.onload = function (showImage) {
                $(imageId)
                        .attr('src', showImage.target.result)
                        .width(100)
                        .height(100);
            };

            reader.readAsDataURL(input.files[0]);
        }

    }
   
    

    
    function delText() {
            $("#getAllCampaigns").val("");
            $("#getAllCampaigns").change();
            $("#tbody").empty();
        }


    $(document).ready(function () {
        $("#getAllCampaigns").click(function () {
            $.ajax({
                type: "post",
                url: "{{asset('cms/campaign/getCampaigns')}}",
                data: {'pageNo': 1},
                cache: true,
                success: function (result) {
                    $("#tbody").empty();
                    $("#pagination").empty();
                    var trHTML = '';
                    $.each(result, function (i, item) {
                        trHTML += "<tr><td width='20%' id='myTitleId' class='hide'>" + item.id + "</td><td class='title' id='mytitle" + item.id + "'>" + item.title + "</td></tr>";
                    });
                    $('#mytable').append(trHTML);

                    $('#myModal').modal('show');

                }
            });
        });
    });

    $("#mytable").on("click", 'tr', function () {
        var x = $(this).find(".title").html();
        var y = $(this).find("#myTitleId").html();
        
        $("#selectedCampaign").val(y);
        $("#getAllCampaigns").val(x);
        $('#myModal').modal('hide');
        $("#getAllCampaigns").change();
        
    });



    $(document).ready(function () {
        $("#search").click(function () {
            var title = $("#titleSearch").val();
            $.ajax({
                type: "post",
                url: "{{asset('cms/campaign/searchCampaigns')}}",
                data: {'title': title},
                cache: true,
                success: function (result) {
                    $("#tbody").empty();
                    $("#pagination").empty();
                    var trHTML = '';
                    $.each(result, function (i, item) {
                        trHTML += "<tr><td width='20%' id='myTitleId' class='hide'>" + item.id + "</td><td class='title' id='mytitle" + item.id + "'>" + item.title + "</td></tr>";
                    });

                    $('#mytable').append(trHTML);
                    $('#myModal').modal('show');
                }
            });
        });
    });










</script>
@endsection
