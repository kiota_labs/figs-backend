@extends('app')

@section('content')


<div class="container-fluid">
     <div>
				<div>
        <div class="col-sm-12">                   

            <ol class="breadcrumb" style="height:auto;">
                <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                @if (\Session::has('homePage'))
                <li><i class="fa fa-user"></i><a href="{{ url('/cms/user/viewUserDetails/'.$userId) }}">Flagged User/Group Details</a></li>
                <li><i class="fa fa-file"></i><a href="{{ url('cms/post/viewUserPosts/'.$userId) }}">All Posts</a></li>
                @elseif (\Session::has('user'))
                <li><i class="fa fa-users"></i><a href="{{ url('cms/flaggedUser/getFlaggedUsers') }}">Flagged Users</a></li>
                <li><i class="fa fa-user"></i><a href="{{ url('/cms/user/viewUserDetails/'.$userId) }}">User Details</a></li>
                <li><i class="fa fa-file"></i><a href="{{ url('cms/post/viewUserPosts/'.$userId) }}">User Posts</a></li>
                @elseif(\Session::has('group'))
                <li><i class="fa fa-users"></i><a href="{{ url('cms/flaggedUser/getFlaggedGroups') }}">Flagged Groups</a></li>
                <li><i class="fa fa-user"></i><a href="{{ url('/cms/user/viewFlaggedGroupDetails/'.$userId) }}">Group Details</a></li>
                <li><i class="fa fa-file"></i><a href="{{ url('cms/post/viewUserPosts/'.$userId) }}">Group Posts</a></li>
                @endif
                <li><i class="fa fa-file"></i>Post Details</li>
            </ol>
        </div>
    </div>
			</div>
    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
          accesskey=""   accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="postId" id="postId" value="{{ $postDetails->id }}">
        
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
        <!--<div class="col-sm-3">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#">Actions</a></li>
                <li><a href="{{ url('cms/campaign/index') }}">View Campaigns</a></li> 
            </ul>
        </div> -->
        <div class="col-sm-12">
            <h2>
                @if($postDetails->role == "group")
                <img class="img-circle img-responsive img-thumbnail" style="width:70px; height:70px;" src="{{{isset($postDetails->userImage) ? asset('images/user/thumb/'.$postDetails->userImage): asset(config('constants.groupThumbImage'))}}}"/>
                @else
                <img class="img-circle img-responsive img-thumbnail" style="width:70px; height:70px;" src="{{{isset($postDetails->userImage) ? asset('images/user/thumb/'.$postDetails->userImage): asset(config('constants.userThumbImage'))}}}"/>
                @endif                {{isset($postDetails->fullName)&&!empty($postDetails->fullName)?$postDetails->fullName:$postDetails->userName}}'s Post Details</h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Post Details</b>
                </div>
                <div class="panel-body">
                    
                    <div class="col-md-4 text-center">
                        <img class="img-circle img-responsive img-thumbnail" src="{{{isset($postDetails->image) ? asset('images/post/'.$postDetails->image): asset(config('constants.postDefaultImage'))}}}"/>
                    </div>
                    
                    
                    <div class="col-md-8">
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Title</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->title}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Description</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->description}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Assigned To Campaign</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->sendToCampaignTitle}}</div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Tapps Received</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->tappsReceived}}</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Create Date</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{date('M j Y ',strtotime($postDetails->createDate))}}</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Status</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->isPublic?'Private':'Public'}}</div>
                    </div>
                         <div class="form-group row" style="text-align:center;">
                        
                        <a class="btn btn-success" id="enable{{$postDetails->id}}" data-id="{{$postDetails->id}}" href="#" data-toggle="modal" data-target="#myModalActivation" style="{{ ($postDetails->isDisabled) ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} padding:5px;">Unhide</a>
                        
                        
                        <a class="btn btn-danger" id="disable{{$postDetails->id}}" data-id="{{$postDetails->id}}" href="#" data-toggle="modal" data-target="#myModal" style="{{ ($postDetails->isDisabled) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}} padding:5px;">Hide</a>
                       
                    </div>
                   
                
                </div>
                </div>
            </div>
           
        </div>



     

    </form>
</div>

 <div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Hide Post</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                    <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                    <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Please enter reason for hiding this post">
                    <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Cancel</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">

            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Unhide</h4>
            </div>
            <div class="modal-body">
                <p>Do You Want To Unhide This Post</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
    function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
    $(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#disableReason').val();
            var id = $('#dialogClubId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/post/disable')}}",
                type: 'POST',
                data: $('#disableReasonForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#disable' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#enable' + id).css({'display': 'inline', 'visibility': 'visible'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                }
            });
        });
    });


    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/post/enable')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
                $('#disable' + userId).css({'display': 'inline', 'visibility': 'visible'});
                $('#enable' + userId).css({'display': 'none', 'visibility': 'hidden'});

            }
        });
    }


</script>

    
@endsection




