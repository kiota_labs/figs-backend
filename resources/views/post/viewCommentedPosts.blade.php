@extends('app')

@section('content')
@if (Session::has('flash_message'))  
<div class="alert alert-success">
    <button data-dismiss="alert" class="close">
        ×
    </button>
    <strong>Success!</strong> {{ Session::get('flash_message') }}
</div>
@endif 

<div class="container">
    <div>
        <div class="col-sm-12">
            <ol class="breadcrumb" style="height:auto;">
                <li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                @if(\Session::has('homePage'))
                <li><i class="fa fa-user"></i><a href="{{ url('/cms/user/viewUserDetails/'.$userId) }}">Flagged User/Group Details</a></li>
                <li><i class="fa fa-file"></i>Commented Posts</li>
                @elseif (\Session::has('user'))
                <li><i class="fa fa-users"></i><a href="{{ url('cms/flaggedUser/getFlaggedUsers') }}">Flagged Users</a></li>
                <li><i class="fa fa-user"></i><a href="{{ url('/cms/user/viewUserDetails/'.$userId) }}">User Details</a></li>
                <li><i class="fa fa-file"></i>User Commented Posts</li>
                @elseif (\Session::has('group'))
                <li><i class="fa fa-users"></i><a href="{{ url('cms/flaggedUser/getFlaggedGroups') }}">Flagged Groups</a></li>
                <li><i class="fa fa-user"></i><a href="{{ url('/cms/user/viewFlaggedGroupDetails/'.$userId) }}">Group Details</a></li>
                <li><i class="fa fa-file"></i>Group Commented Posts</li>
                @endif
            </ol>
        </div>
    </div>
    <div>
        @extends('templates.sidebar')
        @section('sidebar')  
        @endsection

        <div class="col-md-12">

            @if (count($postDetails) > 0)
            <h2>
                @if($postDetails->role == "group")
                <img class="img-circle img-responsive img-thumbnail" style="width:70px; height:70px;" src="{{{isset($postDetails->userImage) ? asset('images/user/thumb/'.$postDetails->userImage): asset(config('constants.groupThumbImage'))}}}"/>
                @else
                <img class="img-circle img-responsive img-thumbnail" style="width:70px; height:70px;" src="{{{isset($postDetails->userImage) ? asset('images/user/thumb/'.$postDetails->userImage): asset(config('constants.userThumbImage'))}}}"/>
                @endif                {{isset($postDetails->fullName)&&!empty($postDetails->fullName)?$postDetails->fullName:$postDetails->userName}}'s Commented Posts</h2> 
            <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
                <thead>
                    <tr>
                        <th class="text-center tdHeader">Title</th>
                        <th class="text-center tdHeader">Assigned To</th>
                        <th class="text-center tdHeader">Tapps Received</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($postDetails as $postDetail)
                    
                    <tr>
                        <td style="vertical-align: middle;">
                        <a href="{{url('/cms/post/userCommentedPostDetails/'.$userId.'/'.$postDetail->id)}}">   
                        <img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($postDetail->image) ? asset('images/post/thumb/'.$postDetail->image): asset(config('constants.postDefaultImage'))}}}"/>
                        </a>
                        <a href="{{url('/cms/post/userCommentedPostDetails/'.$userId.'/'.$postDetail->id)}}">{{ $postDetail->title }}</a></td>
                        <td class="text-center" style="vertical-align: middle;">{{ $postDetail->sendToCampaignTitle?$postDetail->sendToCampaignTitle:'Not Assigned' }}</td>
                        <td class="text-center" style="vertical-align: middle;">{{ $postDetail->tappsReceived }}</td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
            <div class="pull-right">
            <?php echo $postDetails->appends(Request::input())->render(); ?>
            </div>
            @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> No records found for Commented Posts.<br><br>
            </div>
            @endif
        </div>

    </div>
</div>

<script>
     $(document).ready(function ()
    {
        $("#mytable").tablesorter();
    }
    );
</script>

@endsection


