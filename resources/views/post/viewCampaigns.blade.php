@extends('app')

@section('content')
@if (Session::has('flash_message'))  
<div class="alert alert-success">
    <button data-dismiss="alert" class="close">
        ×
    </button>
    <strong>Success!</strong> {{ Session::get('flash_message') }}
</div>
@endif 

<div class="container">
      <div class="row">
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                @if(\Session::has('homePage'))
                                                <li><i class="fa fa-user"></i><a href="{{ url('/cms/user/viewUserDetails/'.$userId) }}">Flagged User/Group Details</a></li>
                                                <li><i class="fa fa-file"></i>Campaigns</li>
                                                @elseif(\Session::has('user'))
                                                <li><i class="fa fa-users"></i><a href="{{ url('cms/flaggedUser/getFlaggedUsers') }}">Flagged Users</a></li>
                                                <li><i class="fa fa-user"></i><a href="{{ url('/cms/user/viewUserDetails/'.$userId) }}">User Details</a></li>
                                                <li><i class="fa fa-file-o"></i>User Campaigns</li>
                                                @elseif(\Session::has('group'))
                                                <li><i class="fa fa-users"></i><a href="{{ url('cms/flaggedUser/getFlaggedGroups') }}">Flagged Groups</a></li>
                                                <li><i class="fa fa-user"></i><a href="{{ url('/cms/user/viewFlaggedGroupDetails/'.$userId) }}">Group Details</a></li>
                                                <li><i class="fa fa-file-o"></i>Group Campaigns</li>
                                                @endif
					</ol>
				</div>
			</div>
    <div class="row">
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
       
        <div class="col-md-12">

            @if (count($postDetails) > 0)
            <h2>
                @if($postDetails->role == "group")
                <img class="img-circle img-responsive img-thumbnail" style="width:70px; height:70px;" src="{{{isset($postDetails->userImage) ? asset('images/user/thumb/'.$postDetails->userImage): asset(config('constants.groupThumbImage'))}}}"/>
                @else
                <img class="img-circle img-responsive img-thumbnail" style="width:70px; height:70px;" src="{{{isset($postDetails->userImage) ? asset('images/user/thumb/'.$postDetails->userImage): asset(config('constants.userThumbImage'))}}}"/>
                @endif                {{isset($postDetails->fullName)&&!empty($postDetails->fullName)?$postDetails->fullName:$postDetails->userName}}'s Campaigns</h2> 
            <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
                <thead>
                    <tr>
                        <th class="text-center tdHeader">Title</th>
                        <th class="text-center tdHeader">Goal Amount</th>
                        <th class="text-center tdHeader">Tapps Received</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($postDetails as $post)
                    
                    <tr>
                        <td style="vertical-align: middle;">
                        <a href="{{url('/cms/campaign/userCampaignDetails/'.$userId.'/'.$post->id)}}">   
                        <img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($post->image) ? asset('images/post/thumb/'.$post->image): asset(config('constants.postDefaultImage'))}}}"/>
                        </a>
                        <a href="{{url('/cms/campaign/userCampaignDetails/'.$userId.'/'.$post->id)}}">{{ $post->title }}</a></td>
                        <td class="text-center" style="vertical-align: middle;">{{ $post->tappsAssigned }}</td>
                        <td class="text-center" style="vertical-align: middle;">{{ $post->tappsReceived }}</td>
                       
                    </tr>
                    @endforeach
                </tbody>

            </table>
            <div class="pull-right">
            <?php echo $postDetails->appends(Request::input())->render(); ?>
            </div>
            @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> No records found for Campaigns.<br><br>
            </div>
            @endif
        </div>
    </div>
</div>
<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Hide Campaign</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Please enter reason for hiding this campaign">
                <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Cancel</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">
                
            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Unhide</h4>
            </div>
            <div class="modal-body">
                <p>Do You Want To Unhide This Campaign</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
   $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
$(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#disableReason').val();
            var id = $('#dialogClubId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/post/disable')}}",
                type: 'POST',
                data: $('#disableReasonForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#disable' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#enable' + id).css({'display': 'inline', 'visibility': 'visible'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                }
            });
        });
    });


    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/post/enable')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
               $('#disable' + userId).css({'display': 'inline', 'visibility': 'visible'});
                $('#enable' + userId).css({'display': 'none', 'visibility': 'hidden'});

            }
        });
    }
    
     $(document).ready(function ()
    {
        $("#mytable").tablesorter();
    }
    );


</script>
@endsection
