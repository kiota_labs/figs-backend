@extends('app')

@section('content')

<div class="container-fluid">
     <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-group"></i>All Posts</li>
					</ol>
				</div>
			</div>
    @extends('templates.sidebar')
        @section('sidebar')
        @endsection
    <div>
        <div class="col-md-12">

            @if (count($posts) > 0)
            <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
                <thead>
                    <tr>
                        <th class="text-center tdHeader"><span>Title</span></th>
                        <th class="text-center tdHeader"><span>Assigned To</span></th>
                        <th class="text-center tdHeader"><span>Tapps Received</span></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                    
                    <tr>
                        <td style="vertical-align: middle;">
                            <a href="{{url('/cms/post/postDetails/'.$post->id)}}"><img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($post->image) ? asset('images/post/thumb/'.$post->image): asset(config('constants.postDefaultImage'))}}}"/></a>
                            <a href="{{url('/cms/post/postDetails/'.$post->id)}}">{{ $post->title }}</a></td>
                        <td class="text-center" style="vertical-align: middle;">{{ $post->sendToCampaignTitle?$post->sendToCampaignTitle:'Not Assigned' }}</td>
                        <td class="text-center" style="vertical-align: middle;">{{ $post->tappsReceived }}</td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
            <div class="pull-right">
            <?php echo $posts->appends(Request::input())->render(); ?>
            </div>
            @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> No records found for Posts.<br><br>
            </div>
            @endif
        </div>
    </div>
</div>

<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Hide campaign</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Please enter reason for hiding this campaign">
                <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Cancel</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">
                
            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Activation</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to unhide this campaign</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
   $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
$(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#disableReason').val();
            var id = $('#dialogClubId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/post/disable')}}",
                type: 'POST',
                data: $('#disableReasonForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#deact_' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#act_' + id).css({'display': 'inline', 'visibility': 'visible'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                }
            });
        });
    });


    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/post/enable')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
               $('#deact_' + userId).css({'display': 'inline', 'visibility': 'visible'});
                $('#act_' + userId).css({'display': 'none', 'visibility': 'hidden'});

            }
        });
    }
    $(document).ready(function ()
    {
        $("#mytable").tablesorter();
    }
    );


</script>

@endsection
