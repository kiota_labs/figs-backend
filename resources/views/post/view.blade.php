@extends('app')

@section('content')


<div class="container-fluid">
     <div>
				<div class="col-sm-12">
					<ol class="breadcrumb" style="height:auto;">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                @if(\Auth::user()->role == 'group')
                                                <li><i class="fa fa-group"></i><a href="{{ url('cms/campaign/index') }}">All Campaigns</a></li>
                                                @elseif(\Auth::user()->role == 'admin')
                                                <li><i class="fa fa-group"></i><a href="{{ url('cms/user/itemRedeemRequests') }}">All Redeem Requests</a></li>
                                                @endif
                                                <li><i class="fa fa-home"></i>Campaign Details</li>
					</ol>
				</div>
			</div>
    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
          accesskey=""   accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @extends('templates.sidebar')
        @section('sidebar')
        @endsection
        <!--<div class="col-sm-3">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#">Actions</a></li>
                <li><a href="{{ url('cms/campaign/index') }}">View Campaigns</a></li> 
            </ul>
        </div> -->
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Campaign Details</b>
                </div>
                <div class="panel-body">
                    
                    <div class="col-md-4 text-center">
                        <img class="img-circle img-responsive img-thumbnail" src="{{{isset($postDetails->image) ? $postDetails->image : asset(config('constants.postDefaultImage'))}}}"/>
                    </div>
                    
                    
                    <div class="col-md-8">
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Title</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->title}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Description</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->description}}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Goal Amount</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->tappsAssigned}}</div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Tapps Received</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->tappsReceived}}</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Create Date</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{date('M j Y ',strtotime($postDetails->createDate))}}</div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">End Date</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->endDate}}</div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 col-xs-4 text-right">Status</div>
                        <div class="col-sm-6 col-xs-8 text-left">{{$postDetails->isPublic?'Private':'Public'}}</div>
                    </div>
                    
                        @if(\Auth::user()->role == 'group')
                    <div class="form-group row text-right">
                       <a class="btn btn-success" id="enable{{$postDetails->id}}" data-id="{{$postDetails->id}}" href="#" data-toggle="modal" data-target="#myModalActivation" style="{{ ($postDetails->isClosed) ? 'visibility:visible; display:inline;' : 'visibility:hidden; display:none;'}} padding:5px;">Open</a>
                            <a class="btn btn-danger" id="disable{{$postDetails->id}}" data-id="{{$postDetails->id}}" href="#" data-toggle="modal" style="{{ ($postDetails->isClosed) ? 'visibility:hidden; display:none;' : 'visibility:visible; display:inline;'}} padding:5px;" data-target="#myModal">Close</a>        
                    </div>
                        @endif
                </div>
                   </div>
            </div>
        </div>



     

    </form>
</div>

<div id="myModal" class="modal fade in" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Close Campaign</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="disableReasonForm" id="disableReasonForm">
                <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                <input type="text" class="form-control" id="disableReason" name="disableReason" placeholder="Please enter reason for closing this campaign">
                <input type="hidden" value="" name="userId" id="dialogClubId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Cancel</button>
                <input type="submit" class="btn btn-success" id="disableSubmit" value="Submit">
                
            </div>
        </div>

    </div>
</div>
<div id="myModalActivation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Open</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to open this campaign</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-default" onClick="Activate()">Submit</button>
                <input type="hidden" value="" name="userId" id="dialogActivateUserId">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
   $('#myModal').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogClubId').val(e.relatedTarget.dataset.id);
    });

    $('#myModalActivation').on('show.bs.modal', function (e) {
        //alert(e.relatedTarget.dataset.id);
        $('#dialogActivateUserId').val(e.relatedTarget.dataset.id);
    });
function delText() {
        $('.myForm').trigger('reset');
        $('.myLabel').html('');
    }
$(document).ready(function () {
        $("#disableSubmit").click(function () {
            var data = $('#disableReason').val();
            var id = $('#dialogClubId').val();
            var searchReg = /^[ Xa-zA-Z0-9-]+$/;
            if (data == '')
            {
                $("#modalLabel").html('* please fill reason to disable');
                return false;
            } else if (!searchReg.test(data)) {
                $("#modalLabel").html("* please enter valid Reason");
                return false;
            }
            $.ajax({
                url: "{{asset('cms/campaign/closeCampaign')}}",
                type: 'POST',
                data: $('#disableReasonForm').serialize(),
                success: function (response) {
                    $('#myModal').modal('hide');
                    $('#disable' + id).css({'display': 'none', 'visibility': 'hidden'});
                    $('#enable' + id).css({'display': 'inline', 'visibility': 'visible'});
                    $('.myLabel').html('');
                    $('.myForm').trigger('reset');
                    $('#ableStatus').html('Inactive');
                }
            });
        });
    });


    function Activate() {

        var userId = document.getElementById('dialogActivateUserId').value;

        $.ajax({
            type: "post",
            url: "{{asset('cms/campaign/openCampaign')}}",
            data: {'userId': userId},
            cache: false,
            success: function (result) {
                $('#myModalActivation').modal('hide');
                $('#disable' + userId).css({'display': 'inline', 'visibility': 'visible'});
                $('#enable' + userId).css({'display': 'none', 'visibility': 'hidden'});
                $('#ableStatus').html('Active');
            }
        });
    }


</script>

    
@endsection




