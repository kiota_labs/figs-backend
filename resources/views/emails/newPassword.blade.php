<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Fig Password</title>
    </head>

    <body style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0; background-color:#F0EDE4;" cz-shortcut-listen="true">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#F0EDE4;">
            <tbody><tr>
                    <td align="center">
                        <table width="600" border="0" cellspacing="0" cellpadding="0" style="background-color:#F0EDE4;">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <img src="{{asset('images/logo_fig.png')}}" alt="Fig" border="0" style="display:block; padding: 40px;">
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="600" height="108" border="0" cellspacing="0" cellpadding="0" style="background-color:#4C4D4F; margin-bottom:50px;">
                            <tbody><tr>
                                    <td align="center">
                                        <table width="600" height="88" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td style="text-align:left;padding-left:40px;padding-top:40px;padding-bottom:40px;padding-right:40px;">
                                                        <p style="font-family:Helvetica, Arial;font-size:12px;color:#fefefe;line-height:18px; text-align:left;">
                                                            <b>Hi,</b><br/>
                                                            Your password has been reset.<br/>
                                                            Your new password is <b>{{$password}}</b>.<br/><br/>
                                                            Please change your password after logging in.<br/><br/>
                                                            
                                                            Regards,<br/>
                                                            Fig Support Team
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>