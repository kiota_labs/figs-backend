@extends('app')

@section('content')

<div class="container-fluid">
    <div>
        @if(\Auth::user()->role == "admin")
         <div>
				<div class="col-sm-12">
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-group"></i><a href="{{ url('cms/user/viewGroups') }}">All Groups</a></li>
                                                <li><i class="fa fa-info"></i><a href="{{ url('/cms/user/viewGroupDetails/'.$id) }}">Group Details</a></li>
						<li><i class="fa fa-users"></i>Group Followers</li>                                                
					</ol>
				</div>
			</div>
        @extends('templates.sidebar')
        @section('sidebar')
        <li class="sub-menu">
                    <a href="{{ url('/cms/user/viewGroupDetails/'.$id) }}" class="">
                        <i class="icon_document_alt"></i>
                        <span>Group Details</span>
                    </a>
                </li> 
        @endsection
         <!--<div class="col-sm-3">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#">Actions</a></li>
                <li><a href="{{ url('/cms/user/viewGroupDetails/'.$id) }}">Group Information</a></li>
            </ul>
        </div> -->
        <div class="col-sm-12">
        <form class="form-inline" style="margin-bottom:9px;" method="POST" enctype="multipart/form-data" action="{{URL::to('/cms/groupFollower/searchGroupFollowers/'.$id)}}">

            @elseif(\Auth::user()->role == "group")
            <div>
				<div class="col-sm-12">
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="{{url('/')}}">Home</a></li>
                                                <li><i class="fa fa-users"></i>Group Followers</li>                                                
					</ol>
				</div>
			</div>
            <div class="col-sm-12">
                
            <form class="form-inline" style="margin-bottom:9px;" method="POST" enctype="multipart/form-data" action="{{URL::to('cms/groupFollower/search')}}">
                @endif   
                <select name="gender" id='gender' class="form-control col-xs-4" style="width: 140px; position: relative;">
                        <option value="">Select Gender</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select> 
                    <select name="age" id='age' class="form-control col-xs-4" style="width: 140px; position: relative;">
                        <option value="">Select Age</option>
                        <option value="10-25">10-25</option>
                        <option value="26-40">26-40</option>
                        <option value="41-55">41-55</option>
                        <option value="56-70">56-70</option>
                        <option value="71-85">71-85</option>
                        <option value="86-100">86-100</option>
                    </select>
                <input type="hidden" id="genderHide" value="{{isset($searchCriteria['gender'])?$searchCriteria['gender']:""}}">
                <input type="hidden" id="ageHide" value="{{isset($searchCriteria['age'])?$searchCriteria['age']:""}}">

                <button class="btn btn-default" style="position: relative; height:34px;" type="submit"><i class="glyphicon glyphicon-search"></i></button>

            </form>

            @if (count($followers) > 0)
            <table class="table table-striped table-bordered table-hover table-responsive" id="mytable">
                <thead>
                    <tr>
                        <th class="text-center tdHeader"><span>Name</span></th>
                        <th class="text-center tdHeader"><span>Gender</span></th>
                        <th class="text-center tdHeader"><span>D.O.B</span></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($followers as $follower)
                    
                    <tr>
                        <td style="vertical-align: middle;"><img class="img-circle img-responsive img-thumbnail imageSize" src="{{{isset($follower->image) ? asset('images/user/thumb/'.$follower->image): asset(config('constants.userDefaultImage'))}}}"/>
                        {{ $follower->fullName }}</td>
                        <td class="text-center" style="vertical-align: middle;">{{ $follower->gender }}</td>
                        <td class="text-center" style="vertical-align: middle;">{{ $follower->dob }}</td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
            <?php echo $followers->appends(Request::input())->render(); ?>
            @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Currently no group followers.<br><br>
            </div>
            @endif
        </div>
    </div>
</div>
</div>
    <script>
        $(document).ready(function(){
    $("#gender").val($("#genderHide").val());
    $("#age").val($("#ageHide").val());
});

 $(document).ready(function ()
    {
        $("#mytable").tablesorter();
    }
    );
        
        </script>
        


@endsection
