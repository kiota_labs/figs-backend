<?php namespace App\Http\Controllers;
use App\Models\FlaggedUser;
use App\Models\User;
use App\Models\Post;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\Settings;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
            Session::put('homePage','homePage');
            
            if(\Auth::user()->role == "admin"){
            $flaggedUsers = DB::table('user')
               ->join('flaggedUser','flaggedUser.flaggedUserId','=','user.id')
               ->select('flaggedUser.flaggedUserId','user.id','user.fullName','user.image','user.isDisabled','user.username','user.role')
               ->whereIn('user.role',['group','user'])
               ->groupBy('user.id')
               ->orderBy('flaggedUser.createDate')
               ->take(5)
               ->get();
       
       
       
            foreach($flaggedUsers as $flaggedUser){
            $flaggedUser->userDetails = DB::table('flaggedUser')
                 ->join('user as u2','flaggedUser.flaggingUserId','=','u2.id')
                 ->select("flaggedUser.createDate AS date",'flaggedUser.flaggedUserId','flaggedUser.flaggingUserId','flaggedUser.flagReason','u2.fullName as flaggedBy','u2.image as flaggedByImage','u2.userName as flaggedByUserName','u2.role as flaggedByRole')
                 ->where('flaggedUser.flaggedUserId',$flaggedUser->flaggedUserId)
                 ->orderBy('u2.id')
                 ->get();
            } 
            
            $users = User::where('role','group')->where('approvalStatus','=',0)->orderBy('createDate','desc')->take(5)->get();
            
		return view('home')->with('flaggedUsers',$flaggedUsers)->with('users',$users);
            }else{
                $posts = DB::table('post as p')
                    ->leftjoin('post as p2','p.sendToCampaignId','=','p2.id')
                    ->select('p.title as title','p.id as id','p.description as description','p.image as image','p.isDisabled as isDisabled','p.tappsReceived as tappsReceived','p2.title as sendToCampaignTitle')
                    ->whereIn('p.postType',array(1,3,4))
                    ->where('p.userId',\Auth::user()->id)
                    ->take(5)
                    ->orderBy('p.id','desc')
                    ->get();
                
                $campaigns = Post::where('postType', '=', 2)
                            ->where('userId',\Auth::user()->id)
                            ->orderBy('id','desc')->take(5)->get();
                return view('home')->with('posts',$posts)->with('campaigns',$campaigns);
            }
	}
        
        public function tappConversionValue(){
        $currentTappValue = Settings::orderBy('id','desc')->first();
        $tappValue = 1;
        if(!is_null($currentTappValue)){
            $tappValue=$currentTappValue->tappValue;
        }
        $tappValueHistory = Settings::orderBy('id','desc')->take(5)->get();
            return view('tappConversion.tappConversionValue')->with('tappValue',$tappValue)->with('tappValueHistory',$tappValueHistory);
        }
        
        public function addConversionValue(){
            
        $tappValue = input::all();
        
        $messages = [
        'newTappValue.required' => 'The :attribute field is required.',
        'newTappValue.numeric' => 'The :attribute field should be numeric.'
         ];

        $rules = array(
            'newTappValue' => array('required', 'numeric')
        );
        

        $validator = Validator::make($tappValue, $rules,$messages);
 
        if ($validator->fails()) {
            return redirect::to('cms/user/tappConversionValue')->withErrors($validator);
        } else{
            
            $value = input::get('newTappValue');
             
            $settings = new Settings();
            $settings->tappValue = $value;
            $settings->save();
            
            return redirect::to('cms/user/tappConversionValue')->with('flash_message', 'Tapp Conversion Value successfully changed.');
            
        } 
        }

}
