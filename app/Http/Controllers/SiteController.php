<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class SiteController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Site Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders website pages
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	/**
	 * Show the application homepage to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        return view('site.index');
	}

    /**
     * Show the application homepage to the user.
     *
     * @return Response
     */
    public function home()
    {
        return view('site.home');
    }

	/**
     * Show the application About Us feature.
     *
     * @return Response
     */
    public function aboutUs()
    {
        return view('site.aboutUs');
    }


    /**
     * Show the application termsOfService.
     *
     * @return Response
     */
    public function termsOfService()
    {
        return view('site.termsOfService');
    }

	 /**
     * Show the application Privacy Policy.
     *
     * @return Response
     */
    public function privacyPolicy()
    {
        return view('site.privacyPolicy');
    }
    
     /**
     * Show the application my profile.
     *
     * @return Response
     */
    public function myProfile()
    {
        return view('site.myProfile');
    }
	
}
