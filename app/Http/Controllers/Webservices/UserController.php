<?php namespace App\Http\Controllers\Webservices;

use \Illuminate\Support\Facades\Request;
use \Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Password;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Middleware\VkUtility;
use App\Models\User;
use App\Models\UserSession;
use App\Models\UserAddress;
use App\Models\Event;
use App\Models\Notification;
use App\Models\UserPassword;

class UserController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    |
    | This controller provides services to process user records
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Check uniqeness of given email
     *
     * @return Response
     */
    public function validateEmail()
    {
        $apiName = 'user/validateEmail';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'email' => array('required', 'email', 'unique:user,email')
        );
        $validator = Validator::make($_JSON, $validationRules);

        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else
            $result = VkUtility::success($apiName, 'Email is unique');
        
        return Response::json($result);
    }
    
    /**
     * Register a user and save record in database
     *
     * @return Response
     */
    public function signup()
    {
        $apiName = 'user/signup';
        $deviceToken = '';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'email' => array('required', 'email', 'unique:user,email'),
            'fullName' => array('required', 'string'),
            'password' => array('required'),
            'phone' => array('string'),
            'referralCodeUsed' => array('string', 'exists:user,referralCode'),
            'deviceToken' => array('string'),
        );
        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else
        {
            $user = new User();
            $user->email = $_JSON['email'];
            $user->password = Hash::make($_JSON['password']);
            $user->fullName = $_JSON['fullName'];
            
            if(isset($_JSON['phone']) && !empty($_JSON['phone']))
                $user->phone = $_JSON['phone'];
            
            if(isset($_JSON['referralCodeUsed']) && !empty($_JSON['referralCodeUsed']))
                $user->referralCodeUsed = $_JSON['referralCodeUsed'];
            
            $user->role = 'user';
            
            //save image
            if(isset($_JSON['image']) && !empty($_JSON['image']))
            {
                $imageDir = public_path() . config('constants.userImage');
                $thumbDir = public_path() . config('constants.userThumb');
                $maxWidth = config('constants.thumbMaxWidth');
                $user->image = VkUtility::saveBase64ImagePng($_JSON['image'], $imageDir, $thumbDir, $maxWidth);
            }
            
            $user->save();

            //generate user's referralCode
            $userIdHex = dechex($user->id);
            $user->referralCode = VkUtility::randomStringAlphpNum(8 - strlen($userIdHex), $userIdHex);
            $user->save();

            //save user password in password history
            $userPassword = new UserPassword();
            $userPassword->userId = $user->id;
            $userPassword->password = $user->password;
            $userPassword->save();
            
            //if referralCodeUsed exists, get referral benefits
            if($user->referralCodeUsed)
                User::getReferralBenefits($user->id, $user->referralCodeUsed);
            
            if(isset($_JSON['deviceToken']))
                $deviceToken = $_JSON['deviceToken'];

            $result = VkUtility::success($apiName, 'User registered successfully');
            $result['user'] = User::getUserById($user->id, 1);
            $result['sessionId'] = $this->login($user->id, 'user', $deviceToken);
        }
        
        return Response::json($result);
    }
    
    /**
     * Check user email and password and allow to login if the credentials correct
     *
     * @return Response
     */
    public function signin()
    {
        $apiName = 'user/signin';
        $deviceToken = '';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'email' => array('required'),   /* input can be an email or a username  --, 'email', 'exists:user,email,isDisabled,0'*/
            'password' => array('required'),
            'deviceToken' => array('string')
        );

        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails())
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($user = User::getUserByEmailOrUsername($_JSON['email'], 0, 1)))
            $result = VkUtility::error($apiName, 99, 'Incorrect username or password. Please check again.');
        else if($user->role != 'user')
            $result = VkUtility::error($apiName, 99, 'Please login on web-portal.');
        else if(!empty($user->facebookId))
            $result = VkUtility::error($apiName, 99, 'Please login via Facebook.');
        else if(!Hash::check($_JSON['password'], $user->password))
            $result = VkUtility::error($apiName, 99, 'Incorrect username or password. Please check again.');
        else
        {
            if(isset($_JSON['deviceToken']))
                $deviceToken = $_JSON['deviceToken'];

            $result = VkUtility::success($apiName, 'User signed in successfully');
            unset($user->password);
            $result['user'] = $user;
            $result['sessionId'] = $this->login($user->userId, 'user', $deviceToken);
        }

        return Response::json($result);
    }
    
    /**
     * Check user facebookId and allow to login if the id correct
     *
     * @return Response
     */
    public function signinFb()
    {
        $apiName = 'user/signinFb';
        $deviceToken = '';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'facebookId' => array('required', 'alpha_num'),
            'email' => array('required', 'email'),
            'fullName' => array('required', 'string'),
            'phone' => array('string'),
            'referralCodeUsed' => array('string', 'exists:user,referralCodeUsed'),
            'deviceToken' => array('string'),
        );

        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else
        {
            $isNewUser = false;
            $user = User::where('facebookId', $_JSON['facebookId'])->first();
            $emailUser = User::getUserByEmailOrUsername($_JSON['email']);
            
            if( (is_null($user) || empty($user)) &&  !is_null($emailUser))
            {
                $result = VkUtility::error($apiName, 102, 'Selected email is already registered.');
                $result['fullName'] = $emailUser->fullName;
                $result['email'] = $emailUser->email;
                $result['image'] = $emailUser->image;
                $result['thumb'] = $emailUser->thumb;
            }
            else
            {
                if(is_null($user) || empty($user))
                {
                        
                    $isNewUser = true;
                    $user = new User();
                    $user->facebookId = $_JSON['facebookId'];
                    $user->role = 'user';
                    $user->email = $_JSON['email'];
                    $user->fullName = $_JSON['fullName'];

                    if(isset($_JSON['phone']) && !empty($_JSON['phone']))
                        $user->phone = $_JSON['phone'];

                    if(isset($_JSON['referralCodeUsed']) && !empty($_JSON['referralCodeUsed']))
                            $user->referralCodeUsed = $_JSON['referralCodeUsed'];

                    //save image
                    if(isset($_JSON['image']) && !empty($_JSON['image']))
                    {
                        $imageDir = public_path() . config('constants.userImage');
                        $thumbDir = public_path() . config('constants.userThumb');
                        $maxWidth = config('constants.thumbMaxWidth');
                        $user->image = VkUtility::saveBase64ImagePng($_JSON['image'], $imageDir, $thumbDir, $maxWidth);
                    }
                    else if(isset($_JSON['imageUrl']) && !empty($_JSON['imageUrl']))
                    {
                        $imageUrl = filter_var($_JSON['imageUrl'], FILTER_SANITIZE_URL);
                        $imageDir = public_path() . config('constants.userImage');
                        $thumbDir = public_path() . config('constants.userThumb');
                        $maxWidth = config('constants.thumbMaxWidth');
                        $user->image = VkUtility::saveImageFromUrl($imageUrl, $imageDir, $thumbDir, $maxWidth);
                    }

                    //set username
                    $username = ($user->fullName) ? preg_replace('/[^A-Za-z0-9\-]/', '', $user->fullName) : 'FB';
                    $username .= '_' . VkUtility::randomStringNum(5);
                    $user->username = $username;

                    $user->save();

                    //generate user's referralCode
                    $userIdHex = dechex($user->id);
                    $user->referralCode = VkUtility::randomStringAlphpNum(8 - strlen($userIdHex), $userIdHex);
                    $user->save();
                    
                    //if referralCodeUsed exists, get referral benefits
                    if($user->referralCodeUsed)
                        User::getReferralBenefits($user->id, $user->referralCodeUsed);
                }

                //fetch user details in proper format and updated data
                $user = User::getUserByFacebookId($_JSON['facebookId'], 1);

                if(isset($_JSON['deviceToken']))
                    $deviceToken = $_JSON['deviceToken'];

                $result = VkUtility::success($apiName, 'User signed in successfully');
                $result['user'] = $user;
                $result['sessionId'] = $this->login($user->userId, 'user', $deviceToken);
                $result['isNewUser'] = $isNewUser;
            }
        }
        return Response::json($result);
    }
    
    /**
     * Logout user and terminate current session
     *
     * @return Response
     */
    public function logout()
    {
        $apiName = 'user/logout';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required')
        );

        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails())
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else
        {
            $userSession->delete();
            $result = VkUtility::success($apiName, 'User logged out successfully');
        }
        return Response::json($result);
    }
    
    /**
     * Reset user's password and send email to user
     *
     * @return Response
     */
    public function resetPassword()
    {
        $apiName = 'user/resetPassword';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'email' => array('required', 'email')
        );
        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if (is_null($user = User::where('email', $_JSON['email'])->first())) 
            $result = VkUtility::error($apiName, 99, 'Selected email does not exist.');
        else
        {
           // $passwordBroker = new \Illuminate\Auth\Passwords\PasswordBroker();
           // $response = $passwordBroker->sendResetLink(['email'=>$user->email], function($m)
           // {
           //     $m->subject($this->getEmailSubject());
           // });

           // switch ($response)
           // {
           //     case PasswordBroker::RESET_LINK_SENT:
           //         $result = VkUtility::success($apiName, 'Please check your email. A link is sent to you to reset your password.');

           //     case PasswordBroker::INVALID_USER:
           //         $result = VkUtility::error($apiName, 99, 'Selected email does not exist.');
           // }


            $password = VkUtility::randomString(6);
            $user->password = Hash::make($password);    //VkUtility::getEncryptedPassword($password, $_JSON['email']);
            $user->save();

            UserSession::where('userId', $user->id)->delete();

            $result = VkUtility::success($apiName, 'User password reset successfully');

            //send email
            $email = $_JSON['email'];
            $data = ['password'=>$password];
            \Mail::send('emails.newPassword', $data, function($message) use ($email)
            {
                $message->from('support@fig.com', 'Fig');
                $message->to($email);
                $message->subject('Fig: New password');
            });
        }
        return Response::json($result);
    }
    
    /**
     * Update user profile details
     *
     * @return Response
     */
    public function updateProfile()
    {
        $apiName = 'user/updateProfile';
        $_JSON = VkUtility::getJsonInput();
        
        $validationRules = array(
            'sessionId' => array('required'),
            'fullName' => array('string'),
            'phone' => array('string'),
            'referralCodeUsed' => array('string', 'exists:user,referralCode'),
        );
        $validator = Validator::make($_JSON, $validationRules);

        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($user = User::where('id', $userSession->userId)->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else
        {
            if(isset($_JSON['fullName']) && !empty($_JSON['fullName']))
                $user->fullName = $_JSON['fullName'];
            else
                $user->fullName = '';
            
            if(isset($_JSON['phone']) && !empty($_JSON['phone']))
                $user->phone = $_JSON['phone'];
            else
                $user->phone = '';
            
            if(isset($_JSON['image']) && !empty($_JSON['image']))
            {
                $imageDir = public_path() . config('constants.userImage');
                $thumbDir = public_path() . config('constants.userThumb');
                $maxWidth = config('constants.thumbMaxWidth');
                $user->image = VkUtility::saveBase64ImagePng($_JSON['image'], $imageDir, $thumbDir, $maxWidth);
            }
            
            //if referralCodeUsed exists, get referral benefits
            if(isset($_JSON['referralCodeUsed']) && !empty($_JSON['referralCodeUsed']) && empty($user->referralCodeUsed))
            {
                $user->referralCodeUsed = $_JSON['referralCodeUsed'];
                User::getReferralBenefits($user->id, $user->referralCodeUsed);
				
				$result = VkUtility::success($apiName, 'User record saved successfully');
            }
			else{
				$result = VkUtility::success($apiName, 'User record saved successfully, but Referral Code Not Acceptable');
			}
                
            $user->save();

            //$result = VkUtility::success($apiName, 'User record saved successfully');
            $result['user'] = User::getUserById($user->id, 1);
        }
        return Response::json($result);
    }
    
    /**
     * Add user address
     *
     * @return Response
     */
    public function addUserAddress()
    {
        $apiName = 'user/addUserAddress';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'placeName' => array('required', 'string'),
            'street' => array('required', 'string'),
            'locality' => array('string'),
            'landmark' => array('string'),
            // 'pincode' => array('string'),
        );
        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($user = User::where('id', $userSession->userId)->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else
        {
            $userAddress = new UserAddress();
            $userAddress->userId = $userSession->userId;
            $userAddress->placeName = $_JSON['placeName'];
            $userAddress->street = $_JSON['street'];
            
            if(isset($_JSON['locality']) && !empty($_JSON['locality']))
                $userAddress->locality = $_JSON['locality'];
            
            if(isset($_JSON['landmark']) && !empty($_JSON['landmark']))
                $userAddress->landmark = $_JSON['landmark'];
            
            // if(isset($_JSON['pincode']) && !empty($_JSON['pincode']))
            //     $userAddress->pincode = $_JSON['pincode'];
            
            $userAddress->save();

            $result = VkUtility::success($apiName, 'User address saved successfully');
            $result['userAddress'] = UserAddress::getById($userAddress->id, $userAddress->userId);
        }
        return Response::json($result);
    }
    
    /**
     * Update user address
     *
     * @return Response
     */
    public function updateUserAddress()
    {
        $apiName = 'user/updateUserAddress';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'userAddressId' => array('required'),
            'placeName' => array('required', 'string'),
            'street' => array('required', 'string'),
            'locality' => array('string'),
            'landmark' => array('string'),
            // 'pincode' => array('string'),
        );
        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($user = User::where('id', $userSession->userId)->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($userAddress = UserAddress::where('id', $_JSON['userAddressId'])->first()))
            $result = VkUtility::error($apiName, 99, 'Selected address does not exist.');
        else
        {
            $userAddress->placeName = $_JSON['placeName'];
            $userAddress->street = $_JSON['street'];
            
            if(isset($_JSON['locality']) && !empty($_JSON['locality']))
                $userAddress->locality = $_JSON['locality'];
            
            if(isset($_JSON['landmark']) && !empty($_JSON['landmark']))
                $userAddress->landmark = $_JSON['landmark'];
            
            // if(isset($_JSON['pincode']) && !empty($_JSON['pincode']))
            //     $userAddress->pincode = $_JSON['pincode'];
            
            $userAddress->save();

            $result = VkUtility::success($apiName, 'User address saved successfully');
            $result['userAddress'] = UserAddress::getById($userAddress->id, $userAddress->userId);
        }
        return Response::json($result);
    }
    
    /**
     * Delete user address
     *
     * @return Response
     */
    public function deleteUserAddress()
    {
        $apiName = 'user/deleteUserAddress';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'userAddressId' => array('required'),
        );
        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($user = User::where('id', $userSession->userId)->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($userAddress = UserAddress::where('id', $_JSON['userAddressId'])->first()))
            $result = VkUtility::error($apiName, 99, 'Selected address does not exist.');
        else
        {
            $userAddress->delete();
            $result = VkUtility::success($apiName, 'User address deleted successfully');
        }
        return Response::json($result);
    }
    
    /**
     * Get user address list
     *
     * @return Response
     */
    public function listUserAddress()
    {
        $apiName = 'user/listUserAddress';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
        );
        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($user = User::where('id', $userSession->userId)->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else
        {
            $result = VkUtility::success($apiName, 'User address saved successfully');
            $result['userAddresses'] = UserAddress::getList($user->id);
        }
        return Response::json($result);
    }
    
    /**
     * Update user's settings
     *
     * @return Response
     */
    public function updateSettings()
    {
        $apiName = 'user/updateSettings';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'paypalId' => array('string'),
            'notifyMe' => array('in:0,1'),
        );
        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($user = User::where('id', $userSession->userId)->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else
        {
            if(isset($_JSON['paypalId']) && !empty($_JSON['paypalId']))
                $user->paypalId = $_JSON['paypalId'];
            else
                $user->paypalId = null;
            
            if(isset($_JSON['notifyMe']) && !empty($_JSON['notifyMe']))
                $user->notifyMe = $_JSON['notifyMe'];
            else
                $user->notifyMe = null;
            
            $user->save();

            $result = VkUtility::success($apiName, 'Settings saved successfully');
            $result['user'] = User::getUserById($user->id, 1);
        }
        return Response::json($result);
    }
    
    /**
     * Change user password
     *
     * @return Response
     */
    public function changePassword()
    {
        $apiName = 'user/changePassword';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'oldPassword' => array('required', 'string'),
            'newPassword' => array('required', 'string')
        );
        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($user = User::where('id', $userSession->userId)->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(!Hash::check($_JSON['oldPassword'], $user->password))
            $result = VkUtility::error($apiName, 99, 'Invalid password. Please try again.');
        else if(UserPassword::isPasswordUsed($user->id, $_JSON['newPassword'], 5))
            $result = VkUtility::error($apiName, 99, 'You can not use last 5 passwords. Please try again.');
        else
        {
            $user->password = Hash::make($_JSON['newPassword']);
            $user->save();

            $userPassword = new UserPassword();
            $userPassword->userId = $user->id;
            $userPassword->password = $user->password;
            $userPassword->save();

            $result = VkUtility::success($apiName, 'Password changed successfully');
        }
        return Response::json($result);
    }
    
    /**
     * Get user profile details
     *
     * @return Response
     */
    public function getProfile()
    {
        $apiName = 'user/getProfile';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
        );
        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($user = User::where('id', $userSession->userId)->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else
        {
            $selectedUser = User::getUserById($user->id, 1);
            $selectedUser->unseenNotificationCount = Notification::where('receiverId', $selectedUser->userId)->where('isSeen', 0)->count();
            $result = VkUtility::success($apiName, 'User record fetched successfully');
            $result['user'] = $selectedUser;
        }
        return Response::json($result);
    }
    
    private function login($userId, $role, $deviceToken = '')
    {
        $userSession = UserSession::where('userId', $userId)->first();
        if (is_null($userSession) || empty($userSession)) 
        {
            $userSession = new UserSession();
            $userSession->userId = $userId;
            $userSession->role = $role;
            $userSession->deviceToken = $deviceToken;
        }
        $i = 0;
        do
        {
            $sessionId = sha1(microtime());
            $userSession->deviceToken = $deviceToken;
            $userSession->sessionId = $sessionId;
            // Safety guard
            if (++$i > 10) 
            {
                throw new Exception('Unknown error 1.', WS_ERR_UNKNOWN);
            }
        } while (!$userSession->save());

        return $sessionId;
    }
}
