<?php namespace App\Http\Controllers\Webservices;

use \Illuminate\Support\Facades\Request;
use \Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Middleware\VkUtility;
use App\Models\User;
use App\Models\UserSession;
use App\Models\Item;
use App\Models\ItemNutrient;
use App\Models\ItemIngredient;
use App\Models\ItemReview;
use App\Models\DayMenu;
use App\Models\WeekMenu;

class DayMenuController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | DayMenu Controller
    |--------------------------------------------------------------------------
    |
    | This controller provides services to process dayMenu records
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Return list of all dayMenu records
     *
     * @return Response
     */
    public function getList()
    {
        $apiName = 'dayMenu/getList';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('string'),
            'itemType' => array('in:1,2,3'),      /* 1 breakfast, 2 all day meal, 3 beverages */
            'exceptions' => array('array')
        );

        $validator = Validator::make($_JSON, $validationRules);
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else
        {
            $userId=0;
            $itemType = 0;
            $exceptions = '';
            $menuDate = date('Y-m-d');

            $userSession = UserSession::where('sessionId', '=', $_JSON['sessionId'])->first();
            if(!(is_null($userSession) || empty($userSession)))
                $userId = $userSession->userId;
            
            if(isset($_JSON['itemType']) && !empty($_JSON['itemType']))
                $itemType = filter_var ($_JSON['itemType'], FILTER_SANITIZE_NUMBER_INT);
            
            if(isset($_JSON['exceptions']) && !empty($_JSON['exceptions']))
                $exceptions = join(',', $_JSON['exceptions']);
            
            $menuItems = DayMenu::getList($userId, $menuDate, $itemType, $exceptions);

            if(is_null($menuItems) || empty($menuItems))
            {
                $this->createTodayMenu();
                $menuItems = DayMenu::getList($userId, $menuDate, $itemType, $exceptions);
            }
            
            foreach($menuItems as $item)
            {
                $item->nutrients = ItemNutrient::getList($item->itemId);
                $item->ingredients = ItemIngredient::getList($item->itemId);
                $item->reviews = ItemReview::getList($item->itemId);
            }
            
            $result = VkUtility::success($apiName, 'Records fetched successfully');
			$timeHours = date('H');
			$timeMin = date('i');
			if(($timeHours >=8 && $timeHours <17) ){
					$result['open']="1";
			}
			else{
				$result['open']="0";
			}
			
			$result['dateH'] = date('H');
			$result['datei'] = date('i');
            $result['menuItems'] = $menuItems;
        }
        
        return Response::json($result)->header("Content-Type", "application/json");
    }

    private function createTodayMenu()
    {
        $menuDate = date('Y-m-d');
        $weekMenuItems = WeekMenu::getList(0, date('N'));
        foreach($weekMenuItems as $item)
        {
            $dayMenu = new DayMenu();
            $dayMenu->menuDate = $menuDate;
            $dayMenu->itemId = $item->itemId;
            $dayMenu->save();
        }
    }
}
