<?php namespace App\Http\Controllers\Webservices;

use \Illuminate\Support\Facades\Request;
use \Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Password;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Middleware\VkUtility;
use App\Models\User;
use App\Models\UserSession;
use App\Models\UserAddress;
use App\Models\UserPassword;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\Item;
use App\Models\Event;
use App\Models\Notification;
use App\Models\DayMenu;
use App\Models\PhoneVerification;

use mPDF;

class PurchaseController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Purchase Controller
    |--------------------------------------------------------------------------
    |
    | This controller provides services to process purchase records
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    
    /**
     * Add a purchase record
     *
     * @return Response
     */
    public function add()
    {
        $apiName = 'purchase/add';
        $deviceToken = '';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('string'),
            'street' => array('string'),
            'locality' => array('string'),
            'landmark' => array('string'),
            'phone' => array('required', 'string'),
            'saveCutlery' => array('required', 'in:0,1'),
            'couponCode' => array('string'),
            'verificationCode' => array('required', 'string'),
            'items' => array('required'),
        );
        $validator = Validator::make($_JSON, $validationRules);
        $userSession = null;

        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if(isset($_JSON['sessionId']) && !empty($_JSON['sessionId']) && is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
            $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
        else if(is_null($phoneVerification = PhoneVerification::where('phone', $_JSON['phone'])->where('verificationCode', $_JSON['verificationCode'])->first()))
            $result = VkUtility::error($apiName, 99, 'Please enter correct verification code.');
		else if ( !isset($_JSON['couponCode']) && empty($_JSON['couponCode']))
			$result = VkUtility::error($apiName, 99, 'Please specify if CouponCode added or not');
        else
        {
            $purchaseDetails = array();
            $isItemValid = true;
            $purchaseItems = $_JSON['items'];
            foreach ($purchaseItems as $purchaseItem)
            {
                $item = Item::find($purchaseItem['itemId']);
                if(empty($item))
                {
                    $result = VkUtility::error($apiName, 99, "Item with id $purchaseItem[itemId] does not exist.");
                    $isItemValid = false;
                    break;
                }
                else if(!$this->validateItem($item))
                {
                    $result = VkUtility::error($apiName, 99, "Item $item->itemName is not availabe for order now.");
                    $isItemValid = false;
                    break;
                }

                $purchaseDetail = new PurchaseDetail();
                $purchaseDetail->itemId = $purchaseItem['itemId'];
                $purchaseDetail->quantity = $purchaseItem['quantity'];
                $purchaseDetail->price = $item->price;

                $purchaseDetails[] = $purchaseDetail;
            }

            if($isItemValid)
            {
                $purchase = new Purchase();
                
                if(!empty($userSession))
                {
                    $purchase->userId = $userSession->userId;
                }
                
                if(isset($_JSON['street']) && !empty($_JSON['street']))
                    $purchase->street = $_JSON['street'];
                
                if(isset($_JSON['locality']) && !empty($_JSON['locality']))
                    $purchase->locality = $_JSON['locality'];
                
                if(isset($_JSON['landmark']) && !empty($_JSON['landmark']))
                    $purchase->landmark = $_JSON['landmark'];
                
                if(isset($_JSON['couponCode']) && !empty($_JSON['couponCode']))
					if($_JSON['couponCode'] == "0"){
						$purchase->couponCode = $_JSON['couponCode'];
						
					}
					else if ($_JSON['couponCode'] == "1"){
						if(!empty($userSession)){
							$user = User::where('id',$userSession->userId)->first();
							if($user->balanceReferralAmount >= 100){
								$user->balanceReferralAmount = $user->balanceReferralAmount - 100;
								$user->save();
							}
							$purchase->couponCode = $_JSON['couponCode'];
						}
						else
							$purchase->couponCode = "0";						
					}
                else
					$purchase->couponCode = "0";
					
                $purchase->phone = $_JSON['phone'];
                $purchase->saveCutlery = $_JSON['saveCutlery'];
                $purchase->purchaseDate = date('Y-m-d H:i:s');
                
                if($purchase->save())
                {
                    $phoneVerification->delete();

                    foreach ($purchaseDetails as $purchaseDetail)
                    {
                        $purchaseDetail->purchaseId = $purchase->id;
                        $purchaseDetail->save();
                    }

                    $result = VkUtility::success($apiName, 'Purchase saved successfully');
                    $newPurchase = Purchase::getPurchaseById($purchase->id);
                    $newPurchase->purchaseDetail = PurchaseDetail::getList($purchase->id);
					
					try
                    {
                        $view = View::make('purchase.bill', ['purchase' => $newPurchase]);
                        $attachment = asset("/pdf/fig_bill_$purchase->id.pdf");
                        require_once base_path('vendor/mpdf/mpdf/mpdf.php');
                        $mpdf=new mPDF();
                        $mpdf->WriteHTML($view);
                        $mpdf->Output($attachment, 'F');

                        VkUtility::sendMail('purchase.bill', $newPurchase, 'Your fig e-bill', $newPurchase->email, $attachment);
                    }
                    catch(Exception $ex)
                    {

                    }
					
                    $result['purchase'] = $newPurchase;
                }
                else
                {
                    $result = VkUtility::error($apiName, 99, 'Unable to save your order, please try again.');
                }
            }
        }
        
        return Response::json($result);
    }

    private function validateItem($item)
    {
        if(empty($item))
            return false;

        // if($item->itemType == 1)

        return true;
    }
	
	
	
	/**
     * Get last 15 purchase record for a specific user
     *
     * @return Response
     */
    public function getUserPurchases()
    {
		
		$apiName = 'purchase/getUserPurchases';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('string'),
        );
		
        $validator = Validator::make($_JSON, $validationRules);

        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if (empty($_JSON['sessionId']))
            $result = VkUtility::error($apiName, 99, 'Please provide sessionId or phone number.');
        else
        {
            if(isset($_JSON['sessionId']) && !empty($_JSON['sessionId']))
            {
                if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
                    $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
                else if(is_null($user = User::where('id', $userSession->userId)->first()))
                    $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
                else
                {
                    $result = VkUtility::success($apiName, 'Last Orders Fetched.');
					$userPurchaseList = Purchase::getList($user->id);		
					//$total=0;
					foreach($userPurchaseList as $individualUserPurchase){
						$individualUserPurchase->purchaseDetail =PurchaseDetail::getList($individualUserPurchase->purchaseId);
						//$itemList = $individualUserPurchase->purchaseDetail;
						// foreach($itemList  as $item){
							
						// }
					}
					$result['userPurchaseList'] = $userPurchaseList;
                }
            }
		}
		return Response::json($result);
	
	}
	
	
	/**	
     * Get todays purchases for pos access
     *
     * @return Response
     */
    public function getTodaysPurchases()
    {
		$apiName = 'purchase/getTodaysPurchases';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('string'),
        );
		
        $validator = Validator::make($_JSON, $validationRules);
/*
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if (empty($_JSON['sessionId']))
            $result = VkUtility::error($apiName, 99, 'Please provide sessionId or phone number.');
        else
        {
            if(isset($_JSON['sessionId']) && !empty($_JSON['sessionId']))
            {
                if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
                    $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
                else if(is_null($user = User::where('id', $userSession->userId)->first()))
                    $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
                else
                {*/
                    $result = VkUtility::success($apiName, 'Last Orders Fetched.');
					$userPurchaseList = Purchase::getListForToday();					
					foreach($userPurchaseList as $individualUserPurchase){
						$total=0;
						$individualUserPurchase->purchaseDetail =PurchaseDetail::getList($individualUserPurchase->purchaseId);
						
						if(!empty($individualUserPurchase->userId)){
							$user = User::where('id',$individualUserPurchase->userId)->first();
							$individualUserPurchase->userName =$user->fullName;
						}
						else{
							$individualUserPurchase->userName ="";
						}
						// $total=0;  //geting total of uprchase.
						// $itemList = $individualUserPurchase->purchaseDetail;
						// foreach($itemList  as $item){
							// $total = $item->price * $item->quantity;
						// }
						// $total = $total + $total*0.13231;
						// if($individualUserPurchase->couponCode){
							// $total=$total+$total-100;
						// }
						// $individualUserPurchase->total=$total;
					}
					$result['userPurchaseList'] = $userPurchaseList;
           /*     }
            }
		}*/
		return Response::json($result);		
	}
	
	
	/**
     * Get todays purchases for pos access
     *
     * @return Response
     */
	public function applyCoupon(){
		
		$apiName = 'purchase/applyCoupon';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('string'),
			'codeBeingUsed' => array('string'),
        );
		
        $validator = Validator::make($_JSON, $validationRules);

        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if (empty($_JSON['sessionId']))
            $result = VkUtility::error($apiName, 99, 'Please Login to Apply Code');
		else if (empty($_JSON['codeBeingUsed']))
            $result = VkUtility::error($apiName, 99, 'Please provide  a Refferal Code');
        else
        {
			if(isset($_JSON['sessionId']) && !empty($_JSON['sessionId']))
            {
                if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
                    $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
                else if(is_null($user = User::where('id', $userSession->userId)->first()))
                    $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
                else
                {
					
					if(is_null($userWithCode = User::where('referralCode', $_JSON['codeBeingUsed'])->first() )){
						$result = VkUtility::error($apiName, 99, 'Enter a Valid Code');
						
					}	
					else{
						
						if($userWithCode->id == $user->id){
							if($userWithCode->balanceReferralAmount >=100){
								$codeAccepted = "1";
								$balanceAvailable = $userWithCode->balanceReferralAmount;
							}
							else{
								$codeAccepted = "0";
								$balanceAvailable = $userWithCode->balanceReferralAmount;
							}
							
							$result = VkUtility::success($apiName, 'Valid Code');		
							$result["codeAccepted"]= $codeAccepted;					
							$result["balanceAvailable"]= $balanceAvailable;
						}
						else{
							
							if(is_null($user->referralCodeUsed)){  // current user has no code yet.
								if(!empty(Purchase::where('userId', $user->id)->first())){
									
									$user->referralCodeUsed="used";
									$user->save();
									$result = VkUtility::error($apiName, 99, 'Code valid only for first purchase.');
									
								}
								else if(isset($_JSON['codeBeingUsed']) && !empty($_JSON['codeBeingUsed']) && empty($user->referralCodeUsed) )
								{
									$user->referralCodeUsed = $_JSON['codeBeingUsed'];
									if(User::getReferralBenefits($user->id, $user->referralCodeUsed)){
										
										$codeAccepted = "1";
										$balanceAvailable = "100.00";
										$result = VkUtility::success($apiName, 'Valid Code');	
										$result["codeAccepted"]= $codeAccepted;					
										$result["balanceAvailable"]= $balanceAvailable;
										$user->referralCodeUsed = $_JSON['codeBeingUsed'];
										$user->save();
									}
									else{
										$result = VkUtility::error($apiName, 99, 'Enter a Valid Code');
									}
								}
							}
							else{
								$result = VkUtility::error($apiName, 99, 'You can only use your own code');
							}
						}
					}
				}
			}
		}

		return Response::json($result);
	
	}	

	
	/**
     * Update any Purchase
     *
     * @return Response
     */
	public function updatePurchase(){
		
		
		$apiName = 'purchase/updatePurchase';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('string'),
			'street' => array('string'),
            'locality' => array('string'),
            'landmark' => array('string'),
            'phone' => array('required', 'string'),
            'saveCutlery' => array('string'),
            'isPaid' => array('string'),
			'isCancelled' => array('string'),
            'cancelReason' => array('string'),
            'purchaseStatus' => array('required', 'in:1,2,3,4'),
            'purchaseId' => array('required'),
            'userId' => array('string'),
        );
		
        $validator = Validator::make($_JSON, $validationRules);

        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        /*else if (empty($_JSON['sessionId']))
            $result = VkUtility::error($apiName, 99, 'Please provide sessionId');*/
		else if (empty($_JSON['purchaseId']))
            $result = VkUtility::error($apiName, 99, 'Please provide a Purchase Id');
		else if (empty($_JSON['street']))
            $result = VkUtility::error($apiName, 99, 'Please provide street for delivery');
		else if (empty($_JSON['locality']))
            $result = VkUtility::error($apiName, 99, 'Please provide a Locality');
		else if (empty($_JSON['landmark']))
            $result = VkUtility::error($apiName, 99, 'Please provide a Landmark');
		else if (empty($_JSON['purchaseStatus']))
            $result = VkUtility::error($apiName, 99, 'Please provide purchase status');
		/*else if (empty($_JSON['isCancelled']))
            $result = VkUtility::error($apiName, 99, 'Please provide isCancelled field as 1 or 0 ');*/
		else if(is_null($purchase = Purchase::where('id', $_JSON['purchaseId'])->first() ))
			$result = VkUtility::error($apiName, 99, 'No purchase record matching available');	

		else{
			//nead to push this status change to mobiles if they are not equal already.
			if($_JSON['purchaseStatus'] < 4){
				$purchase->purchaseStatus = $_JSON['purchaseStatus'];
			}
			else if($_JSON['purchaseStatus'] == 4){
				$purchase->purchaseStatus = $_JSON['purchaseStatus'];
				$purchase->paymentDate = date('Y-m-d H:i:s');
			}
		
			$purchase->updateDate = date('Y-m-d H:i:s');

  		    if(isset($_JSON['street']) && !empty($_JSON['street'])){
				$purchase->street = $_JSON['street'];
		    }
			if(isset($_JSON['locality']) && !empty($_JSON['locality'])){
				$purchase->locality = $_JSON['locality'];
			}
			if(isset($_JSON['landmark']) && !empty($_JSON['landmark'])){
				$purchase->landmark = $_JSON['landmark'];
			}
			if(isset($_JSON['couponCode']) && !empty($_JSON['couponCode'])){
				$purchase->couponCode = $_JSON['couponCode'];
			}
			if(isset($_JSON['isPaid']) && !empty($_JSON['isPaid'])){
				$purchase->isPaid = $_JSON['isPaid'];
				if($_JSON['isPaid'] == "1"){
					$purchase->paymentDate = date('Y-m-d H:i:s');
				}
			}
			
			$purchase->saveCutlery = $_JSON['saveCutlery'];
			$purchase->phone = $_JSON['phone'];
			$purchase->cancelReason = $_JSON['cancelReason'];
		
			if(isset($_JSON['isCancelled']) && !empty($_JSON['isCancelled'])){
				$purchase->isCancelled = $_JSON['isCancelled'];
			}
			
			$purchase->isCancelled = $_JSON['isCancelled'];
			
			$purchase->isPaid = $_JSON['isPaid'];
			$purchase->save();
			
			$result = VkUtility::success($apiName, 'Update Purchase');	
			$result['purchase']=$purchase;
			
		}
		
		return Response::json($result);
	}

	
	/**
     * Update any Purchase
     *
     * @return Response
     */
	public function fetchPurchaseById(){
		
		$apiName = 'purchase/fetchPurchaseById';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            
			'sessionId' => array('string'),
			'purchaseId' => array('required'),
			
        );
        $validator = Validator::make($_JSON, $validationRules);

        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        /*else if (empty($_JSON['sessionId']))
            $result = VkUtility::error($apiName, 99, 'Please provide sessionId');*/
		else if (empty($_JSON['purchaseId']))
            $result = VkUtility::error($apiName, 99, 'Please provide a Purchase Id');
		else if(is_null($purchase = Purchase::where('id', $_JSON['purchaseId'])->first() ))
			$result = VkUtility::error($apiName, 99, 'No purchase record matching available');
		else{
			$result = VkUtility::success($apiName, 'Purchase Fetched.');	
			
			$result['purchaseId'] = $purchase->id;
			$result['userId'] = $purchase->userId;
			$result['userName']= "";
			if(is_null($purchase->userId)){
				$result['userId'] ="";
			}
			else{
				$user =User::where('id', $purchase->userId)->first();
				$result['userName']= $user->fullName;
			}
			$result['street'] = $purchase->street;
			if(is_null($purchase->street))
				$result['street'] = "";
			$result['locality'] = $purchase->locality;
			if(is_null($purchase->locality))
				$result['locality'] ="";
			$result['landmark'] = $purchase->landmark;
			if(is_null($purchase->landmark))
				$result['landmark'] ="";
			$result['phone'] = $purchase->phone;
			$result['saveCutlery'] = $purchase->saveCutlery;
			if(is_null($purchase->saveCutlery))
				$result['saveCutlery'] ="";
			$result['couponCode'] = $purchase->couponCode;
			if(is_null($purchase->couponCode))
				$result['couponCode'] ="";
				$result['paypalTransId'] = $purchase->paypalTransId;
			if(is_null($purchase->paypalTransId ))
				$result['paypalTransId'] = "";
			$result['purchaseStatus'] = $purchase->purchaseStatus;
			if(is_null($purchase->purchaseStatus))
				$result['purchaseStatus'] = "";
			$result['isPaid'] = $purchase->isPaid;
			if(is_null($purchase->isPaid))
				$result['isPaid'] = "";
			$result['isCancelled'] = $purchase->isCancelled;
			if(is_null($purchase->isCancelled))
				$result['isCancelled'] = "";
			$result['cancelReason'] = $purchase->cancelReason;
			if(is_null($purchase->cancelReason))
				$result['cancelReason'] = "";
			$itemList =PurchaseDetail::getList($purchase->id);
			
			$total=0;

			foreach($itemList  as $item){
				$total = $item->price * $item->quantity;
			}
			
			$total = $total + $total*0.13125;
			if($purchase->couponCode){
				$total=$total+$total-100;
			}
			$result['total'] =$total;
			$result['purchaseDetail'] = $itemList;
			
		}
		return Response::json($result);
	}
	
}