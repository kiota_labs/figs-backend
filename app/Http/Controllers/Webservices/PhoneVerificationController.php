<?php namespace App\Http\Controllers\Webservices;

use \Illuminate\Support\Facades\Request;
use \Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Password;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Middleware\VkUtility;
use App\Models\User;
use App\Models\UserSession;
use App\Models\PhoneVerification;
use Aloha\Twilio\Twilio;

class PhoneVerificationController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    |
    | This controller provides services to process phoneVerification records
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * send a verification code to a given phone number using twilio
     *
     * @return Response
     */
    public function getVerificationCode()
    {
        $apiName = 'phoneVerification/getVerificationCode';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('string'),
            'phone' => array('string'),
        );
        $validator = Validator::make($_JSON, $validationRules);

        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if ((!isset($_JSON['sessionId']) || empty($_JSON['sessionId'])) && (!isset($_JSON['phone']) || empty($_JSON['phone'])))
            $result = VkUtility::error($apiName, 99, 'Please provide sessionId or phone number.');
        else
        {
            if(isset($_JSON['sessionId']) && !empty($_JSON['sessionId']))
            {
                if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
                    $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
                else if(is_null($user = User::where('id', $userSession->userId)->first()))
                    $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
                else if(empty($user->phone) && (!isset($_JSON['phone']) || empty($_JSON['phone'])))
                    $result = VkUtility::error($apiName, 99, 'You have not updated your phone number. Please update your profile or enter your phone number.');
                else
                {
					if(isset($_JSON['phone']) && !empty($_JSON['phone'])){
						$phone = $_JSON['phone'];
					}
                    else if($user->phone)
                        $phone = $user->phone;
                    
                    $verificationCode = VkUtility::randomStringNum(6);
                    $this->sendVerificationCode($phone, $verificationCode);
                    $result = VkUtility::success($apiName, 'Verification code sent successfully.');
                    $result['phone'] = $phone;
                }
            }
            else //if(isset($_JSON['phone']) && !empty($_JSON['phone']))
            {
                $phone = $_JSON['phone'];
                $verificationCode = VkUtility::randomStringNum(6);
                $this->sendVerificationCode($phone, $verificationCode);
                $result = VkUtility::success($apiName, 'Verification code sent successfully.');
                $result['phone'] = $phone;
            }
        }
        
        return Response::json($result);
    }

    private function sendVerificationCode($phone, $verificationCode)
    {
        $phoneVerification = new PhoneVerification();
        $phoneVerification->phone = $phone;
        $phoneVerification->verificationCode = $verificationCode;
        $phoneVerification->save();

        $twilioAccountSid = env('TWILIO_ACCOUNT_SID', 'AC666904ea62fe0c3d39e6acd21568ee4c');
        $twilioAuthToken = env('TWILIO_AUTH_TOKEN', 'a76d8ccb69ef97ba48bddecb9f20908e');
        $twilioNumber = env('TWILIO_NUMBER', '+12564108792');
        $message = "Fig verification code : $verificationCode ";

        $twilio = new Twilio($twilioAccountSid, $twilioAuthToken, $twilioNumber);
        $twilio->message($phone, $message);
    }
	
	    /**
     * send a verification code to a given phone number using twilio
     *
     * @return Response
     */
    public function getVerificationCodeOverPos()
    {
        $apiName = 'phoneVerification/getVerificationCode';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'phone' => array('string'),
        );
        $validator = Validator::make($_JSON, $validationRules);

        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else if ($validator->fails()) 
            $result = VkUtility::error($apiName, 99, $validator->errors()->first());
        else if ( (!isset($_JSON['phone']) || empty($_JSON['phone'])))
            $result = VkUtility::error($apiName, 99, 'Please provide a phone number.');
        else
        {
			$phone = $_JSON['phone'];
			$verificationCode = VkUtility::randomStringNum(6);
			//$this->sendVerificationCode($phone, $verificationCode);
			$phoneVerification = new PhoneVerification();
			$phoneVerification->phone = $phone;
			$phoneVerification->verificationCode = $verificationCode;
			$phoneVerification->save();

			$result = VkUtility::success($apiName, 'Verification code generated successfully.');
			//$result['phone'] = $phone;
			$result['code']=$verificationCode;
		}
		return Response::json($result);
	}
}