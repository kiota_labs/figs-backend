<?php namespace App\Http\Controllers\Webservices;

use \Illuminate\Support\Facades\Request;
use \Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Middleware\VkUtility;
use App\Models\User;
use App\Models\UserSession;
use App\Models\Item;
use App\Models\ItemReview;

class ItemReviewController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | ItemReview Controller
    |--------------------------------------------------------------------------
    |
    | This controller provides services to process itemReview records
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Return list of all itemReviews
     *
     * @return Response
     */
    public function getList()
    {
        $apiName = 'itemReview/getList';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'itemId' => array('required', 'integer'),
            'exceptions' => array('array')
        );
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            else if(is_null($item = Item::where('id', $_JSON['itemId'])->where('isDisabled', 0)->first()))
                $result = VkUtility::error($apiName, 99, 'Selected item does not exist.');
            else
            {
                $exceptions = '';
                if(isset($_JSON['exceptions']) && !empty($_JSON['exceptions']))
                    $exceptions = join(',', $_JSON['exceptions']);
                
                $itemReviews = ItemReview::getList($item->id, 0, $exceptions);
                foreach($itemReviews as $itemReview)
                {
                    $itemReview->timeElapsed = VkUtility::getTimeElapsed(date_create($itemReview->createDate), date_create());
                }
                
                $result = VkUtility::success($apiName, 'Records fetched successfully');
                $result['itemReviews'] = $itemReviews;
            }
        }
        return Response::json($result);
    }

    /**
     * Add a itemReview
     *
     * @return Response
     */
    public function add()
    {
        $apiName = 'itemReview/add';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'itemId' => array('required', 'integer'),
            'rating' => array('integer'),
            'comments' => array('required', 'string')
        );
        
        $user = null;
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            else if(is_null($user = User::where('id', $userSession->userId)->first()))
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            else if(is_null($item = Item::where('id', $_JSON['itemId'])->where('isDisabled', 0)->first()))
                $result = VkUtility::error($apiName, 99, 'Selected item does not exist.');
            else
            {
                $itemReview = new ItemReview();
                $itemReview->userId = $userSession->userId;
                $itemReview->itemId = $item->id;
                $itemReview->comments = base64_decode($_JSON['comments']);   //filter_var($_JSON['comments'], FILTER_SANITIZE_STRING);

                if(isset($_JSON['rating']) && !empty($_JSON['rating']))
                    $itemReview->rating = $_JSON['rating'];
                
                $itemReview->save();
                
                //increment item's review and rating count
                $item->timestamps = false;                  /* disable timestamps */
                $item->reviewCount += 1;

                if(isset($_JSON['rating']) && !empty($_JSON['rating']))
                {
                    $item->ratingCount += 1;                    /* if rating is given */
                    $item->totalRating += $_JSON['rating'];     /* if rating is given */
                }

                $item->save();
                
                // get review detail in proper format
                $itemReviews = ItemReview::getList($item->id);
                foreach($itemReviews as $itemReview)
                {
                    $itemReview->timeElapsed = VkUtility::getTimeElapsed(date_create($itemReview->createDate), date_create());
                }
                $result = VkUtility::success($apiName, 'Review saved successfully');
                $result['itemReviews'] = $itemReviews;
            }
        }
        return Response::json($result);
    }

}
