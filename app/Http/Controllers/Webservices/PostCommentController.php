<?php namespace App\Http\Controllers\Webservices;

use \Illuminate\Support\Facades\Request;
use \Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Middleware\VkUtility;
use App\Models\User;
use App\Models\UserSession;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostTag;
use App\Models\Event;
use App\Models\Notification;

class PostCommentController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | PostComment Controller
    |--------------------------------------------------------------------------
    |
    | This controller provides services to process postComment records
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Return list of all postComments
     *
     * @return Response
     */
    public function getList()
    {
        $apiName = 'postComment/getList';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'postId' => array('required', 'integer'),
            'exceptions' => array('array')
        );
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
            {
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            }
            else if(is_null($userSession = UserSession::where('sessionId', '=', $_JSON['sessionId'])->first()))
            {
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            }
            else if(is_null($post = Post::where('id', '=', $_JSON['postId'])->where('isDisabled', '=', 0)->first()))
            {
                $result = VkUtility::error($apiName, 99, 'Selected post does not exist.');
            }
            else
            {
                $exceptions = '';
                if(isset($_JSON['exceptions']) && !empty($_JSON['exceptions']))
                    $exceptions = join(',', $_JSON['exceptions']);
                
                $postComments = PostComment::getList($post->id, 0, $exceptions);
                
                foreach($postComments as $postComment)
                {
                    $postComment->timeElapsed = VkUtility::getTimeElapsed(date_create($postComment->createDate), date_create());
                }
                
                $result = VkUtility::success($apiName, 'Records fetched successfully');
                $result['postComments'] = $postComments;
            }
        }
        return Response::json($result);
    }

    /**
     * Add a postComment
     *
     * @return Response
     */
    public function add()
    {
        $apiName = 'postComment/add';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'postId' => array('required', 'integer'),
            'comment' => array('required', 'string')
        );
        
        $user = null;
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
            {
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            }
            else if(is_null($userSession = UserSession::where('sessionId', '=', $_JSON['sessionId'])->first()))
            {
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            }
            else if(is_null($user = User::where('id', '=', $userSession->userId)->first()))
            {
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            }
            else if(is_null($post = Post::where('id', '=', $_JSON['postId'])->where('isDisabled', '=', 0)->first()))
            {
                $result = VkUtility::error($apiName, 99, 'Selected post does not exist.');
            }
            else
            {
                $postComment = new PostComment();
                $postComment->userId = $userSession->userId;
                $postComment->postId = $post->id;
                $postComment->comment = base64_decode($_JSON['comment']);   //filter_var($_JSON['comment'], FILTER_SANITIZE_STRING);
                
                $postComment->save();
                
                //increment post's comment count
                $post->timestamps = false;  /* disable timestamps */
                $post->commentCount += 1;
                $post->save();
                
                //send notification to post owner
                $userName = (!empty($user->fullName)) ? $user->fullName : ((!empty($user->email)) ? $user->email : 'someone');
                $message = "$userName has commented on one of your " . $post->postTypeName() . '.';
                $data = array(
                    'notificationId'=>Event::COMMENT_CREATED, 
                    'senderUserId'=>$userSession->userId,   /* current user */
                );
                
                if($user->id != $post->userId) //if not commenting on own post
                {
                    $eventId = Event::saveEvent(Event::COMMENT_CREATED, $userSession->userId, $post->id, $postComment->createDate, $post->userId, 1);
                    Notification::saveNotification($post->userId, $message, $eventId);

                    //send push notification
                    $postUserSession = UserSession::where('userId', '=', $post->userId)->with('user')->first();
                    if(!is_null($postUserSession) && !empty($postUserSession) && $postUserSession->deviceToken && $postUserSession->user->notifyComment)
                    {
                        @VkUtility::sendApnsNotification($postUserSession->deviceToken, $message, $postUserSession->deviceBadge, $data);
                    }
                }
                
                //send notifications to users who have commented earlier on this post
                $exceptions = $user->id . ',' . $post->userId;
                $commentingUsers = PostComment::getCommentingUserSessions($post->id, $exceptions);
                $message = "$userName has commented on one of the " . $post->postTypeName() . " you have commented earlier.";
                
                if($commentingUsers)
                {
                    foreach($commentingUsers as $commentingUser)
                    {
                        @VkUtility::sendApnsNotification($commentingUser->deviceToken, $message, $commentingUser->deviceBadge, $data);
                    }
                }
                
                $postComment = PostComment::getById($postComment->id);
                $postComment->timeElapsed = VkUtility::getTimeElapsed(date_create($postComment->createDate), date_create());
                $result = VkUtility::success($apiName, 'Comment saved successfully');
                $result['postComment'] = $postComment;
            }
        }
        return Response::json($result);
    }

}
