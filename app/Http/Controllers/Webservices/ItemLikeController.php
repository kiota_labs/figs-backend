<?php namespace App\Http\Controllers\Webservices;

use \Illuminate\Support\Facades\Request;
use \Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Middleware\VkUtility;
use App\Models\User;
use App\Models\UserSession;
use App\Models\Item;
use App\Models\ItemLike;

class ItemLikeController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | ItemLike Controller
    |--------------------------------------------------------------------------
    |
    | This controller provides services to process itemLike records
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Return list of all itemLikes
     *
     * @return Response
     */
    public function getList()
    {
        $apiName = 'itemLike/getList';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'itemId' => array('required', 'integer'),
            'exceptions' => array('array')
        );
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            else if(is_null($item = Item::where('id', $_JSON['itemId'])->where('isDisabled', 0)->first()))
                $result = VkUtility::error($apiName, 99, 'Selected item does not exist.');
            else
            {
                $exceptions = '';
                if(isset($_JSON['exceptions']) && !empty($_JSON['exceptions']))
                    $exceptions = join(',', $_JSON['exceptions']);
                
                $itemLikes = ItemLike::getList($item->id, 0, $exceptions);
                
                foreach($itemLikes as $itemLike)
                {
                    $itemLike->timeElapsed = VkUtility::getTimeElapsed(date_create($itemLike->createDate), date_create());
                }
                
                $result = VkUtility::success($apiName, 'Records fetched successfully');
                $result['itemLikes'] = $itemLikes;
            }
        }
        return Response::json($result);
    }

    /**
     * Add a itemLike
     *
     * @return Response
     */
    public function add()
    {
        $apiName = 'itemLike/add';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'itemId' => array('required', 'integer')
        );
        
        $user = null;
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            else if(is_null($user = User::where('id', $userSession->userId)->first()))
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            else if(is_null($item = Item::where('id', $_JSON['itemId'])->where('isDisabled', 0)->first()))
                $result = VkUtility::error($apiName, 99, 'Selected item does not exist.');
            else if(!is_null($itemLike = ItemLike::where('itemId', $_JSON['itemId'])->where('userId', $user->id)->first()))
                $result = VkUtility::error($apiName, 99, 'You have already liked this item.');
            else
            {
                $itemLike = new ItemLike();
                $itemLike->userId = $userSession->userId;
                $itemLike->itemId = $item->id;
                
                $itemLike->save();
                
                //increment item's like and rating count
                $item->timestamps = false;                  /* disable timestamps */
                $item->likeCount += 1;

                $item->save();
                
                // get like detail in proper format
                $itemLikes = ItemLike::getList($item->id);
                foreach($itemLikes as $itemLike)
                {
                    $itemLike->timeElapsed = VkUtility::getTimeElapsed(date_create($itemLike->createDate), date_create());
                }
                
                $result = VkUtility::success($apiName, 'Like saved successfully');
                $result['itemLikes'] = $itemLikes;
            }
        }
        return Response::json($result);
    }

    /**
     * Add a itemLike
     *
     * @return Response
     */
    public function remove()
    {
        $apiName = 'itemLike/remove';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'itemId' => array('required', 'integer')
        );
        
        $user = null;
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            else if(is_null($userSession = UserSession::where('sessionId', $_JSON['sessionId'])->first()))
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            else if(is_null($user = User::where('id', $userSession->userId)->first()))
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            else if(is_null($item = Item::where('id', $_JSON['itemId'])->where('isDisabled', 0)->first()))
                $result = VkUtility::error($apiName, 99, 'Selected item does not exist.');
            else if(is_null($itemLike = ItemLike::where('itemId', $_JSON['itemId'])->where('userId', $user->id)->first()))
                $result = VkUtility::error($apiName, 99, 'You have not liked this item.');
            else
            {
                $itemLike->delete();
                
                //increment item's like and rating count
                $item->timestamps = false;                  /* disable timestamps */
                $item->likeCount -= 1;

                $item->save();
                
                // get like detail in proper format
                $itemLikes = ItemLike::getList($item->id);
                foreach($itemLikes as $itemLike)
                {
                    $itemLike->timeElapsed = VkUtility::getTimeElapsed(date_create($itemLike->createDate), date_create());
                }
                
                $result = VkUtility::success($apiName, 'Like removed successfully');
                $result['itemLikes'] = $itemLikes;
            }
        }
        return Response::json($result);
    }

}
