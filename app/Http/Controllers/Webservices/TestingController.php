<?php namespace App\Http\Controllers\Webservices;

use \Illuminate\Support\Facades\Request;
use \Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Middleware\VkUtility;
use App\Http\Middleware\VkXmpp;
use App\Models\User;
use App\Models\UserSession;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Hash;

class TestingController extends Controller {

    /**
     * test push notification
     *
     * @return Response
     */
    public function sendNotification()
    {
        $apiName = 'testing/sendNotification';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'deviceToken' => array('required', 'string')
        );
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
            {
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            }
            else
            {
                $deviceToken = filter_var($_JSON['deviceToken'], FILTER_SANITIZE_STRING);
                $message = 'test notification';
                $deviceBadge = 1;
                $data = array('name'=>'vikas');
                VkUtility::sendApnsNotification($deviceToken, $message, $deviceBadge, $data);
                
                $result = VkUtility::success($apiName, 'notification sent');
            }
        }
        return Response::json($result);
    }
    
    /**
     * test mimeType of base64 image
     *
     * @return Response
     */
    public function getMimeType()
    {
        $apiName = 'testing/getMimeType';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'image' => array('required')
        );
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
            {
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            }
            else
            {
                $mimeType = VkUtility::getMimeTypeFromBase64($_JSON['image']);
                $result = VkUtility::success($apiName, 'mime type found');
                $result['mimeType'] = $mimeType;
            }
        }
        return Response::json($result);
    }
    
    /**
     * generate password hash
     *
     * @return Response
     */
    public function getHash()
    {
        $apiName = 'testing/getHash';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'password' => array('required')
        );
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
            {
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            }
            else
            {
                $hash = Hash::make($_JSON['password']);
                
                $result = VkUtility::success($apiName, 'hash generated');
                $result['hash'] = $hash;
                $result['doesHashMatch'] = (Hash::check($_JSON['password'], $hash));
            }
        }
        return Response::json($result);
    }
}
