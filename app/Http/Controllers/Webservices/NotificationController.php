<?php namespace App\Http\Controllers\Webservices;

use \Illuminate\Support\Facades\Request;
use \Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Middleware\VkUtility;
use App\Models\User;
use App\Models\UserSession;
use App\Models\Notification;
use \Illuminate\Support\Facades\Mail;

class NotificationController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Notification Controller
    |--------------------------------------------------------------------------
    |
    | This controller provides services to process notification records
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    
    /**
     * get notification list of a user
     *
     * @return Response
     */
    public function getList()
    {
        $apiName = 'notification/getList';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required')
        );
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
            {
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            }
            else if(is_null($userSession = UserSession::where('sessionId', '=', $_JSON['sessionId'])->first()))
            {
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            }
            else
            {
                $notifications = Notification::getNotificationsByReceiverId($userSession->userId);
                
                foreach($notifications as &$notification)
                {
                    $notification->timeElapsed = VkUtility::getTimeElapsed(date_create($notification->eventDate), date_create());
                }

                //reset badge counter
                $userSession->deviceBadge = 0;
                $userSession->save();
                
                $result = VkUtility::success($apiName, 'Records fetched successfully');
                $result['notifications'] = $notifications;
                $result['unseenNotificationCount'] = Notification::where('receiverId', $userSession->userId)->where('isSeen', 0)->count();
            }
        }
        return Response::json($result)->header("Connection","close");
    }
    
    /**
     * delete a notification by notification id
     *
     * @return Response
     */
    public function delete()
    {
        $apiName = 'notification/delete';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
            'notificationId' => array('required')
        );
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
            {
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            }
            else if(is_null($userSession = UserSession::where('sessionId', '=', $_JSON['sessionId'])->first()))
            {
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            }
            else if(is_null($notification = Notification::where('id', $_JSON['notificationId'])->first()))
            {
                $result = VkUtility::error($apiName, 99, 'Selected notification does not exist.');
            }
            else
            {
                $notification->delete();
                
                $result = VkUtility::success($apiName, 'Notification removed successfully');
            }
        }
        return Response::json($result);
    }
    
    /**
     * mark notifications as seen
     *
     * @return Response
     */
    public function markAsSeen()
    {
        $apiName = 'notification/markAsSeen';
        $_JSON = VkUtility::getJsonInput();
        $validationRules = array(
            'sessionId' => array('required'),
//            'notificationIds' => array('required'),
//            'eventTypes' => array('required')
        );
        
        if(!isset($_JSON) || empty($_JSON))
            $result = VkUtility::error($apiName, 99, 'No input received.');
        else
        {
            $validator = Validator::make($_JSON, $validationRules);
            if ($validator->fails()) 
            {
                $result = VkUtility::error($apiName, 99, $validator->errors()->first());
            }
            else if(is_null($userSession = UserSession::where('sessionId', '=', $_JSON['sessionId'])->first()))
            {
                $result = VkUtility::error($apiName, 101, 'Your session has expired. Please login again.');
            }
            else
            {
                $eventTypes = '';
                $notificationIds = '';
                
                if(isset($_JSON['eventTypes']) && !empty($_JSON['eventTypes']))
                    $eventTypes = filter_var ($_JSON['eventTypes'], FILTER_SANITIZE_STRING);
                
                if(isset($_JSON['notificationIds']) && !empty($_JSON['notificationIds']))
                    $notificationIds = filter_var ($_JSON['notificationIds'], FILTER_SANITIZE_STRING);
                
                $rowsAffected = Notification::markAsSeen($userSession->userId, $eventTypes, $notificationIds);
                
                $result = VkUtility::success($apiName, "$rowsAffected notification(s) marked as seen.");
            }
        }
        return Response::json($result);
    }
    
}
