<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Models\PostComment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Middleware\VkUtility;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class PostController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Post Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders forms to create a post record
      | And a list of all post records
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the post records to the user.
     *
     * @return Response
     */
    public function index() {
        $posts = Post::where('postType', '=', 2)->
                        where('userId', \Auth::user()->id)->paginate(10);

        return view('post.index')->with('posts', $posts);
    }

    public function posts() {
        $posts = DB::table('post as p')
                ->leftjoin('post as p2', 'p.sendToCampaignId', '=', 'p2.id')
                ->select('p.title as title', 'p.id as id', 'p.description as description', 'p.image as image', 'p.isDisabled as isDisabled', 'p.tappsReceived as tappsReceived', 'p2.title as sendToCampaignTitle')
                ->whereIn('p.postType', array(1, 3, 4))
                ->where('p.userId', \Auth::user()->id)
                ->paginate(10);



        return view('post.posts')->with('posts', $posts);
    }

    public function searchPost() {
        $postName = trim(input::get('postName'));
        $location = trim(input::get('location'));

        $searchCriteria['postName'] = $postName;
        $searchCriteria['location'] = $location;

        $posts = Post::where('postName', 'LIKE', "%$postName%")
                        ->where('location', 'LIKE', "%$location%")->paginate(10);

        return view('post.index')->with('posts', $posts)->with('searchCriteria', $searchCriteria);
    }

    public function showPostForm() {
        return view('post.create');
    }

    public function createCampaign() {
        $postDetails = input::all();

        $rules = array(
            'title' => array('required', 'string'),
            'goal' => array('required', 'numeric'),
            'image' => array('image'),
            'description' => array('required', 'string', 'max:500')
        );

        $post = null;

        $validator = Validator::make($postDetails, $rules);
        if ($validator->fails()) {
            return Redirect::to('cms/campaign/create')->withErrors($validator)->withInput($postDetails);
        } else {


            if (isset($postDetails['id']) && !empty($postDetails['id'])) {
                $posts = Post::where('id', '=', $postDetails['id'])->first();
                if (is_null($posts)) {
                    abort(401, 'Unauthorized action 2.');
                }
                if (isset($postDetails['image']) && !empty($postDetails['image'])) {
                    $oldPicName = $posts->image;
                    $imageDir = public_path() . config('constants.postImage');
                    $thumbDir = public_path() . config('constants.postThumb');

                    @unlink($imageDir . $oldPicName);
                    @unlink($thumbDir . $oldPicName);
                }

                $post = Post::find($postDetails['id']);
            } else {
                $post = new Post();
            }
            $post->title = $postDetails['title'];

            $post->userId = \Auth::user()->id;
            $post->campaignType = 2;
            $post->postType = 2;
            $post->isPublic = $postDetails['isPublic'];


            if (isset($postDetails['description']) && !empty($postDetails['description']))
                $post->description = htmlspecialchars($postDetails['description']);

            if (isset($postDetails['goal']) && !empty($postDetails['goal']))
                $post->tappsAssigned = $postDetails['goal'];

            if (isset($postDetails['endDate']) && !empty($postDetails['endDate'])) {
                $newDate = date("Y-m-d", strtotime($postDetails['endDate']));
                $post->endDate = $newDate;
            }

            if (isset($postDetails['image']) && !empty($postDetails['image'])) {
                $image = input::file('image');
                $imageDir = public_path() . config('constants.postImage');
                $thumbDir = public_path() . config('constants.postThumb');
                $maxWidth = config('constants.thumbMaxWidth');
                $post->image = VkUtility::saveImageFile($image, $imageDir, $thumbDir, $maxWidth);
            }


            $post->save();

            if (isset($postDetails['id']) && !empty($postDetails['id'])) {
                Post::where('sendToCampaignId', '=', $post->id)->where('postType', '=', 2)->update(array('title' => $post->title, 'description' => $post->description, 'endDate' => $post->endDate, 'tappsAssigned' => $post->tappsAssigned));

                //update all reposts
                Post::where('parentPostId', '=', $post->id)->update(array('title' => $post->title, 'description' => $post->description, 'endDate' => $post->endDate, 'tappsAssigned' => $post->tappsAssigned));
                return Redirect::to('cms/post/updatePost/' . $postDetails['id'])->withInput()->with('flash_message', 'Campaign Updated Successfully.');
            }

            return Redirect::to('cms/campaign/create')->with('flash_message', 'Campaign Created Successfully.');
        }
    }

    public function createPost() {
        $postDetails = input::all();

        $rules = array(
            'title' => array('required', 'string'),
            'image' => array('image'),
            'description' => array('required', 'string', 'max:500')
        );

        $post = null;

        $validator = Validator::make($postDetails, $rules);
        if ($validator->fails()) {
            return Redirect::to('cms/campaign/create')->withErrors($validator)->withInput($postDetails);
        } else {


            if (isset($postDetails['id']) && !empty($postDetails['id'])) {
                $posts = Post::where('id', '=', $postDetails['id'])->first();
                if (is_null($posts)) {
                    abort(401, 'Unauthorized action 2.');
                }
                if (isset($postDetails['image']) && !empty($postDetails['image'])) {
                    $oldPicName = $posts->image;
                    $imageDir = public_path() . config('constants.postImage');
                    $thumbDir = public_path() . config('constants.postThumb');

                    @unlink($imageDir . $oldPicName);
                    @unlink($thumbDir . $oldPicName);
                }

                $post = Post::find($postDetails['id']);
            } else {
                $post = new Post();
            }
            $post->title = $postDetails['title'];

            $post->userId = \Auth::user()->id;
            $post->postType = 1;
            $post->isPublic = $postDetails['isPublic'];


            if (isset($postDetails['description']) && !empty($postDetails['description']))
                $post->description = htmlspecialchars($postDetails['description']);

            if (isset($postDetails['selectedCampaign']) && !empty($postDetails['selectedCampaign'])) {
                $post->sendToCampaignId = $postDetails['selectedCampaign'];
            }


            if (isset($postDetails['image']) && !empty($postDetails['image'])) {
                $image = input::file('image');
                $imageDir = public_path() . config('constants.postImage');
                $thumbDir = public_path() . config('constants.postThumb');
                $maxWidth = config('constants.thumbMaxWidth');
                $post->image = VkUtility::saveImageFile($image, $imageDir, $thumbDir, $maxWidth);
            }


            $post->save();

            if (isset($postDetails['id']) && !empty($postDetails['id'])) {
                return Redirect::to('cms/post/updatePost/' . $postDetails['id'])->withInput()->with('flash_message', 'Campaign Updated Successfully.');
            }

            return Redirect::to('cms/post/create')->with('flash_message', 'Post Created Successfully.');
        }
    }

    public function updatePost($id) {
        if (\Auth::user()->role == 'admin') {
            $postDetails = Post::getPostById($id, 0);
        } elseif (\Auth::user()->role == 'group') {
            $postDetails = Post::getPostById($id, 0, \Auth::user()->id);
        }
        if (!is_null($postDetails)) {
            return view('post/create')->with('postDetails', $postDetails);
        } else {
            abort(401, 'Unauthorized action');
        }
    }

    public function viewCreateCampaignForm() {
        return view('post.create');
    }

    public function viewCreatePostForm() {
        return view('post.createPost');
    }

    public function deactivatePost() {
        $id = input::get('id');
        $data = input::get('data');


        $post = Post::find($id);
        $post->disableReason = $data;
        $post->isDisabled = 1;
        $post->save();

        return "post successfully deactivated";
    }

    public function activatePost() {
        $id = input::get('clubId');
        $post = Post::find($id);
        $post->disableReason = " ";
        $post->isDisabled = 0;
        $post->save();

        return "post successfully activated";
    }

    public function viewDetailsPost($id) {
        if (\Auth::user()->role == 'admin') {
            $postDetails = Post::getPostById($id, 0);
        } elseif (\Auth::user()->role == 'group') {
            $postDetails = Post::getPostById($id, 0, \Auth::user()->id);
        }
        if (!is_null($postDetails)) {
            return view('post.view')->with('postDetails', $postDetails);
        } else {
            abort(401, 'Unauthorized action');
        }
    }

    public function userCampaignDetails($userId, $id) {
        if (\Auth::user()->role == 'admin') {
            $postDetails = DB::table('post as p')
                    ->where('p.postType', '=', 2)
                    ->where('p.id', '=', $id)
                    ->first();
            $userDetails = User::where('id', $userId)->first();
            $postDetails->fullName = $userDetails->fullName;
            $postDetails->userName = $userDetails->username;
            $postDetails->role = $userDetails->role;
            $postDetails->userImage = $userDetails->image;
        }
        if (!is_null($postDetails)) {
            return view('post.userCampaigns')->with('postDetails', $postDetails)->with('userId', $userId);
        } else {
            abort(401, 'Unauthorized action');
        }
    }

    public function userPostDetails($userId, $id) {
        if (\Auth::user()->role == 'admin') {
            $postDetails = DB::table('post as p')
                    ->leftjoin('post as p2', 'p.sendToCampaignId', '=', 'p2.id')
                    ->select('p.title as title', 'p.id as id', 'p.createDate as createDate', 'p.description as description', 'p.image as image', 'p.isDisabled as isDisabled', 'p.tappsReceived as tappsReceived', 'p2.title as sendToCampaignTitle', 'p.isPublic')
                    ->whereIn('p.postType', array(1, 3, 4))
                    ->where('p.id', '=', $id)
                    ->first();

            $userDetails = User::where('id', $userId)->first();
            $postDetails->fullName = $userDetails->fullName;
            $postDetails->userName = $userDetails->username;
            $postDetails->role = $userDetails->role;
            $postDetails->userImage = $userDetails->image;
        }

        if (!is_null($postDetails)) {
            return view('post.userPosts')->with('postDetails', $postDetails)->with('userId', $userId);
        } else {
            abort(401, 'Unauthorized action');
        }
    }

    public function postDetails($id) {

        if (\Auth::user()->role == 'group') {
            $postDetails = DB::table('post as p')
                    ->leftjoin('post as p2', 'p.sendToCampaignId', '=', 'p2.id')
                    ->select('p.title as title', 'p.id as id', 'p.createDate as createDate', 'p.description as description', 'p.image as image', 'p.isPublic', 'p.isDisabled as isDisabled', 'p.tappsReceived as tappsReceived', 'p2.title as sendToCampaignTitle')
                    ->whereIn('p.postType', array(1, 3, 4))
                    ->where('p.id', '=', $id)
                    ->where('p.userId', \Auth::user()->id)
                    ->first();

            $postComments = DB::table('postComment')
                    ->join('user', 'postComment.userId', '=', 'user.id')
                    ->select('user.image', 'user.role', 'postComment.comment', 'postComment.id', 'postComment.userId', 'postComment.postId', 'postComment.isDisabled')
                    ->where('postComment.postId', $id)
                    ->orderBy('postComment.id')
                    ->get();

            $postTapps = DB::table('postTapp as p')
                    ->join('user as u', 'p.userId', '=', 'u.id')
                    ->select('u.fullName', 'u.role', 'u.gender', DB::raw('TIMESTAMPDIFF(YEAR, u.dob, CURDATE()) AS age'), 'u.image', 'p.tappCount')
                    ->where('p.postId', '=', $id)
                    ->get();


            $postDetails->postTapps = $postTapps;
        }
        if (!is_null($postDetails)) {
            return view('post.postDetails')->with('postDetails', $postDetails)->with('postComments', $postComments);
        } else {
            abort(401, 'Unauthorized action');
        }
    }

    public function viewCampaignDetails() {
        $postDetails = Post::getList(\Auth::user()->id, 0, "");
        return view('post.index')->with('postDetails', $postDetails);
    }

    public function disable() {
        $id = input::get('userId');
        $reason = input::get('disableReason');

        $user = Post::find($id);
        $user->isDisabled = 1;
        $user->disableReason = $reason;
        $user->timestamps = false;
        $user->save();

        return "campaign disabled";
    }

    public function enable() {
        $id = input::get('userId');

        $user = Post::find($id);
        $user->isDisabled = 0;
        $user->disableReason = null;
        $user->timestamps = false;
        $user->save();

        return "campaign enabled";
    }

    public function closeCampaign() {
        $id = input::get('userId');
        $reason = input::get('disableReason');

        $user = Post::find($id);
        $user->isClosed = 1;
        $user->closeReason = $reason;
        $user->timestamps = false;
        $user->save();

        //update all child campaigns
        Post::where('sendToCampaignId', '=', $id)->where('postType', '=', 2)->update(array('isClosed' => 1));

        //update all reposts
        Post::where('parentPostId', '=', $id)->update(array('isClosed' => 1));

        return "campaign closed";
    }

    public function openCampaign() {
        $id = input::get('userId');

        $user = Post::find($id);
        $user->isClosed = 0;
        $user->closeReason = null;
        $user->timestamps = false;
        $user->save();

        return "campaign open";
    }

    public function viewUserPosts($id) {
        $postDetails = Post::where('userId', '=', $id)
                ->whereIn('postType', array(1, 3, 4))
                ->paginate(12);
        $userDetails = User::where('id', $id)->first();
        $postDetails->fullName = $userDetails->fullName;
        $postDetails->userName = $userDetails->username;
        $postDetails->role = $userDetails->role;
        $postDetails->userImage = $userDetails->image;

        return view('post.viewPosts')->with('postDetails', $postDetails)->with('userId', $id);
    }

    public function viewUserCampaigns($id) {
        $postDetails = Post::where('userId', '=', $id)
                ->where('postType', 2)
                ->paginate(12);
        $userDetails = User::where('id', $id)->first();
        $postDetails->fullName = $userDetails->fullName;
        $postDetails->userName = $userDetails->username;
        $postDetails->role = $userDetails->role;
        $postDetails->userImage = $userDetails->image;

        return view('post.viewCampaigns')->with('postDetails', $postDetails)->with('userId', $id);
    }

    public function viewUserPostComments($id) {
        $postDetails = DB::table('postComment')
                ->join('post', 'postComment.postId', '=', 'post.id')
                ->leftjoin('post as p2', 'post.sendToCampaignId', '=', 'p2.id')
                ->select('post.title', 'post.image', 'post.description', 'post.createDate', 'post.id', 'post.tappsReceived as tappsReceived', 'p2.title as sendToCampaignTitle')
                ->where('postComment.userId', '=', $id)
                ->whereIn('post.postType', array(1, 3, 4))
                ->groupBy('post.id')
                ->paginate(10);

        $userDetails = User::where('id', $id)->first();
        $postDetails->fullName = $userDetails->fullName;
        $postDetails->userName = $userDetails->username;
        $postDetails->role = $userDetails->role;
        $postDetails->userImage = $userDetails->image;

        /* foreach ($postDetails as $postDetail) {
          $postDetail->comments = DB::table('postComment')
          ->join('user', 'postComment.userId', '=', 'user.id')
          ->select('user.image', 'postComment.comment', 'postComment.id', 'postComment.userId', 'postComment.postId', 'postComment.isDisabled')
          ->where('postComment.postId', $postDetail->id)
          ->orderBy('postComment.id')
          ->get();
          } */

        return view('post.viewCommentedPosts')->with('postDetails', $postDetails)->with('userId', $id);
    }

    public function viewUserCampaignComments($id) {
        $postDetails = DB::table('postComment')
                ->join('post', 'postComment.postId', '=', 'post.id')
                ->where('postComment.userId', '=', $id)
                ->where('post.postType', 2)
                ->groupBy('post.id')
                ->paginate(10);

        $userDetails = User::where('id', $id)->first();
        $postDetails->fullName = $userDetails->fullName;
        $postDetails->userName = $userDetails->username;
        $postDetails->role = $userDetails->role;
        $postDetails->userImage = $userDetails->image;

        /* foreach ($postDetails as $postDetail) {
          $postDetail->comments = DB::table('postComment')
          ->join('user', 'postComment.userId', '=', 'user.id')
          ->select('user.image', 'postComment.comment', 'postComment.id', 'postComment.userId', 'postComment.postId', 'postComment.isDisabled')
          ->where('postComment.postId', $postDetail->id)
          ->orderBy('postComment.id')
          ->get();
          } */

        return view('post.viewCommentedCampaigns')->with('postDetails', $postDetails)->with('userId', $id);
    }

    public function userCommentedCampaignDetails($userId, $id) {
        $postDetails = DB::table('post as p')
                ->where('p.postType', 2)
                ->where('p.id', '=', $id)
                ->first();


        $postComments = DB::table('postComment')
                ->join('user', 'postComment.userId', '=', 'user.id')
                ->select('user.image', 'postComment.comment', 'postComment.id', 'postComment.userId', 'postComment.postId', 'postComment.isDisabled')
                ->where('postComment.postId', $id)
                ->orderBy('postComment.id')
                ->get();
        $userDetails = User::where('id', $userId)->first();
        $postDetails->fullName = $userDetails->fullName;
        $postDetails->userName = $userDetails->username;
        $postDetails->role = $userDetails->role;
        $postDetails->userImage = $userDetails->image;


        return view('post.userCampaignComments')->with('postDetails', $postDetails)->with('postComments', $postComments)->with('userId', $userId);
    }

    public function userCommentedPostDetails($userId, $id) {
        $postDetails = DB::table('post as p')
                ->leftjoin('post as p2', 'p.sendToCampaignId', '=', 'p2.id')
                ->select('p.title as title', 'p.id as id', 'p.createDate as createDate', 'p.description as description', 'p.image as image', 'p.isDisabled as isDisabled', 'p.isPublic', 'p.tappsReceived as tappsReceived', 'p2.title as sendToCampaignTitle')
                ->whereIn('p.postType', array(1, 3, 4))
                ->where('p.id', '=', $id)
                ->first();

        $postComments = DB::table('postComment')
                ->join('user', 'postComment.userId', '=', 'user.id')
                ->select('user.image', 'postComment.comment', 'postComment.id', 'postComment.userId', 'postComment.postId', 'postComment.isDisabled')
                ->where('postComment.postId', $id)
                ->orderBy('postComment.id')
                ->get();
        $userDetails = User::where('id', $userId)->first();
        $postDetails->fullName = $userDetails->fullName;
        $postDetails->userName = $userDetails->username;
        $postDetails->role = $userDetails->role;
        $postDetails->userImage = $userDetails->image;

        return view('post.userPostComments')->with('postDetails', $postDetails)->with('postComments', $postComments)->with('userId', $userId);
    }

    public function viewComments() {
        $userId = input::get('userId');
        $postId = input::get('postId');

        $comments = PostComment::where('userId', $userId)->where('postId', $postId)->paginate(15);

        return view('post.viewComments')->with('comments', $comments)->with('userId', $userId);
    }

    public function disableComment() {
        $id = input::get('userId');
        $reason = input::get('disableReason');

        $user = PostComment::find($id);
        $user->isDisabled = 1;
        $user->disableReason = $reason;
        $user->save();

        return "comment disabled";
    }

    public function enableComment() {
        $id = input::get('userId');

        $user = PostComment::find($id);
        $user->isDisabled = 0;
        $user->disableReason = null;
        $user->save();

        return "comment enabled";
    }

    public function addComment() {
        $postId = input::get('postId');
        $comment = input::get('comment');

        $postComment = new PostComment();
        $postComment->userId = \Auth::user()->id;
        $postComment->postId = $postId;
        $postComment->comment = $comment;
        $postComment->save();

        return "comment added successfully";
    }

    public function getCampaigns() {
        $page = 1; //is_null(Input::get('page'))?1:Input::get('page');
        $limit = 1000;
        $current = ($page - 1) * $limit;


        $campaigns = DB::table('post')->where('postType', 2)
                ->orderBy('id')
                ->get();


        return $campaigns;
    }

    public function searchCampaigns() {
        $title = input::get('title');

        $page = 1; //is_null(Input::get('page'))?1:Input::get('page');
        $limit = 1000;
        $current = ($page - 1) * $limit;


        $campaigns = DB::table('post')->where('postType', 2)
                ->where('title', 'like', "%$title%")
                ->orderBy('id')
                ->get();

        return $campaigns;
    }

}
