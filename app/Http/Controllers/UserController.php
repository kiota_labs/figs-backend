<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Post;
use App\Models\WithdrawalRequest;
use App\Models\UserDocument;
use Illuminate\Support\Facades\Session;
use App\Models\UserSession;
use App\Http\Middleware\VkUtility;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use App\Models\Event;
use App\Models\Notification;

class UserController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | User Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders forms to create a user record
      | And a list of all user records
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the user records to the user.
     *
     * @return Response
     */
    public function viewGroups() {
        Session::forget('homePage');
        $users = User::where('role', 'group')->orderBy('createDate', 'desc')->paginate(10);

        return view('user.viewGroups')->with('users', $users);
    }

    public function viewUsers() {
        Session::forget('homePage');
        Session::forget('withdraw');
        Session::forget('redeem');
        Session::forget('withdraw');
        $users = User::where('role', 'user')->orderBy('createDate', 'desc')->paginate(10);

        return view('user.viewUsers')->with('users', $users);
    }

    public function searchUser() {
        $userName = trim(input::get('userName'));

        $searchCriteria['userName'] = $userName;

        $users = User::where('role', 'user')
                        ->where(function($query) use ($userName) {
                            $query->where('fullName', 'LIKE', "%$userName%")
                            ->orWhere('email', 'LIKE', "%$userName%")
                            ->orWhere('username', 'LIKE', "%$userName%");
                        })->orderBy('createDate', 'desc')->paginate(10);


        return view('user.viewUsers')->with('users', $users)->with('searchCriteria', $searchCriteria);
    }

    public function searchGroup() {
        $userName = trim(input::get('userName'));

        $searchCriteria['userName'] = $userName;

        $users = User::where('role', 'group')
                        ->where(function($query) use ($userName) {
                            $query->where('fullName', 'LIKE', "%$userName%")
                            ->orWhere('email', 'LIKE', "%$userName%")
                            ->orWhere('username', 'LIKE', "%$userName%");
                        })->orderBy('createDate', 'desc')->paginate(10);


        return view('user.viewGroups')->with('users', $users)->with('searchCriteria', $searchCriteria);
    }

    public function viewChangePasswordForm() {
        return view('auth/changePassword');
    }

    public function changePassword() {
        $userDetails = input::all();
        $rules = array(
            'password' => 'required|confirmed',
            'oldPassword' => 'required'
        );

        $validator = Validator::make($userDetails, $rules);
        if ($validator->fails()) {
            return Redirect::to('cms/user/changePassword')->withErrors($validator);
        } else {


            $password = input::get('password');
            $userId = \Auth::user()->id;

            if (!\Auth::validate(array('email' => \Auth::user()->email, 'password' => Input::get('oldPassword')))) {
                $validator->getMessageBag()->add('password', 'Old Password Does Not Match With Our Records');
                return Redirect::to('cms/user/changePassword')->withErrors($validator)->with('user', User::find(\Auth::user()->id));
            }

            $user = User::find($userId);
            $user->password = hash::make($password);
            $user->save();


            return Redirect::to('/home')->with('flash_message', 'Password Changed Successfully.');
        }
    }

    public function disable() {
        $id = input::get('userId');
        $reason = input::get('disableReason');

        $user = User::find($id);
        $user->isDisabled = 1;
        $user->disableReason = $reason;
        $user->timestamps = false;
        $user->save();

        $data['contactPerson'] = $user->contactPerson;
        $data['groupName'] = $user->fullName;
        $subject = 'group disabled';
        $view = 'emails.groupDisabled';

        @VkUtility::sendMail($view, $data, $subject, $user->email);



        return "user disabled";
    }

    public function enable() {
        $id = input::get('userId');

        $user = User::find($id);
        $user->isDisabled = 0;
        $user->disableReason = "";
        $user->timestamps = false;
        $user->save();

        $data['contactPerson'] = $user->contactPerson;
        $data['groupName'] = $user->fullName;
        $subject = 'group enabled';
        $view = 'emails.groupEnabled';

        @VkUtility::sendMail($view, $data, $subject, $user->email);

        return "user enabled";
    }

    public function verifyUser() {
        $id = input::get('userId');

        $user = User::find($id);
        $user->isVerified = 1;
        $user->save();

        return "user verified";
    }

    public function approve() {

        $id = input::get('userId');

        $user = User::find($id);
        $user->approvalStatus = 1;
        $user->save();

        $data['contactPerson'] = $user->contactPerson;
        $data['groupName'] = $user->fullName;
        $subject = 'approval successfull';
        $view = 'emails.approvalSuccess';

        @VkUtility::sendMail($view, $data, $subject, $user->email);

        return "user successfully approved";
    }

    public function viewGroupDetails($id) {

        $userDetails = User::getUserById($id);
        $followers = DB::table('groupFollower')
                ->join('user', 'groupFollower.followingUserId', '=', 'user.id')
                ->select('user.fullName', 'user.image', 'user.username', 'user.dob', 'user.gender', 'user.role', 'groupFollower.followedGroupId')
                ->where('groupFollower.followedGroupId', '=', $id)
                ->paginate(10);
        $userDocuments = UserDocument::where('userId', $id)->get();
        if (is_null($userDetails)) {
            abort(401, 'Unauthorized action');
        } else {
            return view('user.viewGroupDetails')->with('userDetails', $userDetails)->with('followers', $followers)->with('userDocuments', $userDocuments);
        }
    }

    public function groupProfile($id) {
        $userDetails = User::getUserById($id);
        $userDocuments = UserDocument::where('userId', $id)->get();
        if (is_null($userDetails)) {
            abort(401, 'Unauthorized action');
        } else {
            return view('user.flaggedGroupDetails')->with('userDetails', $userDetails)->with('userDocuments', $userDocuments);
        }
    }

    public function userDetails($id) {

        $userDetails = User::getUserById($id);
        if (is_null($userDetails)) {
            abort(401, 'Unauthorized action');
        } else {
            return view('user.userDetails')->with('userDetails', $userDetails);
        }
    }

    public function searchGroupFollowers($id) {
        $userDetails = User::getUserById($id);
        $userDocuments = UserDocument::where('userId', $id)->get();

        $gender = trim(input::get('gender'));
        $age = trim(input::get('age'));

        $searchCriteria['gender'] = $gender;
        $searchCriteria['age'] = $age;

        $searchGender = array('m', 'f', '', null);
        if (isset($gender) && !empty($gender)) {
            if ($gender == 'Male')
                $searchGender = array('m');
            elseif ($gender == 'Female')
                $searchGender = array('f');
        }

        if (isset($age) && !empty($age)) {
            $ageGap = explode('-', $age);
            $date = date_create(date('Y-m-d'));
            $date2 = date_sub($date, date_interval_create_from_date_string("$ageGap[0] years"));
            $startDate = date_format($date2, "Y-m-d");

            $date3 = date_sub($date, date_interval_create_from_date_string("$ageGap[1] years"));
            $endDate = date_format($date3, "Y-m-d");

            $ageGap = array($endDate, $startDate);

            $followers = DB::table('groupFollower')
                    ->join('user', 'groupFollower.followingUserId', '=', 'user.id')
                    ->select('user.fullName', 'user.image', 'user.username', 'user.dob', 'user.gender', 'user.role')
                    ->where('groupFollower.followedGroupId', '=', $id)
                    ->whereIn('user.gender', $searchGender)
                    ->whereBetween('user.dob', $ageGap)
                    ->paginate(10);
        } else {


            $followers = DB::table('groupFollower')
                    ->join('user', 'groupFollower.followingUserId', '=', 'user.id')
                    ->select('user.fullName', 'user.image', 'user.username', 'user.dob', 'user.gender', 'user.role')
                    ->where('groupFollower.followedGroupId', '=', $id)
                    ->whereIn('user.gender', $searchGender)
                    ->paginate(10);
        }


        return view('user.viewGroupDetails')->with('userDetails', $userDetails)->with('followers', $followers)->with('searchCriteria', $searchCriteria)->with('userDocuments', $userDocuments);
    }

    public function viewUserDetails($id) {
        $userDetails = User::getUserById($id);
        $totalTapps = DB::table('post')
                ->where('userId', $id)
                ->where('sendToCampaignId', null)
                ->where('sendToUserId', null)
                ->sum('tappsReceived');


        if (is_null($userDetails)) {
            abort(401, 'Unauthorized action');
        } else {
            return view('user.viewUserDetails')->with('userDetails', $userDetails);
        }
    }

    public function viewFlaggedGroupDetails($id) {
        $userDetails = User::getUserById($id);
        $userDocuments = UserDocument::where('userId', $id)->get();
        if (is_null($userDetails)) {
            abort(401, 'Unauthorized action');
        } else {
            return view('user.flaggedGroupDetails')->with('userDetails', $userDetails)->with('userDocuments', $userDocuments);
        }
    }

    public function reject() {
        $rejectionReason = input::get('rejectionReason');
        $groupId = input::get('dialogGroupId');

        $user = User::find($groupId);
        $user->approvalStatus = 2;
        $user->rejectionReason = $rejectionReason;
        $user->save();

        $data['contactPerson'] = $user->contactPerson;
        $data['groupName'] = $user->fullName;
        $subject = 'approval rejected';
        $view = 'emails.approvalRejection';

        @VkUtility::sendMail($view, $data, $subject, $user->email);

        return "user successfully rejected";
    }

    public function editProfile() {
        $userDetails = User::where('id', \Auth::user()->id)->first();
        return view('user.editProfile')->with('userDetails', $userDetails);
    }

    public function postEditProfile() {
        $userDetails = input::all();

        $rules = array(
            'email' => 'email|max:128|unique:user',
            'image' => 'image',
            'groupName' => 'unique:user,fullName'
        );
        $users = User::where('id', '=', $userDetails['userId'])->first();
        if (is_null($users)) {
            abort(401, 'Unauthorized action 2.');
        }
        if (isset($userDetails['email']) && !empty($userDetails['email']) && !is_null($users))
            $rules['email'] = array('email', 'unique:user,email,' . $users->id);


        if (isset($userDetails['groupName']) && !empty($userDetails['groupName']) && !is_null($users))
            $rules['groupName'] = array('unique:user,fullName,' . $users->id);

        $post = null;

        $validator = Validator::make($userDetails, $rules);
        if ($validator->fails()) {
            return Redirect::to('cms/user/editProfile')->withErrors($validator)->withInput($userDetails);
        } else {


            if (isset($userDetails['userId']) && !empty($userDetails['userId'])) {
                $users = User::where('id', '=', $userDetails['userId'])->first();
                if (is_null($users)) {
                    abort(401, 'Unauthorized action 2.');
                }

                if (isset($userDetails['image']) && !empty($userDetails['image'])) {
                    $oldPicName = $users->image;
                    $imageDir = public_path() . config('constants.userImage');
                    $thumbDir = public_path() . config('constants.userThumb');

                    @unlink($imageDir . $oldPicName);
                    @unlink($thumbDir . $oldPicName);
                }

                $user = User::find($userDetails['userId']);
            }


            if (isset($userDetails['email']) && !empty($userDetails['email']))
                $user->email = $userDetails['email'];

            if (isset($userDetails['groupName']) && !empty($userDetails['groupName']))
                $user->fullName = $userDetails['groupName'];

            if (isset($userDetails['description']) && !empty($userDetails['description']))
                $user->description = htmlspecialchars($userDetails['description']);

            if (isset($userDetails['contactPerson']) && !empty($userDetails['contactPerson'])) {
                $user->contactPerson = $userDetails['contactPerson'];
            }


            if (isset($userDetails['image']) && !empty($userDetails['image'])) {
                $image = input::file('image');
                $imageDir = public_path() . config('constants.userImage');
                $thumbDir = public_path() . config('constants.userThumb');
                $maxWidth = config('constants.thumbMaxWidth');
                $user->image = VkUtility::saveImageFile($image, $imageDir, $thumbDir, $maxWidth);
            }


            $user->save();


            return Redirect::to('cms/user/editProfile')->with('flash_message', 'Profile Updated Successfully.');
        }
    }

    public function addDocuments($email) {
        $userDetails = User::where('email', $email)->first();
        return view('user.uploadDocuments')->with('userId', $userDetails->id);
    }

    public function uploadMultipleDocuments() {

        $file = input::file('myfile');
        $id = input::get('id');
        $documentDir = public_path() . config('constants.userDocuments');
        //$extension = $file->getClientOriginalExtension();
        $name = $file->getClientOriginalName();

        $fileName = $name;

        $filePath = $documentDir;
        $file->move($filePath, $fileName);

        $userDocuments = new UserDocument();
        $userDocuments->userId = $id;
        $userDocuments->documentName = $fileName;
        $userDocuments->save();

        return "success";
    }

    public function registrationComplete() {

        return Redirect::to('auth/login')->with('flash_message', 'Successfully Registered,Please check mail');
    }

    public function paypal() {
        return view('withdrawTapps.paypal');
    }

    public function saveWithdrawTappDetails() {
        // The custom hidden field (user id) sent along with the button is retrieved here. 
        //if($_GET['custom']) $user=$_GET['custom']; 
        // The unique transaction id.
        //$raw_post_data = file_get_contents('php://input');
        //dd($raw_post_data);
        //$tx = input::all();
        //dd($tx);
        define("DEBUG", 1);
        // Set to 0 once you're ready to go live
        define("USE_SANDBOX", 1);
        define("LOG_FILE", "./ipn.log");
        // Read POST data
        // reading posted data directly from $_POST causes serialization
        // issues with array data in POST. Reading raw POST data from input stream instead.
        $raw_post_data = file_get_contents('php://input');

        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
// read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data
        if (USE_SANDBOX == true) {
            $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        } else {
            $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
        }
        $ch = curl_init($paypal_url);

        if ($ch == FALSE) {
            return FALSE;
        }
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        if (DEBUG == true) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        }
// CONFIG: Optional proxy configuration
//curl_setopt($ch, CURLOPT_PROXY, $proxy);
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
// Set TCP timeout to 30 seconds
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below. Ensure the file is readable by the webserver.
// This is mandatory for some environments.
        $cert = public_path() . '/cacert.pem';
        curl_setopt($ch, CURLOPT_CAINFO, $cert);
        $res = curl_exec($ch);

        if (curl_errno($ch) != 0) { // cURL error
            if (DEBUG == true) {
                VkUtility::LogInFile(date('[Y-m-d H:i e] ') . "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
            }
            curl_close($ch);
            exit;
        } else {
            // Log the entire HTTP response if debug is switched on.
            if (DEBUG == true) {

                VkUtility::LogInFile(date('[Y-m-d H:i e] ') . "HTTP request of validation request:" . curl_getinfo($ch, CURLINFO_HEADER_OUT) . " for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
                VkUtility::LogInFile(date('[Y-m-d H:i e] ') . "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
            }
            curl_close($ch);
        }
// Inspect IPN validation result and act accordingly
// Split response headers and payload, a better way for strcmp
        $tokens = explode("\r\n\r\n", trim($res));
        $res = trim(end($tokens));
        if (strcmp($res, "VERIFIED") == 0) {
            // check whether the payment_status is Completed
            // check that txn_id has not been previously processed
            // check that receiver_email is your PayPal email
            // check that payment_amount/payment_currency are correct
            // process payment and mark item as paid.
            // assign posted variables to local variables

            $userEmail = $_POST['receiver_email'];
            $adminEmail = $_POST['payer_email'];
            $txn_id = $_POST['txn_id'];
            $requestId = $_POST['custom'];
            $date = $_POST['payment_date'];

            $date1 = date('Y-m-d H:i:s', strtotime($date));

            $recordExists = WithdrawalRequest::where('transactionId', $txn_id)->first();

            if (is_null($recordExists) || empty($recordExists)) {
                $withdrawalRequest = WithdrawalRequest::find($requestId);
                $withdrawalRequest->requestStatus = 1;
                $withdrawalRequest->receiverPaypalId = $userEmail;
                $withdrawalRequest->transactionId = $txn_id;
                $withdrawalRequest->transactionDate = $date1;
                $withdrawalRequest->save();


                $user = User::find($withdrawalRequest->userId);
                $user->balanceTapps -= $withdrawalRequest->tappsRequested;
                $user->save();

                $message = "You have received $withdrawalRequest->tappsRequested Tapps from tapp Admin via paypal";

                $eventId = Event::saveEvent(Event::SUCCESSFUL_PAYPAL_PAYMENT, \Auth::user()->id, $withdrawalRequest->id, $withdrawalRequest->createDate, $user->id, 1);
                Notification::saveNotification($user->id, $message, $eventId);

                $userSession = UserSession::where('userId', $user->id)->first();
                if (!is_null($userSession) && !empty($userSession) && $userSession->deviceToken) {
                    $data = array(
                        'notificationId' => Event::SUCCESSFUL_PAYPAL_PAYMENT,
                        'senderUserId' => \Auth::user()->id, /* current user */
                    );
                    @VkUtility::sendApnsNotification($userSession->deviceToken, $message, $userSession->deviceBadge, $data);
                }

                $data['paypalId'] = $userEmail;
                $data['tapps'] = $withdrawalRequest->tappsRequested;
                $subject = "Tapp Payment Successfull";
                $view = 'emails.paymentSuccess';
                $result = @VkUtility::sendMail($view, $data, $subject, $user->email);

                if (DEBUG == true) {
                    VkUtility::LogInFile(date('[Y-m-d H:i e] ') . "Verified IPN: $req ");
                }


                return Redirect::to('cms/user/withdrawTappRequests')->with('flash_message', 'transaction successfull');
            } else {
                if (DEBUG == true) {
                    VkUtility::LogInFile(date('[Y-m-d H:i e] ') . "Verified IPN: $req ");
                }

                return Redirect::to('cms/user/withdrawTappRequests')->with('flash_message', 'transaction successfull');
            }
        } else if (strcmp($res, "INVALID") == 0) {
            // log for manual investigation
            // Add business logic here which deals with invalid IPN messages
            if (DEBUG == true) {
                VkUtility::LogInFile(date('[Y-m-d H:i e] ') . "Invalid IPN: $req ");
            }
            return Redirect::to('cms/user/withdrawTappRequests')->with('flash_message', 'transaction unsuccessfull.please try again');
        }
    }

}
