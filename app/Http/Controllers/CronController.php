<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserSession;
use App\Models\Post;
use App\Models\Event;
use App\Models\Notification;
use App\Http\Middleware\VkUtility;

class CronController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Cron Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles all automated tasks that needs to be done in background
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Check post expiration and send notification
     *
     * @return Response
     */
    public function sendExpirationAlert()
    {
        //find all posts expiring in next 24 hours
        //send push notifications to post owners
        //create notification records
        $startDate = date('Y-m-d 00:00:00');
        $endDate = date('Y-m-d 23:59:59');
        $posts = Post::whereBetween('endDate', [$startDate, $endDate])
                ->where('isDisabled', 0)
                ->whereNull('sendToCampaignId')
                ->whereNull('parentPostId')
                ->get();

        foreach ($posts as $post)
        {
            $user = $post->user;

            //send notification to post owner
            $message = "Your campaign with title '$post->title' will expire today.";
            $eventId = Event::saveEvent(Event::POST_EXPIRED, $user->id, $post->id, date('Y-m-d'), 0, 1);
            Notification::saveNotification($user->id, $message, $eventId);

            //send push notification
            $userSession = UserSession::where('userId', '=', $user->id)->first();
            if (!is_null($userSession) && !empty($userSession) && $userSession->deviceToken)
            {
                $data = array(
                    'notificationId' => Event::POST_EXPIRED,
                    'postId' => $post->id
                );

                @VkUtility::sendApnsNotification($userSession->deviceToken, $message, $userSession->deviceBadge, $data);
            }
        }
    }

    /**
     * Find inactive users and send notification
     *
     * @return Response
     */
    public function sendInactivityAlert()
    {
        //find all users inactive since last week
        //pick one of their campaigns at random
        //send push notifications to them
        //create notification records

        $date = new DateTime();
        $date->sub(new DateInterval('P7D'));
        $users = User::where('lastAccessDate', '<', $date->format('Y-m-d H:i:s'))
                ->where('isDisabled', 0)
                ->get();

        foreach ($users as $user)
        {
            $posts = Post::where('postType', 2)
                    ->where('isDisabled', 0)
                    ->where(function($query)
                    {
                        $query->where('userId', $user->id)
                        ->orWhere('sendToUserId', $user->id);
                    })
                    ->get();

            if ($posts)
            {
                $index = random_int(0, count($posts) - 1);
                $post = $posts[$index];

                //send notification to post owner
                $tappsReceivedPcnt = $post->tappsReceived * 100 / $post->tappsAssigned;
                $message = "Your campaign with title '$post->title' has collected $post->tappsReceived tapp(s) which is $tappsReceivedPcnt% of the goal.";
                $eventId = Event::saveEvent(Event::USER_INACTIVITY, $user->id, $post->id, date('Y-m-d'), 0, 1);
                Notification::saveNotification($user->id, $message, $eventId);

                //send push notification
                $userSession = UserSession::where('userId', '=', $user->id)->first();
                if (!is_null($userSession) && !empty($userSession) && $userSession->deviceToken)
                {
                    $data = array(
                        'notificationId' => Event::USER_INACTIVITY,
                        'postId' => $post->id
                    );

                    @VkUtility::sendApnsNotification($userSession->deviceToken, $message, $userSession->deviceBadge, $data);
                }
            }
        }
    }

    /**
     * send free badge to all users every week
     *
     * @return Response
     */
    public function sendFreeBadge()
    {
        //find all users 
        //send free badge and a push notifications to them
        //create notification records

        $users = User::where('isDisabled', 0)->whereRaw('DAYOFWEEK(createDate) = DAYOFWEEK(CURDATE())')->get();
        foreach ($users as $user)
        {
            $badgePurchase = new BadgePurchase();
            $badgePurchase->userId = $user->id;
            $badgePurchase->badgeCount = 1;
            $badgePurchase->save();
            
            if ($badgePurchase)
            {
                //send notification to post owner
                $message = "You have received a free badge.";
                $eventId = Event::saveEvent(Event::BADGE_RECEIVED_USING_APP, $user->id, $badgePurchase->id, date('Y-m-d'), 0, 1, 0, 1);
                Notification::saveNotification($user->id, $message, $eventId);

                //send push notification
                $userSession = UserSession::where('userId', '=', $user->id)->first();
                if (!is_null($userSession) && !empty($userSession) && $userSession->deviceToken)
                {
                    $data = array(
                        'notificationId' => Event::BADGE_RECEIVED_USING_APP,
                        'badgePurchaseId' => $badgePurchase->id
                    );

                    @VkUtility::sendApnsNotification($userSession->deviceToken, $message, $userSession->deviceBadge, $data);
                }
            }
        }
    }

}
