<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Post;
use App\Models\WithdrawalRequest;
use App\Models\UserDocument;
use Illuminate\Support\Facades\Session;
use App\Models\UserSession;
use App\Http\Middleware\VkUtility;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\Event;
use App\Models\Notification;

class PurchaseController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Purchase Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders forms to create a purchase record
      | And a list of all purchase records
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    public function bill($id) {
        $purchase = Purchase::getPurchaseById($id);
        $purchase->purchaseDetail = PurchaseDetail::getList($purchase->purchaseId);
        
        if (is_null($purchase)) {
            abort(401, 'Unauthorized action');
        } else {
            return view('purchase.bill')->with('purchase', $purchase);
        }
    }

}
