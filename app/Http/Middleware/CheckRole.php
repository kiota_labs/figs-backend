<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;

class CheckRole {

    /**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = $this->getRoles($request);
        
        if ($this->auth->guest())
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                //abort(401, 'Unauthorized action 1.');
                return Redirect::to('auth/login');
            }
        }
        else if (! $request->user()->hasRole($roles)) 
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                abort(401, 'Unauthorized action 2.');
            }
        }
        
        
        return $next($request);
    }
    
    private function getRoles($request)
    {
        $actions = $request->route()->getAction();
        
        return $actions['roles'];
    }

}
