<?php 
namespace App\Http\Middleware;

/*
 * VkXmpp: ejabberd_xmlrpc implementation 
 */
class VkXmpp
{
//    public static function xmppRegisterUser($userName, $password, $node)
//    {
//        $output;
//        $status;
//        $result = [];
//        
//        //exec('sudo -u ejabberd /usr/sbin/ejabberdctl register '.$userName.' '.$node.' '.$password.' 2>&1', $output, $status);
//        exec('sudo -u daemon /Applications/ejabberd-15.04/bin/ejabberdctl register '.$userName.' '.$node.' '.$password.' 2>&1', $output, $status);
//        if($output == 0)
//        {
//            $result = ['success'=>true, 'status'=>$status, 'message'=>'User registered successfully.'];
//        }
//        else
//        {
//            // Failure, $output has the details
//            $message = '';
//            foreach($output as $o)
//            {
//                $message .= $o . "\n";
//            }
//            
//            $result = ['success'=>false, 'status'=>$status, 'message'=>$message];
//            $result['currentUser'] = exec('whoami');
//        }
//        
//        return $result;
//    }
    
    private static function sendRequest($command, $params)
    {
//        $param_auth = array("user"=>"admin", "server"=>"localhost", "password"=>"password");
//        $param_comm = array("user"=>"testuser", "host"=>"localhost");
//        $param = array($param_auth, $param_comm);
        
        $request = xmlrpc_encode_request($command, $params, (array('encoding' => 'utf-8')));
        
        $context = stream_context_create(array('http' => array(
            'method' => "POST",
            'header' => "User-Agent: XMLRPC::Client mod_xmlrpc\r\n" .
                        "Content-Type: text/xml\r\n" .
                        "Content-Length: ".strlen($request),
            'content' => $request
        )));
        
        $file = file_get_contents("http://127.0.0.1:4560/RPC2", false, $context);
        
        $response = xmlrpc_decode($file);
        
        if (xmlrpc_is_fault($response)) {
            trigger_error("xmlrpc: $response[faultString] ($response[faultCode])");
        } else {
            return $response;
        }
    }
    
    public static function registerUser($userName, $password)
    {
        $host = 'ip-172-31-31-239.us-west-2.compute.internal';
        $command = 'register';
        $params = array('user'=>$userName, 'host'=>$host, 'password'=>$password);
        $response = self::sendRequest($command, $params);
        return $response;
    }
}
