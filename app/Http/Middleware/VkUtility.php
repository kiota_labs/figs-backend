<?php 
namespace App\Http\Middleware;
use Illuminate\Support\Facades\File;

class VkUtility 
{
    public static function randomString($length = 10, $prifix = '')
    {
        //characters without similar looking characters
        $characters = '23456789abdefghjkmnpqrstwxyzABDEFGHJKMNPQRSTWXYZ@!';
        $randomString = '';
        for ($i = 0; $i < $length; ++$i)
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $prifix . $randomString;
    }

    public static function randomStringAlpha($length = 10, $prifix = '')
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; ++$i)
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $prifix . $randomString;
    }

    public static function randomStringNum($length = 10, $prifix = '')
    {
        $characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < $length; ++$i)
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $prifix . $randomString;
    }

    public static function randomStringAlphpNum($length = 10, $prifix = '')
    {
        $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; ++$i)
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $prifix . $randomString;
    }

    // sendApnsNotification: sends push notification
    // $deviceToken: string, device token
    // $message: string, the message to be sent
    public static function sendApnsNotification($deviceToken, $message, $deviceBadge=0, $data=array())
    {
        //path to certificate file
        $certFile = env('APPLE_CERTIFICATE', 'apns_prod.pem');
        //$certFilePath = public_path($certFile);
        //echo $certFile;
        //echo (file_exists($certFile)) ? 'exists' : 'not found'; die;
        
        // passphrase
        $passPhrase = env('APPLE_PASSPHRASE', '');
        
        //is to use sandbox
        $sandbox = env('APPLE_SANDBOX', false);
        //print_r(array($certFile, $passPhrase, $sandbox)); die;
        $streamContext = stream_context_create();
        stream_context_set_option($streamContext,'ssl', 'local_cert', $certFile);
        stream_context_set_option($streamContext, 'ssl', 'passphrase', $passPhrase);
        
        if($sandbox)
            $apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195',$error,$errorString,60,STREAM_CLIENT_CONNECT, $streamContext);
        else
            $apns = stream_socket_client('ssl://gateway.push.apple.com:2195',$error,$errorString,60,STREAM_CLIENT_CONNECT, $streamContext);

        $load = array(
            'aps' => array(
                'alert' => $message,
                'badge' => $deviceBadge,
                'sound' => 'default',
                'data' => $data
            ));
        $payload = json_encode($load);
        //echo $payload; die;
        $apnsMessage = chr(0) . chr(0) . chr(32);

        $apnsMessage .= pack('H*', str_replace(' ','',$deviceToken));
        $apnsMessage .= chr(0) . chr(strlen($payload)) . $payload;

        fwrite($apns, $apnsMessage);
        fclose($apns);
        //echo "Just Wrote ".$payload; die;
    }

    public static function getLocationFromPostcode($zipcode="", $address="")
    {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($zipcode . " " . $address) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);

        $values = array('formattedAddress'=>'', 'latitude'=>0, 'longitude'=>0);
        if(isset($result['results'][0]))
        {
            $values['formattedAddress'] = $result['results'][0]['formatted_address'];
            $values['latitude'] = $result['results'][0]['geometry']['location']['lat'];
            $values['longitude'] = $result['results'][0]['geometry']['location']['lng'];
        }

        return $values;
    }

    public static function saveImageFromUrl($imageUrl, $imageDir, $thumbDir='', $maxWidth=0)
    {
        //set name of the image file
        $fileName = uniqid('', true) . '.jpeg';
        $filePath = $imageDir . $fileName;

        if(copy($imageUrl, $filePath))
        {
            if($thumbDir)
            {
                $maxWidth = ($maxWidth) ? $maxWidth : 160;
                $thumb = new VkThumb($imageDir . $fileName);
                $thumb->setDestination($thumbDir);
                $thumb->setMaxSize($maxWidth);
                $thumb->create();
            }

            return $fileName;
        }
        else
            return '';
    }

    public static function saveBase64ImagePng($base64Image, $imageDir, $thumbDir='', $maxWidth=0)
    {
        //set name of the image file
        $ext = '.' . VkUtility::getMimeTypeFromBase64($base64Image);
        $fileName = uniqid('', true) . $ext;

        $base64Image = trim($base64Image);
        $base64Image = str_replace('data:image/png;base64,', '', $base64Image);
        $base64Image = str_replace('data:image/jpg;base64,', '', $base64Image);
        $base64Image = str_replace('data:image/jpeg;base64,', '', $base64Image);
        $base64Image = str_replace('data:image/gif;base64,', '', $base64Image);
        $base64Image = str_replace(' ', '+', $base64Image);

        $imageData = base64_decode($base64Image);
        $mime_type = finfo_buffer(finfo_open(), base64_decode($base64Image), FILEINFO_MIME_TYPE);
        
        $image = imagecreatefromstring($imageData);
        $filePath = $imageDir . $fileName;
        
        if($ext == '.png')
            $success = imagepng($image, $filePath);
        else if($ext == '.gif')
            $success = imagegif($image, $filePath);
        else
            $success = imagejpeg($image, $filePath);

        if($success)
        {
            if($thumbDir)
            {
                $maxWidth = ($maxWidth) ? $maxWidth : 160;
                $thumb = new VkThumb($imageDir . $fileName);
                $thumb->setDestination($thumbDir);
                $thumb->setMaxSize($maxWidth);
                $thumb->create();
            }

            return $fileName;
        }
        else
            return '';
    }

    public static function saveBase64ImagePngWithName($name, $base64Image, $imageDir, $thumbDir='', $maxWidth=0)
    {
        //set name of the image file
        $fileName = $name . '.png';

        $base64Image = trim($base64Image);
        $base64Image = str_replace('data:image/png;base64,', '', $base64Image);
        $base64Image = str_replace('data:image/jpg;base64,', '', $base64Image);
        $base64Image = str_replace('data:image/jpeg;base64,', '', $base64Image);
        $base64Image = str_replace('data:image/gif;base64,', '', $base64Image);
        $base64Image = str_replace(' ', '+', $base64Image);

        $imageData = base64_decode($base64Image);
        $image = imagecreatefromstring($imageData);
        $filePath = $imageDir . $fileName;

        if(imagepng($image, $filePath))
        {
            if($thumbDir)
            {
                $maxWidth = ($maxWidth) ? $maxWidth : 160;
                $thumb = new VkThumb($imageDir . $fileName);
                $thumb->setDestination($thumbDir);
                $thumb->setMaxSize($maxWidth);
                $thumb->create();
            }

            return $fileName;
        }
        else
            return '';
    }

    public static function saveBase64ImagePngInSizes($base64Image, $imageDir, $sizes=array())
    {
        //set name of the image file
        $time = microtime(true);
        $micro = sprintf("%06d",($time - floor($time)) * 1000000);
        $date = new DateTime(date('Y-m-d H:i:s.'.$micro, $time));
        $fileName = $date->format("YmdHisu") . '.png';

        $base64Image = trim($base64Image);
        $base64Image = str_replace('data:image/png;base64,', '', $base64Image);
        $base64Image = str_replace('data:image/jpg;base64,', '', $base64Image);
        $base64Image = str_replace('data:image/jpeg;base64,', '', $base64Image);
        $base64Image = str_replace('data:image/gif;base64,', '', $base64Image);
        $base64Image = str_replace(' ', '+', $base64Image);

        $imageData = base64_decode($base64Image);
        $image = imagecreatefromstring($imageData);
        $filePath = $imageDir . '/' . $fileName;

        if(imagepng($image, $filePath))
        {
            if(count($sizes) > 0)
            {
                foreach ($sizes as $size)
                {
                    $maxWidth = $size;

                    //the following line is set due to sudden change in thumb size from 50 to 70
                    //to avoid changes at multiple places just before sending new build
                    if($size == 50)
                        $maxWidth = 70;

                    $thumbDir = $imageDir . '/thumb' . $size;
                    if (!file_exists($thumbDir))
                        mkdir($thumbDir, 0777, true);

                    $thumb = new VkThumb($imageDir . '/' . $fileName);
                    $thumb->setDestination($thumbDir);
                    $thumb->setMaxSize($maxWidth);
                    $thumb->create();
                }
            }

            return $fileName;
        }
        else
            return '';
    }

    public static function saveStringImagePng($stringImage, $imageDir, $thumbDir='', $maxWidth=0)
    {
        //set name of the image file
        $time = microtime(true);
        $micro = sprintf("%06d",($time - floor($time)) * 1000000);
        $date = new DateTime(date('Y-m-d H:i:s.'.$micro, $time));
        $fileName = $date->format("YmdHisu") . '.png';

        $image = imagecreatefromstring($stringImage);
        $filePath = $imageDir . '/' . $fileName;

        if(imagepng($image, $filePath))
        {
            if($thumbDir)
            {
                $maxWidth = ($maxWidth) ? $maxWidth : 160;
                $thumb = new VkThumb($imageDir . '/' . $fileName);
                $thumb->setDestination($thumbDir);
                $thumb->setMaxSize($maxWidth);
                $thumb->create();
            }

            return $fileName;
        }
        else
            return '';
    }
    
    public static function saveImageFile($file, $imageDir, $thumbDir='', $maxWidth=0){
        $fileName = uniqid('', true) . '.png';
        
        $filePath = $imageDir . '/' . $fileName;
        $image1 = file::get($file);
        $image = imagecreatefromstring($image1);

        if(imagepng($image, $filePath))
        {
            if($thumbDir)
            {
                $maxWidth = ($maxWidth) ? $maxWidth : 160;
                $thumb = new VkThumb($imageDir . '/' . $fileName);
                $thumb->setDestination($thumbDir);
                $thumb->setMaxSize($maxWidth);
                $thumb->create();
            }

            return $fileName;
        }
        else
            return '';
    }

    public static function uniqueMultiArray($input)
    {
        $serialized = array_map('serialize', $input);
        $unique = array_unique($serialized);
        return array_intersect_key($input, $unique);
    }

    public static function LogInFile($val)
    {
        try
        {
            $fp = fopen('elog/log.html', 'a');
            $msg = '<div style="margin: 10px; padding: 10px; border: 1px solid blue; background: #cccccc;">' . $val . '</div>';
            fwrite($fp, $msg);
            fclose($fp);
        }
        catch(Exception $e){}
    }

    public static function getTimeLeft($dateTime1, $dateTime2)
    {
        $difference = date_diff($dateTime1, $dateTime2, TRUE);
        $timeElapsed = '';

        if($difference->y > 1)
            $timeElapsed = $difference->y . ' years left';
        else if($difference->y == 1)
            $timeElapsed = '1 year left';
        else if($difference->m > 1)
            $timeElapsed = $difference->m . ' months left';
        else if($difference->m == 1)
            $timeElapsed = '1 month left';
        else if($difference->d > 1)
            $timeElapsed = $difference->d . ' days left';
        else if($difference->d == 1)
            $timeElapsed = '1 day left';
        else if($difference->h > 1)
            $timeElapsed = $difference->h . ' hours left';
        else if($difference->h == 1)
            $timeElapsed = '1 hour left';
        else if($difference->i > 1)
            $timeElapsed = $difference->i . ' minutes left';
        else if($difference->i == 1)
            $timeElapsed = '1 minute left';
        else if($difference->s > 1)
            $timeElapsed = $difference->s . ' seconds left';
        else if($difference->s == 1)
            $timeElapsed = '1 second left';

        return $timeElapsed;
    }

    public static function getTimeElapsed($dateTime1, $dateTime2)
    {
        $difference = date_diff($dateTime1, $dateTime2, TRUE);
        $timeElapsed = '';

        if($difference->y > 1)
            $timeElapsed = $difference->y . ' years';
        else if($difference->y == 1)
            $timeElapsed = '1 year';
        else if($difference->m > 1)
            $timeElapsed = $difference->m . ' months';
        else if($difference->m == 1)
            $timeElapsed = '1 month';
        else if($difference->d > 1)
            $timeElapsed = $difference->d . ' days';
        else if($difference->d == 1)
            $timeElapsed = '1 day';
        else if($difference->h > 1)
            $timeElapsed = $difference->h . ' hours';
        else if($difference->h == 1)
            $timeElapsed = '1 hour';
        else if($difference->i > 1)
            $timeElapsed = $difference->i . ' minutes';
        else if($difference->i == 1)
            $timeElapsed = '1 minute';
        else if($difference->s > 1)
            $timeElapsed = $difference->s . ' seconds';
        else if($difference->s == 1)
            $timeElapsed = '1 second';

        return $timeElapsed;
    }

    public static function getTimeElapsed2($dateTime1, $dateTime2)
    {
        $difference = date_diff($dateTime1, $dateTime2, TRUE);
        $timeElapsed = '';

        if($difference->y > 1)
            $timeElapsed = $difference->y . ' years ago';
        else if($difference->y == 1)
        {
            if($difference->m > 1)
                $timeElapsed = '1 year ' . $difference->m . ' months ago';
            else if($difference->m == 1)
                $timeElapsed = '1 year 1 month ago';
            else
                $timeElapsed = '1 year ago';
        }
        else if($difference->m > 1)
            $timeElapsed = $difference->m . ' months ago';
        else if($difference->m == 1)
        {
            if($difference->d > 1)
                $timeElapsed = '1 month ' . $difference->d . ' days ago';
            else if($difference->d == 1)
                $timeElapsed = '1 month 1 day ago';
            else
                $timeElapsed = '1 month ago';
        }
        else if($difference->d > 1)
            $timeElapsed = $difference->d . ' days ago';
        else if($difference->d == 1)
        {
            if($difference->h > 1)
                $timeElapsed = '1 day ' . $difference->h . ' hours ago';
            else if($difference->h == 1)
                $timeElapsed = '1 day 1 hour ago';
            else
                $timeElapsed = '1 day ago';
        }
        else if($difference->h > 1)
            $timeElapsed = $difference->h . ' hours ago';
        else if($difference->h == 1)
        {
            if($difference->i > 1)
                $timeElapsed = '1 hour ' . $difference->i . ' minutes ago';
            else if($difference->i == 1)
                $timeElapsed = '1 hour 1 minute ago';
            else
                $timeElapsed = '1 hour ago';
        }
        else if($difference->i > 1)
            $timeElapsed = $difference->i . ' minutes ago';
        else if($difference->i == 1)
        {
            if($difference->s > 1)
                $timeElapsed = '1 minute ' . $difference->s . ' seconds ago';
            else if($difference->s == 1)
                $timeElapsed = '1 minute 1 second ago';
            else
                $timeElapsed = '1 minute ago';
        }
        else if($difference->s > 1)
            $timeElapsed = $difference->s . ' seconds ago';
        else if($difference->s == 1)
            $timeElapsed = '1 second ago';

        return $timeElapsed;
    }

    public static function getAddressFromLatLong($latitude, $longitude)
    {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false';
        $json = @file_get_contents($url);
        $data=json_decode($json);

        $status = $data->status;
        if($status=="OK")
            return $data->results[0]->formatted_address;
        else
            return '';
    }

    public static function getCityFromLatLong($latitude, $longitude)
    {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false';
        $json = @file_get_contents($url);
        $data=json_decode($json);

        $status = $data->status;
        if($status=="OK")
        {
            $addressComponents = $data->results[0]->address_components;
            $administrativeAreaLevel2 = getAddressComponent($addressComponents, 'administrative_area_level_2');
            $administrativeAreaLevel1 = getAddressComponent($addressComponents, 'administrative_area_level_1');
            $country = getAddressComponent($addressComponents, 'country');

            if(!empty($administrativeAreaLevel2))
                return $administrativeAreaLevel2;
            else if(!empty($administrativeAreaLevel1))
                return $administrativeAreaLevel1;
            else
                return $country;
        }
        else
            return '';
    }

    public static function getAddressComponentsFromLatLong($latitude, $longitude)
    {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false';
        $json = @file_get_contents($url);
        $data=json_decode($json);
        $components = array(
            'sublocalityLevel1' => '', 
            'locality' => '', 
            'administrativeAreaLevel2' => '', 
            'administrativeAreaLevel1' => '', 
            'country' => '', 
            'postalCode' => '', 
            'formattedAddress' => ''
        );
        
        $status = $data->status;
        if($status=="OK")
        {
            $addressComponents = $data->results[0]->address_components;
            $components['sublocalityLevel1'] = self::getAddressComponent($addressComponents, 'sublocality_level_1');
            $components['locality'] = self::getAddressComponent($addressComponents, 'locality');
            $components['administrativeAreaLevel2'] = self::getAddressComponent($addressComponents, 'administrative_area_level_2');
            $components['administrativeAreaLevel1'] = self::getAddressComponent($addressComponents, 'administrative_area_level_1');
            $components['country'] = self::getAddressComponent($addressComponents, 'country');
            $components['postalCode'] = self::getAddressComponent($addressComponents, 'postal_code');
            $components['formattedAddress'] = $data->results[0]->formatted_address;
        }
        
        return $components;
    }

    public static function getAddressComponent($addressComponents, $type)
    {
        for($i=0; $i<count($addressComponents); $i++)
        {
            if(in_array($type, $addressComponents[$i]->types))
                return $addressComponents[$i]->long_name;
        }
        return '';
    }

    public static function compareDistance($a, $b)
    {
        if(!isset($a['distance']) || !isset($b['distance']))
            return 0;

        if($a['distance'] == $a['distance'])
            return 0;

        return ($a['distance'] < $b['distance']) ? -1 : 1;
    }
    
    public static function getJsonInput()
    {
        $jsonInput = file_get_contents("php://input");
        $jsonInput = utf8_encode($jsonInput);
        //$jsonInput = str_replace('\"', '"', $jsonInput);
        //$jsonInput = html_entity_decode($jsonInput);
        //$jsonInput = stripslashes($jsonInput);
        $_JSON = json_decode($jsonInput, 1);
        return $_JSON;
    }
    
    public static function getBase64JsonInput()
    {
        $inputBase64 = file_get_contents("php://input");
        $inputJson = base64_decode($inputBase64);
//        LogInFile($inputJson);
        //$inputJson = utf8_encode($inputJson);
        //LogInFile($inputJson);
        $_JSON = json_decode($inputJson, 1);
        //LogInFile($_JSON);
        return $_JSON;
    }
    
    public static function error($api, $errorCode, $text)
    {
        return array('api'=>$api, 'status'=>'error', 'errorCode'=>$errorCode, 'apiMessage'=>$text);
    }
    
    public static function success($api, $text)
    {
        return array('api'=>$api, 'status'=>'OK', 'apiMessage'=>$text);
    }
    
    public static function getEncryptedPassword($password, $saltText)
    {
        $saltText = strtolower($saltText);  // as email is being used as salt
        $salt = md5($saltText);
        $encryptedPassword = crypt($password, $salt);
        return $encryptedPassword;
    }
    
    public static function getMimeTypeFromBase64($base64Image)
    {
        $mimeType = finfo_buffer(finfo_open(), base64_decode($base64Image), FILEINFO_MIME_TYPE);
        return substr($mimeType, strpos($mimeType, '/')+1);
    }
    
    public static function sendMail($view, $data, $subject, $receiverEmail) {

        \Mail::send($view, ['key' => $data], function($message)use($subject, $receiverEmail) {
            $message->to($receiverEmail, 'Tapp Admin')->subject($subject);
        });
        
        if (count(\Mail::failures()) > 0) {
            $errors = 'Failed to send email, please try again.';
            return $errors;
        }
        return "success";
    }
}
