<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
 * Web page routes
 */

//Route::get('/', 'WelcomeController@index');

Route::get('/', 'SiteController@index');
Route::get('home', 'SiteController@home');

Route::get('aboutUs', 'SiteController@aboutUs');
Route::get('termsOfService', 'SiteController@termsOfService');
Route::get('privacyPolicy', 'SiteController@privacyPolicy');
Route::get('myProfile', 'SiteController@myProfile');


// Route::get('home', 'HomeController@index');

/*
 * Web-services
 */
Route::post('/service/dayMenu/getList', 'Webservices\DayMenuController@getList');

Route::post('/service/user/signup', 'Webservices\UserController@signup');
Route::post('/service/user/signin', 'Webservices\UserController@signin');
Route::post('/service/user/signinFb', 'Webservices\UserController@signinFb');
Route::post('/service/user/validateEmail', 'Webservices\UserController@validateEmail');
Route::post('/service/user/updateProfile', 'Webservices\UserController@updateProfile');
Route::post('/service/user/addUserAddress', 'Webservices\UserController@addUserAddress');
Route::post('/service/user/updateUserAddress', 'Webservices\UserController@updateUserAddress');
Route::post('/service/user/deleteUserAddress', 'Webservices\UserController@deleteUserAddress');
Route::post('/service/user/listUserAddress', 'Webservices\UserController@listUserAddress');

Route::post('/service/user/updateSettings', 'Webservices\UserController@updateSettings');
Route::post('/service/user/changePassword', 'Webservices\UserController@changePassword');
Route::post('/service/user/logout', 'Webservices\UserController@logout');
Route::post('/service/user/resetPassword', 'Webservices\UserController@resetPassword');
Route::post('/service/user/getProfile', 'Webservices\UserController@getProfile');

Route::post('/service/phoneVerification/getVerificationCode', 'Webservices\PhoneVerificationController@getVerificationCode');
Route::post('/service/phoneVerification/getVerificationCodeOverPos', 'Webservices\PhoneVerificationController@getVerificationCodeOverPos');

Route::post('/service/purchase/add', 'Webservices\PurchaseController@add');
Route::post('/service/purchase/getUserPurchases', 'Webservices\PurchaseController@getUserPurchases');
Route::post('/service/purchase/getTodaysPurchases', 'Webservices\PurchaseController@getTodaysPurchases');
Route::post('/service/purchase/applyCoupon', 'Webservices\PurchaseController@applyCoupon');
Route::post('/service/purchase/updatePurchase', 'Webservices\PurchaseController@updatePurchase');
Route::post('/service/purchase/fetchPurchaseById', 'Webservices\PurchaseController@fetchPurchaseById');


Route::post('/service/notification/getList', 'Webservices\NotificationController@getList');
Route::post('/service/notification/delete', 'Webservices\NotificationController@delete');
Route::post('/service/notification/markAsSeen', 'Webservices\NotificationController@markAsSeen');

Route::post('/service/itemReview/add', 'Webservices\ItemReviewController@add');

Route::post('/service/itemLike/add', 'Webservices\ItemLikeController@add');
Route::post('/service/itemLike/remove', 'Webservices\ItemLikeController@remove');


/*
 * reset password
 */
Route::post('/service/password/sendEmail', 'Auth\PasswordController@sendEmail');

/*
 * Web-services for testing
 */
Route::post('/service/testing/sendNotification', 'Webservices\TestingController@sendNotification');
Route::post('/service/testing/getMimeType', 'Webservices\TestingController@getMimeType');
Route::post('/service/testing/getHash', 'Webservices\TestingController@getHash');

