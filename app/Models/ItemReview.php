<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class ItemReview extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'itemReview';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    public static function getQuery()
    {
        $imagePath = asset('/images/user') . '/';
        $thumbPath = asset('/images/user/thumb') . '/';
        
        $sql = 'SELECT ir.id as itemReviewId';
        $sql .= ',ir.userId';
        $sql .= ',ir.itemId';
        $sql .= ',ir.rating';
        $sql .= ',ir.comments';
        $sql .= ',IFNULL(u.email, "") as email';
        $sql .= ',IFNULL(u.username, "") as username';
        $sql .= ',IFNULL(u.fullName, "") as fullName';
        $sql .= ',IFNULL(CONCAT("' . $imagePath . '",u.image), "") as image';
        $sql .= ',IFNULL(CONCAT("' . $thumbPath . '",u.image), "") as thumb';
        $sql .= ',ir.createDate';
        $sql .= ' FROM itemReview ir';
        $sql .= ' JOIN user u ON ir.userId = u.id';
        
        return $sql;
    }
    
    public static function getList($itemId=0, $userId=0, $exceptions='')
    {
        $sql = self::getQuery();
        $sql .= ' WHERE 1';
        $sql .= ' AND ir.isDisabled = 0';
        
        if($userId)
            $sql .= " AND ir.userId = $userId";
        
        if($itemId)
            $sql .= " AND ir.itemId = $itemId";
        
        if($exceptions)
            $sql .= " AND ir.id NOT IN($exceptions)";
        
        $sql .= " ORDER BY ir.id ASC";
        $sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }

}
