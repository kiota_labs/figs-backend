<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class Event extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'event';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    /*
     * Event types
     */
    const POST_CREATED = 1;
    const POST_REPOSTED = 2;
    const COMMENT_CREATED = 3;
    const USER_FOLLOWED = 4;
    const USER_TAGGED = 5;
    const FRIEND_REQUEST_RECEIVED = 6;
    const FRIEND_REQUEST_ACCEPTED = 7;
    const TAPPS_PURCHASED = 8;
    const TAPPS_DONATED = 9;
    const TAPPS_RECEIVED_INVITING = 10;
    const TAPPS_RECEIVED_INVITED = 11;
    const TAPPS_RECEIVED_ON_POST_CREATION = 12;
    const TAPPS_RECEIVED_ON_PROFILE_COMPLETION = 13;
    const BADGE_RECEIVED_USING_APP = 14;
    const POST_EXPIRED = 15;
    const USER_DISABLED = 16;
    const USER_ENABLED = 17;
    const USER_INACTIVITY = 18;
    const TAPPS_RECEIVED_ON_TUTORIAL_COMPLETION = 19;
    const SUCCESSFUL_PAYPAL_PAYMENT = 20;
    const REJECTED_PAYPAL_PAYMENT =21;
    const REDEEM_REQUEST_SENT =22;
    const REDEEM_REQUEST_ACCEPTED =23;
    const REDEEM_REQUEST_REJECTED =24;
    const GOAL_ACHIEVED =25;
    
    /**
     * Returns query to get event records
     */
    public static function getQuery($relatedToPost = 0)
    {
        $imagePath = asset('/images/user') . '/';
        $thumbPath = asset('/images/user/thumb') . '/';
        
        $sql = 'SELECT e.id';
        $sql .= ',e.eventType';
        $sql .= ',e.raiserId';
        $sql .= ',IFNULL(u1.username, "") as raiserUsername';
        $sql .= ',IFNULL(u1.fullName, "") as raiserFullName';
        $sql .= ',IFNULL(CONCAT("' . $imagePath . '",u1.image), "") as raiserImage';
        $sql .= ',IFNULL(CONCAT("' . $thumbPath . '",u1.image), "") as raiserThumb';
        $sql .= ',IFNULL(e.relatedUserId, "") as relatedUserId';
        $sql .= ',IFNULL(u2.username, "") as relatedUsername';
        $sql .= ',IFNULL(u2.fullName, "") as relatedUserFullName';
        $sql .= ',IFNULL(CONCAT("' . $imagePath . '",u2.image), "") as relatedUserImage';
        $sql .= ',IFNULL(CONCAT("' . $thumbPath . '",u2.image), "") as relatedUserThumb';
        $sql .= ',e.elementId';
        $sql .= ',IFNULL(e.tappCount, 0) as tappCount';
        $sql .= ',IFNULL(e.badgeCount, 0) as badgeCount';
        $sql .= ',IFNULL(e.description, "") as description';
        $sql .= ',e.eventDate';
        $sql .= ',e.isNotified';
        $sql .= ' FROM `event` e';
        $sql .= ' JOIN `user` u1 ON e.raiserId=u1.id';
        
        if($relatedToPost)
            $sql .= ' JOIN `post` p ON e.elementId=p.id';
        
        $sql .= ' LEFT JOIN `user` u2 ON e.relatedUserId=u2.id';
        return $sql;
    }
    
    /**
     * Returns all events as per notification status
     * @param type $isNotified
     * @return type
     */
    public static function getEvents($isNotified)
    {
        //fetch all event records
        $sql = self::getQuery();
        $sql .= ' WHERE e.isNotified =' . $isNotified;
        $sql .= ' ORDER BY e.createDate ASC';
        $events = DB::select($sql);
        return $events;
    }
    
    /**
     * Returns all events related to a user
     * @param int $userId
     * @return type
     */
    public static function getListByUserId($userId, $selectedUserId, $eventTypes, $exceptions)
    {
        //fetch all event records
        $sql = self::getQuery();
        $sql .= " WHERE (e.raiserId=$userId OR e.relatedUserId=$userId)";
        
        if($eventTypes)
            $sql .= " AND e.eventType IN($eventTypes)";
        
        if($exceptions)
            $sql .= " AND e.id NOT IN($exceptions)";
        
        $sql .= ' ORDER BY e.createDate DESC';
        $sql .= " LIMIT 15";
        
        $events = DB::select($sql);
        return $events;
    }
    
    /**
     * Returns all events related to a user including tapps and badges
     * @param int $userId
     * @return type
     */
    public static function getTappHistory($userId, $selectedUserId, $exceptions)
    {
        $eventTypesArr = [
            EVENT::POST_CREATED,
            EVENT::TAPPS_PURCHASED,
            EVENT::TAPPS_DONATED,
            EVENT::TAPPS_RECEIVED_INVITING,
            EVENT::TAPPS_RECEIVED_INVITED,
            EVENT::TAPPS_RECEIVED_ON_POST_CREATION,
            EVENT::TAPPS_RECEIVED_ON_PROFILE_COMPLETION,
            EVENT::TAPPS_RECEIVED_ON_TUTORIAL_COMPLETION
        ];
        
        $eventTypes = join(',', $eventTypesArr);
        
        //fetch all event records
        $sql = self::getQuery();
        $sql .= " WHERE (e.raiserId=$userId OR e.relatedUserId=$userId)";
        $sql .= " AND e.eventType IN($eventTypes)";
        $sql .= " AND (e.tappCount>0 OR e.badgeCount>0)";
        
        if($exceptions)
            $sql .= " AND e.id NOT IN($exceptions)";
        
        $sql .= ' ORDER BY e.createDate DESC';
        $sql .= " LIMIT 15";
        
        $events = DB::select($sql);
        return $events;
    }
    
    /**
     * Returns all events related to a user including tapps and badges via posts
     * @param int $userId
     * @return type
     */
    public static function getTappHistoryViaPost($userId, $selectedUserId, $exceptions)
    {
        $eventTypesArr = [
            EVENT::TAPPS_DONATED
        ];
        
        $eventTypes = join(',', $eventTypesArr);
        
        //fetch all event records
        $sql = self::getQuery(1);
        $sql .= " WHERE e.relatedUserId=$userId";
        $sql .= " AND e.eventType IN($eventTypes)";
        $sql .= " AND (e.tappCount>0 OR e.badgeCount>0)";
        $sql .= " AND p.postType=1";
        
        if($exceptions)
            $sql .= " AND e.id NOT IN($exceptions)";
        
        $sql .= ' ORDER BY e.createDate DESC';
        $sql .= " LIMIT 15";
        
        $events = DB::select($sql);
        return $events;
    }
    
    /**
     * Returns all events related to a user including tapps as donation
     * @param int $userId
     * @return type
     */
    public static function getTappHistoryViaDonation($userId, $selectedUserId, $exceptions)
    {
        $eventTypesArr = [
            EVENT::TAPPS_PURCHASED
        ];
        
        $eventTypes = join(',', $eventTypesArr);
        
        //fetch all event records
        $sql = self::getQuery();
        $sql .= " WHERE e.relatedUserId=$userId";
        $sql .= " AND e.eventType IN($eventTypes)";
        $sql .= " AND (e.tappCount>0 OR e.badgeCount>0)";
        
        if($exceptions)
            $sql .= " AND e.id NOT IN($exceptions)";
        
        $sql .= ' ORDER BY e.createDate DESC';
        $sql .= " LIMIT 15";
        
        $events = DB::select($sql);
        return $events;
    }
    
    /**
     * Saves event record and returns new event id
     * @param type $eventType
     * @param type $raiserId
     * @param type $elementId
     * @param type $eventDate
     * @param type $relatedUserId
     * @return int
     */
    public static function saveEvent($eventType, $raiserId, $elementId, $eventDate, $relatedUserId=0, $isNotified=0, $tappCount=0, $badgeCount=0)
    {
        $event = new Event();
        $event->eventType = $eventType;
        $event->raiserId = $raiserId;
        $event->elementId = $elementId;
        $event->tappCount = $tappCount;
        $event->badgeCount = $badgeCount;
        $event->eventDate = $eventDate;
        $event->isNotified = $isNotified;
        
        if($relatedUserId)
            $event->relatedUserId = $relatedUserId;
        
        $event->save();
        
        return ($event->id) ? $event->id : 0;
    }
}
