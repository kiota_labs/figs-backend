<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class Purchase extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'purchase';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    /**
     * 
     * return query to fetch data from purchase table
     */
    public static function getQuery()
    {
        $sql = 'SELECT p.id as purchaseId';
        $sql .= ',IFNULL(p.userId, "") as userId';
        $sql .= ',IFNULL(p.street, "") as street';
        $sql .= ',IFNULL(p.locality, "") as locality';
        $sql .= ',IFNULL(p.landmark, "") as landmark';
        $sql .= ',IFNULL(p.phone, "") as phone';
        $sql .= ',IFNULL(p.saveCutlery, "") as saveCutlery';
        $sql .= ',IFNULL(p.couponCode, "") as couponCode';
        $sql .= ',IFNULL(p.purchaseDate, "") as purchaseDate';
        $sql .= ',IFNULL(p.paymentDate, "") as paymentDate';
        $sql .= ',IFNULL(p.deliveryDate, "") as deliveryDate';
        $sql .= ',IFNULL(p.paypalTransId, "") as paypalTransId';
        $sql .= ',IFNULL(p.purchaseStatus, "") as purchaseStatus';
        $sql .= ',IFNULL(p.isPaid, "") as isPaid';
        $sql .= ',IFNULL(p.isCancelled, "") as isCancelled';
        $sql .= ',IFNULL(p.cancelReason, "") as cancelReason';
        
        $sql .= ' FROM purchase p';
        /* LEFT JOIN because userId might be null */
        $sql .= ' LEFT JOIN user u on p.userId = u.id';
        
        return $sql;
    }
    
    public static function getList($userId=0, $exceptions='')
    {
        $sql = self::getQuery();
        $sql .= ' WHERE 1';
        
        if($userId)
            $sql .= " AND p.userId = $userId";
        
        if($exceptions)
            $sql .= " AND p.id NOT IN($exceptions)";
        
        $sql .= " ORDER BY p.purchaseDate DESC";
        $sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }
    
	public static function getListForToday()
    {
        $sql = self::getQuery();
		$sql .= ' WHERE 1';
		$sql .= " AND CAST(p.purchaseDate AS DATE) >= CAST(CURRENT_TIMESTAMP AS DATE)";
		$sql .= " AND p.isCancelled = 0";      		//	not canceled
		$sql .= " AND p.purchaseStatus != 4";  		// 	delivered
		$sql .= " ORDER BY p.purchaseDate DESC";
		$records = DB::select($sql);

        return $records;
    }
	
    public static function getPurchaseById($purchaseId)
    {
        $sql = self::getQuery();
        $sql .= " WHERE p.id = $purchaseId";
        $records = DB::select($sql);
        
        if(empty($records))
            return null;
        else
            return $records[0];
    }

}
