<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class Notification extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notification';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    /**
     * 
     * relationships
     */
    public function event()
    {
        return $this->belongsTo('App\Models\Event', 'eventId', 'id');
    }
    
    /**
     * Returns query to get notification records
     */
    public static function getQuery()
    {
        $imagePath = asset('/images/user') . '/';
        $thumbPath = asset('/images/user/thumb') . '/';
        
        $sql = 'SELECT n.id';
        $sql .= ',n.receiverId';
        $sql .= ',n.message';
        $sql .= ',n.eventId';
        $sql .= ',n.isSeen';
        $sql .= ',e.eventType';
        $sql .= ',e.raiserId';
        $sql .= ',IFNULL(u1.username, "") as raiserUsername';
        $sql .= ',IFNULL(u1.fullName, "") as raiserFullName';
        $sql .= ',IFNULL(CONCAT("' . $imagePath . '", u1.image), "") as raiserImage';
        $sql .= ',IFNULL(CONCAT("' . $thumbPath . '", u1.image), "") as raiserThumb';
        $sql .= ',IFNULL(e.relatedUserId, "") as relatedUserId';
        $sql .= ',IFNULL(u2.username, "") as relatedUsername';
        $sql .= ',IFNULL(u2.fullName, "") as relatedUserFullName';
        $sql .= ',e.elementId';
        $sql .= ',e.eventDate';
        $sql .= ',e.isNotified';
        $sql .= ' FROM `notification` n';
        $sql .= ' JOIN `event` e ON n.eventId=e.id';
        $sql .= ' JOIN `user` u1 ON e.raiserId=u1.id';
        $sql .= ' LEFT JOIN `user` u2 ON e.relatedUserId=u2.id';
        return $sql;
    }
    
    /**
     * Returns all notification records by receiver id
     * @param type $receiverId
     * @return type
     */
    public static function getNotificationsByReceiverId($receiverId)
    {
        //fetch all notification records
        $sql = self::getQuery();
        $sql .= ' WHERE n.receiverId =' . $receiverId;
        $sql .= ' ORDER BY n.createDate DESC';
        $notifications = DB::select($sql);
        return $notifications;
    }
    
    /**
     * Saves notification record and returns new notification id
     * @param type $receiverId
     * @param type $message
     * @param type $eventId
     * @return int
     */
    public static function saveNotification($receiverId, $message, $eventId)
    {
        $notification = new Notification();
        $notification->receiverId = $receiverId;
        $notification->message = $message;
        $notification->eventId = $eventId;
        $notification->save();
        
        return ($notification->id) ? $notification->id : 0;
    }
    
    /**
     * Mark notification as seen
     * @param type $receiverId
     * @param type $eventType
     * @param type $notificationId
     * @return int
     */
    public static function markAsSeen($receiverId, $eventTypes='', $notificationIds='')
    {
        $defaultEventTypes = Event::FRIEND_REQUEST_ACCEPTED . ',' . Event::TAPPS_PURCHASED;
        $eventTypes = ($eventTypes) ? $eventTypes . ',' . $defaultEventTypes : $defaultEventTypes;
        
        //$sql = 'DELETE n FROM notification n';
        $sql = 'UPDATE notification n';
        $sql .= ' JOIN event e ON n.eventId=e.id';
        $sql .= ' SET n.isSeen=1';
        $sql .= ' WHERE 1';
        $sql .= " AND n.receiverId=$receiverId";
        $sql .= " AND (e.eventType IN ($eventTypes)";
        
        if($notificationIds)
            $sql .= " OR n.id IN ($notificationIds)";
        
        $sql .= ")";
        
        return DB::update($sql);
    }
}
