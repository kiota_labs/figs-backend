<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class UserAddress extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'userAddress';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    public static function getQuery()
    {
        $sql = 'SELECT ua.id as userAddressId';
        $sql .= ',ua.userId';
        $sql .= ',IFNULL(ua.placeName, "") as placeName';
        $sql .= ',IFNULL(ua.street, "") as street';
        $sql .= ',IFNULL(ua.locality, "") as locality';
        $sql .= ',IFNULL(ua.landmark, "") as landmark';
        $sql .= ' FROM userAddress ua';
        
        return $sql;
    }
    
    public static function getList($userId=0, $exceptions='')
    {
        $sql = self::getQuery();
        $sql .= ' WHERE 1';
        
        if($userId)
            $sql .= " AND ua.userId = $userId";
        
        if($exceptions)
            $sql .= " AND ua.id NOT IN($exceptions)";
        
        $sql .= " ORDER BY ua.id";
        //$sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }
    
    public static function getById($userAddressId, $userId)
    {
        $sql = self::getQuery();
        $sql .= ' WHERE 1';
        $sql .= " AND ua.id = $userAddressId";
        $sql .= " AND ua.userId = $userId";
        $records = DB::select($sql);
        
        if(empty($records))
            return null;
        else
            return $records[0];
    }

}
