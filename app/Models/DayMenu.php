<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class DayMenu extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dayMenu';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    public static function getQuery($userId=0)
    {
        $itemImagePath = asset('/images/item') . '/';
        $itemThumbPath = asset('/images/item/thumb') . '/';
        
        $sql = 'SELECT dm.id as dayMenuId';
        $sql .= ',dm.menuDate';
        $sql .= ',dm.itemId';
        $sql .= ',IFNULL(i.itemName, "") as itemName';
        $sql .= ',IFNULL(i.description, "") as itemDescription';
        $sql .= ',i.price';
        $sql .= ',i.itemType';
        $sql .= ',IFNULL(CONCAT("' . $itemImagePath . '",i.image), "") as itemImage';
        $sql .= ',IFNULL(CONCAT("' . $itemThumbPath . '",i.image), "") as itemThumb';
        $sql .= ',i.likeCount';
        $sql .= ',i.reviewCount';
        $sql .= ',i.ratingCount';
        $sql .= ',IF(i.ratingCount = 0, 0, i.totalRating/i.ratingCount) as averageRating';
        
        if($userId)
            $sql .= ',IF(il.id is null, 0, 1) as iLikedIt';
        
        $sql .= ' FROM dayMenu dm';
        $sql .= ' JOIN item i ON dm.itemId=i.id';
        
        if($userId)
            $sql .= " LEFT JOIN itemLike il on il.itemId = i.id AND il.userId=" . $userId;
        
        return $sql;
    }
    
    public static function getList($userId=0, $menuDate = '', $itemType=0, $exceptions='')
    {
        $sql = self::getQuery($userId);
        $sql .= ' WHERE 1';
        $sql .= ' AND i.isDisabled = 0';
        
        if($menuDate)
            $sql .= " AND dm.menuDate = '$menuDate'";
        
        if($itemType)
            $sql .= " AND i.itemType = $itemType";
        
        if($exceptions)
            $sql .= " AND dm.id NOT IN($exceptions)";
        
        $sql .= " ORDER BY dm.menuDate, i.itemType, i.itemName";
        //$sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }

}
