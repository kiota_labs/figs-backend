<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class ItemLike extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'itemLike';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    
    
    public static function getQuery()
    {
        $userImagePath = asset('/images/user') . '/';
        $userThumbPath = asset('/images/user/thumb') . '/';
        
        $sql = 'SELECT il.id as itemLikeId';
        $sql .= ',il.itemId';
        $sql .= ',il.userId';
        $sql .= ',IFNULL(u.username, "") as username';
        $sql .= ',IFNULL(u.fullName, "") as userFullName';
        $sql .= ',IFNULL(CONCAT("' . $userImagePath . '",u.image), "") as userImage';
        $sql .= ',IFNULL(CONCAT("' . $userThumbPath . '",u.image), "") as userThumb';
        $sql .= ',il.createDate';
        $sql .= ' FROM itemLike il';
        $sql .= ' JOIN user u ON il.userId=u.id';
        
        return $sql;
    }
    
    public static function getList($itemId=0, $userId=0, $exceptions='')
    {
        $sql = self::getQuery();
        $sql .= ' WHERE 1';
        $sql .= ' AND u.isDisabled=0';

        if($userId)
            $sql .= " AND il.userId = $userId";
        
        if($itemId)
            $sql .= " AND il.itemId = $itemId";
        
        if($exceptions)
            $sql .= " AND il.id NOT IN($exceptions)";
        
        $sql .= " ORDER BY il.createDate";
        $sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }

}
