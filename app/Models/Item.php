<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class Item extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'item';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    /**
     * 
     * return query to fetch data from item table
     */
    public static function getQuery($userId=0)
    {
        $itemImagePath = asset('/images/item') . '/';
        $itemThumbPath = asset('/images/item/thumb') . '/';
        
        $sql = 'SELECT i.id';
        $sql .= ',IFNULL(i.itemName, "") as itemName';
        $sql .= ',IFNULL(i.description, "") as description';
        $sql .= ',i.price';
        $sql .= ',i.itemType';
        $sql .= ',IFNULL(CONCAT("' . $itemImagePath . '",i.image), "") as itemImage';
        $sql .= ',IFNULL(CONCAT("' . $itemThumbPath . '",i.image), "") as itemThumb';
        $sql .= ',i.likeCount';
        $sql .= ',i.reviewCount';
        $sql .= ',i.ratingCount';
        $sql .= ',IF(i.ratingCount = 0, 0, i.totalRating/i.ratingCount) as averageRating';
        
        if($userId)
            $sql .= ',IF(il.id is null, 0, 1) as iLikedIt';
        
        $sql .= ' FROM item i';

        if($userId)
            $sql .= " LEFT JOIN itemLike il on il.itemId = i.id AND il.userId=" . $userId;
        
        return $sql;
    }
    
    public static function getList($userId=0, $itemType=0, $exceptions='')
    {
        $sql = self::getQuery($userId);
        $sql .= ' WHERE 1';
        $sql .= " AND i.isDisabled = 0";
        
        if($itemType)
            $sql .= " AND i.itemType = $itemType";
        
        if($exceptions)
            $sql .= " AND i.id NOT IN($exceptions)";
        
        $sql .= " ORDER BY i.itemName ASC";
        $sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }

    public function itemTypeName()
    {
        $itemTypes = ['Not mentioned' ,'Breakfast', 'All day meal', 'Beverages'];
        return $itemTypes[$this->itemType];
    }

    public static function getItemTypeName($itemType)
    {
        $itemTypes = ['Not mentioned' ,'Breakfast', 'All day meal', 'Beverages'];
        return $itemTypes[$itemType];
    }

	 public static function getListById($id)
    {
		$userId=0;
        $sql = self::getQuery($userId);
        $sql .= ' WHERE 1';
        $sql .= " AND i.id = $id";
        
        // if($itemType)
            // $sql .= " AND i.itemType = $itemType";
        
        // if($exceptions)
            // $sql .= " AND i.id NOT IN($exceptions)";
        
        // $sql .= " ORDER BY i.itemName ASC";
        // $sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }
}
