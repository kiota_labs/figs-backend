<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Support\Facades\DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    /*
     * use different column for remember_token column
     */
    public function getRememberToken()
    {
        return $this->rememberToken;
    }

    public function setRememberToken($value)
    {
        $this->rememberToken = $value;
    }

    public function getRememberTokenName()
    {
        return 'rememberToken';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role', 'facebookId','email',  'username', 'password', 'fullName', 'street', 'locality', 'landmark', 'phone', 'image', 'referralCode', 'cardNo', 'notifyMe'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];
    
    /**
     * relationship with userSession table
     */
    public function userSession()
    {
        return $this->hasOne('UserSession');
    }
    
    /*
     * check user's role matches with one of the given roles
     * @param $roles an array of roles
     */
    public function hasRole($roles)
    {
        $userRoles = explode(',', $this->role);
        foreach($roles as $role)
        {
            if(in_array($role, $userRoles))
                return true;
        }
        
        return false;
    }
    
    public static function getQuery($includeSettings=0, $includePassword=0, $includeDetails=1)
    {
        $imagePath = asset('/images/user') . '/';
        $thumbPath = asset('/images/user/thumb') . '/';
        
        $sql = 'SELECT u.id as userId';
        $sql .= ',u.role';
        $sql .= ',IFNULL(u.email, "") as email';
        $sql .= ',IFNULL(u.username, "") as username';
        $sql .= ',IFNULL(u.facebookId, "") as facebookId';
        $sql .= ',IFNULL(u.fullName, "") as fullName';
        $sql .= ',IFNULL(CONCAT("' . $imagePath . '",u.image), "") as image';
        $sql .= ',IFNULL(CONCAT("' . $thumbPath . '",u.image), "") as thumb';
        $sql .= ',IFNULL(u.referralCode, "") as referralCode';
        $sql .= ',u.createDate';
        
        if($includeDetails)
        {
            $sql .= ',IFNULL(u.street, "") as street';
            $sql .= ',IFNULL(u.locality, "") as locality';
            $sql .= ',IFNULL(u.landmark, "") as landmark';
            $sql .= ',IFNULL(u.phone, "") as phone';
        }
        
        if($includeSettings)
        {
            $sql .= ',IFNULL(u.cardNo, "") as cardNo';
            $sql .= ',IFNULL(u.paypalId, "") as paypalId';
            $sql .= ',IFNULL(u.notifyMe, "") as notifyMe';
            $sql .= ',IFNULL(u.isPhoneVerified, "") as isPhoneVerified';
            $sql .= ',IFNULL(u.referralCode, "") as referralCode';
            $sql .= ',IFNULL(u.referralCodeUsed, "") as referralCodeUsed';
            $sql .= ',u.balanceReferralAmount';
        }
        
        if($includePassword)
        {
            $sql .= ',u.password';
        }
        
        $sql .= ' FROM user u';
        
        return $sql;
    }
    
    public static function getUserById($selectedUserId, $includeSettings=0)
    {
        $sql = self::getQuery($includeSettings);
        $sql .= ' WHERE 1';
        $sql .= " AND u.id = $selectedUserId";
        $records = DB::select($sql);
        
        if(empty($records))
            return null;
        else
            return $records[0];
    }
    
    public static function search($searchText, $exceptions='')
    {
        $sql = self::getQuery(0, 0, 0);
        $sql .= ' WHERE 1';
        $sql .= ' AND u.isDisabled=0';
        $sql .= ' AND u.role = "user"';
        $sql .= " AND (u.email LIKE '%$searchText%' || u.username LIKE '%$searchText%' || u.fullName LIKE '%$searchText%')";
        
        if($exceptions)
            $sql .= " AND u.id NOT IN ($exceptions)";
        
        $sql .= ' ORDER BY u.username, u.email';
        $sql .= ' LIMIT 15';
        $records = DB::select($sql);
        
        return $records;
    }
    
    public static function getUserByEmail($email, $includeSettings=0, $includePassword=0)
    {
        $sql = self::getQuery($includeSettings, $includePassword);
        $sql .= ' WHERE 1';
        $sql .= ' AND u.isDisabled=0';
        $sql .= " AND u.email = '$email'";
        $records = DB::select($sql);
        
        if(empty($records))
            return null;
        else
            return $records[0];
    }
    
    public static function getUserByEmailOrUsername($email, $includeSettings=0, $includePassword=0)
    {
        $sql = self::getQuery($includeSettings, $includePassword);
        $sql .= ' WHERE 1';
        $sql .= ' AND u.isDisabled=0';
        $sql .= " AND (u.email = '$email' OR u.username= '$email')";
        $records = DB::select($sql);
        
        if(empty($records))
            return null;
        else
            return $records[0];
    }
    
    public static function emailExists($email, $exceptUserId=0)
    {
        $sql = self::getQuery(0, 0, 0, 0);
        $sql .= ' WHERE 1';
        $sql .= " AND u.email = '$email'";
        
        if($exceptUserId)
            $sql .= " AND u.id != $exceptUserId";
        
        $records = DB::select($sql);
        
        return !empty($records);
    }
    
    public static function getUserByEmails($emails, $userId=0)
    {
        $sql = self::getQuery($userId, 0, 0, 0);
        $sql .= ' WHERE 1';
        $sql .= ' AND u.isDisabled=0';
        $sql .= ' AND u.role = "user"';
        $sql .= " AND u.email IN($emails)";
        
        if($userId)
            $sql .= " AND u.id != $userId";
        
        $records = DB::select($sql);
        
        return $records;
    }
    
    public static function getUserByFacebookId($facebookId, $includeSettings=0)
    {
        $sql = self::getQuery($includeSettings, 0);
        $sql .= ' WHERE 1';
        $sql .= ' AND u.isDisabled=0';
        $sql .= " AND u.facebookId = '$facebookId'";
        $records = DB::select($sql);
        
        if(empty($records))
            return null;
        else
            return $records[0];
    }
    
    public static function getUserByFacebookIds($facebookIds)
    {
        $sql = self::getQuery(0, 0, 0);
        $sql .= ' WHERE 1';
        $sql .= ' AND u.isDisabled=0';
        $sql .= ' AND u.role = "user"';
        $sql .= " AND u.facebookId IN($facebookIds)";
        
        if($userId)
            $sql .= " AND u.id != $userId";
        
        $records = DB::select($sql);
        
        return $records;
    }
    
    public static function getReferralBenefits($userId, $referralCodeUsed)
    {
        $referringUser = User::find($userId);
        $referredUser = User::where('referralCode', $referralCodeUsed)->first();

        if(!is_null($referringUser) && !is_null($referredUser) && $referringUser->id != $referredUser->id)
        {
            //give benefit to referringUser
            //give benefit to referredUser
			
			//kohli adding
			$bonusAmount = 100.00;
			$referringUser->balanceReferralAmount = $referringUser->balanceReferralAmount+ $bonusAmount;
			$referredUser->balanceReferralAmount  = $referredUser->balanceReferralAmount + $bonusAmount;
			
			$referredUser->save();
			$referringUser->save();
			
			//kohli added section
            return true;
        }
        return false;
    }
	
}