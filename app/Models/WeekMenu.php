<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class WeekMenu extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'weekMenu';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    public static function getQuery($userId=0)
    {
        $itemImagePath = asset('/images/item') . '/';
        $itemThumbPath = asset('/images/item/thumb') . '/';
        
        $sql = 'SELECT wm.id as weekMenuId';
        $sql .= ',wm.day';
        $sql .= ',wm.itemId';
        $sql .= ',IFNULL(i.itemName, "") as itemName';
        $sql .= ',IFNULL(i.description, "") as description';
        $sql .= ',i.price';
        $sql .= ',i.itemType';
        $sql .= ',IFNULL(CONCAT("' . $itemImagePath . '",i.image), "") as itemImage';
        $sql .= ',IFNULL(CONCAT("' . $itemThumbPath . '",i.image), "") as itemThumb';
        $sql .= ',i.likeCount';
        
        if($userId)
            $sql .= ',IF(il.id is null, 0, 1) as iLikedIt';
        
        $sql .= ' FROM weekMenu wm';
        $sql .= ' JOIN item i ON wm.itemId=i.id';
        
        if($userId)
            $sql .= " LEFT JOIN itemLike il on il.itemId = i.id AND il.userId=" . $userId;
        
        return $sql;
    }
    
    public static function getList($userId=0, $day = 0, $itemType=0, $exceptions='')
    {
        $sql = self::getQuery($userId);
        $sql .= ' WHERE 1';
        $sql .= ' AND i.isDisabled = 0';
        
        if($day)
            $sql .= " AND wm.day = $day";
        
        if($itemType)
            $sql .= " AND wm.itemType = $itemType";
        
        if($exceptions)
            $sql .= " AND wm.id NOT IN($exceptions)";
        
        $sql .= " ORDER BY wm.day, i.itemType, i.itemName";
        //$sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }

    public function dayName()
    {
        $dayNames = ['Not mentioned' ,'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        return $dayNames[$this->day];
    }

    public static function getDayName($day)
    {
        $dayNames = ['Not mentioned' ,'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        return $dayNames[$day];
    }

}
