<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class PhoneVerification extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'phoneVerification';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    /**
     * 
     * return query to fetch data from phoneVerification table
     */
    public static function getQuery($userId=0)
    {
        $sql = 'SELECT pa.id as phoneVerificationId';
        $sql .= ',pa.phone';
        $sql .= ',pa.verificationCode';
        $sql .= ' FROM phoneVerification pa';

        return $sql;
    }

}
