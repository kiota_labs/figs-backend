<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Friend;
use App\Models\GroupFollower;
use App\Models\UserPasswordTag;
use \Illuminate\Support\Facades\Hash;

class UserPassword extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'userPassword';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    /**
     * 
     * relationships
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'userId', 'id');
    }
    
    public static function getQuery()
    {
        $sql = 'SELECT up.id';
        $sql .= ',up.userId';
        $sql .= ',up.password';
        $sql .= ',up.createDate';
        $sql .= ' FROM userPassword up';
        
        return $sql;
    }
    
    public static function isPasswordUsed($userId, $password, $limit=5)
    {
        $userPasswords = UserPassword::where('userId', $userId)->orderBy('createDate', 'desc')->take($limit)->get();
        $isPasswordUsed = false;
            
        if(!is_null($userPasswords))
        {
            foreach ($userPasswords as $userPassword)
            {
                if(Hash::check($password, $userPassword->password))
                {
                    $isPasswordUsed = true;
                    break;
                }
            }
        }

        return $isPasswordUsed;
    }

}
