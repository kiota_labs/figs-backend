<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class ItemIngredient extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'itemIngredient';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    public static function getQuery()
    {
        $sql = 'SELECT ii.id as itemIngredientId';
        $sql .= ',ii.itemId';
        $sql .= ',ii.key';
        $sql .= ',ii.value';
        $sql .= ' FROM itemIngredient ii';
        
        return $sql;
    }
    
    public static function getList($itemId=0, $exceptions='')
    {
        $sql = self::getQuery();
        $sql .= ' WHERE 1';
        $sql .= ' AND ii.isDisabled = 0';
        
        if($itemId)
            $sql .= " AND ii.itemId = $itemId";
        
        if($exceptions)
            $sql .= " AND ii.id NOT IN($exceptions)";
        
        $sql .= " ORDER BY ii.id ASC";
        //$sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }

}
