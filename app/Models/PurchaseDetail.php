<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class PurchaseDetail extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'purchaseDetail';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    public static function getQuery()
    {
        $itemImagePath = asset('/images/item') . '/';
        $itemThumbPath = asset('/images/item/thumb') . '/';
        
        $sql = 'SELECT pd.id as purchaseDetailId';
        $sql .= ',pd.purchaseId';
        $sql .= ',pd.itemId';
        $sql .= ',pd.quantity';
        $sql .= ',pd.price';
        $sql .= ',IFNULL(i.itemName, "") as itemName';
        $sql .= ',IFNULL(i.description, "") as itemDescription';
        $sql .= ',i.itemType';
        $sql .= ',IFNULL(CONCAT("' . $itemImagePath . '",i.image), "") as itemImage';
        $sql .= ',IFNULL(CONCAT("' . $itemThumbPath . '",i.image), "") as itemThumb';
        $sql .= ' FROM purchaseDetail pd';
        $sql .= ' JOIN item i ON pd.itemId=i.id';
        
        return $sql;
    }
    
    public static function getList($purchaseId=0)
    {
        $sql = self::getQuery();
        $sql .= ' WHERE 1';
        
        if($purchaseId)
            $sql .= " AND pd.purchaseId = $purchaseId";
        
        $sql .= " ORDER BY pd.id ASC";
        
        $records = DB::select($sql);
        
        return $records;
    }

}
