<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSession extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'userSession';

    /*
     * Disable timestamps fields
     */
    public $timestamps = false;
    
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'userId');
    }
}
