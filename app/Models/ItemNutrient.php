<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class ItemNutrient extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'itemNutrient';

    /*
     * Disable timestamps fields
     */
    //public $timestamps = false;

    /*
     * use different columns for created_at and updated_at
     */
    const CREATED_AT = 'createDate';
    const UPDATED_AT = 'updateDate';
    
    public static function getQuery()
    {
        $sql = 'SELECT inu.id as itemNutrientId';
        $sql .= ',inu.itemId';
        $sql .= ',inu.key';
        $sql .= ',inu.value';
        $sql .= ' FROM itemNutrient inu';
        
        return $sql;
    }
    
    public static function getList($itemId=0, $exceptions='')
    {
        $sql = self::getQuery();
        $sql .= ' WHERE 1';
        $sql .= ' AND inu.isDisabled = 0';
        
        if($itemId)
            $sql .= " AND inu.itemId = $itemId";
        
        if($exceptions)
            $sql .= " AND inu.id NOT IN($exceptions)";
        
        $sql .= " ORDER BY inu.id ASC";
        //$sql .= " LIMIT 15";
        
        $records = DB::select($sql);
        
        return $records;
    }

}
